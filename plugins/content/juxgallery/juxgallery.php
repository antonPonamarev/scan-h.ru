<?php

/**
 * @version		$Id$
 * @author		Nguyen Dinh Luan
 * @package		Joomla!
 * @subpackage	Facebook_Comment Plugin
 * @copyright	Copyright (C) 2008 - 2011 by Joomseller Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL version 3
 */
// no direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.module.helper');
jimport('joomla.plugin.plugin');

class plgContentJUXGallery extends JPlugin
{

    /**
     * Constructor
     *
     * @param   object  &$subject  The object to observe
     *
     * @param   array   $config    An array that holds the plugin configuration
     */
    public function __construct(&$subject, $config)
    {
	parent::__construct($subject, $config);
	$this->loadLanguage();
    }

    /**
     * Content before display
     *
     * @param   string  $context   The context of the content being passed to the plugin.
     * @param   object  &$article  The article object.  Note $article->text is also
     *
     * @return	void
     */
    public function onContentBeforeDisplay($context, &$article)
    {
	// Don't run this plugin when the content is being indexed
	if ($context == 'com_finder.indexer')
	{
	    return true;
	}
	// expression to search for (positions)
	$regex = '/{jux-gallery album=+(.*?)\/}/i';
	$style = $this->params->def('style', 'none');
	if (!empty($article->text))
	{
	    // Find all instances of plugin and put in $matches for loadposition
	    // $matches[0] is full pattern match, $matches[1] is the position
	    preg_match_all($regex, $article->text, $matches, PREG_SET_ORDER);

	    // No matches, skip this
	    if ($matches)
	    {
		foreach ($matches as $index => $match)
		{
		    $matcheslist = explode(',', $match[1]);

		    // We may not have a module style so fall back to the plugin default.
		    if (!array_key_exists(1, $matcheslist))
		    {
			$matcheslist[1] = $style;
		    }

		    $albumID = trim($matcheslist[0]);
		    $style = trim($matcheslist[1]);

		    if (isset($albumID))
		    {
			$output = $this->loadJUXGallery($albumID);
			$article->text = @preg_replace("|$match[0]|", addcslashes($output, '\\$'), $article->text, 1);
		    }
		    // We should replace only first occurrence in order to allow positions with the same name to regenerate their content:
		}
	    }
	}
    }

    /**
     * Load Album
     *
     * @param   Int  $albumID  Album id
     *
     * @return void
     */
    public function loadJUXGallery($albumID)
    {
	$document = JFactory::getDocument();
	$renderer = $document->loadRenderer('module');
//	$listAlbum = explode('/', $albumID);
//	$params = array('listAlbum' => $listAlbum);
	$params = $this->params->__toString();
//	var_dump($this->params->__toString());
	//get module as an object
	$database = JFactory::getDBO();
	$database->setQuery("SELECT * FROM #__modules WHERE module='mod_jux_gallery' AND #__modules.published	= 1");
	$modules = $database->loadObjectList();

	$module = $modules[0];
//	var_dump($module->params);
	$module->params = $this->changeParams($albumID, $module->params, $params);
//	var_dump($params);die;
	$contents = $renderer->render($module, $module->params);
	return $contents;
    }

    public function changeParams($albumID, $params, $plg)
    {
	$params = explode('],', $params);
	$plg = explode('{', $plg);
	$albumIDs = explode('/', $albumID);
	$text = '{"listAlbum":[';
	for ($i = 1; $i <= sizeof($albumIDs); $i++)
	{
	    if ($i < sizeof($albumIDs))
	    {
		$albumText = '"' . $albumIDs[$i-1] . '",';
	    }
	    if ($i == sizeof($albumIDs))
	    {
		$albumText = '"' . $albumIDs[$i-1] . '"';
	    }
	    $text = $text . $albumText;
	}

	$text = $text . ']';
	$params[0] = $text;
	$array = array();
	array_push($array, $params[0]);
	array_push($array, $plg[1]);

	$paramsCustom = implode(',', $array);
	return $paramsCustom;
    }

}