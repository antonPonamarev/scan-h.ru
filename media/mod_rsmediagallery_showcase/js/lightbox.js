(function($) {
	$.fn.hasScrollBar = function() {
		var compare = this.innerHeight();
		if (this.get(0) == document.body) compare = $(window).height();
		return this.get(0) ? this.get(0).scrollHeight > compare : false;
	}
})(jQuery);

var RSShowcaseLightbox = {}

RSShowcaseLightbox.$ = jQuery; 

RSShowcaseLightbox.Settings = {
	show_title				: 1,
	show_description		: 1,
	show_pictures_numbering : 1,
	temp_loader				: 'none'
}

RSShowcaseLightbox.Actions = {
	nextElement 			: 'none',
	prevElement 			: 'none',
	isMouseOutside			: false,
	loadedResize 			: false,
	current_number 			: 0,
	total_number 			: 0,
	
	Load : function(loadTarget, direction) {
		var target 		= loadTarget.attr('data-target');
		var title 		= loadTarget.attr('data-title');
		var description = loadTarget.attr('data-description');
		var target_type	= target.split('.');
		target_type	= target_type.pop();
		
		if (!direction) {
			RSShowcaseLightbox.$('body').addClass('rsp_cursor_zoom_out');
			RSShowcaseLightbox.$('body').append('<div class="rsp_body_cover"></div>');
			RSShowcaseLightbox.$('body').append('<div class="rsp_body_cover_picture"></div>');
			
			RSShowcaseLightbox.$('.rsp_body_cover').animate({opacity: '0.8'}, 500);
			RSShowcaseLightbox.$('.rsp_body_cover_picture').animate({opacity: '1'}, 500, function() {
				RSShowcaseLightbox.Actions.prepareLoading(loadTarget, target, title, description, direction, target_type);
			});
			RSShowcaseLightbox.Settings.temp_loader = RSShowcaseLightbox.$('.rsp_body_cover').css('background-image');
		}
		else {
			RSShowcaseLightbox.$('.rsp_cover_inner').animate({opacity: '0'}, 300, function() {
				RSShowcaseLightbox.$(this).remove();
				RSShowcaseLightbox.Actions.prepareLoading(loadTarget, target, title, description, direction, target_type);
			});
		}
	},
	prepareLoading : function (loadTarget, target, title, description, direction, target_type){
		var tmp = new Image();
		tmp.src = target;
		tmp.onload = function() {
			var target_w = tmp.width;
			var target_h = tmp.height;
			
			// initiate the picture load
			if (direction) {
				if (RSShowcaseLightbox.$('.rsp_cover_arrow_button_right').length > 0 ) RSShowcaseLightbox.$('.rsp_cover_arrow_button_right').remove();
				if (RSShowcaseLightbox.$('.rsp_cover_arrow_button_left').length > 0 ) RSShowcaseLightbox.$('.rsp_cover_arrow_button_left').remove();
					
				
			}
			RSShowcaseLightbox.Actions.innerLoad(loadTarget, target, title, description, target_w, target_h, direction, target_type);
			RSShowcaseLightbox.Actions.loadResize();
		}
	},
	innerLoad : function(loadTarget, target, title, description, target_w, target_h, direction, target_type) {
			var inner_container = RSShowcaseLightbox.$("<div>", {
				'class': "rsp_cover_inner"
			});
			var button_container = RSShowcaseLightbox.$("<div>",{
				'class': "rsp_cover_close_button"
			});
			var picture_container = RSShowcaseLightbox.$('<div>', {
				'class': "rsp_cover_picture_container rsp_cursor_pointer"+(target_type!='png' ? " rsp_box_shadow": "")
			});
			var details_container = RSShowcaseLightbox.$('<div>', {
				'class': "rsp_cover_details_container rsp_cursor_auto"
			});
			
			var actual_image = RSShowcaseLightbox.$("<img>", {
				src: target,
				alt: title,
				'data-target-width': target_w,
				'data-target-height': target_h,
			});
			
			var nextPicture = RSShowcaseLightbox.Actions.getNextElement(loadTarget);
			var prevPicture = RSShowcaseLightbox.Actions.getPrevElement(loadTarget);
			
			if (nextPicture) {
				var next_button_container = RSShowcaseLightbox.$('<button>', {
					'class': "rsp_cover_arrow_button rsp_cover_arrow_button_right"
				});
				next_button_container.attr('onclick','RSShowcaseLightbox.Actions.LoadNext()');
			}
			
			if (prevPicture) {
				var prev_button_container = RSShowcaseLightbox.$('<button>', {
					'class': "rsp_cover_arrow_button rsp_cover_arrow_button_left"
				});
				prev_button_container.attr('onclick','RSShowcaseLightbox.Actions.LoadPrev()');
			}
			
			var html_changed = false;
			if(!direction) {
				if (RSShowcaseLightbox.$('body').hasScrollBar()) {
					var window_with_scorll = RSShowcaseLightbox.$(window).width();
					RSShowcaseLightbox.$('html').css('overflow', 'hidden');
					var window_no_scorll = RSShowcaseLightbox.$(window).width();
					
					var scroll_w = window_no_scorll - window_with_scorll;
					RSShowcaseLightbox.$('html').css('margin-right',scroll_w);
					html_changed = true;
				}
				else {
					RSShowcaseLightbox.$('html').css('overflow', 'hidden');
				}
			}
			else {
				html_changed = parseInt(RSShowcaseLightbox.$('html').css('margin-right')) > 0 ? true : false;
			}
			
			// set the numbering
			RSShowcaseLightbox.Actions.getLoadedNumber(loadTarget);
			RSShowcaseLightbox.Actions.getTotalNumber();
			
			// build the html
			if (nextPicture) {
				actual_image.attr('onclick', 'RSShowcaseLightbox.Actions.LoadNext()');
			}
			picture_container.append(actual_image);
			if (RSShowcaseLightbox.Settings.show_title) {
				details_container.append('<h3>'+title+'</h3>');
			}
			if (RSShowcaseLightbox.Settings.show_description) {
				details_container.append('<p>'+description+'</p>');
			}
			if (RSShowcaseLightbox.Settings.show_pictures_numbering) 	{
				details_container.append('<div class="rsp_count_pictures">'+RSShowcaseLightbox.Actions.current_number+' / '+RSShowcaseLightbox.Actions.total_number+'</div>');
			}
			button_container.append('<button class="rsp_cover_close" title="Close(Esc)">×</button>');
			inner_container.append(button_container);
			inner_container.append(picture_container);
		
			inner_container.append(details_container);

			// display the html
			RSShowcaseLightbox.$('.rsp_body_cover_picture').append(inner_container);
			
			// remove the loader from background
			RSShowcaseLightbox.$('.rsp_body_cover').css('background-image','none');
			
			if (nextPicture) {
				RSShowcaseLightbox.$('.rsp_body_cover_picture').append(next_button_container);
			}
			
			if (prevPicture) {
				RSShowcaseLightbox.$('.rsp_body_cover_picture').append(prev_button_container);
			}
			
			// calculate the with and height of the picture container to fit the window
			var current_window_w = RSShowcaseLightbox.$(window).width();
			var current_window_h = RSShowcaseLightbox.$(window).height();
			var details_container_h = details_container.height();
			var button_container_h = button_container.height();
			
			
			var all_h = details_container_h + button_container_h + target_h;
			var check_ratio = false;
			
			if (all_h > current_window_h) {
				var new_picture_h = current_window_h - (details_container_h + button_container_h);
				var ratio = target_h / new_picture_h;
				
				var new_inner_container_w = target_w / ratio;
				inner_container.width(new_inner_container_w);
				
				actual_image.css('max-height',new_picture_h);
				check_ratio = true;
			}
			
			if (!check_ratio) inner_container.width(target_w);
			
			// if the inner_container has the resulted width larger than the window
			if (inner_container.width() >= current_window_w) {
				var recalculated_w = current_window_w - ((current_window_w * 5) / 100);
				inner_container.width(recalculated_w);
			}
			
			// vertical align the picture
			var new_all  = details_container_h + button_container_h + actual_image.height();
			var inner_cont_margin_top =  (current_window_h - new_all)  / 2; 
			inner_container.css('margin-top', inner_cont_margin_top);
			
			// apply the close action
			RSShowcaseLightbox.Actions.closeBox('rsp_body_cover_picture', html_changed, false); // <- for clicking out of the container
			RSShowcaseLightbox.Actions.closeBox(button_container, html_changed, true); // -< for clicking the "x" button
			if (!RSShowcaseLightbox.Settings.show_title && !RSShowcaseLightbox.Settings.show_description && !RSShowcaseLightbox.Settings.show_pictures_numbering) {
				RSShowcaseLightbox.Actions.closeBox(details_container, html_changed, true);
			}
			RSShowcaseLightbox.Actions.closeOnEscape(html_changed); // <- for pressing "ESC"
			
	},
	closeBox : function(element, html_changed, just_close) {
		if (!just_close) {
			RSShowcaseLightbox.$(document).on('click', function(e) {
				var evt = window.event || e; 
				if (!evt.target) evt.target = evt.srcElement;
				
				if (RSShowcaseLightbox.$(evt.target).hasClass(element)) {
					RSShowcaseLightbox.Actions.isMouseOutside = true; 
				}
				else {
					RSShowcaseLightbox.Actions.isMouseOutside = false;
				}
				
				if (RSShowcaseLightbox.Actions.isMouseOutside) {
					RSShowcaseLightbox.Actions.removeElements(html_changed);
				}
			});
		}
		else {
			element.unbind('click').on('click', function() {
				if (just_close) {
					RSShowcaseLightbox.Actions.removeElements(html_changed);
				}
			});
		}
	},
	removeElements: function(html_changed) {
		RSShowcaseLightbox.$('.rsp_body_cover').animate({opacity: '0'}, 500, function(){ 
			RSShowcaseLightbox.$('.rsp_body_cover').remove();
			RSShowcaseLightbox.$('body').removeClass('rsp_cursor_zoom_out');
		});
		RSShowcaseLightbox.$('.rsp_body_cover_picture').animate({opacity: '0'}, 500, function(){ 
			RSShowcaseLightbox.$('.rsp_body_cover_picture').remove();
			RSShowcaseLightbox.$('html').css('overflow', 'auto');
			if (html_changed) {
				RSShowcaseLightbox.$('html').css('margin-right','0px');
			}
		});	
	},
	closeOnEscape : function (html_changed) {
		RSShowcaseLightbox.$(document).keyup(function(e) {
		  if (e.keyCode == 27) { RSShowcaseLightbox.Actions.removeElements(html_changed); } 
		});
	},		
	getNextElement : function(loadedTarget) {
		var row_elements 	= new Array();
		var current_key 	= 'none';
		loadedTarget.parent().find('.rsp_picture_container').each(function(index, elem){
			if(RSShowcaseLightbox.$(elem).css('display') != 'none'){
				row_elements.push(RSShowcaseLightbox.$(elem));
				if (RSShowcaseLightbox.$(elem).context == loadedTarget.context) current_key = index;
			}
		});
		if (current_key == 'none') return false;
		
		if (row_elements[(current_key+1)]) {
			return this.nextElement = row_elements[(current_key+1)];
		}
		else return false;
	},
	getPrevElement : function(loadedTarget) {
		var row_elements 	= new Array();
		var current_key 	= 'none';
		loadedTarget.parent().find('.rsp_picture_container').each(function(index, elem){
			row_elements.push(RSShowcaseLightbox.$(elem));
			if (RSShowcaseLightbox.$(elem).context == loadedTarget.context) current_key = index;
		});
		if (current_key == 'none') return false;
		
		if (row_elements[(current_key-1)]) {
			return this.prevElement = row_elements[(current_key-1)];
		}
		else return false;
	},
	LoadNext : function() {
		RSShowcaseLightbox.$('.rsp_body_cover').css('background-image',RSShowcaseLightbox.Settings.temp_loader);
		this.Load(this.nextElement, true);
	},
	LoadPrev : function() {
		RSShowcaseLightbox.$('.rsp_body_cover').css('background-image',RSShowcaseLightbox.Settings.temp_loader);
		this.Load(this.prevElement, true);
	},
	
	loadResize : function() {
		if (!this.loadedResize) {
			RSShowcaseLightbox.$(window).resize(function() {
				if (RSShowcaseLightbox.$('.rsp_body_cover').length > 0) {
					// containers
					var inner_container = RSShowcaseLightbox.$('.rsp_cover_inner');
					var details_container = RSShowcaseLightbox.$('.rsp_cover_details_container');
					var button_container = RSShowcaseLightbox.$('.rsp_cover_close_button');
					var picture_container = RSShowcaseLightbox.$('.rsp_cover_picture_container');
					var actual_image = RSShowcaseLightbox.$('.rsp_cover_picture_container img');
					
					// calculate the with and height of the picture container to fit the window
					var current_window_w = RSShowcaseLightbox.$(window).width();
					var current_window_h = RSShowcaseLightbox.$(window).height();
					var details_container_h = details_container.height();
					var button_container_h = button_container.height();
					
					var target_h = actual_image.attr('data-target-height');
					var target_w = actual_image.attr('data-target-width');
					
					var all_h = details_container_h + button_container_h + target_h;
					var check_ratio = false;
					
					if (all_h > current_window_h) {
						var new_picture_h = current_window_h - (details_container_h + button_container_h);
						var ratio = target_h / new_picture_h;
						
						var new_inner_container_w = target_w / ratio;
						inner_container.width(new_inner_container_w);
						
						actual_image.css('max-height',new_picture_h);
						if (new_inner_container_w > actual_image.width()) inner_container.width(actual_image.width());
						check_ratio = true;
					}
					
					if (!check_ratio) inner_container.width(target_w);
					
					// if the inner_container has the resulted width larger than the window
					if (inner_container.width() >= current_window_w) {
						var recalculated_w = current_window_w - ((current_window_w * 5) / 100);
						inner_container.width(recalculated_w);
					}
					
					// vertical align the picture
					if (!check_ratio) {
						var inner_cont_margin_top =  (current_window_h - all_h)  / 2;
					}
					else {
						var inner_cont_margin_top =  (current_window_h - inner_container.height())  / 2; 
					}
					inner_container.css('margin-top', inner_cont_margin_top);
					RSShowcaseLightbox.Actions.loadedResize = true;
				}
			});
		}
	},
	getLoadedNumber : function(loadedTarget) {
		var current_key 	= 'none';
		loadedTarget.parent().find('.rsp_picture_container').each(function(index, elem){
			if (RSShowcaseLightbox.$(elem).context == loadedTarget.context) current_key = index;
		});
		
		this.current_number = current_key + 1;
	},
	
	getTotalNumber : function() {
		this.total_number = RSShowcaseLightbox.$('.rsp_picture').length;
	}
}