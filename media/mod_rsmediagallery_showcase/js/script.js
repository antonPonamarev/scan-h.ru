var RSShowcaseScript = {}

RSShowcaseScript.$ = jQuery;

RSShowcaseScript.Init = {
	
	Start : function(params) {
		RSShowcaseScript.$(document).ready(function($) {
			RSShowcaseScript.Init.Load(params);
			
			$(window).resize(function() {
				RSShowcaseScript.Init.OnResize(params.id);
			});
		});
	},
	
	Load : function(params) {
		var container 				 = params.id;
	
		RSShowcaseScript.$(container).fadeIn(3000);
		
		// resize to fit the with of the container
		this.Resize(container);
		
		// center the images inside the div if necessary
		this.CenterElements(container);
		
		// hover effect
		this.SetHoverEffect(container, params.openIn);
		
		// on click actions
		this.ClickEvents(params);
	},
	
	OnResize: function(container) {
		RSShowcaseScript.$(container).css('padding-left','0px');
		RSShowcaseScript.$(container).find('.rsp_picture_container:hidden').show();  // if there are pictures hidden from preview window resize
		
		// resize to fit the with of the container
		this.Resize(container);
	},
	
	CenterElements : function(container) {
		RSShowcaseScript.$(container).find('.rsp_picture').each(function(index, val){
			var img_cont_w = RSShowcaseScript.$(val).width();
			
			var img = RSShowcaseScript.$(val).find('img');
			var img_w = img.width();
			var img_h = img.height();
			
			if (img_h > img_cont_w) {
				var diff = img_h - img_cont_w;
				img.css('margin-top',-(diff/2));
			}
			
			if (img_w > img_cont_w) {
				var diff = img_w - img_cont_w;
				img.css('margin-left',-(diff/2));
			}
		});
	},
	
	SetHoverEffect : function(container, open_in) {
		RSShowcaseScript.$(container+' > .rsp_picture_container').hover(
			function() {
				var dimension = RSShowcaseScript.$(this).width();
				var font_size = (dimension * 20) / 100;
				var icon_top  = (dimension / 2) - (font_size / 2);
				RSShowcaseScript.$(this).find('.rsp_picture').animate({opacity: '0.8'}, 300);
				if (open_in == 'slideshow') {
					var this_append = RSShowcaseScript.$(this);
				}
				else {
					var this_append = RSShowcaseScript.$(this).find('a');
				}
				this_append.append('<div class="rsp_icon_over" style="font-size:'+font_size+'px; padding-top:'+icon_top+'px"><i class="fa fa-search"></i></div>');
				RSShowcaseScript.$('.rsp_icon_over').animate({opacity: '1'}, 300);
				
			},
			function() {
				RSShowcaseScript.$(this).find('.rsp_picture').animate({opacity: '1'}, 300);
				RSShowcaseScript.$('.rsp_icon_over').animate({opacity: '0'}, 300, function() {
					RSShowcaseScript.$(this).remove();
				});
				
			}
		);
	},
	
	ClickEvents : function(params) {
		if (params.openIn == 'slideshow') {
			RSShowcaseScript.$(params.id+' > .rsp_picture_container').on('click', function(){
				RSShowcaseLightbox.Settings.show_title 		        = params.title;
				RSShowcaseLightbox.Settings.show_description 	    = params.description;
				RSShowcaseLightbox.Settings.show_pictures_numbering = params.numbering;
				RSShowcaseLightbox.Actions.Load(RSShowcaseScript.$(this), false);			
			});
		}
	},
	
	Resize : function(container) {
		var offsets = [];
		RSShowcaseScript.$(container).find('.rsp_picture_container').each(function(){
			offsets.push(RSShowcaseScript.$(this).offset().top);
		});
		
		var counts 		  = this.countOccurrences(offsets);
		var found_per_row = 0;
		for(var i=0; i < offsets.length; i++) {
			if (found_per_row < counts[offsets[i]]) {
				found_per_row = counts[offsets[i]];
			}
		}
		
		var nr_left_pictures = offsets.length % found_per_row;
		if (nr_left_pictures >0 ) RSShowcaseScript.$(container+' > .rsp_picture_container').slice(-nr_left_pictures).hide();
		
		var pictures_container_margin = (parseInt(RSShowcaseScript.$(container+' > .rsp_picture_container').css('margin-left')) * 2) * found_per_row;
		var pictures_container_w = (RSShowcaseScript.$(container+' > .rsp_picture_container').width() * found_per_row) + pictures_container_margin;
		
		var cont_padding_left = (RSShowcaseScript.$(container).width() - pictures_container_w) / 2;
		if (cont_padding_left >= 0) {
			RSShowcaseScript.$(container).css('padding-left',cont_padding_left+'px');
		}
	},
	
	countOccurrences : function(vector) {
		var counts = {};
		for(var i = 0; i< vector.length; i++) {
			var num = vector[i];
			counts[num] = counts[num] ? counts[num]+1 : 1;
		}
		return counts;
	},

}