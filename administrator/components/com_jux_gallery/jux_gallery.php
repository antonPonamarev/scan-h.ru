<?php

/**
 * @version		$Id$
 * @author		JoomlaUX
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_jux_gallery'))
{
    return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

// add general stylesheet and javascript
$admin_live_site = JURI::base();
$live_slite = JURI::root();
$document = JFactory::getDocument();

$document->addStyleSheet($admin_live_site . '/components/com_jux_gallery/assets/css/jux_gallery.css');
$document->addStyleSheet($admin_live_site . '/components/com_jux_gallery/assets/css/buttons.css');
$document->addScript($admin_live_site . '/components/com_jux_gallery/assets/js/jux_gallery.js');

$document->addScriptDeclaration("var admin_live_site = '$admin_live_site';");
$document->addScriptDeclaration("var live_site='$live_slite';");



// require libraries
require_once(JPATH_COMPONENT . '/libraries/factory.php');

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

// Register helper class
JLoader::register('JUX_GalleryHelper', dirname(__FILE__) . '/helpers/jux_gallery.php');

// import joomla controller library
jimport('joomla.application.component.controller');

// Get an instance of the controller prefixed by jux_gallery
$controller = JControllerLegacy::getInstance('jux_gallery');

// Perform the Request task
$controller->execute(JFactory::getApplication()->input->get('task'));

// Redirect if set by the controller
$controller->redirect();
