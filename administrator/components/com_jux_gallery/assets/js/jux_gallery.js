/**
 * @version	$Id$
 * @author	JoomlaUX
 * @package	Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license	http://www.gnu.org/licenses/gpl.html
 */

/*Album*/

/*
 * Function albumCreate
 * @returns boolean
 */
function albumCreate() {
    var album = jQuery('#createAlbumModal'),
	    title = album.find("#albumTitle").val();
    var url = "index.php?option=com_jux_gallery&task=category.albumCreate";
    jQuery.post(url, {
	title: title
    }, function(respon) {
	album.modal("hide");
	album.find("#albumTitle").val('');
	window.location = respon;
    }, "json");
}

/*
 * @param {type} id
 * @returns {album information} 
 */
function viewAlbumEdit(id) {
    var elm = jQuery('#arrayorder_' + id),
	    albumTitle = elm.data('title'),
	    albumAlias = elm.data('alias'),
	    albumStatus = elm.data('status'),
	    editAlbumModal = jQuery('#editAlbumModal');
    editAlbumModal.data('album-id', id);
    editAlbumModal.find('#editAlbumTitle').val(albumTitle);
    editAlbumModal.find('#editAlbumAlias').val(albumAlias);
//    editAlbumModal.find('#editAlbumStatus').data('status', albumStatus);
    jQuery('#editAlbumStatus').attr('data-status',albumStatus);
    jQuery('#albumPublish').removeClass().addClass('jux-button has-text is-success');
    jQuery('#albumUnPublish').removeClass().addClass('jux-button has-text is-danger ');
    if (albumStatus === 1) {
	editAlbumModal.find('#albumPublish').addClass('is-disabled');
	editAlbumModal.find('#albumUnPublish').addClass('is-embossed');
    } else {
	editAlbumModal.find('#albumUnPublish').addClass('is-disabled');
	editAlbumModal.find('#albumPublish').addClass('is-embossed');
    }
    editAlbumModal.modal('show');
}

function deleteAlbum($draggable, $helper) {
    if (confirm("If you want, We\'ll delete the all file on the album. However, if you want to see they again, you\'ll have to upload")) {
	var albumID = $draggable.attr('id');
	var currentUrl = window.location.href;
	var url = 'index.php?option=com_jux_gallery&task=category.deleteAlbum';
	jQuery.post(url, {
	    albumID: albumID,
	    currentUrl: currentUrl
	}, function(respon) {
	    $draggable.addClass('animated bounceOut');
	    setTimeout(function() {
		$draggable.remove();
	    }, 1000);
	    if (respon !== true) {
		window.location = respon;
	    }
	}, 'json');
    }
}

function publish() {
    var editAlbumModal = jQuery('#editAlbumModal');
    editAlbumModal.find('#editAlbumStatus').attr('data-status', 1);   
    editAlbumModal.find('#editAlbumStatus').data('status', 1);   
//    editAlbumModal.find('#albumPublish').addClass('is-disabled');
//    editAlbumModal.find('#albumUnPublish').removeClass('is-disabled');
    saveAlbum();
}
function unpublish() {
    var editAlbumModal = jQuery('#editAlbumModal');
    editAlbumModal.find('#editAlbumStatus').attr('data-status', 0);
    editAlbumModal.find('#editAlbumStatus').data('status', 0);
//    editAlbumModal.find('#albumUnPublish').addClass('is-disabled');
//    editAlbumModal.find('#albumPublish').removeClass('is-disabled');
    saveAlbum();
}

function saveAlbum() {
    var editAlbumModal = jQuery('#editAlbumModal'),
	    albumID = editAlbumModal.data("album-id"),
	    album = jQuery("#arrayorder_" + albumID),
	    album_title = editAlbumModal.find("#editAlbumTitle").val();
    album_alias = editAlbumModal.find("#editAlbumAlias").val();
    album_status = editAlbumModal.find("#editAlbumStatus").data('status');
    var url = "index.php?option=com_jux_gallery&task=category.saveAlbum";
    jQuery.post(url, {
	albumID: albumID,
	album_title: album_title,
	album_alias: album_alias,
	album_status: album_status
    }, function(respon) {
    });

    album.data("title", album_title);
    album.data("alias", album_alias);
    album.data("status", album_status);
    $this = album.find('a.has-text');
    if (album_status === 1) {
	$this.removeClass();
	$this.empty();
	$this.addClass('jux-button is-success is-embossed has-icon has-text');
	$this.append('<i class="jux-icon-check"></i>&nbsp;' + album_title);
    } else if (album_status === 0) {
	$this.removeClass();
	$this.empty();
	$this.addClass('jux-button is-danger is-embossed has-icon has-text');
	$this.append('<i class="jux-icon-cross"></i>&nbsp;' + album_title);
    }
//	album.find('a.has-text').text(album_title);
//	editAlbumModal.find("#editAlbumTitle").val(album_title);
//	editAlbumModal.find("#editAlbumAlias").val(album_alias);
//	editAlbumModal.find("#editAlbumStatus").val(album_status);
    editAlbumModal.modal("hide");
}

/*Function Save And Show*/

function openSS() {
    var menuItem = jQuery('#menuitem'),
	    moduleItem = jQuery('#moduleitem'),
	    insideArticle = jQuery('#insideArticle');
    menuItem.css('display', 'block');
    menuItem.addClass('animated bounceInLeft');

    moduleItem.css('display', 'block');
    moduleItem.addClass('animated bounceInRight');

    insideArticle.css('display', 'block');
    insideArticle.addClass('animated bounceInLeft');

    jQuery('#iconTable').attr('onclick', 'closeSS()');
}

function closeSS() {
    var menuItem = jQuery('#menuitem'),
	    moduleItem = jQuery('#moduleitem'),
	    insideArticle = jQuery('#insideArticle'),
	    menuType = jQuery('.menuType'),
	    listModule = jQuery('.listModule'),
	    listMenu = jQuery('.listMenu');

    if (menuType.css('display') === 'block') {
	closeMenuTypes();
    }
    if (listModule.css('display') === 'block') {
	closeListModule()();
    }
    if (listMenu.css('display') === 'block') {
	closeListMenu()();
    }
    menuItem.removeClass('bounceInLeft');
    menuItem.addClass('bounceOut');
    setTimeout(function() {
	menuItem.css('display', 'none');
    }, 1000);
    moduleItem.removeClass('bounceInRight');
    moduleItem.addClass('bounceOut');
    setTimeout(function() {
	moduleItem.css('display', 'none');
    }, 1000);
    insideArticle.removeClass('bounceInLeft');
    insideArticle.addClass('bounceOut');
    setTimeout(function() {
	insideArticle.css('display', 'none');
    }, 1000);
    jQuery('#iconTable').attr('onclick', 'openSS()');
}

function showListMenu() {
    var menuItem = jQuery('#menuitem'),
	    moduleItem = jQuery('#moduleitem'),
	    insideArticle = jQuery('#insideArticle'),
	    listMenu = jQuery('.listMenu'),
	    menuType = jQuery('.menuType');
    //IN Save AND Show
    if(moduleItem.css('display') === 'block'){
	moduleItem.removeClass('bounceInRight');
	moduleItem.addClass('bounceOut');
	setTimeout(function() {
	    moduleItem.css('display', 'none');
	},1000);
    }
    if(insideArticle.css('display') === 'block'){
	insideArticle.removeClass('bounceInLeft');
	insideArticle.addClass('bounceOut');
	setTimeout(function() {
	    insideArticle.css('display', 'none');
	},1000);
    }
    //IN Show List MenuType
    if (menuType.css('display') === 'block') {
	menuItem.find('i').removeClass('jux-icon-minus').addClass('jux-icon-plus');
	menuItem.find('a.has-icon').attr('onclick','showMenuTypes()');
	menuType.removeClass('bounceInLeft');
	menuType.addClass('bounceOut');
	setTimeout(function() {
	    menuType.css('display', 'none');
	},1000);
    }
    
    setTimeout(function() {
	listMenu.css('display', 'block');
	listMenu.addClass('animated bounceInLeft');
    }, 1000);
    setTimeout(function() {
	jQuery('#listItem').attr('onclick', 'closeListMenu()');
    }, 2000);

}

function closeListMenu() {
    var menuItem = jQuery('#menuitem'),
	moduleItem = jQuery('#moduleitem'),
	insideArticle = jQuery('#insideArticle'),
	    listMenu = jQuery('.listMenu');

    listMenu.removeClass('bounceInLeft');
    listMenu.addClass('bounceOut');

    setTimeout(function() {
	listMenu.css('display', 'none');
	openSS();
    }, 1000);
    setTimeout(function() {
	jQuery('#listItem').attr('onclick', 'showListMenu()');
    }, 2000);

}

function showMenuTypes() {
    var menuItem = jQuery('#menuitem'),
	    moduleItem = jQuery('#moduleitem'),
	    insideArticle = jQuery('#insideArticle'),
	    menuType = jQuery('.menuType'),
	    listMenu = jQuery('.listMenu'),
	    icon = menuItem.find('i');

    icon.removeClass('jux-icon-plus');
    icon.addClass('jux-icon-minus');
    //IN Save AND Show   
    if(moduleItem.css('display') === 'block'){
	moduleItem.removeClass('bounceInRight');
	moduleItem.addClass('bounceOut');
	setTimeout(function() {
	    moduleItem.css('display', 'none');
	},1000);
    }
    if(insideArticle.css('display') === 'block'){
	insideArticle.removeClass('bounceInLeft');
	insideArticle.addClass('bounceOut');
	setTimeout(function() {
	    insideArticle.css('display', 'none');
	},1000);
    }
    
    //IN List Menu
    if (listMenu.css('display') === 'block') {
	listMenu.removeClass('bounceInLeft');
	listMenu.addClass('bounceOut');
	jQuery('#listItem').attr('onclick', 'showListMenu()');
	setTimeout(function() {
	    listMenu.css('display', 'none');
	},1000);
    }

    setTimeout(function() {
	menuType.css('display', 'block');
	menuType.addClass('animated bounceInLeft');
    }, 1000);
    setTimeout(function() {
	menuItem.find('a.has-icon').attr('onclick', 'closeMenuTypes()');
    }, 2000);
}

function closeMenuTypes() {
    var menuItem = jQuery('#menuitem'),
	    moduleItem = jQuery('#moduleitem'),
	    insideArticle = jQuery('#insideArticle'),
	    menuType = jQuery('.menuType'),
	    icon = menuItem.find('i');

    icon.removeClass('jux-icon-minus');
    icon.addClass('jux-icon-plus');

    menuType.removeClass('bounceInLeft');
    menuType.addClass('bounceOut');

    setTimeout(function() {
	menuType.css('display', 'none');
	openSS();
    }, 1000);
    setTimeout(function() {
	menuItem.find('a.has-icon').attr('onclick', 'showMenuTypes()');
    }, 2000);
}

function showListModule() {
    var menuItem = jQuery('#menuitem'),
	    moduleItem = jQuery('#moduleitem'),
	    insideArticle = jQuery('#insideArticle'),
	    listModule = jQuery('.listModule');

    menuItem.removeClass('bounceInLeft');
    menuItem.addClass('bounceOut');

    insideArticle.removeClass('bounceInLeft');
    insideArticle.addClass('bounceOut');

    setTimeout(function() {
	menuItem.css('display', 'none');
	insideArticle.css('display', 'none');
	listModule.css('display', 'block');
	listModule.addClass('animated bounceInLeft');
    }, 1000);
    setTimeout(function() {
	moduleItem.attr('onclick', 'closeListModule()');
    }, 2000);
}

function closeListModule() {
    var menuItem = jQuery('#menuitem'),
	    moduleItem = jQuery('#moduleitem'),
	    insideArticle = jQuery('#insideArticle'),
	    listModule = jQuery('.listModule');

    listModule.removeClass('bounceInLeft');
    listModule.addClass('bounceOut');

    setTimeout(function() {
	listModule.css('display', 'none');
	menuItem.css('display', 'block');
	menuItem.addClass('bounceInRight');

	insideArticle.css('display', 'block');
	insideArticle.addClass('bounceInLeft');
    }, 1000);
    setTimeout(function() {
	moduleItem.attr('onclick', 'showListModule()');
    }, 2000);
}

function syntaxRegex(id) {
    var elm = jQuery('#insideArticleModal'),
	    syntax = jQuery('#syntax-plugin'),
	    album = jQuery('#album_' + id),
	    icon = album.find('i'),
	    syntax_val = syntax.val();

    syntax_val = syntax_val.replace('}', '');
    var listID = syntax_val.replace('{jux-gallery album=', '');
    if (album.attr('class').contains('is-danger')) {
	album.removeClass('is-danger');
	album.addClass('is-success');

	icon.removeClass();
	icon.addClass('jux-icon-check');

	listID += id + '/';
    } else if (album.attr('class').contains('is-success')) {
	album.removeClass('is-success');
	album.addClass('is-danger');

	icon.removeClass();
	icon.addClass('jux-icon-cross');

	listID = listID.replace(id + '/', '');
    }
    syntax_val = '{jux-gallery album=';
    syntax.val(syntax_val + listID + '}');
//	    syntax.attr('value',syntax_val+listID+'}');
}

function insideArticleModal() {
    var elm = jQuery('#listAlbum > div'),
	    syntax = jQuery('#syntax-plugin'),
	    insideArticlModal = jQuery('#insideArticleModal'),
	    albumSelect = jQuery('#albumSelect');
    albumSelect.empty();
    syntax.val('{jux-gallery album=}');
    elm.each(function() {
	var album = jQuery(this)
		, albumID = album.attr('id');
	var a = '<a class="jux-button has-text is-danger is-embossed has-icon" style="margin:5px;" id="album_' + albumID.replace('arrayorder_', '') + '" onclick="syntaxRegex(' + albumID.replace('arrayorder_', '') + ')"><i class="jux-icon-cross"></i>' + album.data('title') + '</a>';
	albumSelect.append(a);
    });
    insideArticlModal.modal("show");
}

function closeInsideArticleModal() {
    var insideArticlModal = jQuery('#insideArticleModal'),
	    syntax = jQuery('#syntax-plugin'),
	    albumSelect = jQuery('#albumSelect');
    syntax.val('{jux-gallery album=}');
    albumSelect.empty();
    insideArticlModal.hide();
}

/* Function Item */

function itemEdit(id) {
    var elm = jQuery('#item_' + id),
	    editItemModal = jQuery('#editItemModal'),
	    item_title = elm.data("title");
    item_url = elm.data("url");
    item_desc = elm.data("desc");
    editItemModal.data("item-id", id);
    editItemModal.find("#itemTitle").val(item_title);
    editItemModal.find("#itemLink").val(item_url);
    editItemModal.find("#itemDescription").val(item_desc);
    editItemModal.modal("show");
}

function saveItem() {
    var editItemModal = jQuery('#editItemModal'),
	    itemId = editItemModal.data("item-id"),
	    item = jQuery("#item_" + itemId),
	    item_title = editItemModal.find("#itemTitle").val();
    item_url = editItemModal.find("#itemLink").val();
    item_desc = editItemModal.find("#itemDescription").val();
    var url = "index.php?option=com_jux_gallery&task=category.editItem";
    jQuery.post(url, {
	itemId: itemId,
	item_title: item_title,
	item_url: item_url,
	item_desc: item_desc
    }, function(respon) {
    });

    item.data("title", item_title);
    item.data("url", item_url);
    item.data("desc", item_desc);
    editItemModal.find("#itemTitle").val(item_title);
    editItemModal.find("#itemLink").val(item_url);
    editItemModal.find("#itemDescription").val(item_desc);
    editItemModal.modal("hide");
}

function itemDeleteAll() {
    if (confirm("If you want, We\'ll delete the file on the folder. However, if you want to see that file again, you\'ll have to upload that file")) {
	var item = jQuery('a.edit');
	var item_id = new Array();
	var i = 0;
	item.each(function() {
	    item_id[i] = item[i].id;
	    i++;
	});
	var url = "index.php?option=com_jux_gallery&task=category.deleteAll";
	jQuery.post(url, {
	    itemId: item_id
	}, function(res) {
	    jQuery(".files").empty();
	    jQuery('.gridly').gridly();
	}, "json");
    }
}

function saveOrder() {
    var item = jQuery('div.brick');
    var items = new Array();
    var i = 0;

    item.each(function() {
	var obj = {};
	obj.order = jQuery(item[i]).data('position');
	obj.id = jQuery(item[i]).find('a.edit').attr('id');
	items[i] = obj;
	i++;
    });
    var url = "index.php?option=com_jux_gallery&task=category.saveOrder";
    jQuery.post(url, {
	items: items
    }, function(respon) {
	jQuery('.gridly').gridly();
//	jQuery('#saveOrderItems').css('display', 'none');
    }, "json");
}

function updateAlbum($album, $item) {
    var itemID = $item.id,
	    albumID = $album.id;
    var url = 'index.php?option=com_jux_gallery&task=category.updateAlbum';
    jQuery.post(url, {
	itemID: itemID,
	albumID: albumID}
    , function(respon) {
	if (respon) {
	    $item.addClass('animated bounceOut');
	    setTimeout(function() {
		$item.remove();
		jQuery('.gridly').gridly({
		    base: 60, // px 
		    gutter: 20, // px
		    columns: 12
		});
	    }, 1000);

	}
    }, 'json');
}
