<?php
/**
 * @version		$Id$
 * @author		JoomlaUX
 * @package		Joomla.Administrator
 * @subpackage	com_jux_portfolio
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

// Include the component HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

// Load the tooltip behavior.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
$check = isset($this->item->type_media) ? $this->item->type_media : 0;
?>

<script type="text/javascript">
	//	Joomla.submitbutton = function(task) {
	//		if (task == 'item.cancel' || document.formvalidator.isValid(document.id('item-form'))) {
	//			Joomla.submitform(task, document.getElementById('item-form'));
	//		} else {
	//			alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
	//		}
	//	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_jux_portfolio&view=item&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="item-form" class="form-validate" enctype="multipart/form-data">
    <div class="width-55 fltlft">
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td valign="top">
					<fieldset class="adminform" width="100%">
						<legend><?php echo JText::_('COM_JUX_PORTFOLIO_LEGEND_DETAILS'); ?></legend>
						<ul class="adminformlist">
							<li><?php echo $this->form->getLabel('title'); ?>
								<?php echo $this->form->getInput('title'); ?></li>

							<li><?php echo $this->form->getLabel('alias'); ?>
								<?php echo $this->form->getInput('alias'); ?></li>							

							<li><?php echo $this->form->getLabel('published'); ?>
								<?php echo $this->form->getInput('published'); ?></li>

							<li><?php echo $this->form->getLabel('cat_id'); ?>
								<?php echo $this->form->getInput('cat_id'); ?></li>

							<li><?php echo $this->form->getLabel('thumbnail'); ?>
								<?php echo $this->form->getInput('thumbnail'); ?></li>

							<li><?php echo $this->form->getLabel('auto_bw'); ?>
								<?php echo $this->form->getInput('auto_bw'); ?></li>

							<li><?php echo $this->form->getLabel('thumbnail_bw'); ?>
								<?php echo $this->form->getInput('thumbnail_bw'); ?></li>

							<li><?php echo $this->form->getLabel('type_media'); ?>
								<?php echo $this->form->getInput('type_media'); ?></li>

						</ul>
						<div class="clr"></div>
						<?php echo $this->form->getLabel('description'); ?>
						<div class="clr"></div>
						<?php echo $this->form->getInput('description'); ?>

					</fieldset>
                </td>
            </tr>
        </table>
    </div>
    <div class="width-45 fltlft">
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td valign="top">
					<?php echo JHtml::_('sliders.start', 'content-sliders-' . $this->item->id, array('useCookie' => 1)); ?>
					<?php echo JHtml::_('sliders.panel', JText::_('COM_JUX_PORTFOLIO_LEGEND_GALLERY'), 'media-details'); ?>
					<fieldset class="panelform">
						<ul class="adminformlist">
							<li><?php echo $this->form->getLabel('ratio_size'); ?>
								<?php echo $this->form->getInput('ratio_size'); ?></li>

							<li><?php echo $this->form->getLabel('video_url'); ?>
								<?php echo $this->form->getInput('video_url'); ?></li>

							<li><?php echo $this->form->getLabel('large_image'); ?>
								<?php echo $this->form->getInput('large_image'); ?></li>

							<li><?php echo $this->form->getLabel('url'); ?>
								<?php echo $this->form->getInput('url'); ?></li>

							<li><?php echo $this->form->getLabel('caption_title'); ?>
								<?php echo $this->form->getInput('caption_title'); ?></li>

							<li><?php echo $this->form->getLabel('caption_position'); ?>
								<?php echo $this->form->getInput('caption_position'); ?></li>
						</ul>
					</fieldset>
					<?php echo JHtml::_('sliders.panel', JText::_('COM_JUX_PORTFOLIO_LEGEND_LIGHTBOX'), 'lightbox-details'); ?>
					<fieldset class="panelform">
						<ul class="adminformlist">
							<?php
							$fieldSets = $this->form->getFieldsets('params');
							foreach ($fieldSets as $name => $fieldSet):
								foreach ($this->form->getFieldset($name) as $field):
									?>
									<li><?php echo $field->label; ?>
										<?php echo $field->input; ?></li>
									<?php
								endforeach;
							endforeach;
							?>
						</ul>
					</fieldset>
					<?php echo JHtml::_('sliders.panel', JText::_('COM_JUX_PORTFOLIO_CATEGORY_FIELDSET_PUBLISHING'), 'publishing-details'); ?>
					<fieldset class="panelform">
						<ul class="adminformlist">
							<li><?php echo $this->form->getLabel('access'); ?>
								<?php echo $this->form->getInput('access'); ?></li>

							<li><?php echo $this->form->getLabel('language'); ?>
								<?php echo $this->form->getInput('language'); ?></li>

							<li><?php echo $this->form->getLabel('id'); ?>
								<?php echo $this->form->getInput('id'); ?></li>

							<li><?php echo $this->form->getLabel('created_by'); ?>
								<?php echo $this->form->getInput('created_by'); ?></li>

							<li><?php echo $this->form->getLabel('created'); ?>
								<?php echo $this->form->getInput('created'); ?></li>

							<li><?php echo $this->form->getLabel('ordering'); ?>
								<?php echo $this->form->getInput('ordering'); ?></li>
						</ul>
					</fieldset>
					<?php echo JHtml::_('sliders.end'); ?>
                </td>
            </tr>
        </table>
    </div>
	<div>
		<?php //echo $this->form->getInput('id');   ?>
        <input type="hidden" name="option" value="com_jux_portfolio" />
        <input type="hidden" name="controller" value="item" />
        <input type="hidden" name="return" value="<?php echo JRequest::getCmd('return'); ?>" />
        <input type="hidden" name="task" value="" />
		<?php echo JHTML::_('form.token'); ?>
    </div>   
</form>
<div class="clr"></div>
<?php
echo JUX_PortfolioFactory::getFooter();