<?php
/**
 * @version		$Id$
 * @author		JoomlaUX
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

// Include the component HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

// Load the tooltip behavior.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', 'select');
?>
<script type="text/javascript">

    Joomla.submitbutton = function(task)
    {
	if (task == 'item.cancel' || document.formvalidator.isValid(document.id('item-form'))) {
<?php echo $this->form->getField('description')->save(); ?>
	    Joomla.submitform(task, document.getElementById('item-form'));
	}
    }
</script>
<form action="<?php echo JRoute::_('index.php?option=com_jux_gallery&view=item&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="item-form" class="form-validate" enctype="multipart/form-data">
    <div class="row-fluid">
	<!-- Begin Content -->
	<div class="span10 form-horizontal">
	    <ul class="nav nav-tabs">
		<li class="active"><a href="#general" data-toggle="tab"><?php echo JText::_('COM_JUX_GALLERY_LEGEND_DETAILS'); ?></a></li>
		<li><a href="#publishing" data-toggle="tab"><?php echo JText::_('COM_JUX_GALLERY_CATEGORY_FIELDSET_PUBLISHING'); ?></a></li>
	    </ul>

	    <div class="tab-content">
		<!-- Begin Tabs -->
		<div class="tab-pane active" id="general">
		    <fieldset>
			<div class="control-group">
			    <div class="control-label"><?php echo $this->form->getLabel('title'); ?></div>
			    <div class="controls"><?php echo $this->form->getInput('title'); ?></div>
			</div>

			<div class="control-group">
			    <div class="control-label"><?php echo $this->form->getLabel('alias'); ?></div>
			    <div class="controls"><?php echo $this->form->getInput('alias'); ?></div>
			</div>

			<div class="control-group">
			    <div class="control-label"><?php echo $this->form->getLabel('cat_id'); ?></div>
			    <div class="controls"><?php echo $this->form->getInput('cat_id'); ?></div>
			</div>

			<div class="control-group">
			    <div class="control-label"><?php echo $this->form->getLabel('url'); ?></div>
			    <div class="controls"><?php echo $this->form->getInput('url'); ?></div>
			</div>
			
			<div class="control-group">
			    <div class="control-label"><?php echo $this->form->getLabel('large_image'); ?></div>
			    <div class="controls"><?php echo $this->form->getInput('large_image'); ?></div>
			</div>

			<div class="control-group">
			    <div class="control-label"><?php echo $this->form->getLabel('description'); ?></div>
			    <div class="controls"><?php echo $this->form->getInput('description'); ?></div>
			</div>
		    </fieldset>
		</div>

				<div class="tab-pane" id="publishing">
		    <div class="control-group">
			<div class="control-label"><?php echo $this->form->getLabel('id'); ?></div>
			<div class="controls"><?php echo $this->form->getInput('id'); ?></div>
		    </div>

		    <div class="control-group">
			<div class="control-label"><?php echo $this->form->getLabel('created_by'); ?></div>
			<div class="controls"><?php echo $this->form->getInput('created_by'); ?></div>
		    </div>

		    <div class="control-group">
			<div class="control-label"><?php echo $this->form->getLabel('created'); ?></div>
			<div class="controls"><?php echo $this->form->getInput('created'); ?></div>
		    </div>
		</div>
	    </div>
	    <!-- End Tabs -->
	</div>
	<!-- End Content -->

	<!-- Begin Sidebar -->
	<div class="span2">
	    <h4><?php echo JText::_('JDETAILS'); ?></h4>
	    <hr />
	    <fieldset class="form-vertical">
		<div class="control-group">
		    <div class="controls"><?php echo $this->form->getValue('title'); ?></div>
		</div>

		<div class="control-group">
		    <div class="control-label"><?php echo $this->form->getLabel('published'); ?></div>
		    <div class="controls"><?php echo $this->form->getInput('published'); ?></div>
		</div>

		<div class="control-group">
		    <div class="control-label"><?php echo $this->form->getLabel('ordering'); ?></div>
		    <div class="controls"><?php echo $this->form->getInput('ordering'); ?></div>
		</div>

		<div class="control-group">
		    <div class="control-label"><?php echo $this->form->getLabel('access'); ?></div>
		    <div class="controls"><?php echo $this->form->getInput('access'); ?></div>
		</div>
		<div class="control-group">
		    <div class="control-label"><?php echo $this->form->getLabel('language'); ?></div>
		    <div class="controls"><?php echo $this->form->getInput('language'); ?></div>
		</div>
	    </fieldset>
	</div>
	<!-- End Sidebar -->
    </div>

    <input type="hidden" name="task" value="" />
    <?php echo JHtml::_('form.token'); ?>
</form>
<?php echo JUX_GalleryFactory::getFooter(); ?>



