<?php

/**
 * @version		$Id$
 * @author		JoomlaUX
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * View to edit a item.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @since		1.0
 */
class JUX_GalleryViewItem extends JViewLegacy {

	protected $form;
	protected $item;
	protected $state;

	/**
	 * Display the view
	 */
	public function display($tpl = null) {
		// Initialise variables.
		$this->form = $this->get('Form');
		$this->item = $this->get('Item');
		$this->state = $this->get('State');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		$this->addToolbar();
		if (JVERSION < '3.0.0')
		{
			$this->setLayout($this->getLayout() . '25');
		}
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.0
	 */
	protected function addToolbar() {
		JRequest::setVar('hidemainmenu', true);

		$user = JFactory::getUser();
		$isNew = empty($this->item->id);
		$checkedOut = isset($this->item->checked_out) && (!($this->item->checked_out == 0 || $this->item->checked_out == $user->get('id')));
		$canDo = JUX_GalleryHelper::getActions();

		$title = JText::_('COM_JUX_GALLERY_ITEM_MANAGER_' . ($isNew ? 'ADD' : 'EDIT'));
		$document = &JFactory::getDocument();
		$document->setTitle($title);
		JToolBarHelper::title($title, 'item.png');

		// For new records, check the create permission.
		if ($isNew) {
			JToolBarHelper::apply('item.apply');
			JToolBarHelper::save('item.save');
			JToolBarHelper::save2new('item.save2new');
			JToolBarHelper::cancel('item.cancel');
		} else {
			// Can't save the record if it's checked out.
			if (!$checkedOut) {
				// Since it's an existing record, check the edit permission, or fall back to edit own if the owner.
				if ($canDo->get('core.edit') || ($canDo->get('core.edit.own') && $this->item->created_by == $userId)) {
					JToolBarHelper::apply('item.apply');
					JToolBarHelper::save('item.save');

					// We can save this record, but check the create permission to see if we can return to make a new one.
					if ($canDo->get('core.create')) {
						JToolBarHelper::save2new('item.save2new');
					}
				}
			}

			// If checked out, we can still save
			if ($canDo->get('core.create')) {
				JToolBarHelper::save2copy('item.save2copy');
			}
			JToolBarHelper::cancel('item.cancel', 'JTOOLBAR_CLOSE');
		}
	}

}
