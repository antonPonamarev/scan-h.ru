<?php

/**
 * @version		$Id$
 * @author		JoomlaUX
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * View class for a list of categories.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @since		1.0
 */
class JUX_GalleryViewCategories extends JViewLegacy {

	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 *
	 * @return	void
	 */
	public function display($tpl = null) {

		//Load Submenu
		if ($this->getLayout() !== 'modal') {
			JUX_GalleryHelper::addSubmenu('categories');
		}

		$this->items = & $this->get('Items');
		$this->state = & $this->get('State');
		$this->pagination = & $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolBar();
		if (JVERSION >= '3.0.0') {
			$this->sidebar = JHtmlSidebar::render();
		}
		
		if (JVERSION < '3.0.0') {
			$this->setLayout($this->getLayout().'25');
		}
		
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.0
	 */
	function addToolBar() {

		// set page title
		$canDo = JUX_GalleryHelper::getActions();
		$user = JFactory::getUser();
		$document = &JFactory::getDocument();
		$document->setTitle(JText::_('COM_JUX_GALLERY_CATEGORY_MANAGER'));
		JToolBarHelper::title(JText::_('COM_JUX_GALLERY_CATEGORY_MANAGER'), 'category.png');

		if ($canDo->get('core.create')) {
			JToolBarHelper::addNew('category.add');
		}

		if (($canDo->get('core.edit')) || ($canDo->get('core.edit.own'))) {
			JToolBarHelper::editList('category.edit');
		}

		if ($canDo->get('core.edit.state')) {
			JToolBarHelper::divider();
			JToolBarHelper::publish('categories.publish', 'JTOOLBAR_PUBLISH', true);
			JToolBarHelper::unpublish('categories.unpublish', 'JTOOLBAR_UNPUBLISH', true);
			JToolBarHelper::divider();

			JToolBarHelper::checkin('categories.checkin');
			JToolBarHelper::divider();
		}
		if ($this->state->get('filter.published') == -2 && $canDo->get('core.delete')) {
			JToolBarHelper::deleteList('', 'categories.delete', 'JTOOLBAR_EMPTY_TRASH');
			JToolBarHelper::divider();
		} elseif ($canDo->get('core.edit.state')) {
			JToolBarHelper::trash('categories.trash');
			JToolBarHelper::divider();
		}

		if ($canDo->get('core.admin')) {
			JToolBarHelper::preferences('com_jux_gallery');
		}
		if (JVERSION >= '3.0.0') {
			//Sidebar
			JHtmlSidebar::setAction('index.php?option=com_jux_gallery&view=categories');

			JHtmlSidebar::addFilter(
					JText::_('JOPTION_SELECT_PUBLISHED'), 'filter_published', JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), 'value', 'text', $this->state->get('filter.published'), true)
			);

			JHtmlSidebar::addFilter(
					JText::_('JOPTION_SELECT_ACCESS'), 'filter_access', JHtml::_('select.options', JHtml::_('access.assetgroups'), 'value', 'text', $this->state->get('filter.access'))
			);

			JHtmlSidebar::addFilter(
					JText::_('JOPTION_SELECT_LANGUAGE'), 'filter_language', JHtml::_('select.options', JHtml::_('contentlanguage.existing', true, true), 'value', 'text', $this->state->get('filter.language'))
			);
		}
	}

	protected function getSortFields() {
		return array(
			'a.published' => JText::_('JSTATUS'),
			'a.title' => JText::_('JGLOBAL_TITLE'),
			'a.access' => JText::_('JGRID_HEADING_ACCESS'),
			'language' => JText::_('JGRID_HEADING_LANGUAGE'),
			'a.id' => JText::_('JGRID_HEADING_ID')
		);
	}

}
