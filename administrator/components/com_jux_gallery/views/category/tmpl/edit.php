<?php
/**
 * @version		$Id$
 * @author		JoomlaUX
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('jquery.ui');
//JHtml::_('jquery.ui', array('sortable'));
JHtml::_('jquery.ui', array('sortable'));
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.multiselect');
JHtml::_('behavior.modal');

$document = JFactory::getDocument();
$document->addScript(JUri::base() . 'components/com_jux_gallery/assets/js/jquery-ui.js');
$document->addScript(JUri::base() . 'components/com_jux_gallery/assets/js/jquery.ui.draggable.js');
$document->addScript(JUri::base() . 'components/com_jux_gallery/assets/js/jquery.ui.droppable.js');
$document->addScript(JUri::base() . 'components/com_jux_gallery/assets/js/jquery.zclip.min.js');


$categories = $this->categories;
$preferences = $this->preferences;
$firstItems = $this->firstItems;
$menuTypes = $this->menuTypes;
$modules = $this->module;
$listMenus = $this->listMenus;
?>
<script type="text/javascript">
    Joomla.submitbutton = function(task)
    {
	if (task === 'category.cancel' || document.formvalidator.isValid(document.id('item-form'))) {
<?php echo $this->form->getField('description')->save(); ?>
	    Joomla.submitform(task, document.getElementById('item-form'));
	}
    };

    jQuery(document).ready(function() {
	function slideOut() {
	    setTimeout(function() {
		jQuery("#response").slideUp("slow", function() {
		});
	    }, 2000);
	}
	jQuery("#response").hide();
	jQuery(function() {
	    jQuery('#listAlbum').sortable({
		opacity: 0.8, cursor: 'move', connectWith: '#deleteArea', update: function() {
		    var order = jQuery(this).sortable("serialize") + '&update=update';
		    var url = 'index.php?option=com_jux_gallery&task=category.saveOrderAlbum';
		    jQuery.post(url, order, function(respon) {
			if (respon) {
			    jQuery("#response").slideDown('slow');
			    slideOut();
			}
		    });
		}
	    });

	    jQuery('#deleteArea').droppable({
		accept: '#listAlbum > div',
		hoverClass: 'dropAreaHover',
		drop: function(event, ui) {
		    deleteAlbum(ui.draggable, ui.helper);
		},
		activeClass: 'dropAreaHover'
	    });

	    
	});
	
	jQuery('#copySyntax').zclip({
	    path:'components/com_jux_gallery/assets/js/ZeroClipboard.swf',
	    copy:function(){return jQuery('#syntax-plugin').val();},
	    afterCopy:function(){
		jQuery('#copyNotice').css('opacity',1);
		setTimeout(function() {
		jQuery("#copyNotice").css("opacity",0);
	    }, 1500);
	    }
	});
    });
</script>

<form action="<?php echo JRoute::_('index.php?option=com_jux_gallery&view=category&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="item-form" class="form-validate form-horizontal" enctype="multipart/form-data">
    <div class="row-fluid">
	<!--List all Categories-->
	<div class="span2 form-horizontal">
	    <div class="sidebar-nav">
		<div class="jux-button-group-row">
		    <div class="jux-button-group">
			<a class="jux-button is-embossed" style="width: 143px" href="<?php echo JUri::current().'?option=com_jux_gallery'?>"><?php echo JText::_('COM_JUX_GALLERY_LIST_CATEGORIES'); ?></a>
			<a class="jux-button has-icon is-embossed" href="javascript:void(0)" onclick="jQuery('#createAlbumModal').modal('show');"><i class="jux-icon-plus"></i></a>
		    </div>
		</div>
		<hr class="jux-separator">
		<div class="jux-button-group-row">
		    <div id="response" class="jux-button-group">
			<a class="jux-button is-disabled is-success has-text" style="width: 188px"><?php echo JText::_('COM_JUX_GALLERY_SAVE_ORDER_ALBUM'); ?></a>
		    </div>
		</div>
		<div id="listAlbum">
		    <?php
		    foreach ($categories as $category):
			$id = JRequest::getInt('id');
			if ($id == $category->id)
			    $active = TRUE;
			else
			    $active = FALSE;
			?>
    		    <div class="jux-button-group-row sortable-handler hasTooltip <?php echo ($active) ? : 'droppable'; ?>" id="arrayorder_<?php echo $category->id; ?>" data-title="<?php echo $category->title;?>" data-alias="<?php echo $category->alias;?>" data-status="<?php echo $category->published;?>">
    			<div class="jux-button-group">
    			    <a class="jux-button <?php echo ($category->published == 1) ? 'is-success' : 'is-danger'; ?> <?php echo ($active) ? 'is-disabled' : 'is-embossed'; ?> has-icon has-text" href="<?php echo JRoute::_(JUri::base() . 'index.php?option=com_jux_gallery&view=category&layout=edit&id=' . $category->id); ?>" style="width: 143px;text-align: left;"><i class="<?php echo ($category->published == 1) ? 'jux-icon-check' : 'jux-icon-cross'; ?>"></i><?php echo '&nbsp;' . JText::_($category->title); ?></a>
			    <a class="jux-button is-embossed has-icon" href="javascript:void(0)" onclick="viewAlbumEdit(<?php echo $category->id;?>);"><i class="jux-icon-tool"></i></a>
    			</div>
    		    </div>
		    <?php endforeach; ?>
		</div>
		<hr class="jux-separator">
		<div class="jux-button-group-row" id="deleteArea">
		    <div class="jux-button-group">
			<a class="jux-button is-disabled has-icon has-text" style="width: 188px"><i class="jux-icon-recycle"></i><?php echo '&nbsp;' . JText::_('COM_JUX_GALLERY_DELETE_AREA'); ?></a>
		    </div>
		</div>
		<hr class="jux-separator">
		<div class="jux-button-group-row">
		    <div class="jux-button-group" id="SS">
			<a class="jux-button is-embossed has-icon is-success" onclick="openSS();" href="javascript:void(0)" id="iconTable"><i class="jux-icon-table"></i></a>
			<a class="jux-button has-text is-success is-disabled" style="width: 147px"><?php echo JText::_('COM_JUX_GALLERY_SAVE_AND_SHOW');?></a>
		    </div>
		</div>
		<div class="jux-button-group-row" id="menuitem">
		    <div class="jux-button-group">	
			<a class="jux-button has-text is-success" id="listItem" style="width:143px;" onclick="showListMenu();" href="javascript:void(0)"><?php echo JText::_('COM_JUX_GALLERY_SAVE_MENU_ITEM');?></a>
			<a class="jux-button has-icon" onclick="showMenuTypes();" href="javascript:void(0)"><i class="jux-icon-plus"></i></a>
		    </div>
		</div>
		<?php foreach ($menuTypes as $menuType):?>
		<?php $href = JUri::current().'?option=com_jux_gallery&task=launchAdapter&type=menu&menutype='.$menuType->menutype;?>
		<div class="menuType jux-button-group-row ">
		    <div class="jux-button-group">
			<a class="jux-button has-text is-success" style="width: 188px;" href="<?php echo $href;?>">
			    <?php echo JText::_($menuType->title);?>
			</a>
		    </div>
		</div>
		<?php endforeach;?>
		<?php foreach ($listMenus as $listMenu):?>
		<?php $uri = JUri::current().'?option=com_menus&task=item.edit&id='.$listMenu->id;?>
		<div class="listMenu jux-button-group-row ">
		    <div class="jux-button-group">
			<a class="jux-button has-text <?php echo ($listMenu->published == 1) ? 'is-success' : 'is-danger';?>" style="width: 188px;" href="<?php echo $uri;?>">
			    <?php echo JText::_($listMenu->title);?>
			</a>
		    </div>
		</div>
		<?php endforeach;?>
		<div class="jux-button-group-row" id="moduleitem">
		    <div class="jux-button-group">
			<a class="jux-button has-text is-success" style="width: 143px;" onclick="showListModule();" href="javascript:void(0)">
			    <?php echo JText::_('COM_JUX_GALLERY_SAVE_MODULE_ITEM');?>
			</a>
			<a class="jux-button is-embossed has-icon" href="<?php echo JUri::current().'?option=com_jux_gallery&task=launchAdapter&type=module';?>"><i class="jux-icon-plus"></i></a>
		    </div>
		</div>
		<?php foreach ($modules as $module):?>
		<?php $url = JUri::current().'?option=com_modules&task=module.edit&id='.$module->id;?>
		<div class="listModule jux-button-group-row ">
		    <div class="jux-button-group">
			<a class="jux-button has-text <?php echo $module->published == 1 ? 'is-success' : 'is-danger';?>" style="width: 188px;" href="<?php echo JRoute::_($url);?>">
			    <?php echo JText::_($module->title);?>
			</a>
		    </div>
		</div>
		<?php endforeach;?>
		<div class="jux-button-group-row" id="insideArticle">
		    <div class="jux-button-group">
			<a class="jux-button has-text is-success" style="width: 188px;" href="javascript:void(0)" onclick="/*jQuery('#insideArticleModal').modal('show')*/ insideArticleModal();">
			    <?php echo JText::_('COM_JUX_GALLERY_SAVE_INSIDE_ARTICLE');?>
			</a>
		    </div>
		</div>
		<hr class="jux-separator">
		<div class="jux-button-group-row">
		    <div class="jux-button-group">
			<a class="jux-button has-icon has-text is-embossed" style="width: 188px;" href="javascript:void(0)" onclick="location.href = '<?php echo $preferences; ?>';"><i class="jux-icon-options"></i><?php echo '&nbsp;' . JText::_('JToolbar_Options'); ?></a>
		    </div>
		</div>
	    </div>
	</div>
	<!-- Begin Conten -->
	<div class="span10 form-horizontal jux-content">
	    <div class="tab-content">
		<div class="tab-pane <?php echo empty($this->item->id) ? 'active' : ''; ?>">
		    <div class="span12 form-horizontal">
			<div id="allAlbum">
			    <?php foreach ($categories as $category): ?>
    			    <div class="boxs">
				<?php $i = 1; foreach ($firstItems as $firstItem):?>
				    <?php if ($category->id == $firstItem->cat_id) :
					if($i == $category->count || $i == 3)
					{
					    $class = 'front';
					} elseif($i%2 == 0 && $i< $category->count){
					    $class = 'even';
					}  elseif($i%2 != 0 && $i < $category->count)
					{
					    $class = 'odd';
					}  
				    ?>
				    <div class="box <?php echo $class;?>" >
					<img src="<?php echo JUri::root() . $firstItem->large_image; ?>" class="itemImage"/>
					<a href="<?php echo JUri::current() . '?option=com_jux_gallery&view=category&layout=edit&id=' . $category->id; ?>">
					    <span class="caption scale-caption">
						<h3><?php echo $category->title; ?></h3>
					    </span>
					    <span class="numOfImages"><?php echo $category->count;?></span>
					</a>
				    </div>
				    <?php $i++; if ($i > 3) break; endif;?>
				<?php endforeach; ?>
    			    </div>
			    <?php endforeach; ?>
			</div>
		    </div>
		</div>
		<!--Add Items of Category-->
		<div class="tab-pane <?php echo empty($this->item->id) ? '' : 'active'; ?>" id="items">
		    <?php echo $this->loadTemplate('images'); ?>
		</div>
		<input type="hidden" name="task" value="category.upload" />
		<?php echo JHtml::_('form.token'); ?>
	    </div>
	</div>
	<!-- End Content -->
    </div>
</form>

<div id="createAlbumModal" class="modal hide fade">
    <div class="modal-header">
	<h3>Create Album</h3>
    </div>
    <div class="modal-body">
	<div class="control-group">
	    <label class="control-label" for="albumTitle">Title</label>
	    <div class="controls">
		<input type="text" id="albumTitle">
	    </div>
	</div>
    </div>
    <div class="modal-footer jux-button-group-row">
	<div class="jux-button-group">
	<a data-dismiss="modal" class="jux-button is-danger has-icon"><i class="jux-icon-cross"></i></a>
	<a data-url="#" href="javascript:void(0)" onclick="albumCreate();" data-type="#" class="jux-button is-success has-icon"><i class="jux-icon-check"></i></a>
	</div>
    </div>
</div>

<div id="editAlbumModal" class="modal hide fade">
    <div class="modal-header">
	<h3>Edit Album</h3>
    </div>
    <div class="modal-body">
	<div class="control-group">
	    <label class="control-label" for="editAlbumTitle">Title</label>
	    <div class="controls">
		<input type="text" id="editAlbumTitle">
	    </div>
	</div>
	<div class="control-group">
	    <label class="control-label" for="editAlbumAlias">Alias</label>
	    <div class="controls">
		<input type="text" id="editAlbumAlias">
	    </div>
	</div>
	<div class="control-group">
	    <label class="control-label" for="editAlbumStatus">Status</label>
	    <div class="controls" id="editAlbumStatus" data-status="">
		<div class="jux-button-group">
		    <a class="jux-button has-text is-success is-embossed" id="albumPublish" onclick="publish();"><?php echo JText::_('JTOOLBAR_PUBLISH');?></a>
		    <a class="jux-button has-text is-danger is-embossed" id="albumUnPublish" onclick="unpublish();"><?php echo JText::_('JTOOLBAR_UNPUBLISH');?></a>
		</div>
	    </div>
	</div>
    </div>
    <div class="modal-footer jux-button-group-row">
	<div class="jux-button-group">
	<a data-dismiss="modal" class="jux-button is-danger has-icon"><i class="jux-icon-cross"></i></a>
	<a data-url="#" href="javascript:void(0)" onclick="saveAlbum();" data-type="#" class="jux-button is-success has-icon"><i class="jux-icon-check"></i></a>
	</div>
    </div>
</div>

<div id="insideArticleModal" class="modal hide fade">
    <div class="modal-header">
	<h3>Plugin Syntax Details</h3>
    </div>
    <div class="modal-body">
	<div class="control-group">
	    <div class="jux-button-group-row">
		<div class="jux-button-group" id="albumSelect">
		</div>
	    </div>
	</div>
	<div class="control-group">
	    <label class="control-label" for="editAlbumTitle">Please insert following text to your article at the position where you want to show album
	    </label>
	</div>
	    <div class="jux-button-group-row">
		<div class="jux-button-group">
		    <input type="text" id="syntax-plugin" value="{jux-gallery album=}" readonly="true"/>
		    <div class="copied" id="copyNotice">Copy successful</div>
		</div>
		<div class="jux-button-group">
		    <a class="jux-button has-text is-success is-embossed" id="copySyntax">
			<?php echo JText::_('Copy Syntax');?>
		    </a>
		</div>
	    </div>
    </div>
    <div class="modal-footer jux-button-group-row">
	<div class="jux-button-group">
	    <a data-dismiss="modal" class="jux-button is-danger has-icon" href="" onclick="closeInsideArticleModal();"><i class="jux-icon-cross"></i></a>
	</div>
    </div>
</div>
<?php echo JUX_GalleryFactory::getFooter(); ?>