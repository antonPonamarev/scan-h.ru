<?php
/**
 * @version	$Id$
 * @author	Joomseller
 * @package	Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2008 - 2013 by Joomseller Solutions. All rights reserved.
 * @license	http://www.gnu.org/licenses/gpl.html
 */
// No direct access.
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
//JHtml::_('bootstrap.modal', 'editItem');
//JHtml::_('JUX_GALLERY.formbehavior');

$document = JFactory::getDocument();
$document->addStyleSheet(JURI::base() . 'components/com_jux_gallery/assets/css/jquery.fileupload-ui.css');
$document->addStyleSheet(JURI::base() . 'components/com_jux_gallery/assets/css/jquery.gridly.css');
//$document->addStyleSheet(JURI::base(). 'components/com_jux_gallery/assets/css/jquery.fileupload-ui-noscript.css');


$document->addScript(JUri::base() . 'components/com_jux_gallery/assets/js/jquery.gridly.js');
$document->addScript(JURI::base() . 'components/com_jux_gallery/assets/js/jquery.ui.widget.js');
$document->addScript(JURI::base() . 'components/com_jux_gallery/assets/js/tmpl.min.js');
$document->addScript(JURI::base() . 'components/com_jux_gallery/assets/js/jquery.iframe-transport.js');
$document->addScript(JURI::base() . 'components/com_jux_gallery/assets/js/jquery.fileupload.js');
$document->addScript(JURI::base() . 'components/com_jux_gallery/assets/js/jquery.fileupload-process.js');
$document->addScript(JURI::base() . 'components/com_jux_gallery/assets/js/jquery.fileupload-validate.js');
$document->addScript(JURI::base() . 'components/com_jux_gallery/assets/js/jquery.fileupload-ui.js');

$params = JComponentHelper::getParams('com_jux_gallery');
$allowed_exts = str_replace(' ', '', $params->get('file_allowed_ext', 'jpeg,png,bmp'));
$max_file_size = (int) $params->get('file_max_size', 5);
$id = $this->item->id;
?>
<!--form upload--> 
<div id="fileupload">
    <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
    <div class="jux-button-group-row">
	    <div class="jux-button-group" id="saveOrderItems">
		<a class="jux-button is-success is-disabled "><?php echo JText::_('COM_JUX_GALLERY_SAVE_ORDER'); ?></a>
		<a class="jux-button has-icon is-success is-embossed" onclick="saveOrder();"><i class="jux-icon-config"></i>
		</a>
	    </div>
	</div>
    <div role="presentation" class="container">
	<div class="files gridly">

	</div>
    </div>
    <?php echo JHtml::_('form.token'); ?>
    <div class="clr"></div>
    <div class="container-fluid fileupload-buttonbar">
	<input class="hidden" name="cat_id" value="<?php echo $id; ?>"/>
	<div class="jux-button-group-row">
	    <div class="jux-button-group">
		<a class="jux-button is-success is-disabled"><?php echo JText::_('COM_JUX_GALLERY_MASS_UPLOAD_ADD_FILE'); ?></a>
		<a class="jux-button has-icon is-success is-embossed fileinput-button"><i class="jux-icon-upload"></i>
		<input type="file" name="files[]" multiple></a>
	    </div>
	    <div class="jux-button-group">
		<a class="jux-button is-danger is-disabled"><?php echo JText::_('COM_JUX_GALLERY_DELETE_ALL_ITEM'); ?></a>
		<a class="jux-button has-icon is-danger is-embossed" onclick="itemDeleteAll();"><i class="jux-icon-delete"></i>
		</a>
	    </div>
	    <!-- The loading indicator is shown during file processing -->
	    <span class="fileupload-loading"></span>

	</div>
	<!-- The global progress information -->
	<div class="span5 fileupload-progress fade">
	    <!-- The global progress bar -->
	    <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
		<div class="bar" style="width:0%;"></div>
	    </div>
	    <!-- The extended global progress information -->
	    <div class="progress-extended">&nbsp;</div>
	</div>
    </div>

    <!--note for upload--> 
    <br>
</div>
<!--The template to display image available for upload--> 
<script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <div class="brick small template-upload" style="background:url(<?php echo JUri::root() ?>{%=file.large_image%});">
    {% if(file.error){ %}
    <div>
    <span class="error">{%=file.error%}</br>Can not Upload !</span>
    </div>
    {% }else{ %}
    <div>
    <a class="delete" title="Delete" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}"{% if (file.delete_with_credentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}><i class="icon-trash icon-white"></i></a>
    <a class="edit" title="Edit" id="item_{%=file.itemID%}" data-title="{%=file.title%}" data-url="{%=file.url%}" data-desc="{%=file.description%}" onclick="itemEdit({%=file.itemID%})"><i class="icon-pencil-2 icon-white"></i></a>
    </div>
    {% } %}
    </div>
    {% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <div id="itemID_{%= file.itemID%}" class="brick small template-download ui-widget-content" style="background:url(<?php echo JUri::root() ?>{%=file.large_image%});">
    {% if(file.error){ %}
    <div>
    <span class="error">{%=file.error%}</br>Can not Upload !</span>
    </div>
    {% }else{ %}
    <div>
    <a class="delete" title="Delete" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}"{% if (file.delete_with_credentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}><i class="icon-trash icon-white"></i></a>
    <a class="edit" title="Edit" id="item_{%= file.itemID%}" data-title="{%= file.title%}" data-url="{%= file.url%}" data-desc="{%= file.description%}" onclick="itemEdit({%= file.itemID%})"><i class="icon-pencil-2 icon-white"></i></a>
    </div>
    {% } %}
    </div>
    {% } %}
</script>
<script type="text/javascript">
    jQuery(function() {
    'use strict';

    // Initialize the jQuery File Upload widget:
    jQuery('#fileupload').fileupload({
	url: '<?php echo JURI::base() . 'index.php?option=com_jux_gallery&task=category.upload'; ?>',
	autoUpload: true,
	maxFileSize: <?php echo $max_file_size * 1048576; ?>,
	acceptFileTypes: /(\.|\/)(<?php echo implode('|', explode(',', $allowed_exts)); ?>)$/i
    });
    
    jQuery('#fileupload').bind('fileuploadcompleted', function(e, data) {
	jQuery('.gridly').gridly({
	    base: 60, // px 
	    gutter: 20, // px
	    columns: 12
	}
    );
	jQuery('.gridly > div').draggable();
	jQuery('#listAlbum > div.droppable').droppable({
	    accept: '.gridly > div',
	    hoverClass: 'dropArea-Hover',
	    activeClass: 'dropAreaHover',
	    drop:function(event, ui){
//		jQuery(this).find('a.has-text').css('height','80');
		updateAlbum(event.target , event.toElement);
	    }
	});
    });

    jQuery('#fileupload').bind('fileuploaddestroyed', function(e, data) {
	jQuery('.gridly').gridly({
	    base: 60, // px 
	    gutter: 20, // px
	    columns: 12
	});
    });

    // Load existing files:
    jQuery('#fileupload').addClass('fileupload-processing');
	jQuery.ajax({
	// Uncomment the following to send cross-domain cookies:
	//xhrFields: {withCredentials: true},
	    url: '<?php echo JURI::base() . 'index.php?option=com_jux_gallery&task=category.getFiles&id=' . $id; ?>',
	    dataType: 'json',
	    context: jQuery('#fileupload')[0]
	}).always(function() {
	    jQuery(this).removeClass('fileupload-processing');
	}).done(function(result) {
	    jQuery(this).fileupload('option', 'done')
	.call(this, null, {result: result});
	});
    });
</script>
<div id="editItemModal" class="modal hide fade">
    <div class="modal-header">
	<h3>Edit Item</h3>
    </div>
    <div class="modal-body">
	<div class="control-group">
	    <label class="control-label" for="itemTitle">Title</label>
	    <div class="controls">
		<input type="text" id="itemTitle">
	    </div>
	</div>
	<div class="control-group">
	    <label class="control-label" for="itemLink">Link</label>
	    <div class="controls">
		<input type="text" id="itemLink">
	    </div>
	</div>
	<div class="control-group">
	    <label class="control-label" for="itemDescription">Description</label>
	    <div class="controls">
		<textarea id="itemDescription" cols="80" rows="6"></textarea>
	    </div>
	</div>
    </div>
    <div class="modal-footer jux-button-group-row">
	<div class="jux-button-group">
	<a data-dismiss="modal" class="jux-button is-danger has-icon"><i class="jux-icon-cross"></i></a>
	<a data-url="#" href="javascript:void(0)" onclick="saveItem();" data-type="#" class="jux-button is-success has-icon"><i class="jux-icon-check"></i></a>
	</div>
    </div>
</div>