<?php
/**
 * @version		$Id$
 * @author		JoomlaUX
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html
 */
// no direct access
defined('_JEXEC') or die('Restricted access');
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', 'select');

$model = JModelLegacy::getInstance('category', 'JUX_GalleryModel');
$form = $model->getForm();
?>
<script type="text/javascript">
    Joomla.submitbutton = function(task)
    {
	if (document.formvalidator.isValid(document.id('addNew'))) {
	    <?php echo $this->form->getField('description')->save(); ?>
	    Joomla.submitform(task, document.getElementById('addNew'));
	    //document.getElementById('addNew').hide();
	}
    }
</script>
<form action="<?php echo JRoute::_('index.php?option=com_jux_gallery&view=category&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="addNew" class="form-validate form-horizontal" enctype="multipart/form-data">
    <div class="row-fluid">
	<!-- Begin Conten -->
	<div class="span10 form-horizontal">
	    <ul class="nav nav-tabs">
		<li class="active"><a href="#general" data-toggle="tab"><?php echo empty($this->item->id) ? JText::_('COM_JUX_GALLERY_CATEGORY_NEW') : JText::sprintf('COM_JUX_GALLERY_CATEGORY_EDIT', $this->item->id); ?></a></li>
	    </ul>
	    <div class="tab-content">
		<!-- Begin Tabs -->
		<div class="tab-pane active" id="general">
		    <div class="control-group">
			<div class="control-label">
			    <?php echo $form->getLabel('title'); ?>
			</div>
			<div class="controls">
			    <?php echo $form->getInput('title'); ?>
			</div>
		    </div>
		    <div class="control-group">
			<div class="control-label">
			    <?php echo $form->getLabel('alias'); ?>
			</div>
			<div class="controls">
			    <?php echo $form->getInput('alias'); ?>
			</div>
		    </div>
		    <div class="control-group">
			<div class="control-label">
			    <?php echo $form->getLabel('description'); ?>
			</div>
			<div class="controls">
			    <?php echo $form->getInput('description'); ?>
			</div>
		    </div>
		</div>
		<!-- End tab general -->
		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>
	    </div>
	</div>
	<!-- End Content -->
    </div>
    <div style="text-align: center;">
	<button type="button" class="btn btn-success" onclick="Joomla.submitbutton('category.apply');" style="width: 148px">
	    <span class="icon-apply icon-white"></span>&#160;<?php echo JText::_('JSAVE') ?>
	</button>
    </div>
</form>