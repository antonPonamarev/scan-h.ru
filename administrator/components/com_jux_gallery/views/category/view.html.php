<?php
/**
 * @version		$Id$
 * @author		JoomlaUX
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * View to edit a category.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @since		1.0
 */
class JUX_GalleryViewCategory extends JViewLegacy {

	protected $form;
	protected $item;
	protected $state;
	protected $images;
	protected $categories;

	/**
	 * Display the view
	 */
	public function display($tpl = null) {
		// Initialise variables.
		$this->form = $this->get('Form');
		$this->item = $this->get('Item');
		$this->state = $this->get('State');
		$this->images = $this->get('Images');
		$this->categories = $this->get('Categories');
		$this->preferences = $this->get('Preferences');
		$this->firstItems = $this->get('firstItems');
		$this->menuTypes = $this->get('menuTypes');
		$this->module = $this->get('Module');
		$this->listMenus = $this->get('ListMenu');
		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		if (JVERSION < '3.0.0') {
			$this->setLayout($this->getLayout().'25');
		}
//		if($this->getLayout()) 
//		    $tpl =$this->getLayout();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since	1.0
	 */
	protected function addToolbar(){

		$user		= JFactory::getUser();
		$isNew		= empty($this->item->id);
//		$checkedOut	= isset($this->item->checked_out) && (!($this->item->checked_out == 0 || $this->item->checked_out == $user->get('id')));
//		$canDo		= JUX_GalleryHelper::getActions();

		$title = JText::_('COM_JUX_GALLERY_CATEGORY_MANAGER_' . ($isNew ? 'ADD' : 'EDIT'));
		$document = JFactory::getDocument();
		$document->setTitle($title);
		JToolBarHelper::title($title, 'category.png');
	}
	
}
