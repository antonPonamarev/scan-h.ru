<?php

/**
 * @version		$Id$
 * @author		JoomlaUX
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, SEE LICENSE.php
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * category table
 *  
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 */
class JUX_GalleryTableCategory extends JTable
{

    /**
     * Constructor
     *
     * @param object Database connector object
     * @since 1.0
     */
    public function __construct(& $db)
    {
	parent::__construct('#__jux_gallery_categories', 'id', $db);
    }

    /**
     * Overloaded check function
     *
     * @return boolean
     * @see JTable::check
     * @since 1.0
     */
    public function check()
    {

	if (empty($this->title) || trim($this->title) == '')
	{
	    $this->setError(JText::_('COM_JUX_GALLERY_WARNING_PROVIDE_VALID_NAME'));
	    return false;
	}

	// alias
	if (empty($this->alias) || trim($this->alias) == '')
	{
	    $this->alias = JFilterOutput::stringURLSafe($this->title);
	}
	
	//Unique Category Alias
	$table = JTable::getInstance('Category', 'JUX_GalleryTable');
	while ($table->load(array('alias' => $this->alias)) && ($table->id != $this->id || $this->id == 0))
	{
	    $alias = $alias = JString::increment($this->alias, 'dash');
	    $this->alias = $alias;
	}

	if (trim(str_replace('-', '', $this->alias)) == '')
	{
	    $this->alias = JFactory::getDate()->format('Y-m-d-H-i-s');
	}

	//Unique Category Alias
	$table = JTable::getInstance('Category', 'JUX_GalleryTable');
	if ($table->load(array('alias' => $this->alias)) && ($table->id != $this->id || $this->id == 0))
	{
	    $this->setError(JText::_('COM_JUX_GALLERY_ERROR_UNIQUE_CATEGORY_ALIAS'));
	    return false;
	}


	//Clean up keywords -- eliminate extra spaces between phrases
	// and cr (\r) and lf (\n) characters from string
	if (!empty($this->metakey))
	{
	    // Only process if not empty
	    $bad_characters = array("\n", "\r", "\"", "<", ">"); // array of characters to remove
	    $after_clean = JString::str_ireplace($bad_characters, "", $this->metakey); // remove bad characters
	    $keys = explode(',', $after_clean); // create array using commas as delimiter
	    $clean_keys = array();

	    foreach ($keys as $key)
	    {
		if (trim($key))
		{
		    // Ignore blank keywords
		    $clean_keys[] = trim($key);
		}
	    }
	    $this->metakey = implode(", ", $clean_keys); // put array back together delimited by ", "
	}

	return parent::check();
    }

}