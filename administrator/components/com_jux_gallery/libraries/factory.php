<?php
/**
 * @version		$Id$
 * @author		JoomlaUX
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

/**
 * jux_gallery Component - Factory Library
 * 
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 */
class JUX_GalleryFactory {
	
	/**
	 * Get credits footer string.
	 */
	public static function getFooter() {
		return '<div class="copyright"><span class="title">JUX Gallery</span> copyright (C) 2012 - 2013 by JoomlaUX - <a href="http://www.joomlaux.com" target="_blank">http://www.joomlaux.com</a></div>';
	}

	/**
	 * Get model of component
	 */
	function &getModel($type, $prefix = 'JUX_GalleryModel', $config = array()) {
		static $instance;

		if(!isset($instance[$type]) || !is_object($instance[$type])) {
			$instance[$type] = JModel::getInstance($type, $prefix, $config);
		}

		return $instance[$type];
	}
}
