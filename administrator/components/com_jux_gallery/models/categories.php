<?php

/**
 * @version		$Id$
 * @author		JoomlaUX
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of categories records.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 */
class JUX_GalleryModelCategories extends JModelList
{

    /**
     * Constructor.
     *
     * @param	array	An optional associative array of configuration settings.
     * @see		JController
     * @since	1.0
     */
    public function __construct($config = array())
    {
	if (empty($config['filter_fields']))
	{
	    $config['filter_fields'] = array(
		'id', 'a.id',
		'title', 'a.title',
		'alias', 'a.alias',
		'published', 'a.published',
		'created', 'a.created',
		'created_by', 'a.created_by',
		'description', 'a.description',
		'checked_out', 'a.checked_out',
		'checked_out_time', 'a.checked_out_time',
		'access', 'a.access', 'access_level',
		'language', 'a.language',
		'ordering', 'a.ordering'
	    );
	}

	parent::__construct($config);
    }

    /**
     * Override method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @param	string	An optional ordering field.
     * @param	string	An optional direction (asc|desc).
     *
     * @return	void
     * @since	1.0
     */
    protected function populateState($ordering = null, $direction = null)
    {
	// Initialise variables.
	$app = JFactory::getApplication();

	// Adjust the context to support modal layouts.
	if ($layout = JRequest::getVar('layout'))
	{
	    $this->context .= '.' . $layout;
	}

	$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
	$this->setState('filter.search', $search);

	$published = $this->getUserStateFromRequest($this->context . '.filter.published', 'filter_published', '');
	$this->setState('filter.published', $published);

	$access = $this->getUserStateFromRequest($this->context . '.filter.access', 'filter_access', 0, 'int');
	$this->setState('filter.access', $access);

	$language = $this->getUserStateFromRequest($this->context . '.filter.language', 'filter_language', '');
	$this->setState('filter.language', $language);

	// List state information.
	parent::populateState('a.title', 'asc');
    }

    /**
     * Override method to add more properties to store id.
     *
     * @param	string		$id	A prefix for the store id.
     *
     * @return	string		A store id.
     * @since	1.0
     */
    protected function getStoreId($id = '')
    {
	// Compile the store id.
	$id .= ':' . $this->getState('filter.search');
	$id .= ':' . $this->getState('filter.published');
	$id .= ':' . $this->getState('filter.access');
	$id .= ':' . $this->getState('filter.language');

	return parent::getStoreId($id);
    }

    /**
     * Build an SQL query to load the list data.
     *
     * @return	JDatabaseQuery
     * @since	1.0
     */
    protected function getListQuery()
    {

	// Create a new query object.
	$db = $this->getDbo();
	$query = $db->getQuery(true);
	$user = JFactory::getUser();

	// Select the required fields from the table.
	$query->select($this->getState('list.select', 'a.*'));
	$query->from('#__jux_gallery_categories AS a');

	// Join over the users for the checked out user
	$query->select('uc.name AS editor');
	$query->leftJoin('#__users AS uc ON uc.id=a.checked_out');

	// Join over the access groups
	$query->select('ag.title AS access_level');
	$query->leftJoin('#__viewlevels AS ag ON ag.id = a.access');

	// Join over the language
	$query->select('l.title AS language_title');
	$query->leftJoin($db->quoteName('#__languages') . ' AS l ON l.lang_code = a.language');

	// Filter by published state
	$published = $this->getState('filter.published');
	if (is_numeric($published))
	{
	    $query->where('a.published = ' . (int) $published);
	} else if ($published === '')
	{
	    $query->where('(a.published = 0 OR a.published = 1)');
	}

	// Filter by search in title.
	$search = $this->getState('filter.search');
	if (!empty($search))
	{
	    if (stripos($search, 'id:') === 0)
	    {
		$query->where('a.id = ' . (int) substr($search, 3));
	    } else
	    {
		$search = $db->Quote('%' . $db->escape($search, true) . '%');
		$query->where('(a.title LIKE ' . $search . ' OR a.alias LIKE ' . $search . ')');
	    }
	}

	// Implement View Level Access
	if (!$user->authorise('core.admin'))
	{
	    $groups = implode(',', $user->getAuthorisedViewLevels());
	    $query->where('a.access IN (' . $groups . ')');
	}

	// Filter by access level
	if ($access = $this->getState('filter.access'))
	{
	    $query->where('a.access = ' . (int) $access);
	}

	// Filter on the language
	if ($language = $this->getState('filter.language'))
	{
	    $query->where('a.language = ' . $db->quote($language));
	}

	// Add the list ordering clause.
	$orderCol = $this->state->get('list.ordering');

	$orderDirn = $this->state->get('list.direction');
	if ($orderCol == 'a.ordering')
	{
	    $orderCol = 'a.ordering';
	}

	$query->order($db->escape($orderCol . ' ' . $orderDirn));
	// echo nl2br(str_replace('#__','jos_',$query));

	return $query;
    }

}