<?php

/**
 * @version	$Id$
 * @author	JoomlaUX
 * @package	Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license	http://www.gnu.org/licenses/gpl.html
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modeladmin');

/**
 * Item Model for a item.
 *
 * @package	Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @since	1.0
 */
class JUX_GalleryModelItem extends JModelAdmin
{

    /**
     * @var    string  The prefix to use with controller messages.
     * @since  1.0
     */
    protected $text_prefix = 'COM_JUX_GALLERY_ITEM';

    /**
     * Constructor.
     *
     * @param   array  $config  An optional associative array of configuration settings.
     *
     * @since   1.0
     */
    public function __construct($config = array())
    {
	parent::__construct($config);
    }

    /**
     * Method to test whether a record can be deleted.
     *
     * @param	object	$record	A record object.
     *
     * @return	boolean	True if allowed to delete the record. Defaults to the permission set in the component.
     * @since	1.0
     */
    protected function canDelete($record)
    {
	$user = JFactory::getUser();

	if ($record->id)
	{
	    return $user->authorise('core.delete', 'com_jux_gallery');
	} else
	{
	    return parent::canDelete($record);
	}
    }

    /**
     * Method to test whether a record can have its state edited.
     *
     * @param	object	$record	A record object.
     *
     * @return	boolean	True if allowed to change the state of the record. Defaults to the permission set in the component.
     * @since	1.0
     */
    protected function canEditState($record)
    {
	$user = JFactory::getUser();

	if ($record->id)
	{
	    return $user->authorise('core.edit.state', 'com_jux_gallery');
	} else
	{
	    return parent::canEditState($record);
	}
    }

    /**
     * Returns a Table object, always creating it
     *
     * @param	type	$type	The table type to instantiate
     * @param	string	$prefix	A prefix for the table class name. Optional.
     * @param	array	$config	Configuration array for model. Optional.
     *
     * @return	JTable	A database object
     * @since	1.0
     */
    public function getTable($type = 'item', $prefix = 'jux_galleryTable', $config = array())
    {
	return JTable::getInstance($type, $prefix, $config);
    }

    /**
     * Method to get the row form.
     *
     * @param	array	$data		Data for the form.
     * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
     *
     * @return	mixed	A JForm object on success, false on failure
     * @since	1.0
     */
    public function getForm($data = array(), $loadData = true)
    {
	$form = $this->loadForm('com_jux_gallery.item', 'item', array('control' => 'jform', 'load_data' => $loadData));
	if (empty($form))
	{
	    return false;
	}
	return $form;
    }

    /**
     * Method to get the data that should be injected in the form.
     *
     * @return	mixed	The data for the form.
     * @since	1.0
     */
    protected function loadFormData()
    {
	// Check the session for previously entered form data.
	$data = JFactory::getApplication()->getUserState('com_jux_gallery.edit.item.data', array());
	$post = JRequest::getVar('jform', array(), 'post', 'array');
	if (empty($data))
	{
	    $data = $this->getItem();
	}

	return $data;
    }

    /**
     * Prepare and sanitise the table prior to saving.
     *
     * @param	JTable	$table
     *
     * @return	void
     * @since	1.0
     */
    protected function prepareTable($table)
    {
	$date = JFactory::getDate();
	$user = JFactory::getUser();

	$table->title = htmlspecialchars_decode($table->title, ENT_QUOTES);
	$table->alias = JApplication::stringURLSafe($table->alias);

	if (empty($table->id))
	{
	    // Set created info
	    if (empty($table->created_by))
	    {
		$table->created_by = $user->get('id');
	    }
	    
	      $table->created = $date->toSql();
	      
	    // Set language info
	    if (empty($table->language))
	    {
		$table->language = '*';
	    }

	    // Set ordering to the last item if not set
	    if (empty($table->ordering))
	    {
		$db = JFactory::getDbo();
		$db->setQuery('SELECT MAX(ordering) FROM #__jux_gallery_items');
		$max = $db->loadResult();

		$table->ordering = $max + 1;
	    }
	}
    }

    public function getItem($pk = null)
    {
	$item = parent::getItem($pk);
	return $item;
    }

    /**
     * Method to save the form data.
     *
     * @param	array	The form data.
     *
     * @return	boolean	True on success.
     * @since	1.6
     */
    public function save($data)
    {
	// Initialise variables.
	$pk = (!empty($data['id'])) ? $data['id'] : (int) $this->getState('item.id');
	$isNew = true;
	$table = $this->getTable();
	// Load the parameters.
	// Load the row if saving an existing item.
	if ($pk > 0)
	{
	    $table->load($pk);
	    $isNew = false;
	}

	if (!$data['id'])
	{
	    $datenow = JFactory::getDate();
	    $data['created'] = $datenow->toSQL();
	}

	if (parent::save($data))
	{
	    $row = parent::getItem($this->getState($this->getName() . '.id'));
	    return true;
	}
	return false;
    }

    public function store($data)
    {
	parent::save($data);
	$row = parent::getItem($this->getState($this->getName() . '.id'));
	$data['id'] = (int) $row->id;

	return $data;
    }

    public function delete(&$pk = null)
    {
	// delete image of item
	$count = count($pk);
	$file_dir_largeimage = array();
	for ($i = 0; $i < $count; $i++)
	{
	    $item = $this->getItem($pk[$i]);
	    if ($item->large_image)
	    {
		$file_dir_largeimage = JPATH_SITE . DS . $item->large_image;
		if (JFile::exists($file_dir_largeimage))
		{
		    JFile::delete($file_dir_largeimage);
		}
	    }
	}

	if (parent::delete($pk))
	{
	    return true;
	}
	return false;
    }

    /**
     * Method to change the title & alias.
     *
     * @param   integer  $parent_id  The id of the parent.
     * @param   string   $alias      The alias.
     * @param   string   $title      The title.
     *
     * @return  array  Contains the modified title and alias.
     *
     * @since	1.7
     */
    public function generateNewTitle($category_id,$alias, $title)
    {
	// Alter the title & alias
	$table = $this->getTable();
	while ($table->load(array('alias' => $alias)))
	{
	    $title = JString::increment($title);
	    $alias = JString::increment($alias, 'dash');
	}

	return array($title, $alias);
    }

}