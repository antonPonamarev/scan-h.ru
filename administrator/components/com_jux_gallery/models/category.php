<?php

/**
 * @version		$Id$
 * @author		JoomlaUX
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modeladmin');

/**
 * Item Model for a category.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @since		1.0
 */
class JUX_GalleryModelCategory extends JModelAdmin
{

    /**
     * @var    string  The prefix to use with controller messages.
     * @since  1.0
     */
    protected $text_prefix = 'COM_JUX_GALLERY_CATEGORY';

    /**
     * Constructor.
     *
     * @param   array  $config  An optional associative array of configuration settings.
     *
     * @since   1.0
     */
    public function __construct($config = array())
    {
	parent::__construct($config);
    }

    /**
     * Method to test whether a record can be deleted.
     *
     * @param	object	$record	A record object.
     *
     * @return	boolean	True if allowed to delete the record. Defaults to the permission set in the component.
     * @since	1.0
     */
    protected function canDelete($record)
    {
	$user = JFactory::getUser();

	if ($record->id)
	{
	    return $user->authorise('core.delete', 'com_jux_gallery');
	} else
	{
	    return parent::canDelete($record);
	}
    }

    /**
     * Method to test whether a record can have its state edited.
     *
     * @param	object	$record	A record object.
     *
     * @return	boolean	True if allowed to change the state of the record. Defaults to the permission set in the component.
     * @since	1.0
     */
    protected function canEditState($record)
    {
	$user = JFactory::getUser();

	if ($record->id)
	{
	    return $user->authorise('core.edit.state', 'com_jux_gallery');
	} else
	{
	    return parent::canEditState($record);
	}
    }

    /**
     * Returns a Table object, always creating it
     *
     * @param	type	$type	The table type to instantiate
     * @param	string	$prefix	A prefix for the table class name. Optional.
     * @param	array	$config	Configuration array for model. Optional.
     *
     * @return	JTable	A database object
     * @since	1.0
     */
    public function getTable($type = 'category', $prefix = 'jux_galleryTable', $config = array())
    {
	return JTable::getInstance($type, $prefix, $config);
    }

    /**
     * Method to get the row form.
     *
     * @param	array	$data		Data for the form.
     * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
     *
     * @return	mixed	A JForm object on success, false on failure
     * @since	1.0
     */
    public function getForm($data = array(), $loadData = true)
    {
	$form = $this->loadForm('com_jux_gallery.category', 'category', array('control' => 'jform', 'load_data' => $loadData));
	if (empty($form))
	{
	    return false;
	}
	return $form;
    }

    /**
     * Method to get the data that should be injected in the form.
     *
     * @return	mixed	The data for the form.
     * @since	1.0
     */
    protected function loadFormData()
    {
	// Check the session for previously entered form data.
	$data = JFactory::getApplication()->getUserState('com_jux_gallery.edit.category.data', array());

	if (empty($data))
	{
	    $data = $this->getItem();
	}

	return $data;
    }

    /**
     * Prepare and sanitise the table prior to saving.
     *
     * @param	JTable	$table
     *
     * @return	void
     * @since	1.0
     */
    protected function prepareTable($table)
    {
	$date = JFactory::getDate();
	$user = JFactory::getUser();

	$table->title = htmlspecialchars_decode($table->title, ENT_QUOTES);
	$table->alias = JApplication::stringURLSafe($table->alias);

	if (empty($table->id))
	{
	    // Set created info
	    if (empty($table->created_by))
	    {
		$table->created_by = $user->get('id');
	    }
	    $table->created = $date->toSql();

	    // Set ordering to the last item if not set
	    if (empty($table->ordering))
	    {
		$db = JFactory::getDbo();
		$db->setQuery('SELECT MAX(ordering) FROM #__jux_gallery_categories');
		$max = $db->loadResult();

		$table->ordering = $max + 1;
	    }

	    // Set default language
	    if (empty($table->language))
	    {
		$table->language = '*';
	    }
	} else
	{
	    // Set the values
	    if ($table->created == '0000-00-00 00:00:00')
	    {
		$date = JFactory::getDate();
		$table->created = $date->toSql();
	    }
	}
    }

    /**
     * Method to change the title & alias.
     *
     * @param   integer  $parent_id  The id of the parent.
     * @param   string   $alias      The alias.
     * @param   string   $title      The title.
     *
     * @return  array  Contains the modified title and alias.
     *
     * @since	1.7
     */
    public function generateNewTitle($id,$alias, $title)
    {
	// Alter the title & alias
	$table = $this->getTable();
	while ($table->load(array('alias' => $alias)))
	{
//	    $title = JString::increment($title);
	    $alias = JString::increment($alias, 'dash');
	}

	return array($title, $alias);
    }

    public function getUploadFiles($id)
    {
	// Create a new query object
	$db = $this->getDbo();
	$query = $db->getQuery(true);

	$delete_url = JUri::base() . 'index.php?option=com_jux_gallery&task=category.delete&id=';
	$edit_url = JUri::base() . 'index.php?option=com_jux_gallery&task=category.editItem&id=';

	//Select the required fields from the table.
	$query->select("a.*,a.id AS itemID, CONCAT('$delete_url',a.id) AS delete_url, CONCAT('$edit_url',a.id) AS edit_url, 'DELETE' AS delete_type , 'EDIT' AS edit_type");
	$query->from('#__jux_gallery_items AS a');

	// Filter by published state
	$query->where('a.published = 1');
	$query->where('a.cat_id = ' . $id);

	$query->order('a.ordering ASC');
	$db->setQuery($query);

	try
	{
	    $items = $db->loadObjectList();
	} catch (RuntimeException $e)
	{
	    return array();
	}
	return $items;
    }

    /**
     * Method to delete item.
     *
     * @param   array  &$pks  An array of item ids.
     *
     * @return  boolean  Returns true on success, false on failure.
     *
     * @since   3.0
     */
    public function delete(&$pks)
    {
	// Initialise variables.
	$db = JFactory::getDbo();
	$query = $db->getQuery(true);

	$query->select('a.large_image');
	$query->from('#__jux_gallery_items as a');
	$query->where('a.id = ' . $pks);

	$db->setQuery($query);
	$items = $db->loadObjectList();

	foreach ($items as $item)
	{
	    $this->deleteImages($item->large_image);
	}


	$query = $db->getQuery(true);
	$query->delete();
	$query->from('#__jux_gallery_items');
	$query->where('id = ' . $pks);
	$db->setQuery($query);
//	if ($db->setQuery($query))
//	{
//	    throw new Exception($this->_db->getErrorMsg());
//	}
//Check for a database error.
	if (!$this->_db->query())
	{
	    throw new Exception($this->_db->getErrorMsg());
	}

//Clear modules cache
	$this->cleanCache();

	return true;
    }

    /**
     * Method to edit item.
     *
     * @param   array  &$pks  An array of item ids.
     *
     * @return  boolean  Returns true on success, false on failure.
     *
     * @since   3.0
     */
    public function editItem($itemId, $itemTitle, $itemUrl, $itemDesc)
    {
//	if (strpos($itemUrl, 'http') !== FALSE)
//	{
//	    $itemUrl = str_replace('http', '', $itemUrl);
//	
	$data = array(
	    'title' => $itemTitle,
	    'url' => $itemUrl,
	    'description' => $itemDesc
	);
	$itemTable = $this->getTable('item');
	$error = array();
	try
	{
	    $itemTable->load($itemId);
	    if (!$itemTable->bind($data))
	    {
		$error[] = $itemTable->getError();
	    }
	    // Store the data.
	    if (!$itemTable->store())
	    {
		
	    }
	} catch (Exception $e)
	{
	    $error[] = $e->getMessage();
	    return $error;
	}
	return $itemTable->load($itemId);
    }

    /**
     * Method to delete all item.
     *
     * @param   array  &$pks  An array of item ids.
     *
     * @return  boolean  Returns true on success, false on failure.
     *
     * @since   3.0
     */
    public function deleteAll($itemId)
    {
	foreach ($itemId as $item)
	{
	    $item = str_replace('item_', '', $item);
	    $this->delete($item);
	}
	$this->cleanCache();

	return true;
    }

    /**
     * Method to Order all item.
     *
     * @param   array  &$pks  An array of item ids.
     *
     * @return  boolean  Returns true on success, false on failure.
     *
     * @since   3.0
     */
    public function saveOrder($items = null, $order = null)
    {
	$itemTable = $this->getTable('item');
	$error = array();

	foreach ($items as $item)
	{
	    $data = array(
		'ordering' => $item['order']
	    );
	    try
	    {
		$item['id'] = str_replace('item_', '', $item['id']);
		$itemTable->load($item['id']);
		if (!$itemTable->bind($data))
		{
		    $error[] = $itemTable->getError();
		}
		// Store the data.
		if (!$itemTable->store())
		{
		    
		}
	    } catch (Exception $e)
	    {
		$error[] = $e->getMessage();
		return $error;
	    }
	}
	return true;
    }

    public function saveOrderAlbum($albums)
    {
	$albumTable = $this->getTable('category');
	$error = array();
	$order = 1;
	foreach ($albums as $album)
	{
	    $data = array(
		'ordering' => $order
	    );
	    try
	    {
		$albumTable->load($album);
		if (!$albumTable->bind($data))
		{
		    $error[] = $albumTable->getError();
		}
		// Store the data.
		if (!$albumTable->store())
		{
		    $error[] = $albumTable->getError();
		}
	    } catch (Exception $e)
	    {
		$error[] = $e->getMessage();
		return $error;
	    }
	    $order++;
	}
	return true;
    }

    /**
     * Function to Save Order Album.
     * Call by AJAX.
     */
    public function deleteAlbum($album, $current)
    {
	$albumTable = $this->getTable('category');
	$itemTable = $this->getTable('item');
	$error = array();
	$id = str_replace('arrayorder_', '', $album);

	$currentUrl = explode('&id=', $current);
	if (sizeof($currentUrl) > 1)
	{
	    $currentID = $currentUrl[1];
	} else
	{
	    $currentID = 0;
	}
//	$currentid = JRequest::getInt('id');
	if ($id == $currentID)
	{
	    $url = JUri::current() . '?option=com_jux_gallery';
	}

	$db = JFactory::getDbo();
	$query = $db->getQuery(true);
	$query->select('a.id,a.large_image');
	$query->from('#__jux_gallery_items AS a');
	$query->where('a.cat_id = ' . $id);

	$db->setQuery($query);
	$items = $db->loadObjectList();
	foreach ($items as $item)
	{
	    try
	    {
		$itemTable->delete($item->id);
		$this->deleteImages($item->large_image);
	    } catch (Exception $e)
	    {
		$error[] = $e->getMessage();
		return $error;
	    }
	}

	try
	{
	    $albumTable->delete($id);
	    if (isset($url))
	    {
		return $url;
	    } else
	    {
		return TRUE;
	    }
//	    return TRUE;
	} catch (Exception $e)
	{
	    $error[] = $e->getMessage();
	    return $error;
	}
    }

    public function updateAlbum($albumID, $itemID)
    {
	$itemTable = $this->getTable('item');
	$albumID = str_replace('arrayorder_', '', $albumID);
	$itemID = str_replace('itemID_', '', $itemID);
	$error = array();

	$data = array(
	    'cat_id' => $albumID
	);

	try
	{
	    $itemTable->load($itemID);
	    if (!$itemTable->bind($data))
	    {
		$error[] = $itemTable->getError();
	    }
	    // Store the data.
	    if (!$itemTable->store())
	    {
		$error[] = $itemTable->getError();
	    } else
	    {
		return true;
	    }
	} catch (Exception $e)
	{
	    $error[] = $e->getMessage();
	    return $error;
	}
    }

    public function albumCreate($albumTitle)
    {
	$albumTable = $this->getTable('category');
	$title = $this->generateNewTitle(0,JFilterOutput::stringURLSafe($albumTitle), $albumTitle);
	$data = array(
	    'title' => $title[0],
	    'alias' => $title[1]
	);
	$error = array();
	try
	{
//	    $albumTable->load();
	    if (!$albumTable->bind($data))
	    {
		$error[] = $albumTable->getError();
	    }
	    $this->prepareTable($albumTable);
	    // Store the data.
	    if (!$albumTable->save($data))
	    {
		$error[] = $albumTable->getError();
	    } else
	    {
		$url = JUri::current() . '?option=com_jux_gallery&view=category&layout=edit&id=' . $albumTable->get('id');
		return $url;
	    }
	} catch (Exception $e)
	{
	    $error[] = $e->getMessage();
	    return $error;
	}
    }

    public function saveAlbum($albumID, $albumTitle, $albumAlias, $albumStatus){
	$albumTable = $this->getTable('category');
	$data = array(
	    'title' => $albumTitle,
	    'alias' => $albumAlias,
	    'published' => $albumStatus
	);
	$error = array();
	try
	{
	    $albumTable->load($albumID);
	    if (!$albumTable->bind($data))
	    {
		$error[] = $albumTable->getError();
	    }
	    // Store the data.
//	    $this->prepareTable($albumTable);
	    if (!$albumTable->save($data))
	    {
		
	    }  else
	    {
		return True;
	    }
	} catch (Exception $e)
	{
	    $error[] = $e->getMessage();
	    return $error;
	}
    }

    public function getCategories()
    {
	$db = JFactory::getDbo();

	$query = $db->getQuery(true);
	$query->select('a.*');
	$query->from('#__jux_gallery_categories as a');
	
	$query->select('COUNT(i.id) as count');
	$query->leftJoin('#__jux_gallery_items as i ON a.id = i.cat_id');
	$query->group('a.id');
	
	$query->order('a.published DESC');
	$query->order('a.ordering ASC');

	$db->setQuery($query);
	$categories = $db->loadObjectList();
	return $categories;
    }

    public function getPreferences()
    {
	$component = 'com_jux_gallery';
	$path = '';
	$component = urlencode($component);
	$path = urlencode($path);

	$uri = (string) JUri::getInstance();
	$return = urlencode(base64_encode($uri));

	// Add a button linking to config for component.
	$url = 'index.php?option=com_config&amp;view=component&amp;component=' . $component . '&amp;path=' . $path . '&amp;return=' . $return;
	return $url;
    }

    public function getFirstItems()
    {
	$db = JFactory::getDbo();

	$query = $db->getQuery(true);
	$query->select('a.*');
	$query->from('#__jux_gallery_items as a');
	// Join over the categories
	$query->select('c.ordering AS category_order');
	$query->leftJoin('#__jux_gallery_categories AS c ON c.id = a.cat_id');

	$query->order('category_order, a.id DESC');
	$db->setQuery($query);
	$firstItems = $db->loadObjectList();
//	echo $query;
//	var_dump($firstItems);die;
	return $firstItems;
    }
    
    public function getMenuTypes(){
	$db = JFactory::getDbo();
	
	$query = $db->getQuery(true);
	$query->select('a.*');
	$query->from('#__menu_types AS a');
	$query->order('a.id');
	
	$db->setQuery($query);
	$menuTypes = $db->loadObjectList();

	return $menuTypes;
    }
    
    public function getListMenu(){
	$db = JFactory::getDbo();
	
	$query = $db->getQuery(true);
	$query->select('a.*');
	$query->from('#__menu AS a');
	
	$query->leftJoin('#__extensions AS e ON a.component_id = e.extension_id');
	$query->where('e.element = "com_jux_gallery"');
	$query->where('a.menutype != "main"');
	
	$db->setQuery($query);
	$listMenu = $db->loadObjectList();
	
	return $listMenu;
    }
    
    public function getModule(){
	$db = JFactory::getDbo();
	
	$query = $db->getQuery(true);
	$query->select('a.*');
	$query->from('#__modules AS a');
	$query->where('a.module = "mod_jux_gallery"');
	$query->order('a.published DESC');
//	$query->where('a.published = 1');
	
	$db->setQuery($query);
	$getModule = $db->loadObjectList();
	
	return $getModule;	
    }

    protected function deleteImages($src){
	$image = JPATH_ROOT . '/' . $src;
	JFile::delete($image);
    }
}