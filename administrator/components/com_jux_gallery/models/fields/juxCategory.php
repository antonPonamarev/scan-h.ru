<?php
/**
 * @version		$Id$
 * @author		JoomlaUX
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.form.formfield');

/**
 * JoomlaUX category Field class.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_jux_gallery
 * @since       1.0
 */
class JFormFieldJUXcategory extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var    string
	 */
	protected $type = 'JUXcategory';
	
	/**
	 * Method to get the field input markup.
	 *
	 * @return  string   The field input markup.
	 *
	 * @since   1.0
	 */
	protected function getInput() {
		// Initialize variables.
		$html = array();
		$attr = '';

		// Initialize some field attributes.
		$attr .= $this->element['class'] ? ' class="' . (string) $this->element['class'] . '"' : '';
		$attr .= $this->element['size'] ? ' size="' . (int) $this->element['size'] . '"' : '1';

		// Initialize JavaScript field attributes.
		$attr .= $this->element['onchange'] ? ' onchange="' . (string) $this->element['onchange'] . '"' : '';

		$data = $this->getData();
		$first_option[] = JHtml::_('select.option', '0', JText::_('COM_JUX_GALLERY_OPTION_SELECT_CATEGORY'));
		$data = array_merge($first_option, $data);
		
		return JHTML::_('select.genericlist', $data, $this->name, $attr, 'value', 'text', $this->value, $this->id, true);
		
		return implode($html);
	}
	
	/**
	 * Method to get list of categories.
	 *
	 * @return	array
	 */
	protected function getData() {
		// Get a database object.
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);

		$query->select('`id` AS value, `title` AS text');
		$query->from('#__jux_gallery_categories');
		$query->where('published = 1');
		$query->order('ordering');

		$db->setQuery($query);
		
		try{
			$data = $db->loadObjectList();
		} catch (JDatabaseException $e) {
			$je = new JException($e->getMessage());
			$this->setError($je);
			return array();
		}

		return $data;
	}
}
