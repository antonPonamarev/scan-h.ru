<?php

/**
 * @version		1.0.0
 * @author		Joomseller
 * @package		Joomla.Site
 * @subpackage	mod_jse_slideshow
 * @copyright	Copyright (C) 2008 - 2013 by Joomseller. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, See LICENSE.txt
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.form.formfield');
jimport('joomla.filesystem.folder');

class JFormFieldJuxFolder extends JFormField {

    protected $type = 'jsefolder';

    public function getInput() {        
        $jseFolder = array();
		$jseFolder[0] = new stdClass();
        $jseFolder[0]->name = 'images';
        $jseFolder[0]->text = 'images';
        $jseFolder[0]->value = 'images/';
        
        $this->buildTree('images', 0, '', $jseFolder);

        // Initialize field attributes.
        $class = $this->element['class'] ? (string) $this->element['class'] : '';
        $attr = '';
        $attr .= $this->element['onchange'] ? ' onchange="' . (string) $this->element['onchange'] . '"' : '';
        $attr .= ' class="inputbox ' . $class . '"'; 
        if(substr($this->value,0,1) =='/'){
        	$this->value = substr($this->value,1);
        }
		if(substr($this->value, -1) != '/'){
			$this->value = $this->value . '/';
		}
		$document	= JFactory::getDocument();
//		$document->addScript(JUri::root().'modules/mod_jse_slideshow/assets/colorpicker/js/jscolor.js');
		$document->addScript(JUri::root().'administrator/components/com_jux_gallery/assets/elements/jsegallery/jquery.gridly.js');
		$document->addScript(JUri::root().'administrator/components/com_jux_gallery/assets/elements/jsegallery/upload/js/vendor/jquery.ui.widget.js');
		$document->addScript(JUri::root().'administrator/components/com_jux_gallery/assets/elements/jsegallery/upload/js/jquery.iframe-transport.js');
		$document->addScript(JUri::root().'administrator/components/com_jux_gallery/assets/elements/jsegallery/upload/js/jquery.fileupload.js');

		
		$document->addStyleSheet(JUri::root().'administrator/components/com_jux_gallery/assets/elements/jsegallery/jquery.gridly.css');
		$document->addStyleSheet(JUri::root().'administrator/components/com_jux_gallery/assets/elements/jsegallery/upload/css/style.css');
		$document->addStyleSheet(JUri::root().'administrator/components/com_jux_gallery/assets/elements/jsegallery/upload/css/jquery.fileupload-ui.css');
		$document->addStyleSheet(JUri::root().'administrator/components/com_jux_gallery/assets/elements/jsegallery/upload/css/bootstrap.min.css');
		$document->addStyleSheet(JUri::root().'administrator/components/com_jux_gallery/assets/elements/jsegallery/upload/css/bootstrap-responsive.min.css');
		
		
        return JHTML::_('select.genericlist', $jseFolder, $this->name, trim($attr), 'value', 'text', $this->value, $this->id);
    }

    public function buildTree($folder, $depth, $path, &$jseFolder) {
        if($path){
			$folder = $path . '/' . $folder;
		}
        $subs = JFolder::folders(JPATH_ROOT . '/' . $folder);
		if(!empty($subs)){
			foreach ($subs as $sub) {
				$obj = new stdClass();
				$obj->name = $sub;
				$obj->text = str_repeat('- - ', $depth + 1) . $sub;
				$obj->value = $folder . '/' . $sub . '/';
				$jseFolder[] = $obj;
				$this->buildTree($sub, $depth + 1, $folder, $jseFolder);
			}
		}
    }

}