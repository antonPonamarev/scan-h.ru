<?php

/**
 * @version	1.0.0
 * @author	Joomlaux
 * @package	Joomla.Admin
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2008 - 2013 by Joomseller. All rights reserved.
 * @license	http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL, See LICENSE.txt
 */
defined('_JEXEC') or die('Restricted access');

jimport('joomla.form.formfield');

class JFormFieldJsegallery extends JFormField
{

    protected $type = 'Jsegallery';

    protected function getInput()
    {
	Jhtml::_('behavior.modal');
	Jhtml::_('stylesheet', JURI::root() . 'administrator/components/com_jux_gallery/assets/elements/jsegallery/style.css');

	$jseGalleryId = $this->id; //"jform_params_gallery"
	$params = $this->form;
	
	if (!isset($params))
	{
	    $params = new stdClass();
	    $folder = '';
	    $ordering = array();
	}
	if ($params->getValue('folder') == '')
	{ //"images/resized/images/sampledata/parks/animals/"
	    $folder = '';
	} else
	{
	    if (substr($params->getValue('folder'), 0, 1) == '/')
	    {
		$folder = substr($params->getValue('folder'), 1);
	    }
	    if (substr(trim($params->getValue('folder')), -1) != '/')
	    {
		$folder = trim($params->getValue('folder')) . '/';
	    }
	}
	if ($params->getValue('ordering_items') == '')
	{
	    $ordering = array();
	}

	//Check data format && convert it to json data if it is older format		
	$updateFormatData = 0;
	if ($this->value && !$this->isJson($this->value))
	{
	    $this->value = $this->convertFormatData($this->value, $params->getValue('folder'));
	    if (isset($this->element["updatedata"]) && $this->element["updatedata"])
	    {
		$updateFormatData = 1;
	    }
	}
	$ordering = (array)$params->getValue('ordering_items');
	$ordering = array_flip($ordering);
	ksort($ordering);
	//Create element
	$button = '<input type="button" id="jseGetImages" value="' . JText::_("MOD_JSE_SLIDESHOW_FIELD_IMAGE_GET_IMAGES") . '" style="display: none;" /><br /><div id="listImages" class="gridly"></div><div id=\'img-element-data-form\' style=\'display: none;\'></div>';
	$button .= '<!-- The fileinput-button span is used to style the file input field as button -->
			<span class="btn btn-success fileinput-button">
				<i class="icon-plus icon-white"></i>
				<span>Select files...</span>
				<!-- The file input field used as target for the file upload widget -->
				<input id="fileupload" type="file" name="files[]" multiple>
			</span>
			<br>
			<br>
			<!-- The global progress bar -->
			<div id="progress" class="progress progress-success progress-striped" style="width: 100%;">
				<div class="bar"></div>
			</div>
			<!-- The container for the uploaded files -->
			<div id="files" class="files"></div>
			<script type="text/javascript">
				/*jslint unparam: true */
				/*global window, $ */
				jQuery(function () {
					
					jQuery("#fileupload").fileupload({						
						url: location.href + "&jserequest=images&path="+folder_path+"&command=uploadImages",
						dataType: "json",
						done: function (e, data) {
							for(var i = 0; i < name_image.length; i++){
								if(name_image[i][0] == folder_path){
									if(jQuery.inArray(data.result.file_name,name_image[i]) == -1)
									{
										var length = name_image[i].length;
										name_image[i][length] = data.result.file_name;
										gallerys[i][2][length-1] = "\"image\":\""+data.result.file_name+"\",\"title\":\"\",\"max_width\":\"\",\"data_xy\":\"\",\"data_z\":\"\",\"data_scale\":\"\",\"data_rotate_x\":\"\",\"data_rotate_y\":\"\",\"data_rotate_z\":\"\",\"background_color\":\"\",\"link\":\"\",\"description\":\"\"";
									}
								}
							}
							var data_images = data.result.images.images;							
							jseupdateImages(data_images, "#listImages");
							
							for(var i = 0; i < gallerys.length; i++){
								if(gallerys[i][0] == folder_path){
									jQuery("#' . $jseGalleryId . '").val(gallerys[i][1]);
								}
							}
							
							jQuery("#progress .bar").css(
								"width",
								0 + "%"
							);
						},
						progressall: function (e, data) {
							var progress = parseInt(data.loaded / data.total * 100, 10);
							jQuery("#progress .bar").css(
								"width",
								progress + "%"
							);
						}
					});
					jQuery("#fileupload").bind("fileuploadchange", function (e, data) { 
						jQuery("#fileupload").fileupload({
							url: location.href + "&jserequest=images&path="+folder_path+"&command=uploadImages"});
					});
				});
		</script>';
	$button .= '<textarea style="display: none;" rows="6" cols="60" name="' . $this->name . '" id="' . $jseGalleryId . '" >' . htmlspecialchars($this->value, ENT_COMPAT, 'UTF-8') . '</textarea>';




	$js = 'var name_image = new Array();
			name_image[0] = new Array();
			var gallerys = new Array();
			gallerys[0] = new Array();
			gallerys[0][0] = "' . $params->getValue('folder') . '";
			gallerys[0][1] = \'' . addslashes($this->value) . '\';
			gallerys[0][2] = gallerys[0][1].split("},{");
			gallerys[0][2][0] = gallerys[0][2][0].substring(2);
			gallerys[0][2][gallerys[0][2].length-1] = gallerys[0][2][gallerys[0][2].length-1].substring(0,gallerys[0][2][gallerys[0][2].length-1].length-2);
			name_image[0][0] = "' . $params->getValue('folder') . '";			
			';
	for ($i = 0; $i < count($ordering); $i++)
	{
	    $js .= 'name_image[0][' . ($i + 1) . '] = \'' . $ordering[$i + 1] . '\';';
	}

	$js .= '
		var autoUpdate = ' . $updateFormatData . ';
		var folder_path = jQuery("select#jform_params_folder option:selected").val();
			if(folder_path == ""){
				alert("' . JText::_("MOD_JSE_SLIDESHOW_FIELD_FOLDER_PATH_REQUIRED") . '");				
			}
			if(folder_path == undefined){
				folder_path =  "' . $params->getValue('folder') . '";			
			}
		var index_folder = 1;
		var index_gallery = 1;
		var index = 0;
		jQuery(document).ready(function(){			 
			jQuery("#jseGetImages").click(function(){
				jseListImages();
			});
			jseListImages();
			/*
			if(autoUpdate){
				Joomla.submitbutton("module.apply");
			}
			*/
		});
		

		function changetype(obj){
			
			jseListImages(jQuery(obj).val());
			
		}
		function jseListImages(type){
			folder_name = jQuery("div#jform_params_folder_chzn span:first").text();
			folder_path = jQuery("#jform_params_folder").val();					
			if(folder_path == ""){
				alert("' . JText::_("MOD_JSE_SLIDESHOW_FIELD_FOLDER_PATH_REQUIRED") . '");
				return;
			}
			if(folder_path == undefined){
				folder_path =  "' . $params->getValue('folder') . '";
			}
			query = "jserequest=images&path="+folder_path+"&command=loadImages";
			if(type) query+="&type="+type;	
			jQuery.ajax({
				url: location.href,
				data: query,
				type: "post",
				beforeSend: function(){
					jQuery("#listImages").html("<img src=\"' . JURI::root() . 'administrator/components/com_jux_gallery/assets/elements/jsegallery/loading.gif\" style=\"height: 30px; width: 30px;\" />");
				},
				success: function(responseJSON){
						var data = jQuery.parseJSON(responseJSON);
						if (!data.success) {
							jQuery("#' . $jseGalleryId . '").val("");
							jQuery("#listImages").html("<strong style=\'color: red\'>' . JText::_("MOD_JSE_SLIDESHOW_FIELD_IMAGE_FOLDER_EMPTY") . '</strong>");
							return;	
						}
						else {									
							jseupdateImages(data.images, "#listImages");
							for(var i = 0; i < gallerys.length; i++){
								if(gallerys[i][0] == folder_path){
									jQuery("#' . $jseGalleryId . '").val(gallerys[i][1]);
								}
							}
						}
				}					
			});
			
			return false;
		}	

		function jseupdateImages(images, boxID){
			var is_folder_new = 1;
			index = 0;
			for(; index < name_image.length; index++){
				if(name_image[index][0] == folder_path){
					is_folder_new = 0;
					break;
				}
			}
			var data = "";
			var jsonData = "[";
			
			if(is_folder_new){				
				name_image[index_folder] = new Array();
				name_image[index_folder][0] = folder_path;
				gallerys[index_gallery] = new Array();
				gallerys[index_gallery][0] = folder_path;
				gallerys[index_gallery][2] = new Array();
				if(images.length){
						for(var i=0; i<images.length; i++){
							name_image[index_folder][i+1] = images[i].image;
							data += "<div class=\'img-element brick\' style=\'background-image: url("+images[i].imageSrc+");\'>";
							data += "<div class=\'no\' >"+ (i + 1) +"</div>";
							data += "<div class=\'delete\' name=\"" + images[i].image + "\" id=\"" + i + "\">&times;</div>";
							data += 	"<div class=\'edit\' onclick=\'jseFormpopup(\"#img-element-data-form\", " + i + ", \"" + images[i].image + "\"); return false;\' >'
		. JText::_("MOD_JSE_SLIDESHOW_FIELD_IMAGE_EDIT")
		. '</div>";							
							gallerys[index_gallery][2][i] = "\"image\":\""+images[i].image+"\",\"title\":\"\",\"max_width\":\"\",\"data_xy\":\"\",\"data_z\":\"\",\"data_scale\":\"\",\"data_rotate_x\":\"\",\"data_rotate_y\":\"\",\"data_rotate_z\":\"\",\"background_color\":\"\",\"link\":\"\",\"description\":\"\"";
							data += "<div class=\"gallery\" style=\"display: none;\">"+gallerys[index_gallery][2][i]+"</div>"
							data += "<input type=\"hidden\" name=\"jform[params][ordering]["+ images[i].image +"]\" value=\""+ (i + 1) +"\"/>";
							data += "</div>";
							jsonData += "{\"image\":\""+images[i].image+"\",\"title\":\"\",\"max_width\":\"\",\"data_xy\":\"\",\"data_z\":\"\",\"data_scale\":\"\",\"data_rotate_x\":\"\",\"data_rotate_y\":\"\",\"data_rotate_z\":\"\",\"background_color\":\"\",\"link\":\"\",\"description\":\"\"}";
							if(i < (images.length - 1)){
								jsonData += ",";
							}
						}
					jsonData += "]";
				}
				jQuery(boxID).html(data);
				jQuery("#' . $jseGalleryId . '").val(jsonData);
				jQuery(".gridly").gridly({ base: 60, gutter: 10, columns: 3, });
				gallerys[index_gallery][1] = "[{"+gallerys[index_gallery][2].join("},{")+"}]";
				index_folder++;
				index_gallery++;
			}
			else {
				var jse_slideshow_num_images = 0;
				for(var j=1; j<name_image[index].length; j++){
					for(var i=0; i<images.length; i++){
						if(images[i].image == name_image[index][j]){
							data += "<div class=\'img-element brick\' style=\'background-image: url("+images[i].imageSrc+");\'>";
							data += "<div class=\'no\' >"+ (i + 1) +"</div>";
							data += "<div class=\'delete\' name=\"" + images[i].image + "\" id=\"" + i + "\">&times;</div>";
							data += 	"<div class=\'edit\' onclick=\'jseFormpopup(\"#img-element-data-form\", " + i + ", \"" + images[i].image + "\"); return false;\' >'
		. JText::_("MOD_JSE_SLIDESHOW_FIELD_IMAGE_EDIT")
		. '</div>";							
							data += "<div class=\"gallery\" style=\"display: none;\">"+gallerys[index][2][j-1]+"</div>";
							data += "<input type=\"hidden\" name=\"jform[params][ordering]["+ images[i].image +"]\" value=\""+ (i + 1) +"\"/>";
							data += "</div>";
							jsonData += "{\"image\":\""+images[i].image+"\",\"title\":\"\",\"max_width\":\"\",\"data_xy\":\"\",\"data_z\":\"\",\"data_scale\":\"\",\"data_rotate_x\":\"\",\"data_rotate_y\":\"\",\"data_rotate_z\":\"\",\"background_color\":\"\",\"link\":\"\",\"description\":\"\"}";
							if(i < (images.length - 1)){
								jsonData += ",";
							}
							images.splice(i,1);
						}
					}
				}
				if(images.length){
					for(var i=0; i<images.length; i++){
						data += "<div class=\'img-element brick\' style=\'background-image: url("+images[i].imageSrc+");\'>";
						data += "<div class=\'no\' >"+ (i + 1 + jse_slideshow_num_images) +"</div>";
						data += "<div class=\'delete\' name=\"" + images[i].image + "\" id=\"" + (i + jse_slideshow_num_images) + "\">&times;</div>";
						data += 	"<div class=\'edit\' onclick=\'jseFormpopup(\"#img-element-data-form\", " + (i + jse_slideshow_num_images) + ", \"" + images[i].image + "\"); return false;\' >'
		. JText::_("MOD_JSE_SLIDESHOW_FIELD_IMAGE_EDIT")
		. '</div>";
						data += "<div class=\"gallery\" style=\"display: none;\">"+gallerys[index][2][i + jse_slideshow_num_images]+"</div>";
						data += "<input type=\"hidden\" name=\"jform[params][ordering]["+ images[i].image +"]\" value=\""+ (i + 1 + jse_slideshow_num_images) +"\"/>";
						data += "</div>";
					}
				}

				jsonData += "]";
				jQuery(boxID).html(data);
				jQuery("#' . $jseGalleryId . '").val(jsonData);
				jQuery(".gridly").gridly({ base: 60, gutter: 10, columns: 3, });				
			}
			
			
		}		
		
		function jseFormpopup(el, key, imgname){
			var form = jsedataForm(key, imgname);
			jQuery(el).append(form);
			SqueezeBox.open($("img-element-data-form-"+key),{
                handler:"adopt",
                size:{
                    x:630,
                    y:500
                }
            });
			//update data for image form
			var data = jQuery("#' . $jseGalleryId . '").val();
			var jseimg = new Object();
			jseimg.title = "";
			jseimg.link = "";
			jseimg.description = "";
			jseimg.max_width = "";
			jseimg.data_xy="";
			jseimg.data_z="";
			jseimg.data_scale="";
			jseimg.data_rotate_x="";
			jseimg.data_rotate_y="";
			jseimg.data_rotate_z="";
			jseimg.background_color="";
			//query = "jserequest=images&command=validData&imgname="+imgname+"&data="+data;
			jQuery.ajax({
				url: location.href,
				data: {jserequest:"images", command:"validData", imgname:imgname, data:data},
				type: "post",				
				success: function(responseJSON){					
					var jseResponse = jQuery.parseJSON(responseJSON);
					jQuery("#img-element-data-form-"+key).find("#imgtitle").val(jseResponse.title);
					jQuery("#img-element-data-form-"+key).find("#imgmax_width").val(jseResponse.max_width);
					jQuery("#img-element-data-form-"+key).find("#imgdata_xy").val(jseResponse.data_xy);
					jQuery("#img-element-data-form-"+key).find("#imgdata_z").val(jseResponse.data_z);
					jQuery("#img-element-data-form-"+key).find("#imgdata_scale").val(jseResponse.data_scale);
					jQuery("#img-element-data-form-"+key).find("#imgdata_rotate_x").val(jseResponse.data_rotate_x);
					jQuery("#img-element-data-form-"+key).find("#imgdata_rotate_y").val(jseResponse.data_rotate_y);
					jQuery("#img-element-data-form-"+key).find("#imgdata_rotate_z").val(jseResponse.data_rotate_z);
					jQuery("#img-element-data-form-"+key).find("#imgbackground_color").val(jseResponse.background_color);
					jQuery("#img-element-data-form-"+key).find("#imglink").val(jseResponse.link);
					jQuery("#img-element-data-form-"+key).find("#imgdescription").val(jseResponse.description);
					
					// Load color picker
					jscolor.dir = "' . JURI::root() . 'modules/mod_jse_slideshow/assets/colorpicker/images/";
					jscolor.init();
				}
			});

		}
		
		function jseCloseImgForm(key){
			SqueezeBox.close($("img-element-data-form-"+key));
		}
		
		function jseUpdateImgData(key, imgname){
			
			var title = jQuery("#img-element-data-form-"+key).find("#imgtitle").val();
			var max_width = jQuery("#img-element-data-form-"+key).find("#imgmax_width").val();
			var data_xy = jQuery("#img-element-data-form-"+key).find("#imgdata_xy").val();
			var data_z = jQuery("#img-element-data-form-"+key).find("#imgdata_z").val();
			var data_scale = jQuery("#img-element-data-form-"+key).find("#imgdata_scale").val();
			var data_rotate_x = jQuery("#img-element-data-form-"+key).find("#imgdata_rotate_x").val();
			var data_rotate_y = jQuery("#img-element-data-form-"+key).find("#imgdata_rotate_y").val();
			var data_rotate_z = jQuery("#img-element-data-form-"+key).find("#imgdata_rotate_z").val();
			var background_color = jQuery("#img-element-data-form-"+key).find("#imgbackground_color").val();
			var link = jQuery("#img-element-data-form-"+key).find("#imglink").val();
			var description = jQuery("#img-element-data-form-"+key).find("#imgdescription").val();	
			var data = jQuery("#' . $jseGalleryId . '").val();
			//process data
			query = "jserequest=images&command=updateData&imgname="+imgname+"&title="+title+"&link="+link+"&max_width="+max_width+"&data_xy="+data_xy+"&data_z="+data_z+"&data_scale="+data_scale+"&data_rotate_x="+data_rotate_x+"&data_rotate_y="+data_rotate_y+"&data_rotate_z="+data_rotate_z+"&background_color="+background_color+"&description="+description+"&data="+data;
			jQuery.ajax({
				url: location.href,
				data: query,
				type: "post",
				beforeSend: function(){
					
				},
				success: function(responseJSON){					
					jQuery("#' . $jseGalleryId . '").val(responseJSON);
					var i = 0;
					for(; i < gallerys.length; i++){
						if(gallerys[i][0] === folder_path){							
							gallerys[i][2][key] = "\"image\":\""+imgname+"\",\"title\":\""+title+"\",\"max_width\":\""+max_width+"\",\"data_xy\":\""+data_xy+"\",\"data_z\":\""+data_z+"\",\"data_scale\":\""+data_scale+"\",\"data_rotate_x\":\""+data_rotate_x+"\",\"data_rotate_y\":\""+data_rotate_y+"\",\"data_rotate_z\":\""+data_rotate_z+"\",\"background_color\":\""+background_color+"\",\"link\":\""+link+"\",\"description\":\""+description+"\"";
							gallerys[i][1] = "[{"+gallerys[i][2].join("},{")+"}]";
							break;
						}
					}
				}					
			});
			jseCloseImgForm(key);
		}
		
		function jsedataForm(key, imgname){
			
			//create form for image data
			var html = "";		
			html += "<div id=\'img-element-data-form-"+key+"\' class=\'img-element-data-form\'>";
				html += "<fieldset class=\'panelform\' >";					
					html += "<ul>";
						html += "<li >";
							html += "<label>' . JText::_("MOD_JSE_SLIDESHOW_FIELD_IMAGE_TITLE") . '</label>";
							html += "<input type=\'text\' name=\'imgtitle\' id=\'imgtitle\' value=\'\' size=\'50\' />";
						html += "</li>";						
						html += "<li>";
							html += "<label>' . JText::_("MOD_JSE_SLIDESHOW_FIELD_IMAGE_DESCRIPTION") . '</label>";
							html += "<textarea rows=\'3\' cols=\'50\' name=\'imgdescription\' id=\'imgdescription\' ></textarea>";
						html += "</li>";						
						html += "<li >";
							html += "<label>' . JText::_("MOD_JSE_SLIDESHOW_FIELD_IMAGE_LINK") . '</label>";
							html += "<input type=\'text\' name=\'imglink\' id=\'imglink\' value=\'\' size=\'50\' />";
						html += "</li>";
						html += "<li>";
							html += "<label>' . JText::_("MOD_JSE_SLIDESHOW_FIELD_IMAGE_BACKGROUND_COLOR") . '</label>";
							html += "<input type=\'text\'  class=\'color\' name=\'imgbackground_color\' id=\'imgbackground_color\' size=\'50\' />";
						html += "</li>";
						html += "<li>";
							html += "<label>' . JText::_("MOD_JSE_SLIDESHOW_FIELD_IMAGE_DATA_XY") . '</label>";
								html += "<select name=\'imgdata_xy\' id=\'imgdata_xy\'  >";									
									html += "<option value=\'center\'>Center</option>";
									html += "<option value=\'top\'>Top</option>";
									html += "<option value=\'top-right\'>Top - Right</option>";
									html += "<option value=\'right\'>Right</option>";
									html += "<option value=\'bottom-right\'>Bottom - Right</option>";
									html += "<option value=\'bottom\'>Bottom</option>";
									html += "<option value=\'bottom-left\'>Bottom - Left</option>";
									html += "<option value=\'left\'>Left</option>";
									html += "<option value=\'top-left\'>Top - Left</option>";
									html += "<option value=\'random\'  >Random</option>";
								html += "</select>";
						html += "</li>";
						html += "<li>";
							html += "<label>' . JText::_("MOD_JSE_SLIDESHOW_FIELD_IMAGE_DATA_Z") . '</label>";
								html += "<select name=\'imgdata_z\' id=\'imgdata_z\' >";
									html += "<option value=\'on\'>On</option>";
									html += "<option value=\'front\'  >Front</option>";									
									html += "<option value=\'back\'>Back</option>";
									html += "<option value=\'random\'  >Random</option>";
								html += "</select>";
						html += "</li>";
						html += "<li >";
							html += "<label>' . JText::_("MOD_JSE_SLIDESHOW_FIELD_IMAGE_MAX_WIDTH") . '</label>";
							html += "<input type=\'text\' name=\'imgmax_width\' id=\'imgmax_width\' size=\'10\' />";
							html += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . JText::_("MOD_JSE_SLIDESHOW_FIELD_IMAGE_MAX_WIDTH_DESC") . '";
						html += "</li>";
						html += "<li>";
							html += "<label>' . JText::_("MOD_JSE_SLIDESHOW_FIELD_IMAGE_DATA_SCALE") . '</label>";
							html += "<input type=\'text\' name=\'imgdata_scale\' id=\'imgdata_scale\'  size=\'10\' />";
							html += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . JText::_("MOD_JSE_SLIDESHOW_FIELD_IMAGE_DATA_SCALE_DESC") . '";
						html += "</li>";
						html += "<li>";
							html += "<label>' . JText::_("MOD_JSE_SLIDESHOW_FIELD_IMAGE_DATA_ROTATE_X") . '</label>";
							html += "<input type=\'text\' name=\'imgdata_rotate_x\' id=\'imgdata_rotate_x\'   size=\'10\' />";
							html += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . JText::_("MOD_JSE_SLIDESHOW_FIELD_IMAGE_DATA_ROTATE_X_DESC") . '";
						html += "</li>";
						html += "<li>";
							html += "<label>' . JText::_("MOD_JSE_SLIDESHOW_FIELD_IMAGE_DATA_ROTATE_Y") . '</label>";
							html += "<input type=\'text\' name=\'imgdata_rotate_y\' id=\'imgdata_rotate_y\'  size=\'10\' />";
							html += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . JText::_("MOD_JSE_SLIDESHOW_FIELD_IMAGE_DATA_ROTATE_Y_DESC") . '";
						html += "</li>";
						html += "<li>";
							html += "<label>' . JText::_("MOD_JSE_SLIDESHOW_FIELD_IMAGE_DATA_ROTATE_Z") . '</label>";
							html += "<input type=\'text\' name=\'imgdata_rotate_z\' id=\'imgdata_rotate_z\'  size=\'10\' />";
							html += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . JText::_("MOD_JSE_SLIDESHOW_FIELD_IMAGE_DATA_ROTATE_Z_DESC") . '";
						html += "</li>";											
					html += "</ul>";
					html += "<div class=\'btn-image-data-popup\' style=\'width: 100%; display: block; float: left; margin-top: 10px;\'>";
					html += "<input onclick=\'jseUpdateImgData("+key+", \""+imgname+"\"); return false;\' type=\'button\' value=\'' . JText::_("MOD_JSE_SLIDESHOW_FIELD_IMAGE_UPDATE") . '\' >";
					html += "<input onclick=\'jseCloseImgForm("+key+"); return false;\' type=\'button\' value=\'' . JText::_("MOD_JSE_SLIDESHOW_FIELD_IMAGE_CANCEL") . '\' >";
					html += "</div>";
				html += "</fieldset>";
			html += "</div>";

			return html;
		}		

		';


	$doc = JFactory::getDocument();
	$doc->addScriptDeclaration($js);

	return $button;
    }

    /*
     * Check data format for update data type from old version to json format
     * @string data string
     * @return boolean
     */

    function isJson($string)
    {
	return ((is_string($string) && (is_object(json_decode($string)) || is_array(json_decode($string))))) ? true : false;
    }

    function convertFormatData($string, $folder = null)
    {
	$data = array();
	$description = $this->parseDescNew($string, $folder);

	if (!empty($description))
	{
	    $i = 0;
	    foreach ($description as $key => $v)
	    {
		$data[$i] = new stdClass();
		$data[$i]->image = $key;
		$data[$i]->title = "";
		$data[$i]->link = isset($v["url"]) ? $v["url"] : '';
		$data[$i]->description = str_replace(array("\n", "\r"), "<br />", $v["caption"]);
		$data[$i]->data_xy = isset($v['data_xy']) ? $v['data_xy'] : '';
		$data[$i]->max_width = isset($v['max_width']) ? $v['max_width'] : '';
		$data[$i]->data_z = isset($v['data_z']) ? $v['data_z'] : '';
		$data[$i]->data_scale = isset($v['data_scale']) ? $v['data_scale'] : '';
		$data[$i]->data_rotate_x = isset($v['data_rotate_x']) ? $v['data_rotate_x'] : '';
		$data[$i]->data_rotate_y = isset($v['data_rotate_y']) ? $v['data_rotate_y'] : '';
		$data[$i]->data_rotate_z = isset($v['data_rotate_z']) ? $v['data_rotate_z'] : '';
		$data[$i]->background_color = isset($v['background_color']) ? $v['background_color'] : '';
		$i++;
	    }
	}
	if (!empty($data))
	{
	    return json_encode($data);
	}
	return '';
    }

    /**
     *
     * Parse description
     * @param string $description
     * @return array
     */
    function parseDescNew($description, $folder = null)
    {

	$regex = '#\[desc ([^\]]*)\]([^\[]*)\[/desc\]#m';
	$description = str_replace(array("{{", "}}"), array("<", ">"), $description);
	preg_match_all($regex, $description, $matches, PREG_SET_ORDER);
	$publish = 0;
	$descriptionArray = array();
	foreach ($matches as $match)
	{
	    $params = $this->parseParams($match[1]);

	    if (is_array($params))
	    {
		$img = isset($params['img']) ? trim($params['img']) : '';

		if (!$img)
		    continue;
		$publish = 1;
		$dot = strrpos($img, '.');
		$url = isset($params['url']) ? trim($params['url']) : '';
		$data_xy = isset($params['data_xy']) ? trim($params['data_xy']) : '';
		$max_width = isset($params['max_width']) ? trim($params['max_width']) : '';
		$data_z = isset($params['data_z']) ? trim($params['data_z']) : '';
		$data_scale = isset($params['data_scale']) ? trim($params['data_scale']) : '';
		$data_rotate_x = isset($params['data_rotate_x']) ? trim($params['data_rotate_x']) : '';
		$data_rotate_y = isset($params['data_rotate_y']) ? trim($params['data_rotate_y']) : '';
		$data_rotate_z = isset($params['data_rotate_z']) ? trim($params['data_rotate_z']) : '';
		$background_color = isset($params['background_color']) ? trim($params['background_color']) : '';
		$descriptionArray[$img] = array('url' => $url, 'caption' => str_replace("\n", "<br />", trim($match[2])), 'max_width' => $max_width, 'data_xy' => $data_xy, 'data_z' => $data_z, 'size_text_title' => $size_text_title, 'size_text_description' => $size_text_description, 'size_text_link' => $size_text_link, 'data_scale' => $data_scale, 'data_rotate_x' => $data_rotate_x, 'data_rotate_y' => $data_rotate_y, 'data_rotate_z' => $data_rotate_z, 'background_color' => $background_color);
	    }
	}

	return $descriptionArray;
    }

    /**
     * get parameters from configuration string.
     *
     * @param string $string;
     * @return array.
     */
    function parseParams($string)
    {
	$string = html_entity_decode($string, ENT_QUOTES);
	$regex = "/\s*([^=\s]+)\s*=\s*('([^']*)'|\"([^\"]*)\"|([^\s]*))/";
	$params = null;
	if (preg_match_all($regex, $string, $matches))
	{
	    for ($i = 0; $i < count($matches[1]); $i++)
	    {
		$key = $matches[1][$i];
		$value = $matches[3][$i] ? $matches[3][$i] : ($matches[4][$i] ? $matches[4][$i] : $matches[5][$i]);
		$params[$key] = $value;
	    }
	}
	return $params;
    }

}