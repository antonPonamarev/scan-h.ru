<?php

/**
 * @version	$Id$
 * @author	JoomlaUX
 * @package	Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license	http://www.gnu.org/licenses/gpl.html
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.html.html');
jimport('joomla.form.formfield');
jimport('joomla.form.fields.list');
JFormHelper::loadFieldClass('list');
jimport('joomla.form.helper');


class JFormFieldjuxalbum extends JFormFieldList
{

    /**
     * The field type.
     *
     * @var         string
     */
    protected $type = 'juxalbum';

    /**
     * Method to get a list of options for a list input.
     *
     * @return      array           An array of JHtml options.
     */
    protected function getOptions()
    {
	$db = JFactory::getDBO();
	$query = $db->getQuery(true);
	$query->select('id,title');
	$query->from('#__jux_gallery_categories as a');
	$query->where('a.published = 1');
	$query->order('a.title');
	$db->setQuery((string) $query);
	$messages = $db->loadObjectList();
	$options = array();
	if ($messages)
	{
	    foreach ($messages as $message)
	    {
		$options[] = JHtml::_('select.option', $message->id, $message->title);
	    }
	}
	$options = array_merge(parent::getOptions(), $options);
	return $options;
    }

}

?>
