<?php

/**
 * @version	$Id$
 * @author	JoomlaUX
 * @package	Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license	http://www.gnu.org/licenses/gpl.html
 */
defined('_JEXEC') or die('Restricted access');

jimport('joomla.form.formfield');
jimport('joomla.filesystem.folder');


/**
 * JoomlaUX Template Field class.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_jux_gallery
 * @since       1.0
 */
class JFormFieldJUXTemplate extends JFormField {

	/**
	 * The form field type.
	 *
	 * @var    string
	 */
	protected $type = 'JUXTemplate';

	/**
	 * Method to get the field input markup.
	 *
	 * @return  string   The field input markup.
	 */
	protected function getInput() {
		// Initialize variables.
		$html = array();
		$attr = '';

		// Initialize some field attributes.
		$attr .= $this->element['class'] ? ' class="' . (string) $this->element['class'] . '"' : '';
		$attr .= $this->element['size'] ? ' size="' . (int) $this->element['size'] . '"' : '1';

		// Initialize JavaScript field attributes.
		$attr .= $this->element['onchange'] ? ' onchange="' . (string) $this->element['onchange'] . '"' : '';

		$data = $this->getData();

		return JHTML::_('select.genericlist', $data, $this->name, $attr, 'value', 'text', $this->value, $this->id, true);
	}

	/**
	 * Method to get list of template.
	 *
	 * @return	array
	 */
	protected function getData() {
		$folders = JFolder::folders(JPATH_ROOT . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_jux_gallery' . DIRECTORY_SEPARATOR . 'templates');
		$data = array();
		foreach ($folders as $folder) {
			$data[] = JHTML::_('select.option', $folder, $folder);
		}

		return $data;
	}

}
