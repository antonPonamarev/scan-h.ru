<?php

/**
 * @version	$Id$
 * @author	JoomlaUX
 * @package	Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license	http://www.gnu.org/licenses/gpl.html
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.html.html');
jimport('joomla.form.formfield');
jimport('joomla.form.helper');

class JFormFieldJuxStyleAlbum extends JFormFieldRadio{
    /**
     * The field type.
     *
     * @var         string
     */
    
     protected $type = 'JuxStyleAlbum';
     /**
     * Method to get a list of options for a list input.
     *
     * @return      array           An array of JHtml options.
     */
     
     protected function getOptions()
     {
	 $options = array();
	 
	 $options[0] = JHtml::_('select.option', 0, JText::_('COM_JUX_GALLERY_STYLE_ALBUM_BOX'));
	 $options[1] = JHtml::_('select.option', 1, JText::_('COM_JUX_GALLERY_STYLE_ALBUM_BUTTON'));
	 
//	 $onclick_0 = !empty($options['onclick']) ? (string)$options['onclick'] : '';
	 
	 // Add default onclick
	 $onclick_0 ='juxShowAlbumBox(0)';
	 $onclick_1 ='juxShowAlbumBox(1)';
	 
	 $options[0]->onclick = $onclick_0;
	 $options[1]->onclick = $onclick_1;
	 
	 $options = array_merge(parent::getOptions(), $options);
	 
	return $options;
     }
}
?>
