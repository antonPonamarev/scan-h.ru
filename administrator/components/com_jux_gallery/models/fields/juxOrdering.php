<?php

/**
 * @version		$Id$
 * @author		JoomlaUX
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html
 */
defined('_JEXEC') or die('Restricted access');

jimport('joomla.form.formfield');

/**
 * JoomlaUX Ordering Field class.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_jux_gallery
 * @since       1.0
 */
class JFormFieldJUXOrdering extends JFormField {

	/**
	 * The form field type.
	 *
	 * @var    string
	 */
	protected $type = 'JUXOrdering';

	/**
	 * Method to get the field input markup.
	 *
	 * @return  string   The field input markup.
	 *
	 * @since   1.0
	 */
	protected function getInput() {
		// Initialize variables.
		$html = array();
		$attr = '';

		// Initialize some field attributes.
		$attr .= $this->element['class'] ? ' class="' . (string) $this->element['class'] . '"' : '';

		// To avoid user's confusion, readonly="true" should imply disabled="true".
		if ((string) $this->element['readonly'] == 'true' || (string) $this->element['disabled'] == 'true') {
			$attr .= ' disabled="disabled"';
		}

		$attr .= $this->element['size'] ? ' size="' . (int) $this->element['size'] . '"' : '';

		// Initialize JavaScript field attributes.
		$attr .= $this->element['onchange'] ? ' onchange="' . (string) $this->element['onchange'] . '"' : '';

		$tableName = $this->element['table'] ? $this->element['table'] : JRequest::getVar('view');
		$query = 'SELECT ordering AS value, title AS text'
				. ' FROM #__jux_gallery_' . $this->element['table']
				. ' WHERE #__jux_gallery_'.$this->element['table'].'.published != -2 '
				. ' ORDER BY ordering'
		;

		if (isset($this->value)) {
			$neworder = 0;
		} else {
			$neworder = 1;
		}

		$html[] = JHtml::_('list.ordering', $this->name, $query, $attr, $this->value, $neworder);

		return implode($html);
	}

}
