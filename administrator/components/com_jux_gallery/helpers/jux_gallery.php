<?php

/**
 * @version		$Id$
 * @author		JoomlaUX
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html
 */
// No direct access
defined('_JEXEC') or die;

/**
 * jux_gallery component helper.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @since		1.0
 */
class JUX_GalleryHelper {

	/**
	 * Configure the Linkbar.
	 *
	 * @param	string	$vName	The name of the active view.
	 *
	 * @return	void
	 * @since	1.0
	 */
	public static function addSubmenu($vName) {
		if ((JVERSION >= '2.5.5') && (JVERSION < '3.0.0')) {
			JSubMenuHelper::addEntry(JText::_('COM_JUX_GALLERY_CATEGORIES'), 'index.php?option=com_jux_gallery&view=categories', $vName == 'categories');
			JSubMenuHelper::addEntry(JText::_('COM_JUX_GALLERY_ITEMS'), 'index.php?option=com_jux_gallery&view=items', $vName == 'items');
		} elseif(JVERSION >= '3.0.0') {
			JHtmlSidebar::addEntry(JText::_('COM_JUX_GALLERY_CATEGORIES'), 'index.php?option=com_jux_gallery&view=categories', $vName == 'categories');
			JHtmlSidebar::addEntry(JText::_('COM_JUX_GALLERY_ITEMS'), 'index.php?option=com_jux_gallery&view=items', $vName == 'items');
		}
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return	JObject
	 * @since	1.0
	 */
	public static function getActions() {
		$user = JFactory::getUser();
		$result = new JObject;
		$assetName = 'com_jux_gallery';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action) {
			$result->set($action, $user->authorise($action, $assetName));
		}
		return $result;
	}

	/**
	 * get component info
	 *
	 * @return type
	 */
	public static function getComponentInfo()
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__extensions');
		$query->where('element=\'com_jux_gallery\' AND type=\'component\'');
		$db->setQuery($query);
		$result = $db->loadObject();
		return $result;
	}
	
	/**
	 * Get module info
	 *
	 * @return type
	 */
	public static function getModuleInfo()
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__extensions');
		$query->where('element=\'mod_jux_gallery\' AND type=\'module\'');
		$db->setQuery($query);
		$result = $db->loadObject();
		return $result;
	}
}
