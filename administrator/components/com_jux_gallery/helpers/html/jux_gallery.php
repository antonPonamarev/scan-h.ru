<?php

/**
 * @version		$Id$
 * @author		JoomlaUX
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Utility class for creating HTML lists for jux_gallery component
 * 
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 */
class JHtmlJUX_Gallery {

	/**
	 * Display list of category.
	 */
	function category($name = 'cat_id', $active = 0, $javascript = '', $size = 1) {
		$db = &JFactory::getDbo();
		$query = 'SELECT id AS value, title AS text'
				. ' FROM #__jux_gallery_categories AS a'
				. ' WHERE a.published = 1'
				. ' ORDER BY ordering'
		;
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		return $rows;
//		$list[]		= JHtml::_('select.option', '', JText::_('COM_JUX_GALLERY_OPTION_SELECT_CATEGORY'));
//		$list			= array_merge($list, $rows);
//		return JHTML::_('select.genericlist', $list, $name, 'class="inputbox" size="' . $size . '" ' . $javascript, 'value', 'text', intval($active));
	}

	/**
	 * Display (Un) published icon
	 * @return string
	 */
	function icon($value, $text1 = 'COM_JUX_GALLERY_YES', $text2 = 'COM_JUX_GALLERY_NO', $imgY = 'tick.png', $imgX = 'publish_x.png') {
		$img = $value ? $imgY : $imgX;
		$alt = $value ? JText::_($text1) : JText::_($text2);
		$img = JHtml::_('image', 'admin/' . $img, $alt, NULL, true);
		return $img;
	}

//	public static function category($extension, $config = array('filter.published' => array(0, 1))) {
//		$extension = 'com_jux_gallery';
//		$hash = md5($extension . '.' . serialize($config));
//
////		if (!isset(self::$items[$hash])) {
//			$config = (array) $config;
//			$db = JFactory::getDbo();
//			$query = $db->getQuery(true);
//
//			$query->select('a.id, a.title');
//			$query->from('#__juxfolio_categories AS a');
//			$query->where('a.published = 1');
//
//			// Filter on the published state
//			if (isset($config['filter.published'])) {
//				if (is_numeric($config['filter.published'])) {
//					$query->where('a.published = ' . (int) $config['filter.published']);
//				} elseif (is_array($config['filter.published'])) {
//					JArrayHelper::toInteger($config['filter.published']);
//					$query->where('a.published IN (' . implode(',', $config['filter.published']) . ')');
//				}
//			}
//
////			$query->order('a.lft');
//
//			$db->setQuery($query);
//			$items = $db->loadObjectList();
//
//			// Assemble the list options.
////			self::$items[$hash] = array();
////
////			foreach ($items as &$item) {
//////				$repeat = ($item->level - 1 >= 0) ? $item->level - 1 : 0;
//////				$item->title = str_repeat('- ', $repeat) . $item->title;
////				self::$items[$hash][] = JHtml::_('select.option', $item->id, $item->title);
////			}
//			// Special "Add to root" option:
//			self::$items = JHtml::_('select.option', '1', JText::_('JLIB_HTML_ADD_TO_ROOT'));
////		}
//	}
}
