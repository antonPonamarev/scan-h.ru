<?php

/**
 * @version		$Id$
 * @author		JoomlaUX
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controllerform');

/**
 * jux_gallery Component - category Controller.
 * 
 * @package     Joomla.Administrator
 * @subpackage  com_jux_gallery
 * @since       1.0
 */
class JUX_GalleryControllerCategory extends JControllerForm
{

    /**
     * The prefix to use with controller messages.
     * 
     * @var    string  
     */
    protected $text_prefix = 'COM_JUX_GALLERY_CATEGORY';

    /**
     * The URL view item variable.
     *
     * @var    string
     */
    protected $view_item = 'category';

    /**
     * The URL view list variable.
     *
     * @var    string
     */
    protected $view_list = 'categories';

    /**
     * Constructor.
     *
     * @param   array  $config  An optional associative array of configuration settings.
     *
     * @since   1.0
     */
    public function __construct($config = array())
    {
	parent::__construct($config);
    }

    /**
     * Method to get a model object, loading it if required.
     *
     * @param   string  $name    The model name. Optional.
     * @param   string  $prefix  The class prefix. Optional.
     * @param   array   $config  Configuration array for model. Optional.
     *
     * @return  object  The model.
     *
     * @since   12.2
     */
    public function getModel($name = '', $prefix = '', $config = array('ignore_request' => true))
    {
	if (empty($name))
	{
	    $name = $this->context;
	}

	return parent::getModel($name, $prefix, $config);
    }

    /**
     * Method override to check if you can add a new record.
     *
     * @param   array  $data  An array of input data.
     *
     * @return  boolean
     *
     * @since   1.0
     */
    protected function allowAdd($data = array())
    {
	$user = JFactory::getUser();
	$allow = null;
	$allow = $user->authorise('core.create', 'com_jux_gallery');
	if ($allow === null)
	{
	    return parent::allowAdd($data);
	} else
	{
	    return $allow;
	}
    }

    /**
     * Method override to check if you can edit an existing record.
     *
     * @param   array   $data  An array of input data.
     * @param   string  $key   The name of the key for the primary key.
     *
     * @return  boolean
     *
     * @since   1.0
     */
    protected function allowEdit($data = array(), $key = 'id')
    {
	$user = JFactory::getUser();
	$allow = null;
	$allow = $user->authorise('core.edit', 'com_jux_gallery');
	if ($allow === null)
	{
	    return parent::allowEdit($data, $key);
	} else
	{
	    return $allow;
	}
    }

    /**
     * Method to save a record.
     *
     * @param   string  $key     The name of the primary key of the URL variable.
     * @param   string  $urlVar  The name of the URL variable if different from the primary key (sometimes required to avoid router collisions).
     *
     * @return  boolean  True if successful, false otherwise.
     *
     * @since   1.6
     */
    public function save($key = null, $urlVar = null)
    {
	// Check for request forgeries.
	// JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
	// Initialise variables.
	$app = JFactory::getApplication();
	$model = $this->getModel('category', '', array());
	$data = JRequest::getVar('jform', array(), 'post', 'array');
	$task = $this->getTask();
	$context = 'com_jux_gallery.edit.category';
	$recordId = JRequest::getInt('id');

	if (!$this->checkEditId($context, $recordId))
	{
	    // Somehow the person just went to the form and saved it - we don't allow that.
	    $this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_UNHELD_ID', $recordId));
	    $this->setMessage($this->getError(), 'error');
	    $this->setRedirect(JRoute::_('index.php?option=com_jux_gallery&view=categories' . $this->getRedirectToListAppend(), false));

	    return false;
	}

	// Populate the row id from the session.
	$data['id'] = $recordId;

	// The save2copy task needs to be handled slightly differently.
	if ($task == 'save2copy')
	{
	    // Check-in the original row.
	    if ($model->checkin($data['id']) === false)
	    {
		// Check-in failed, go back to the item and display a notice.
		$this->setMessage(JText::sprintf('JLIB_APPLICATION_ERROR_CHECKIN_FAILED', $model->getError()), 'warning');
		return false;
	    }

	    // Reset the ID and then treat the request as for Apply.
	    $data['id'] = 0;
	    $task = 'apply';
	    //set data for item-2

	    list($title, $alias) = $model->generateNewTitle($data['alias'], $data['title']);
	    $data['title'] = $title;
	    $data['alias'] = $alias;
	}

	// Validate the posted data.
	// This post is made up of two forms, one for the item and one for params.
	$form = $model->getForm($data);
	if (!$form)
	{
	    JError::raiseError(500, $model->getError());
	    return false;
	}

	$data = $model->validate($form, $data);

	// Check for validation errors.
	if ($data === false)
	{
	    // Get the validation messages.
	    $errors = $model->getErrors();
	    // Push up to three validation messages out to the user.
	    for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++)
	    {
		if ($errors[$i] instanceof Exception)
		{
		    $app->enqueueMessage($errors[$i]->getMessage(), 'warning');
		} else
		{
		    $app->enqueueMessage($errors[$i], 'warning');
		}
	    }

	    // Save the data in the session.
	    $app->setUserState('com_jux_gallery.edit.category.data', $data);

	    // Redirect back to the edit screen.
	    $this->setRedirect(JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_item . $this->getRedirectToItemAppend($recordId), false));

	    return false;
	}

	// Attempt to save the data.
	if (!$model->save($data))
	{
	    // Save the data in the session.
	    $app->setUserState('com_jux_gallery.edit.category.data', $data);

	    // Redirect back to the edit screen.
	    $this->setMessage(JText::sprintf('JLIB_APPLICATION_ERROR_SAVE_FAILED', $model->getError()), 'warning');
	    $this->setRedirect(JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_item . $this->getRedirectToItemAppend($recordId), false));

	    return false;
	}

	// Save succeeded, check-in the row.
	if ($model->checkin($data['id']) === false)
	{
	    // Check-in failed, go back to the row and display a notice.
	    $this->setMessage(JText::sprintf('JLIB_APPLICATION_ERROR_CHECKIN_FAILED', $model->getError()), 'warning');
	    $this->setRedirect(JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_item . $this->getRedirectToItemAppend($recordId), false));

	    return false;
	}

	$this->setMessage(JText::_('COM_JUX_GALLERY_CATEGOTY_SUCCESSFULLY_SAVED'));

	// Redirect the user and adjust session state based on the chosen task.
	switch ($task)
	{
	    case 'apply':
		// Set the row data in the session.
		$recordId = $model->getState($this->context . '.id');
		$this->holdEditId($context, $recordId);
		$app->setUserState('com_jux_gallery.edit.category.data', null);
		// Redirect back to the edit screen.
		$this->setRedirect(JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_item . $this->getRedirectToItemAppend($recordId), false));

		break;

	    case 'save2new':
		// Clear the row id and data in the session.
		$this->releaseEditId($context, $recordId);
		$app->setUserState('com_menus.edit.category.data', null);

		// Redirect back to the edit screen.

		$this->Redirect(JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_item . $this->getRedirectToItemAppend(), false));
		break;

	    default:
		// Clear the row id and data in the session.
		$this->releaseEditId($context, $recordId);
		$app->setUserState('com_menus.edit.category.data', null);

		// Redirect to the list screen.
		$this->setRedirect(JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_list . $this->getRedirectToListAppend(), false));
		break;
	}
    }

    /**
     * Function to get existed files.
     * Call by AJAX.
     */
    public function getFiles()
    {
	if (!$this->allowEdit())
	{
	    $this->generate_response(array());

	    jexit();
	}
	$id = JRequest::getInt('id');

	$model = $this->getModel();
	$files = $model->getUploadFiles($id);

	$this->generate_response(array('files' => $files));
	jexit();
    }

    /**
     * Function to handler upload files.
     * Call by AJAX.
     *
     */
    public function upload()
    {
	//Get Path of Gallery folder
	$params = JComponentHelper::getParams('com_jux_gallery');
	$path = $params->get('images_upload_path');
	// Access check.
	if (!$this->allowAdd())
	{
	    jexit();
	}
	// Get the uploaded file information
	$files = $this->input->files->get('files', null, 'array');
	$files_title = $this->input->post->get('files-title', null, 'array');
	$cat_id = $this->input->post->get('cat_id');
	if (is_array($files))
	{
	    foreach ($files as $index => $file)
	    {
		if (empty($files_title) || !isset($files_title[$file['name']]))
		{
		    $file['title'] = substr($file['name'], 0, strrpos($file['name'], '.'));
		} else
		{
		    $file['title'] = $files_title[$file['name']];
		}
		$file['name'] = str_replace(' ','_',$file['name']);
		$response = new stdClass();
		$response->title = $file['title'];
		$response->name = $file['name'];
		$response->size = $file['size'];
		// If there is uploaded file, process it
		if (is_array($file) && isset($file['name']) && !empty($file['name']))
		{
		    if ($this->_uploadFile($file))
		    {
			$data['id'] = 0;
			$data['title'] = $file['title'];
			$path = str_replace('{root}/', '', $path);
			$data['large_image'] = $path . '/' . $file['name'];
//			$data['size'] = $file['size'];
//			$data['ext'] = $file['ext'];
//			$data['temp_upload'] = 1;
//			$data['changelogs'] = '';
			$data['cat_id'] = $cat_id;
			// Attempt to save the data.
			$model = $this->getModel('Item');
			if ($model->save($data))
			{
			    $response->large_image = $data['large_image'];
			    $response->itemID = $model->getState($model->getName() . '.id', '');
			    $response->url = $model->getState($model->getName() . '.url', '');
			    $response->description = $model->getState($model->getName() . '.description', '');
			    $response->delete_url = JURI::base() . 'index.php?option=com_jux_gallery&task=category.delete&id=' . $model->getState($model->getName() . '.id', '');
			    $response->edit_url = JURI::base() . 'index.php?option=com_jux_gallery&task=category.editItem&id=' . $model->getState($model->getName() . '.id', '');
			} else
			{
			    if (count($errors = $model->get('Errors')))
			    {
				$response->error = implode("\n", $errors);
			    }
			}
		    } else
		    {
			if ($file['error'])
			{
			    $response->error = $file['error'];
			}
		    }
		}
		$responses[] = $response;
	    }
	}
	$this->generate_response(array('files' => $responses));
	jexit();
    }

    protected function _uploadFile(&$file)
    {
	// Make sure that file uploads are enabled in php
	if (!(bool) ini_get('file_uploads'))
	{
	    JError::raiseWarning('', JText::_('COM_JSE_REAL_ESTATE_MSG_UPLOAD_WARN'));
	    return false;
	}
	// Check if there was a problem uploading the file.
	if ($file['error'])
	{
	    return false;
	}
	if ($file['size'] < 1)
	{
	    $file['error'] = JText::_('COM_JSE_REAL_ESTATE_MSG_UPLOAD_WARNUPLOADERROR');
	    return false;
	}

	$params = JComponentHelper::getParams('com_jux_gallery');


	// Build the appropriate paths
	$base_path = str_replace('{root}', JPATH_ROOT, $params->get('images_upload_path'));
	// Change name of file extension
	$ext = explode(".", $file['name']);
	$ext[1] = 'jpg';
	$file['name'] = implode('.', $ext);
	
	// Change name of file if exits
	if (file_exists($base_path . '\\' . $file['name']))
	{
	    $temp = explode(".", $file['name']);
	    $temp[0] = JString::increment($temp[0], 'dash');
	    $file['name'] = implode('.', $temp);
	}

	$file_dest = $base_path . '\\' . $file['name'];
	$file_src = $file['tmp_name'];

	// Move uploaded file
	jimport('joomla.filesystem.file');
	$uploaded = JFile::upload($file_src, $file_dest);

	// Unpack the downloaded package file
	if ($uploaded)
	{
//	    $this->convertImage($file_dest, $base_path, 100);
	    return true;
	}

	$file['error'] = JText::_('COM_JSE_REAL_ESTATE_MSG_UPLOAD_WARNUPLOADERROR');
	return false;
    }

    /**
     * Works out an installation package from a HTTP upload
     *
     * @return package definition or false on failure
     */
    protected function convertImage($originalImage, $outputImage, $quality)
    {
	// jpg, png, gif or bmp?
	$exploded = explode('.', $originalImage);
	$ext = $exploded[count($exploded) - 1];

	if (preg_match('/jpg|jpeg/i', $ext))
	    $imageTmp = imagecreatefromjpeg($originalImage);
	else if (preg_match('/png/i', $ext))
	    $imageTmp = imagecreatefrompng($originalImage);
	else if (preg_match('/gif/i', $ext))
	    $imageTmp = imagecreatefromgif($originalImage);
	else if (preg_match('/bmp/i', $ext))
	    $imageTmp = imagecreatefromwbmp($originalImage);
	else
	    return false;

	// quality is a value from 0 (worst) to 100 (best)
	imagejpeg($imageTmp, $outputImage, $quality);
	imagedestroy($imageTmp);

	return true;
    }

    /**
     * Function to delete uploaded files.
     * Call by AJAX.
     */
    public function delete()
    {
	$id = JRequest::getInt('id');
	if (empty($id))
	{
	    jexit();
	}

	$model = $this->getModel();
	$files = $model->delete($id);
	$this->generate_response(array('files' => $files));
	jexit();
    }

    /**
     * Function to editItem uploaded files.
     * Call by AJAX.
     */
    public function editItem()
    {
	$itemId = $this->input->post->get("itemId");
	$itemTitle = $this->input->post->get("item_title",'','string');
	$itemUrl = $this->input->post->get("item_url",'','string');
	$itemDesc = $this->input->post->get("item_desc",'','string');
	if (empty($itemId))
	{
	    jexit();
	}

	$model = $this->getModel();
	$files = $model->editItem($itemId, $itemTitle, $itemUrl, $itemDesc);

	$this->generate_response(array('files' => $files));
	jexit();
    }

    
    public function deleteAll()
    {
	$itemID = $this->input->post->get('itemId', '', 'array');
	if (empty($itemID))
	{
	    jexit();
	}

	$model = $this->getModel();
	$files = $model->deleteAll($itemID);

	$this->generate_response(array('files' => $files));
	jexit();
    }

    /**
     * Function to Delete Album.
     * Call by AJAX.
     */
    public function deleteAlbum(){
	$album = $this->input->post->get('albumID');
	$current = $this->input->post->get('currentUrl','','string');
	if(empty($album)){
	    jexit();
	}
	$model = $this->getModel();
	$respon = $model->deleteAlbum($album, $current);

	$this->generate_response($respon);
	jexit();
    }

        /**
     * Function to Save Order Images.
     * Call by AJAX.
     */
    public function saveOrder()
    {
	$items = $this->input->post->get('items', '', 'array');
	if (empty($items))
	{
	    jexit();
	}
	$model = $this->getModel();
	$files = $model->saveOrder($items);

	$this->generate_response(array('files' => $files));
	jexit();
    }
    
    /**
     * Function to Save Order Album.
     * Call by AJAX.
     */
    public function saveOrderAlbum(){
	$album = $this->input->post->get('arrayorder','','array');
	if(empty($album)){
	    jexit();
	}
	$model = $this->getModel();
	
	$respon = $model->saveOrderAlbum($album);
	$this->generate_response($respon);
	jexit();
    }
    
    public function updateAlbum(){
	$albumID = $this->input->post->get('albumID');
	$itemID = $this->input->post->get('itemID');
	
	if(empty($albumID) || empty($itemID)){
	    jexit();
	}
	
	$model = $this->getModel();
	
	$respon = $model->updateAlbum($albumID, $itemID);
	$this->generate_response($respon);
	jexit();
    }
    
    public function albumCreate(){
	$albumTitle = $this->input->post->get('title','','string');
	
	if(empty($albumTitle)){
	    jexit();
	}
	
	$model = $this->getModel();
	
	$respon = $model->albumCreate($albumTitle);
	$this->generate_response($respon);
	jexit();
    }
    
    public function saveAlbum(){
	$albumID = $this->input->post->get('albumID');
	$albumTitle = $this->input->post->get('album_title','','string');
	$albumAlias = $this->input->post->get('album_alias','','string');
	$albumStatus = $this->input->post->get('album_status','','string');
	
	if(empty($albumID)){
	    jexit();
	}
	$model = $this->getModel();
	$reponse = $model->saveAlbum($albumID, $albumTitle, $albumAlias, $albumStatus);

	$this->generate_response($reponse);
	jexit();
    }

    protected function generate_response($content)
    {
	echo json_encode($content);
    }

}