<?php

/**
 * @version	$Id$
 * @author	JoomlaUX
 * @package	Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license	http://www.gnu.org/licenses/gpl.html
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controllerform');

/**
 * jux_gallery Component - item Controller.
 * 
 * @package     Joomla.Administrator
 * @subpackage  com_jux_gallery
 * @since       1.0
 */
class JUX_GalleryControllerItem extends JControllerForm
{

    /**
     * The prefix to use with controller messages.
     * 
     * @var    string  
     */
    protected $text_prefix = 'COM_JUX_GALLERY_ITEM';

    /**
     * The URL view item variable.
     *
     * @var    string
     */
    protected $view_item = 'item';

    /**
     * The URL view list variable.
     *
     * @var    string
     */
    protected $view_list = 'items';

    /**
     * Constructor.
     *
     * @param   array  $config  An optional associative array of configuration settings.
     *
     * @since   1.0
     */
    public function __construct($config = array())
    {
	parent::__construct($config);
    }

    /**
     * Method override to check if you can add a new record.
     *
     * @param   array  $data  An array of input data.
     *
     * @return  boolean
     *
     * @since   1.0
     */
    protected function allowAdd($data = array())
    {
	$user = JFactory::getUser();
	$allow = null;
	$allow = $user->authorise('core.create', 'com_jux_gallery');
	if ($allow === null)
	{
	    return parent::allowAdd($data);
	} else
	{
	    return $allow;
	}
    }

    /**
     * Method override to check if you can edit an existing record.
     *
     * @param   array   $data  An array of input data.
     * @param   string  $key   The name of the key for the primary key.
     *
     * @return  boolean
     *
     * @since   1.0
     */
    protected function allowEdit($data = array(), $key = 'id')
    {
	$user = JFactory::getUser();
	$allow = null;
	$allow = $user->authorise('core.edit', 'com_jux_gallery');
	if ($allow === null)
	{
	    return parent::allowEdit($data, $key);
	} else
	{
	    return $allow;
	}
    }

    /**
     * Method to save a record.
     *
     * @param   string  $key     The name of the primary key of the URL variable.
     * @param   string  $urlVar  The name of the URL variable if different from the primary key (sometimes required to avoid router collisions).
     *
     * @return  boolean  True if successful, false otherwise.
     *
     * @since   1.6
     */
    public function save($key = null, $urlVar = null)
    {
	// Check for request forgeries.
//		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
	// Initialise variables.
	$app = JFactory::getApplication();
	$model = $this->getModel('item', '', array());
	$data = JRequest::getVar('jform', array(), 'post', 'array');
	$task = $this->getTask();
	$context = 'com_jux_gallery.edit.item';
	$recordId = JRequest::getInt('id');

	if (!$this->checkEditId($context, $recordId))
	{
	    // Somehow the person just went to the form and saved it - we don't allow that.
	    $this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_UNHELD_ID', $recordId));
	    $this->setMessage($this->getError(), 'error');
	    $this->setRedirect(JRoute::_('index.php?option=com_jux_gallery&view=items' . $this->getRedirectToListAppend(), false));

	    return false;
	}

	// Populate the row id from the session.
	$data['id'] = $recordId;

	// The save2copy task needs to be handled slightly differently.
	if ($task == 'save2copy')
	{
	    // Check-in the original row.
	    if ($model->checkin($data['id']) === false)
	    {
		// Check-in failed, go back to the item and display a notice.
		$this->setMessage(JText::sprintf('JLIB_APPLICATION_ERROR_CHECKIN_FAILED', $model->getError()), 'warning');
		return false;
	    }

	    // Reset the ID and then treat the request as for Apply.
	    $data['id'] = 0;
	    $task = 'apply';

	    list($title, $alias) = $model->generateNewTitle($data['alias'], $data['title']);
	    $data['title'] = $title;
	    $data['alias'] = $alias;
	}

	// Validate the posted data.
	// This post is made up of two forms, one for the item and one for params.
	$form = $model->getForm($data);
	if (!$form)
	{
	    JError::raiseError(500, $model->getError());
	    return false;
	}

	$data = $model->validate($form, $data);
	// Check for validation errors.
	if ($data === false)
	{
	    // Get the validation messages.
	    $errors = $model->getErrors();
	    // Push up to three validation messages out to the user.
	    for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++)
	    {
		if ($errors[$i] instanceof Exception)
		{
		    $app->enqueueMessage($errors[$i]->getMessage(), 'warning');
		} else
		{
		    $app->enqueueMessage($errors[$i], 'warning');
		}
	    }

	    // Save the data in the session.
	    $app->setUserState('com_jux_gallery.edit.item.data', $data);

	    // Redirect back to the edit screen.
	    $this->setRedirect(JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_item . $this->getRedirectToItemAppend($recordId), false));

	    return false;
	}

	// Attempt to save the data.
	if (!$row = $model->save($data))
	{
	    // Save the data in the session.
	    $app->setUserState('com_jux_gallery.edit.item.data', $data);

	    // Redirect back to the edit screen.
	    $this->setMessage(JText::sprintf('JLIB_APPLICATION_ERROR_SAVE_FAILED', $model->getError()), 'warning');
	    $this->setRedirect(JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_item . $this->getRedirectToItemAppend($recordId), false));

	    return false;
	}

	// Save succeeded, check-in the row.
	if ($model->checkin($data['id']) === false)
	{
	    // Check-in failed, go back to the row and display a notice.
	    $this->setMessage(JText::sprintf('JLIB_APPLICATION_ERROR_CHECKIN_FAILED', $model->getError()), 'warning');
	    $this->setRedirect(JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_item . $this->getRedirectToItemAppend($recordId), false));

	    return false;
	}
	//valide with large_image & video_link
	if (!empty($data['large_image']) || !empty($data['video_url']))
	{
	    $this->setMessage(JText::_('COM_JUX_GALLERY_ITEM_SECCESSFULLY_SAVED'));
	} else
	{
	    if ($data['type_media'])
	    {
		$this->setMessage(JText::sprintf('COM_JUX_GALLERY_VALIDATE_VIDEO', $model->getError()), 'warning');
	    } else
	    {
		$this->setMessage(JText::sprintf('COM_JUX_GALLERY_VALIDATE_IMAGE', $model->getError()), 'warning');
	    }
	}

	// Redirect the user and adjust session state based on the chosen task.
	switch ($task)
	{
	    case 'apply':
		// Set the row data in the session.
		$recordId = $model->getState($this->context . '.id');
		$this->holdEditId($context, $recordId);
		$app->setUserState('com_jux_gallery.edit.item.data', null);

		// Redirect back to the edit screen.
		$this->setRedirect(JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_item . $this->getRedirectToItemAppend($recordId), false));
		break;

	    case 'save2new':
		// Clear the row id and data in the session.
		$this->releaseEditId($context, $recordId);
		$app->setUserState('com_menus.edit.item.data', null);

		// Redirect back to the edit screen.
		$this->setRedirect(JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_item . $this->getRedirectToItemAppend(), false));
		break;

	    default:
		// Clear the row id and data in the session.
		$this->releaseEditId($context, $recordId);
		$app->setUserState('com_menus.edit.item.data', null);

		// Redirect to the list screen.
		$this->setRedirect(JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_list . $this->getRedirectToListAppend(), false));
		break;
	}
    }

}