<?php

/**
 * @version		$Id$
 * @author		JoomlaUX
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

/**
 * Component Controller
 *
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 */
class JUX_GalleryController extends JControllerLegacy
{

    /**
     * @var		string	The default view.
     * @since	1.0
     */
    protected $default_view = 'category';

    /**
     * Method to display a view.
     *
     * @param	boolean			If true, the view output will be cached
     * @param	array			An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
     *
     * @return	JController		This object to support chaining.
     * @since	1.0
     */
    public function display($cachable = false, $urlparams = false)
    {

	$document = JFactory::getDocument();
	$viewType = $document->getType();
	$viewName = $this->input->get('view', $this->default_view);
	$viewLayout = $this->input->get('layout', 'edit');

	$view = $this->getView($viewName, $viewType, '', array('base_path' => $this->basePath, 'layout' => $viewLayout));

	// Get/Create the model
	if ($model = $this->getModel($viewName))
	{
	    // Push the model into the view (as default)
	    $view->setModel($model, true);
	}

	$view->document = $document;

	$conf = JFactory::getConfig();

	// Display the view
	if ($cachable && $viewType != 'feed' && $conf->get('caching') >= 1)
	{
	    $option = $this->input->get('option');
	    $cache = JFactory::getCache($option, 'view');

	    if (is_array($urlparams))
	    {
		$app = JFactory::getApplication();

		if (!empty($app->registeredurlparams))
		{
		    $registeredurlparams = $app->registeredurlparams;
		} else
		{
		    $registeredurlparams = new stdClass;
		}

		foreach ($urlparams as $key => $value)
		{
		    // Add your safe url parameters with variable type as value {@see JFilterInput::clean()}.
		    $registeredurlparams->$key = $value;
		}

		$app->registeredurlparams = $registeredurlparams;
	    }

	    $cache->get($view, 'display');
	} else
	{
	    $view->display();
	}

	return $this;
    }

    /*
     * Function to check if someone trying to access edit form from direct url.
     * 
     * @param	string	$view	View
     * @param	string	$layout	Layout.
     * @param	string	$id		id.
     *
     * @return	boolean			True if this's illegal request.
     */

    protected function isIllegaRequest($view, $layout, $id)
    {
	switch ($view)
	{
	    case 'category':
	    case 'item':

		return $layout == 'edit' && !$this->checkEditId("com_jux_gallery.edit.$view", $id);
	    default:
		return false;
	}
    }

    /**
     * Launch adapter
     *
     * @return  boolean
     */
    function launchAdapter()
    {
	// Get user input
	$app = JFactory::getApplication();
	$type = $app->input->getCmd('type');
	// Store user state
	switch ($type)
	{
	    case 'module':
		// Get module info
		$moduleInfo = JUX_GalleryHelper::getModuleInfo();

		// Generate redirect link
		$link = 'index.php?option=com_modules&task=module.add&eid=' . $moduleInfo->extension_id;

		$this->setRedirect($link);
		break;

	    case 'menu':
		// Get component info
		$componentInfo = JUX_GalleryHelper::getComponentInfo();

		// Generate data for creating new menu item
		$data = array(
		    'type' => 'component',
		    'title' => '',
		    'alias' => '',
		    'note' => '',
		    'link' => 'index.php?option=com_jux_gallery&view=items',
		    'published' => '1',
		    'access' => '1',
		    'menutype' => $this->input->getCmd('menutype'),
		    'parent_id' => '1',
		    'browserNav' => '0',
		    'home' => '0',
		    'language' => '*',
		    'template_style_id' => '0',
		    'id' => '0',
		    'component_id' => $componentInfo->extension_id
		);

		// Fake user state for add/edit menu item page
		$app->setUserState('com_menus.edit.item.data', $data);
		$app->setUserState('com_menus.edit.item.type', 'component');
		$app->setUserState('com_menus.edit.item.link', 'index.php?option=com_jux_gallery&view=items');

		// Generate redirect link
		$link = 'index.php?option=com_menus&view=item&layout=edit';

		$this->setRedirect($link);
		break;
	}

	return true;
    }

}