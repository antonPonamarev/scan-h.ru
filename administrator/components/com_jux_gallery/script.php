<?php

/**
 * @version		$Id$
 * @author		JoomlaUX
 * @package		Joomla.Administrator
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

class com_jux_galleryInstallerScript {
	/*
	 * $parent is the class calling this method.
	 * $type is the type of change (install, update or discover_install, not uninstall).
	 * preflight runs before anything else and while the extracted files are in the uploaded temp folder.
	 * If preflight returns false, Joomla will abort the update and undo everything already done.
	 */

	function preflight($type, $parent) {
		// this component does not work with Joomla releases prior to 1.6
		// abort if the current Joomla release is older
	}

	/**
	 *
	 * @param type $parent is the class calling this method
	 */
	function install($parent) {
		jimport('joomla.filesystem.folder');

		$folder = JPATH_SITE . '/images/jux_gallery';

		$message = '';
		if (!JFolder::exists($folder)) {

			if (JFolder::create($folder, 0755)) {
				$message .= '<p><b><span style="color:#009933">Folder</span> ' . $folder
						. ' <span style="color:#009933">created!</span></b></p>';
			} else {
				$message .= '<p><b><span style="color:#CC0033">Folder</span> ' . $folder
						. ' <span style="color:#CC0033">creation failed!</span></b> Please create it manually.</p>';
			}
			//Folder exist
		} else {
			$message .= '<p><b><span style="color:#009933">Folder</span> ' . $folder
					. ' <span style="color:#009933">exists!</span></b></p>';
		}
		echo '<p>' . $message . '</p>';
		// Jump directly to the newly installed component configuration page

	}

	/**
	 *
	 * @param type $parent is the class calling this method
	 */
	function uninstall($parent) {
		
	}

	/**
	 *
	 * @param type $parent is the class calling this method
	 */
	function update($parent) {
		
	}

	/**
	 * Method to run after an install/update/uninstall method
	 * 
	 * @param type $type is the type of change (install, update or discover_install)
	 * @param type $parent is the class calling this method
	 */
	function postflight($type, $parent) {
		// $parent is the class calling this method
		// $type is the type of change (install, update or discover_install)
	}

}

?>