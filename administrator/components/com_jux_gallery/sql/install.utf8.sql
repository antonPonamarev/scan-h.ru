
-- --------------------------------------------------------

--
-- Table structure for table juxfolio_categories
--
CREATE TABLE IF NOT EXISTS `#__jux_gallery_categories` (
  `id` int(10) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `published` tinyint(3) NOT NULL default '1',
  `ordering` int(11) NOT NULL default '0',
  `description` text NOT NULL,
  `checked_out` int(10) NOT NULL default '0',
  `checked_out_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `access` int(10) NOT NULL default '0',
  `language` char(7) COLLATE utf8_general_ci NOT NULL,
  `created` datetime NOT NULL default '0000-00-00 00:00:00',
  `created_by` int(10) NOT NULL default '0',
  `image` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table juxfolio_items
--
CREATE TABLE IF NOT EXISTS `#__jux_gallery_items` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `cat_id` int(10) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `published` tinyint(3) NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `checked_out` int(10) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8_general_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) NOT NULL DEFAULT '0',
  `url` varchar(255) NOT NULL,
  `type_media` int(11) NOT NULL,
  `large_image` varchar(255) NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;