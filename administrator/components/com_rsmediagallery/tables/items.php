<?php
/**
* @package RSMediaGallery!
* @copyright (C) 2011-2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/licenses/gpl-3.0.html
*/

defined('_JEXEC') or die('Restricted access');

class RSMediaGalleryTableItems extends JTable
{
	public $id 				= null;
	
	public $original_filename 	= null;
	public $filename			= null;
	public $title 				= null;
	public $url 				= null;
	public $description 		= null;
	public $type 				= null;
	public $params 				= null;
	public $free_aspect 		= null;
	public $hits 				= null;
	public $created 			= null;
	public $modified 			= null;
	
	public $published 			= 1;
	public $ordering 			= null;
	
	public function __construct(& $db) {
		parent::__construct('#__rsmediagallery_items', 'id', $db);
	}
}