<?php
/**
* @package RSMediaGallery!
* @copyright (C) 2011-2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/licenses/gpl-3.0.html
*/

defined('_JEXEC') or die('Restricted access');

class RSMediaGalleryControllerItems extends RSMediaGalleryController
{
	public function __construct()
	{
		parent::__construct();
		
		$this->registerTask('publishitems',   'changestatus');
		$this->registerTask('unpublishitems', 'changestatus');
		
		$this->registerTask('tagitems',   'changetag');
		$this->registerTask('untagitems', 'changetag');
	}
	
	public function getItems()
	{
		JRequest::setVar('view',   'rsmediagallery');
		JRequest::setVar('layout', 'items');
		JRequest::setVar('format', 'raw');
		
		parent::display();
	}
	
	public function getItem()
	{
		JRequest::setVar('view',   'rsmediagallery');
		JRequest::setVar('layout', 'item');
		JRequest::setVar('format', 'raw');
		
		parent::display();
	}
	
	public function saveItem()
	{		
		JRequest::setVar('view',   'rsmediagallery');
		JRequest::setVar('layout', 'save');
		JRequest::setVar('format', 'raw');
		
		parent::display();
	}
	
	public function uploadItem()
	{		
		JRequest::setVar('view',   'rsmediagallery');
		JRequest::setVar('layout', 'upload');
		JRequest::setVar('format', 'raw');
		
		parent::display();
	}
	
	public function getSuggestions()
	{
		JRequest::setVar('view',   'rsmediagallery');
		JRequest::setVar('layout', 'autocomplete');
		JRequest::setVar('format', 'raw');
		
		parent::display();
	}
	
	public function getImagesFtp()
	{
		JRequest::setVar('view',   'rsmediagallery');
		JRequest::setVar('layout', 'navigate');
		JRequest::setVar('format', 'raw');
		
		parent::display();
	}
	
	public function addImagesFtp()
	{
		JRequest::setVar('view',   'rsmediagallery');
		JRequest::setVar('layout', 'addimages');
		JRequest::setVar('format', 'raw');
		
		parent::display();
	}
	
	public function delItems()
	{
		// pass the request to the model to delete the selected items
		$model = $this->getModel('rsmediagallery');
		$model->delItems();
		
		// now, we need to pass this to getItems() so that we can show the total images
		$this->getItems();
	}
	
	public function saveOrder()
	{
		// pass the request to the model to delete the selected items
		$model = $this->getModel('rsmediagallery');
		$model->saveOrder();
	}
	
	public function changeStatus()
	{
		// pass the request to the model to publish or unpublish the selected items
		$model = $this->getModel('rsmediagallery');
		$model->changeStatus(JRequest::getVar('task') == 'publishitems' ? 1 : 0);
	}
	
	public function changeTag()
	{
		// pass the request to the model to tag or untag the selected items
		$model = $this->getModel('rsmediagallery');
		$model->changeTag(JRequest::getVar('task') == 'tagitems' ? 1 : 0);
	}
	
	public function buildFilterThumb() 
	{
		$model = $this->getModel('rsmediagallery');
		$model->buildFilterThumb();
	}
	
	public function checkMemory()
	{
		JRequest::setVar('view',   'rsmediagallery');
		JRequest::setVar('layout', 'checkmemory');
		JRequest::setVar('format', 'raw');
		
		parent::display();
	
	}
}