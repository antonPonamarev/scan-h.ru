<?php
/**
* @package RSMediaGallery!
* @copyright (C) 2011-2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/licenses/gpl-3.0.html
*/

defined('_JEXEC') or die('Restricted access');

class RSMediaGalleryViewUpdates extends JViewLegacy
{
	protected $hash;
	protected $jversion;
	protected $version;
	protected $sidebar;
	
	public function display($tpl = null) {
		$this->addToolbar();
		
		$this->hash 	= $this->get('hash');
		$this->jversion = $this->get('joomlaVersion');
		$this->version  = $this->get('version');
		
		parent::display($tpl);
	}
	
	protected function addToolbar() {
		// set title
		JToolBarHelper::title(' ','rsmediagallery');
		
		JToolBarHelper::cancel('cancel');
	}
}