<?php
/**
* @package RSMediaGallery!
* @copyright (C) 2011-2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/licenses/gpl-3.0.html
*/

defined('_JEXEC') or die('Restricted access');

JHTML::_('behavior.keepalive');

?>
<div id="rsmg_wrapper">
    <div id="rsmg_container">
    	<div id="rsgallery_container_top">
        	<form action="index.php?option=com_rsmediagallery" id="rsmg_filter_form">
        	<div class="rsmg_filter_toolbar">
            	<ul>
                	<li id="rsmg_column"><a href="javascript: void(0);"><?php echo JText::_('COM_RSMEDIAGALLERY_TITLE'); ?></a>
                    	<ul>
                        	<li><a href="javascript: void(0);" rel="title" class="rsmg_tick"><?php echo JText::_('COM_RSMEDIAGALLERY_TITLE'); ?></a></li>
                        	<li><a href="javascript: void(0);" rel="description"><?php echo JText::_('COM_RSMEDIAGALLERY_DESC'); ?></a></li>
                        	<li><a href="javascript: void(0);" rel="tags"><?php echo JText::_('COM_RSMEDIAGALLERY_TAGS'); ?></a></li>
                        	<li><a href="javascript: void(0);" rel="published"><?php echo JText::_('COM_RSMEDIAGALLERY_PUBLISHED'); ?></a></li>
                        </ul>
                    </li>
                    <li id="rsmg_operator"><a href="javascript: void(0);"><?php echo JText::_('COM_RSMEDIAGALLERY_CONTAINS'); ?></a>
                    	<ul>
                        	<li><a href="javascript: void(0);" rel="is"><?php echo JText::_('COM_RSMEDIAGALLERY_IS'); ?></a></li>
                           <li><a href="javascript: void(0);" rel="is_not"><?php echo JText::_('COM_RSMEDIAGALLERY_IS_NOT'); ?></a></li>
                           <li><a href="javascript: void(0);" rel="contains" class="rsmg_tick"><?php echo JText::_('COM_RSMEDIAGALLERY_CONTAINS'); ?></a></li>
                           <li><a href="javascript: void(0);" rel="contains_not"><?php echo JText::_('COM_RSMEDIAGALLERY_DOES_NOT_CONTAIN'); ?></a></li>                            
                        </ul>
                    </li>
                    <li>
                        <input type="text" value="" id="rsmg_filter_text" />
                    </li>                    	                    	
                </ul>
            </div><!-- .rsmg_filter_toolbar -->
				
            <div class="rsmg_filter_toolbar">            
            	<ul>
                	<li id="rsmg_order"><a href="javascript: void(0);"><?php echo $this->translate($this->ordering); ?></a>
                    	<ul>
                        	<li><a href="javascript: void(0);" rel="ordering" <?php if ($this->ordering == 'i.ordering') { ?>class="rsmg_tick"<?php } ?>><?php echo JText::_('COM_RSMEDIAGALLERY_FREE_ORDERING'); ?></a></li>
                        	<li><a href="javascript: void(0);" rel="title" <?php if ($this->ordering == 'i.title') { ?>class="rsmg_tick"<?php } ?>><?php echo JText::_('COM_RSMEDIAGALLERY_TITLE'); ?></a></li>
                        	<li><a href="javascript: void(0);" rel="description" <?php if ($this->ordering == 'i.description') { ?>class="rsmg_tick"<?php } ?>><?php echo JText::_('COM_RSMEDIAGALLERY_DESC'); ?></a></li>
                        	<li><a href="javascript: void(0);" rel="tags" <?php if ($this->ordering == 't.tag') { ?>class="rsmg_tick"<?php } ?>><?php echo JText::_('COM_RSMEDIAGALLERY_TAGS'); ?></a></li>
                        	<li><a href="javascript: void(0);" rel="hits" <?php if ($this->ordering == 'i.hits') { ?>class="rsmg_tick"<?php } ?>><?php echo JText::_('COM_RSMEDIAGALLERY_HITS'); ?></a></li>
							<li><a href="javascript: void(0);" rel="created" <?php if ($this->ordering == 'i.created') { ?>class="rsmg_tick"<?php } ?>><?php echo JText::_('COM_RSMEDIAGALLERY_CREATED_DATE'); ?></a></li>
							<li><a href="javascript: void(0);" rel="modified" <?php if ($this->ordering == 'i.modified') { ?>class="rsmg_tick"<?php } ?>><?php echo JText::_('COM_RSMEDIAGALLERY_MODIFIED_DATE'); ?></a></li>
                        </ul>
                    </li>
                    <li id="rsmg_direction" class="rsmg_no_background"><a href="javascript: void(0);"><?php echo $this->translate($this->direction); ?></a>
                    	<ul>
                        	<li><a href="javascript: void(0);" rel="asc" <?php if ($this->direction == 'asc') { ?>class="rsmg_tick"<?php } ?>><?php echo JText::_('COM_RSMEDIAGALLERY_ASCENDING'); ?></a></li>
                        	<li><a href="javascript: void(0);" rel="desc" <?php if ($this->direction == 'desc') { ?>class="rsmg_tick"<?php } ?>><?php echo JText::_('COM_RSMEDIAGALLERY_DESCENDING'); ?></a></li>
                        </ul>
                    </li>
                    <li></li>                    	                    	
                </ul>
			</div><!-- .rsmg_filter_toolbar -->

			<div class="rsmg_filter_toolbar">            
            	<ul>
                    <li id="rsmg_limit" class="rsmg_no_background"><a href="javascript: void(0);"><?php echo JText::sprintf('COM_RSMEDIAGALLERY_ITEMS_PER_PAGE', $this->limit); ?></a>
                    	<ul>
                        	<li><a href="javascript: void(0);" rel="5" <?php if ($this->limit == 5) { ?>class="rsmg_tick"<?php } ?>><?php echo JText::sprintf('COM_RSMEDIAGALLERY_ITEMS_PER_PAGE', 5); ?></a></li>
                        	<li><a href="javascript: void(0);" rel="10" <?php if ($this->limit == 10) { ?>class="rsmg_tick"<?php } ?>><?php echo JText::sprintf('COM_RSMEDIAGALLERY_ITEMS_PER_PAGE', 10); ?></a></li>
                        	<li><a href="javascript: void(0);" rel="15" <?php if ($this->limit == 15) { ?>class="rsmg_tick"<?php } ?>><?php echo JText::sprintf('COM_RSMEDIAGALLERY_ITEMS_PER_PAGE', 15); ?></a></li>
                        	<li><a href="javascript: void(0);" rel="20" <?php if ($this->limit == 20) { ?>class="rsmg_tick"<?php } ?>><?php echo JText::sprintf('COM_RSMEDIAGALLERY_ITEMS_PER_PAGE', 20); ?></a></li>
                        	<li><a href="javascript: void(0);" rel="25" <?php if ($this->limit == 25) { ?>class="rsmg_tick"<?php } ?>><?php echo JText::sprintf('COM_RSMEDIAGALLERY_ITEMS_PER_PAGE', 25); ?></a></li>
							<li><a href="javascript: void(0);" rel="30" <?php if ($this->limit == 30) { ?>class="rsmg_tick"<?php } ?>><?php echo JText::sprintf('COM_RSMEDIAGALLERY_ITEMS_PER_PAGE', 30); ?></a></li>
                        	<li><a href="javascript: void(0);" rel="50" <?php if ($this->limit == 50) { ?>class="rsmg_tick"<?php } ?>><?php echo JText::sprintf('COM_RSMEDIAGALLERY_ITEMS_PER_PAGE', 50); ?></a></li>
                        	<li><a href="javascript: void(0);" rel="100" <?php if ($this->limit == 100) { ?>class="rsmg_tick"<?php } ?>><?php echo JText::sprintf('COM_RSMEDIAGALLERY_ITEMS_PER_PAGE', 100); ?></a></li>
                        </ul>
                    </li>
                    <li></li>                    	                    	
                </ul>
            </div><!-- .rsmg_filter_toolbar -->

			<button type="button" id="rsmg_filter" class="rsmg_button btn btn-success"><?php echo JText::_('COM_RSMEDIAGALLERY_FILTER'); ?></button>

            </form>
            <a href="javascript: void(0);" id="rsmg_select_all"><?php echo JText::_('COM_RSMEDIAGALLERY_SELECT_ALL'); ?></a>
            <div id="rsmg_info"></div>
        </div><!-- rsgallery_container_top -->
		<ul id="rsmg_filters">
		<?php if ($this->columns) { ?>
			<?php for ($i=0; $i<count($this->columns); $i++) { ?>
			<?php $is_published_column = $this->columns[$i] == 'published'; ?>
			<li>
				<span><?php echo $this->translate($this->columns[$i]); ?></span>
				<span><?php echo $this->translate($this->operators[$i]); ?></span>
				<strong><?php echo $is_published_column ? '' : $this->escape($this->values[$i]); ?></strong>
				<a class="rsmg_close" <?php if ($is_published_column) { ?>style="margin-left: 0; padding-left: 0; border-left: 0"<?php } ?> href="javascript: void(0);"></a>
				<input type="hidden" name="rsmg_columns[]" 	 value="<?php echo $this->escape($this->columns[$i]); ?>" 	/>
				<input type="hidden" name="rsmg_operators[]" value="<?php echo $this->escape($this->operators[$i]); ?>" />
				<input type="hidden" name="rsmg_values[]" 	 value="<?php echo $this->escape($this->values[$i]); ?>" 	/>
			</li>
			<?php } ?>
			<?php if (count($this->columns) > 2) { ?>
			<li id="rsmg_clear_filters"><?php echo JText::_('COM_RSMEDIAGALLERY_CLEAR_ALL_FILTERS'); ?></li>
			<?php } ?>
		<?php } ?></ul>
        <ul id="rsmg_items" class="rsmg_full_width"></ul>
		<div id="rsmg_item_detail" class="rsmg_hidden">
			<form action="index.php?option=com_rsmediagallery" id="rsmg_save_form">
				<div class="rsmg_box_left">
					<div class="rsmg_detail_selectors">
						<ul>
							<li><a href="javascript:void(0);" class="selected" onclick="RSMediagallery.Helpers.showAndHide('.rsmg_box_left_general_data',['#rsmg_effect_predefined_filters','#rsmg_effect_filters'], this)"><?php echo JText::_('COM_RSMEDIAGALLERY_GENERAL_DATA'); ?></a></li>
							<li><a href="javascript:void(0);" onclick="RSMediagallery.Helpers.showAndHide('#rsmg_effect_predefined_filters',['#rsmg_effect_filters','.rsmg_box_left_general_data'], this)"><?php echo JText::_('COM_RSMEDIAGALLERY_PREDEFINED_EFFECTS'); ?></a></li>
							<li><a href="javascript:void(0);" onclick="RSMediagallery.Helpers.showAndHide('#rsmg_effect_filters',['#rsmg_effect_predefined_filters','.rsmg_box_left_general_data'], this)"><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECTS'); ?></a></li>
						</ul>
					</div>
					<div class="rsmg_clear"></div>
					<div class="rsmg_box_left_general_data">
						<p><label for="rsmg_detail_title"><?php echo JText::_('COM_RSMEDIAGALLERY_TITLE'); ?></label><input type="text" value="" name="title" id="rsmg_detail_title" /></p>
						<p><label for="rsmg_detail_tags"><?php echo JText::_('COM_RSMEDIAGALLERY_TAGS'); ?></label><textarea id="rsmg_detail_tags" name="tags"></textarea><small><?php echo JText::_('COM_RSMEDIAGALLERY_TAGS_HINT'); ?></small></p>
						<p><label for="rsmg_detail_url"><?php echo JText::_('COM_RSMEDIAGALLERY_URL'); ?></label><input type="text" value="" name="url" id="rsmg_detail_url" /></p>
						<p><label for="rsmg_detail_description"><?php echo JText::_('COM_RSMEDIAGALLERY_DESC'); ?></label><textarea id="rsmg_detail_description" name="description"></textarea></p>
						
						<p><label><?php echo JText::_('COM_RSMEDIAGALLERY_CREATED_DATE'); ?></label><span id="rsmg_created_date"></span></p>
						<p><label><?php echo JText::_('COM_RSMEDIAGALLERY_MODIFIED_DATE'); ?></label><span id="rsmg_modified_date"></span></p>
						<p><label for="rsmg_detail_hits"><?php echo JText::_('COM_RSMEDIAGALLERY_HITS'); ?></label><input type="text" value="" name="hits" id="rsmg_detail_hits" /></p>
						<p><label for="rsmg_detail_published"><input type="checkbox" name="published" id="rsmg_detail_published" /><?php echo JText::_('COM_RSMEDIAGALLERY_PUBLISHED'); ?></label></p>
					</div>
					<div id="rsmg_effect_predefined_filters" class="rsmg_effect_filters">	
						<?php if ($this->imageFilterExists) { ?>
						<div class="rsmg_effect_predefined_container">
							<a href="javascript:void(0);" class="rsmg_effect_sepia" onclick="RSMediagallery.Filters.applyFilters('sepia')"/><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_SEPIA'); ?> <br/> <img src="components/com_rsmediagallery/assets/images/effects/sepia.jpg"/></a>
							<a href="javascript:void(0);" class="rsmg_effect_amaro" onclick="RSMediagallery.Filters.applyFilters('amaro')"/><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_AMARO'); ?> <br/> <img src="components/com_rsmediagallery/assets/images/effects/amaro.jpg"/></a>
							<a href="javascript:void(0);" class="rsmg_effect_nashville" onclick="RSMediagallery.Filters.applyFilters('nashville')"/><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_NASHVILLE'); ?> <br/> <img src="components/com_rsmediagallery/assets/images/effects/nashville.jpg"/></a>
							<a href="javascript:void(0);" class="rsmg_effect_seventyseven" onclick="RSMediagallery.Filters.applyFilters('seventyseven')"/><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_SEVENTYSEVEN'); ?> <br/> <img src="components/com_rsmediagallery/assets/images/effects/1977.jpg"/></a>
							<a href="javascript:void(0);" class="rsmg_effect_highcontrast" onclick="RSMediagallery.Filters.applyFilters('highcontrast')"/><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_HIGH_CONTRAST'); ?> <br/> <img src="components/com_rsmediagallery/assets/images/effects/high-contrast.jpg"/></a>
							<a href="javascript:void(0);" class="rsmg_effect_light" onclick="RSMediagallery.Filters.applyFilters('light')"/><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_LIGHT'); ?> <br/> <img src="components/com_rsmediagallery/assets/images/effects/light.jpg"/></a>
							<a href="javascript:void(0);" class="rsmg_effect_vintage" onclick="RSMediagallery.Filters.applyFilters('vintage')"/><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_VINTAGE'); ?> <br/> <img src="components/com_rsmediagallery/assets/images/effects/vintage.jpg"/></a>
							<a href="javascript:void(0);" class="rsmg_effect_hot" onclick="RSMediagallery.Filters.applyFilters('hot')"/><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_HOT'); ?> <br/> <img src="components/com_rsmediagallery/assets/images/effects/hot.jpg"/></a>
							<a href="javascript:void(0);" class="rsmg_effect_blueseasoning" onclick="RSMediagallery.Filters.applyFilters('blueseasoning')"/><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_BLUE_SEASONING'); ?> <br/> <img src="components/com_rsmediagallery/assets/images/effects/blue-seasoning.jpg"/></a>
							<a href="javascript:void(0);" class="rsmg_effect_stretch" onclick="RSMediagallery.Filters.applyFilters('stretch')"/><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_STRETCH'); ?> <br/> <img src="components/com_rsmediagallery/assets/images/effects/stretch.jpg"/></a>
							<a href="javascript:void(0);" class="rsmg_effect_greenhornet" onclick="RSMediagallery.Filters.applyFilters('greenhornet')"/><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_GREEN_HORNET'); ?> <br/> <img src="components/com_rsmediagallery/assets/images/effects/green-hornet.jpg"/></a>
							<a href="javascript:void(0);" class="rsmg_effect_grounded" onclick="RSMediagallery.Filters.applyFilters('grounded')"/><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_GROUNDED'); ?> <br/> <img src="components/com_rsmediagallery/assets/images/effects/grounded.jpg"/></a>
							<a href="javascript:void(0);" class="rsmg_effect_coldbreeze" onclick="RSMediagallery.Filters.applyFilters('coldbreeze')"/><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_COLD_BREEZE'); ?> <br/> <img src="components/com_rsmediagallery/assets/images/effects/cold-breeze.jpg"/></a>
							<a href="javascript:void(0);" class="rsmg_effect_diva" onclick="RSMediagallery.Filters.applyFilters('diva')"/><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_DIVA'); ?> <br/> <img src="components/com_rsmediagallery/assets/images/effects/diva.jpg"/></a>
							<a href="javascript:void(0);" class="rsmg_effect_lifeonmars" onclick="RSMediagallery.Filters.applyFilters('lifeonmars')"/><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_LIFE_ON_MARS'); ?> <br/> <img src="components/com_rsmediagallery/assets/images/effects/life-on-mars.jpg"/></a>
							<a href="javascript:void(0);" class="rsmg_effect_morning" onclick="RSMediagallery.Filters.applyFilters('morning')"/><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_MORING'); ?> <br/> <img src="components/com_rsmediagallery/assets/images/effects/morning.jpg"/></a>
							<a href="javascript:void(0);" class="rsmg_effect_oldphoto" onclick="RSMediagallery.Filters.applyFilters('oldphoto')"/><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_OLD_PHOTO'); ?> <br/> <img src="components/com_rsmediagallery/assets/images/effects/old-photo.jpg"/></a>
							<a href="javascript:void(0);" class="rsmg_effect_deepfocus" onclick="RSMediagallery.Filters.applyFilters('deepfocus')"/><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_DEEP_FOCUS'); ?> <br/> <img src="components/com_rsmediagallery/assets/images/effects/deep-focus.jpg"/></a>
							<a href="javascript:void(0);" class="rsmg_effect_wornvintage" onclick="RSMediagallery.Filters.applyFilters('wornvintage')"/><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_WORN_VINTAGE'); ?> <br/> <img src="components/com_rsmediagallery/assets/images/effects/worn-vintage.jpg"/></a>
							<a href="javascript:void(0);" class="rsmg_effect_mist" onclick="RSMediagallery.Filters.applyFilters('mist')"/><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_MIST'); ?> <br/> <img src="components/com_rsmediagallery/assets/images/effects/mist.jpg"/></a>
							<a href="javascript:void(0);" class="rsmg_effect_overblown" onclick="RSMediagallery.Filters.applyFilters('overblown')"/><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_OVERBLOWN'); ?> <br/> <img src="components/com_rsmediagallery/assets/images/effects/overblown.jpg"/></a>
						</div>
						<button class="rsmg_button_reset_effects btn btn-danger" type="button" onclick="RSMediagallery.Filters.resetPredefined()"><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_RESET_PREDEFINED_EFFECTS'); ?></button>
						<?php } else { ?>
							<p><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_IMAGE_FILTER_NOT_EXISTS'); ?></p>
						<?php } ?>
						<div class="rsmg_clear"></div>
						<p><label><?php echo JText::_('COM_RSMEDIAGALLERY_ORIGINAL_IMAGE_INFO'); ?></label>
						<span class="rsmg_float_left"><?php echo JText::_('COM_RSMEDIAGALLERY_RESOLUTION'); ?>: <span class="rsgm_image_res"></span></span> <span class="rsmg_float_right"><?php echo JText::_('COM_RSMEDIAGALLERY_SIZE'); ?>:<span class="rsmg_size_on_disk"></span></span></p>
						<div class="rsmg_clear"></div>
						<p style="margin-top:5px"><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_SIZE_NOTICE'); ?></p>
						<div class="rsmg_clear"></div>
					</div>
					<div id="rsmg_effect_filters" class="rsmg_effect_filters">
						<?php if ($this->imageFilterExists) { ?>
							<div class="rsmg_clear"></div>
							<p><label for="rsmg_effect_greyscale"><input type="checkbox" name="filters[]" id="rsmg_effect_greyscale" value="greyscale" onclick="RSMediagallery.Filters.applyFilters('none')"/> <?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_GREYSCALE'); ?></label></p>
							<div class="rsmg_clear"></div>
							<p><label for="rsmg_effect_negate"><input type="checkbox" name="filters[]" id="rsmg_effect_negate" value="negate" onclick="RSMediagallery.Filters.applyFilters('none')"/> <?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_NEGATE'); ?></label></p>
							<div class="rsmg_clear"></div>
							<p><label for="rsmg_effect_sketchy"><input type="checkbox" name="filters[]" id="rsmg_effect_sketchy" value="sketchy" onclick="RSMediagallery.Filters.applyFilters('none')"/> <?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_SKETCHY'); ?></label></p>
							
							<div class="rsmg_clear"></div>
							<p><label for="rsmg_effect_brightness"><input type="checkbox" name="filters[]" id="rsmg_effect_brightness" value="brightness" onclick="RSMediagallery.Helpers.showContainer(this,'.rsmg_effect_brightness_container'); RSMediagallery.Filters.applyFilters('none')"/> <?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_BRIGHTNESS'); ?></label></p>
							<div class="rsmg_effect_brightness_container">
								<input type="text" value="0" name="attributes[brightness][]" class="rsmg_filter_input rsmg_effect_brightness_value" onkeydown="return RSMediagallery.Helpers.isNumber(this, event)" onchange="RSMediagallery.Filters.changeSlider('brightness', this.value)" />
								<div class="rsmg_float_left rsmg_filter_slider">
									<div id="rsmg_brightness"></div>
								</div>
							</div>
							<div class="rsmg_clear"></div>
							<p><label for="rsmg_effect_contrast"><input type="checkbox" name="filters[]" id="rsmg_effect_contrast" value="contrast" onclick="RSMediagallery.Helpers.showContainer(this,'.rsmg_effect_contrast_container'); RSMediagallery.Filters.applyFilters('none')"/> <?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_CONTRAST'); ?></label></p>
							<div class="rsmg_effect_contrast_container">
								<input type="text" value="0" name="attributes[contrast][]" class="rsmg_filter_input rsmg_effect_contrast_value" onkeydown="return RSMediagallery.Helpers.isNumber(this, event)" onchange="RSMediagallery.Filters.changeSlider('contrast', this.value)" />
								<div class="rsmg_float_left rsmg_filter_slider">
									<div id="rsmg_contrast"></div>
								</div>
							</div>
							<div class="rsmg_clear"></div>
							<?php if($this->pixelate) { ?>
								<p><label for="rsmg_effect_pixelate"><input type="checkbox" name="filters[]" id="rsmg_effect_pixelate" value="pixelate" onclick="RSMediagallery.Helpers.showContainer(this,'.rsmg_effect_pixelate_container'); RSMediagallery.Filters.applyFilters('none')" /> <?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_PIXELATE'); ?></label></p>
								<div class="rsmg_effect_pixelate_container">
									<input type="text" value="1" name="attributes[pixelate][]" class="rsmg_filter_input rsmg_effect_pixelate_value" onkeydown="return RSMediagallery.Helpers.isNumber(this, event)" onchange="RSMediagallery.Filters.changeSlider('pixelate', this.value)"/>
									<div class="rsmg_float_left rsmg_filter_slider">
										<div id="rsmg_pixelate"></div>
									</div>
								</div>
								<div class="rsmg_clear"></div>
							<?php } ?>
							<button class="rsmg_button_reset_effects btn btn-danger" type="button" onclick="RSMediagallery.Filters.resetFilters(true)"><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_RESET_EFFECTS'); ?></button>
							<p><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_NOTICE'); ?></p>
							<p><label><?php echo JText::_('COM_RSMEDIAGALLERY_ORIGINAL_IMAGE_INFO'); ?></label>
						<?php } else { ?>
							<p><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_IMAGE_FILTER_NOT_EXISTS'); ?></p>
						<?php } ?>
						<span class="rsmg_float_left"><?php echo JText::_('COM_RSMEDIAGALLERY_RESOLUTION'); ?>: <span class="rsgm_image_res"></span></span> <span class="rsmg_float_right"><?php echo JText::_('COM_RSMEDIAGALLERY_SIZE'); ?>:<span class="rsmg_size_on_disk"></span></span></p>
						<div class="rsmg_clear"></div>
						<p style="margin-top:5px"><?php echo JText::_('COM_RSMEDIAGALLERY_EFFECT_SIZE_NOTICE'); ?></p>
						<div class="rsmg_clear"></div>
					</div>
				</div>
				<div id="rsmg_box_thumb">
					<p style="margin-bottom:10px"><label><?php echo JText::_('COM_RSMEDIAGALLERY_SELECT_THUMBNAIL'); ?></label></p>
					<div style="margin-bottom:5px">
						<div class="pull-right"><?php echo JText::_('COM_RSMEDIAGALLERY_SELECTION_RESOLUTION'); ?>:  <span id="rsmg_resolution_label"></span></div>
					</div>
					<div class="rsmg_clear"></div>
					<div>
						<label for="rsmg_detail_free_aspect" class="pull-left"><input type="checkbox" name="free_aspect_ratio" id="rsmg_detail_free_aspect" onchange="rsmg_change_aspect_ratio()" onclick="rsmg_change_aspect_ratio()" /><?php echo JText::_('COM_RSMEDIAGALLERY_FREE_ASPECT_RATIO'); ?></label>
						<div id="rsmg_non_fixed_ratio" class="pull-right"><span><?php echo JText::_('COM_RSMEDIAGALLERY_SELECTION_RATIO_LABEL');?></span><span id="rsmg_ratio_label"></span></div>
					</div>
					
					<p><img style="display: none;" src="" alt="" /></p>
				</div>
				<span class="rsmg_clear"></span>
				<p>
					<span id="rsmg_save_result"></span>
				</p>
				<p class="rsmg_buttons_container">
					<img style="display: none;" id="rsmg_detail_loader" src="components/com_rsmediagallery/assets/images/loader-small-darkgrey.gif" />
					<button type="button" class="rsmg_button_save btn btn-success"><?php echo JText::_('COM_RSMEDIAGALLERY_SAVE'); ?></button>
					<button type="button" class="rsmg_button_apply btn btn-info"><?php echo JText::_('COM_RSMEDIAGALLERY_APPLY'); ?></button>
					<button type="button" class="rsmg_button_cancel btn btn-danger"><?php echo JText::_('COM_RSMEDIAGALLERY_CANCEL'); ?></button>
				</p>
				<input type="hidden" id="rsmg_x1" value="" />
				<input type="hidden" id="rsmg_y1" value="" />
				<input type="hidden" id="rsmg_x2" value="" />
				<input type="hidden" id="rsmg_y2" value="" />
				<input type="hidden" id="rsmg_w" value="" />
				<input type="hidden" id="rsmg_h" value="" />
				<input type="hidden" id="rsmg_admin_w" value="" />
				<input type="hidden" id="rsmg_admin_h" value="" />
			</form>
			<div class="rsmg_left_arrow"></div>
		</div>
		<span class="rsmg_clear"></span>
        <a href="javascript: void(0);" id="rsmg_load_more"><?php echo JText::_('COM_RSMEDIAGALLERY_LOAD_MORE'); ?></a>
    </div><!-- rsgallery_container -->   
</div><!-- rsgallery_main -->
<div id="rsmg_version">
	<div class="pull-left">
		<p><img src="components/com_rsmediagallery/assets/images/rsmediagallery.gif" alt="" valign="middle" /><?php echo JText::sprintf('COM_RSMEDIAGALLERY_VERSION_TEXT', 'http://www.rsjoomla.com/joomla-extensions/joomla-gallery.html', $this->version, 'http://www.rsjoomla.com'); ?></p>
		<p><?php echo JText::sprintf('COM_RSMEDIAGALLERY_FREE_SOFTWARE_TEXT', 'http://www.gnu.org/licenses/gpl-3.0.html'); ?></p>
	</div>
	<div id="rsmg_update_code" class="pull-right">
		<p><?php echo JText::_('COM_RSMEDIAGALLERY_UPDATE_CODE'); ?>
			<?php if ($this->isCorrectCode) { ?>
			<span class="correct-code"><?php echo $this->escape($this->code); ?></span>
			<?php } elseif ($this->isIncorrectCode) { ?>
			<span class="incorrect-code"><?php echo $this->escape($this->code); ?></span>
			<?php } elseif ($this->isMissingCode) { ?>
			<span class="missing-code"><?php echo JText::_('COM_RSMEDIAGALLERY_PLEASE_ENTER_YOUR_CODE_IN_THE_CONFIGURATION'); ?></span>
			<?php } ?>
			
			<button onclick="document.location.href='index.php?option=com_rsmediagallery&view=updates'" type="button" class="button btn btn-primary" <?php echo !$this->isCorrectCode ? 'disabled="disabled"' : ''; ?>><?php echo JText::_('COM_RSMEDIAGALLERY_CHECK_FOR_UPDATES'); ?></button>
		</p>
		</div>
	</div>
</div>
<div class="rsmg_hidden">
    <div id="rsmg_dialog_confirm_delete">
		<h2><?php echo JText::_('COM_RSMEDIAGALLERY_WARNING'); ?></h2>
		<p><span class="ui-icon ui-icon-alert icon-trash"></span><?php echo JText::_('COM_RSMEDIAGALLERY_WARNING_PERMANENTLY_REMOVE'); ?></p>
	</div>
	<div id="rsmg_dialog_message">
		<h2><?php echo JText::_('COM_RSMEDIAGALLERY_INFORMATION'); ?></h2>
		<p><?php echo JText::_('COM_RSMEDIAGALLERY_PLEASE_SELECT_ITEMS'); ?></p>
		<div><img src="components/com_rsmediagallery/assets/images/tutorial-multiple-selection.jpg" alt="<?php echo JText::_('COM_RSMEDIAGALLERY_TUTORIAL_MULTIPLE_SELECTION_IMAGE'); ?>" /></div>
	</div>
	<?php if ($this->hasNoItems) { ?>
	<div id="rsmg_dialog_no_items">
		<h2><?php echo JText::_('COM_RSMEDIAGALLERY_INFORMATION'); ?></h2>
		<p><?php echo JText::_('COM_RSMEDIAGALLERY_PLEASE_UPLOAD_ITEMS'); ?></p>
		<div><center><img src="components/com_rsmediagallery/assets/images/tutorial-upload-items.jpg" alt="<?php echo JText::_('COM_RSMEDIAGALLERY_TUTORIAL_UPLOAD_ITEMS_IMAGE'); ?>" /></center></div>
		<p><?php echo JText::_('COM_RSMEDIAGALLERY_PLEASE_IMPORT_ITEMS'); ?></p>
		<div><center><img src="components/com_rsmediagallery/assets/images/tutorial-import-items.jpg" alt="<?php echo JText::_('COM_RSMEDIAGALLERY_PLEASE_IMPORT_ITEMS_IMAGE'); ?>" /></center></div>
	</div>
	<?php } ?>
    <div id="rsmg_dialog_tag">
    	<h2><?php echo JText::_('COM_RSMEDIAGALLERY_TAG_MULTIPLE_FILES'); ?></h2>
        <p>
        	<label for="rsmg_tags_text"><?php echo JText::_('COM_RSMEDIAGALLERY_TAGS'); ?></label>
            <textarea name="tags" id="rsmg_tags_text"></textarea>
            <small><?php echo JText::_('COM_RSMEDIAGALLERY_TAGS_DESC'); ?></small>
		</p>
        <p>
        	<?php echo JText::_('COM_RSMEDIAGALLERY_TAG_ACTIONS_TO_PERFORM'); ?><br />
            <label for="rsmg_action0"><input name="actions" type="radio" value="" id="rsmg_action0" checked="checked" /><?php echo JText::_('COM_RSMEDIAGALLERY_ADD'); ?></label>
            <label for="rsmg_action1"><input name="actions" type="radio" value="" id="rsmg_action1" /><?php echo JText::_('COM_RSMEDIAGALLERY_REMOVE'); ?></label>
        </p>
		<p id="rsmg_tags_text_error"><?php echo JText::_('COM_RSMEDIAGALLERY_PLEASE_ENTER_TAGS'); ?></p>
    </div>
	<div id="rsmg_dialog_upload">
		<h2><?php echo JText::_('COM_RSMEDIAGALLERY_ADD_MULTIPLE_FILES'); ?></h2>
        <p>
        	<label for="rsmg_add_tags_text"><?php echo JText::_('COM_RSMEDIAGALLERY_ADD_TAGS_AUTOMATICALLY'); ?></label>
            <textarea name="tags" id="rsmg_add_tags_text"></textarea>
            <small><?php echo JText::_('COM_RSMEDIAGALLERY_TAGS_DESC'); ?></small>
		</p>
		<p>
			<label for="rsmg_add_published"><input type="checkbox" name="published" id="rsmg_add_published" value="1" /> <?php echo JText::_('COM_RSMEDIAGALLERY_ADD_PUBLISHED_AUTOMATICALLY'); ?></label>
		</p>
		<p>
			<label for="rsmg_add_files"><?php echo JText::_('COM_RSMEDIAGALLERY_SELECT_FILES_TO_UPLOAD'); ?></label>
			<input type="file" id="rsmg_add_files" name="upload" value="select files" multiple="multiple" />
		</p>
		<p>
			<ul id="rsmg_add_results"></ul>
		</p>
	</div>
	<div id="rsmg_dialog_upload_ftp">
		<h2><?php echo JText::_('COM_RSMEDIAGALLERY_IMPORT_CHOOSE_FILES'); ?></h2>
		<div class="rsmg_ftp_results">
			<button class="rsmg_ftp_results_close" onclick="rsmg_hide_element(jQuery,'.rsmg_ftp_results','slide');"><?php echo JText::_('COM_RSMEDIAGALLERY_CLOSE'); ?></button>
			<ul id="rsmg_ftp_add_results"></ul>
		</div>
		<div class="rsmg_clear"></div>
		<div class="rsmg_ftp_add_tags">
        	<label for="rsmg_add_tags_text"><?php echo JText::_('COM_RSMEDIAGALLERY_ADD_TAGS_AUTOMATICALLY'); ?></label>
            <input type="text" name="tags" id="rsmg_add_ftp_tags_text"/><br/>
            <small><?php echo JText::_('COM_RSMEDIAGALLERY_TAGS_DESC'); ?></small>
		</div>
		<div class="rsmg_ftp_publish">
			<label for="rsmg_add_ftp_published"><input type="checkbox" name="published" id="rsmg_add_ftp_published" value="1" /> <?php echo JText::_('COM_RSMEDIAGALLERY_ADD_PUBLISHED_AUTOMATICALLY'); ?></label>
		</div>
		<div class="rsmg_clear"></div>
		<div class="rsmg_ajax_content">
			
		</div>
	</div>
</div>