<?php
/**
* @package RSMediaGallery!
* @copyright (C) 2011-2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/licenses/gpl-3.0.html
*/

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

class RSMediaGalleryViewRsmediagallery extends JViewLegacy
{
	public function display($tpl = null)
	{
		JToolBarHelper::title(' ','rsmediagallery');
		
		$jversion = new JVersion();
		
		$src = JDEBUG ? '.src' : '';
		
		$document = JFactory::getDocument();
		
		if ($jversion->isCompatible('3.0')) {
			JHtml::_('jquery.framework');
		} else {
			$document->addScript('components/com_rsmediagallery/assets/js/jquery'.$src.'.js?v='._RSMEDIAGALLERY_VERSION);
			$document->addScriptDeclaration("jQuery.noConflict();");
		}

		$document->addScript('components/com_rsmediagallery/assets/js/jquery.ui.new'.$src.'.js?v='._RSMEDIAGALLERY_VERSION);
		$document->addScript('components/com_rsmediagallery/assets/js/jquery.superfish'.$src.'.js?v='._RSMEDIAGALLERY_VERSION);
		$document->addScript('components/com_rsmediagallery/assets/js/jquery.timers'.$src.'.js?v='._RSMEDIAGALLERY_VERSION);
		$document->addScript('components/com_rsmediagallery/assets/js/jquery.dropshadow'.$src.'.js?v='._RSMEDIAGALLERY_VERSION);
		$document->addScript('components/com_rsmediagallery/assets/js/jquery.mbtooltip'.$src.'.js?v='._RSMEDIAGALLERY_VERSION);
		$document->addScript('components/com_rsmediagallery/assets/js/jquery.jrumble'.$src.'.js?v='._RSMEDIAGALLERY_VERSION);
		$document->addScript('components/com_rsmediagallery/assets/js/jquery.imgareaselect'.$src.'.js?v='._RSMEDIAGALLERY_VERSION);
		$document->addScript('components/com_rsmediagallery/assets/js/jquery.fileupload'.$src.'.js?v='._RSMEDIAGALLERY_VERSION);
		$document->addScript('components/com_rsmediagallery/assets/js/jquery.iframe-transport'.$src.'.js?v='._RSMEDIAGALLERY_VERSION);
		$document->addScript('components/com_rsmediagallery/assets/js/jquery.postmessage-transport'.$src.'.js?v='._RSMEDIAGALLERY_VERSION);
		$document->addScript('components/com_rsmediagallery/assets/js/jquery.xdr-transport'.$src.'.js?v='._RSMEDIAGALLERY_VERSION);
		$document->addScript('components/com_rsmediagallery/assets/js/jquery.fancyzoom'.$src.'.js?v='._RSMEDIAGALLERY_VERSION);
		$document->addScript('components/com_rsmediagallery/assets/js/jquery.script'.$src.'.js?v='._RSMEDIAGALLERY_VERSION);
		$document->addScriptDeclaration("function rsmg_get_root() { return '".addslashes(rtrim(JURI::root(), '/'))."'; }");

		$document->addScriptDeclaration("function rsmg_get_lang(id) {
			switch (id)
			{
				default: return id;
				case 'COM_RSMEDIAGALLERY_UNPUBLISHED': 	return '".JText::_('COM_RSMEDIAGALLERY_UNPUBLISHED', true)."'; break;
				case 'COM_RSMEDIAGALLERY_PUBLISHED': 		return '".JText::_('COM_RSMEDIAGALLERY_PUBLISHED', true)."'; break;
				case 'COM_RSMEDIAGALLERY_PUBLISH_DESC': 	return '".JText::_('COM_RSMEDIAGALLERY_PUBLISH_DESC', true)."'; break;
				case 'COM_RSMEDIAGALLERY_UNPUBLISH_DESC': return '".JText::_('COM_RSMEDIAGALLERY_UNPUBLISH_DESC', true)."'; break;
				case 'COM_RSMEDIAGALLERY_BTN_TAGS_DESC': 	return '".JText::_('COM_RSMEDIAGALLERY_BTN_TAGS_DESC', true)."'; break;
				case 'COM_RSMEDIAGALLERY_PREVIEW_DESC': 	return '".JText::_('COM_RSMEDIAGALLERY_PREVIEW_DESC', true)."'; break;
				case 'COM_RSMEDIAGALLERY_EDIT_DESC': 		return '".JText::_('COM_RSMEDIAGALLERY_EDIT_DESC', true)."'; break;
				case 'COM_RSMEDIAGALLERY_DELETE_DESC': 	return '".JText::_('COM_RSMEDIAGALLERY_DELETE_DESC', true)."'; break;
				case 'COM_RSMEDIAGALLERY_LOAD_MORE': 		return '".JText::_('COM_RSMEDIAGALLERY_LOAD_MORE', true)."'; break;
				case 'COM_RSMEDIAGALLERY_LOAD_ALL': 		return '".JText::_('COM_RSMEDIAGALLERY_LOAD_ALL', true)."'; break;
				case 'COM_RSMEDIAGALLERY_ARE_YOU_SURE': 	return '".JText::_('COM_RSMEDIAGALLERY_ARE_YOU_SURE', true)."'; break;
				case 'COM_RSMEDIAGALLERY_DELETE': 		return '".JText::_('COM_RSMEDIAGALLERY_DELETE', true)."'; break;
				case 'COM_RSMEDIAGALLERY_CANCEL': 		return '".JText::_('COM_RSMEDIAGALLERY_CANCEL', true)."'; break;
				case 'COM_RSMEDIAGALLERY_CLOSE': 			return '".JText::_('COM_RSMEDIAGALLERY_CLOSE', true)."'; break;
				case 'COM_RSMEDIAGALLERY_SAVE_CHANGES': 	return '".JText::_('COM_RSMEDIAGALLERY_SAVE_CHANGES', true)."'; break;
				case 'COM_RSMEDIAGALLERY_PLEASE_WAIT': 	return '".JText::_('COM_RSMEDIAGALLERY_PLEASE_WAIT', true)."'; break;
				case 'COM_RSMEDIAGALLERY_TAGS_HAVE_BEEN_ADDED': 	return '".JText::_('COM_RSMEDIAGALLERY_TAGS_HAVE_BEEN_ADDED', true)."'; break;
				case 'COM_RSMEDIAGALLERY_TAGS_HAVE_BEEN_REMOVED': return '".JText::_('COM_RSMEDIAGALLERY_TAGS_HAVE_BEEN_REMOVED', true)."'; break;
				case 'COM_RSMEDIAGALLERY_CLEAR_ALL_FILTERS': 		return '".JText::_('COM_RSMEDIAGALLERY_CLEAR_ALL_FILTERS', true)."'; break;
				case 'COM_RSMEDIAGALLERY_ITEMS_PER_PAGE': 		return '".JText::_('COM_RSMEDIAGALLERY_ITEMS_PER_PAGE', true)."'; break;
				case 'COM_RSMEDIAGALLERY_SELECT_ALL_PAGES': 		return '".JText::_('COM_RSMEDIAGALLERY_SELECT_ALL_PAGES', true)."'; break;
				case 'COM_RSMEDIAGALLERY_SELECT_ALL': 			return '".JText::_('COM_RSMEDIAGALLERY_SELECT_ALL', true)."'; break;
				case 'COM_RSMEDIAGALLERY_ITEM_SAVED': 	return '".JText::_('COM_RSMEDIAGALLERY_ITEM_SAVED', true)."'; break;
				case 'COM_RSMEDIAGALLERY_ITEM_DELETED': 	return '".JText::_('COM_RSMEDIAGALLERY_ITEM_DELETED', true)."'; break;
				case 'COM_RSMEDIAGALLERY_ITEMS_DELETED': 	return '".JText::_('COM_RSMEDIAGALLERY_ITEMS_DELETED', true)."'; break;
				case 'COM_RSMEDIAGALLERY_SELECTED_ITEMS': return '".JText::_('COM_RSMEDIAGALLERY_SELECTED_ITEMS', true)."'; break;
				case 'COM_RSMEDIAGALLERY_IMPORT_ADD_TO_GALLERY': return '".JText::_('COM_RSMEDIAGALLERY_IMPORT_ADD_TO_GALLERY', true)."'; break;
				case 'COM_RSMEDIAGALLERY_IMPORT_FINAL_SUCCESS': return '".JText::_('COM_RSMEDIAGALLERY_IMPORT_FINAL_SUCCESS', true)."'; break;
				case 'COM_RSMEDIAGALLERY_IMPORT_SELECT_ITEMS': return '".JText::_('COM_RSMEDIAGALLERY_IMPORT_SELECT_ITEMS', true)."'; break;
				
				case 'COM_RSMEDIAGALLERY_IMPORT_PREVIEW': return '".JText::_('COM_RSMEDIAGALLERY_IMPORT_PREVIEW', true)."'; break;
				case 'COM_RSMEDIAGALLERY_IMPORT_FILE': return '".JText::_('COM_RSMEDIAGALLERY_IMPORT_FILE', true)."'; break;
				case 'COM_RSMEDIAGALLERY_IMPORT_DIMENSIONS': return '".JText::_('COM_RSMEDIAGALLERY_IMPORT_DIMENSIONS', true)."'; break;
				case 'COM_RSMEDIAGALLERY_IMPORT_SELECT': return '".JText::_('COM_RSMEDIAGALLERY_IMPORT_SELECT', true)."'; break;
				case 'COM_RSMEDIAGALLERY_IMPORT_SELECT_ALL': return '".JText::_('COM_RSMEDIAGALLERY_IMPORT_SELECT_ALL', true)."'; break;
				case 'COM_RSMEDIAGALLERY_ERROR_FILTER_VALUE': return '".JText::_('COM_RSMEDIAGALLERY_ERROR_FILTER_VALUE', true)."'; break;
				case 'COM_RSMEDIAGALLERY_ERROR_FILTER_MEMORY_LIMIT_VALUE': return '".JText::_('COM_RSMEDIAGALLERY_ERROR_FILTER_MEMORY_LIMIT_VALUE', true)."'; break;
				
			}
		}");

		$document->addStyleSheet('components/com_rsmediagallery/assets/css/jquery.ui.css?v='._RSMEDIAGALLERY_VERSION);
		$document->addStyleSheet('components/com_rsmediagallery/assets/css/jquery.superfish.css?v='._RSMEDIAGALLERY_VERSION);
		$document->addStyleSheet('components/com_rsmediagallery/assets/css/style.css?v='._RSMEDIAGALLERY_VERSION);

		$jversion = new JVersion();
		if ($jversion->isCompatible('3.0')) {
			$joomlacss = 'j3.css';
		} else {
			$joomlacss = 'j2.css';
		}
		$document->addStyleSheet('components/com_rsmediagallery/assets/css/'.$joomlacss.'?v='._RSMEDIAGALLERY_VERSION);

		// IE fixes
		$document->addCustomTag('<!--[if IE]><link type="text/css" href="components/com_rsmediagallery/assets/css/ie.css" media="screen" rel="stylesheet" /><![endif]-->');
		$document->addCustomTag('<!--[if IE 7]><link type="text/css" href="components/com_rsmediagallery/assets/css/ie7.css" media="screen" rel="stylesheet" /><![endif]-->');

		$toolbar = JToolBar::getInstance('toolbar');
		$toolbar->addButtonPath(JPATH_ADMINISTRATOR.'/components/com_rsmediagallery/helpers/buttons');

		$toolbar->appendButton('RSMediaGallery', 'toolbar-start', '_default');
		$toolbar->appendButton('RSMediaGallery', 'upload');
		$toolbar->appendButton('RSMediaGallery', 'import');
		$toolbar->appendButton('RSMediaGallery', 'publish');
		$toolbar->appendButton('RSMediaGallery', 'unpublish');
		$toolbar->appendButton('RSMediaGallery', 'tag');
		$toolbar->appendButton('RSMediaGallery', 'remove');

		// load Options button
		$user = JFactory::getUser();
		if ($user->authorise('core.admin', 'com_rsmediagallery')) 
			JToolBarHelper::preferences('com_rsmediagallery');

		$toolbar->appendButton('RSMediaGallery', 'toolbar-end', '_default');
		$toolbar->appendButton('RSMediaGallery', 'toolbar-start', '_edit');
		$toolbar->appendButton('RSMediaGallery', 'save');
		$toolbar->appendButton('RSMediaGallery', 'apply');
		$toolbar->appendButton('RSMediaGallery', 'cancel');
		$toolbar->appendButton('RSMediaGallery', 'toolbar-end', '_edit');

		$filters = $this->get('filters');
		$this->columns 		= $filters[0];
		$this->operators 	= $filters[1];
		$this->values		= $filters[2];

		$orderings = $this->get('orderings');
		$this->ordering 	= $orderings[0];
		$this->direction 	= $orderings[1];

		$this->hasNoItems = $this->get('hasnoitems');
		$this->limit = $this->get('limit');

		$this->version = (string) new RSMediaGalleryVersion();
		$this->code = $this->get('updateCode');
		$this->isMissingCode 	= strlen($this->code) == 0;
		$this->isIncorrectCode 	= strlen($this->code) > 0 && strlen($this->code) != 20;
		$this->isCorrectCode 	= strlen($this->code) == 20;
		$this->colorize		= (int) (version_compare(PHP_VERSION, '5.2.5') >= 0);
		$this->pixelate		= (int) (version_compare(PHP_VERSION, '5.3.0') >= 0);
		$document->addScriptDeclaration('var rsmg_colorize = '.$this->colorize.'; var rsmg_pixelate = '.$this->pixelate.';');
		
		
		$this->imageFilterExists = function_exists('imagefilter');
		
		parent::display($tpl);
	}

	protected function translate($text)
	{
		switch ($text)
		{
			// columns & ordering
			case 'title':
			case 'i.title':
				return JText::_('COM_RSMEDIAGALLERY_TITLE');
			break;
			
			case 'description':
			case 'i.description':
				return JText::_('COM_RSMEDIAGALLERY_DESC');
			break;
			
			case 'tags':
			case 't.tag':
				return JText::_('COM_RSMEDIAGALLERY_TAGS');
			break;
			
			case 'hits':
			case 'i.hits':
				return JText::_('COM_RSMEDIAGALLERY_HITS');
			break;
			
			case 'published':
				return JText::_('COM_RSMEDIAGALLERY_PUBLISHED');
			break;
			
			case 'created':
			case 'i.created':
				return JText::_('COM_RSMEDIAGALLERY_CREATED_DATE');
			break;
			
			case 'modified':
			case 'i.modified':
				return JText::_('COM_RSMEDIAGALLERY_MODIFIED_DATE');
			break;
			
			// operators
			case 'is':
				return JText::_('COM_RSMEDIAGALLERY_IS');
			break;
			
			case 'is_not':
				return JText::_('COM_RSMEDIAGALLERY_IS_NOT');
			break;
			
			case 'contains':
				return JText::_('COM_RSMEDIAGALLERY_CONTAINS');
			break;
			
			case 'contains_not':
				return JText::_('COM_RSMEDIAGALLERY_DOES_NOT_CONTAIN');
			break;
			
			// ordering
			case 'i.ordering':
				return JText::_('COM_RSMEDIAGALLERY_FREE_ORDERING');
			break;
			
			// direction
			case 'asc':
				return JText::_('COM_RSMEDIAGALLERY_ASCENDING');
			break;
			
			case 'desc':
				return JText::_('COM_RSMEDIAGALLERY_DESCENDING');
			break;
		}
	}
}