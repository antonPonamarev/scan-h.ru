<?php
/**
* @package RSMediaGallery!
* @copyright (C) 2011-2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/licenses/gpl-3.0.html
*/

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');

class RSMediaGalleryViewRsmediagallery extends JViewLegacy
{
	public function display($tpl = null)
	{
		jimport('joomla.environment.browser');
		if (!function_exists('json_encode'))
			require_once(JPATH_COMPONENT.'/helpers/jsonwrapper/json.php');
		
		$layout 	= $this->getLayout();
		$browser 	= JBrowser::getInstance();
		$current 	= $browser->getBrowser();
		// assume supported
        $supported 	= true;
        // IE doesn't like AJAX uploads for now
        if ($current == 'msie' && $layout == 'upload')
            $supported = false;
        // Opera as well...
        if ($current == 'opera' && $layout == 'upload')
            $supported = false;
        if ($supported)
        {
            $document = JFactory::getDocument();
            $document->setMimeEncoding('application/json');
        }
		
		switch ($layout)
		{
			case 'items':
				$items = $this->get('items');
				foreach ($items as $i => $item)
				{
					$item->params = $item->params ? unserialize($item->params) : array();
					$items[$i] = $item;
				}
				$this->result = $this->_getResult($items, $this->get('total'));
			break;
			
			case 'item':
				$this->result = $this->get('item');
			break;
			
			case 'save':
				// pass the request to the model to save the item
				$this->result = $this->get('saveresult');
			break;
			
			case 'upload':
				// pass the request to the model to upload the item (one at a time)
				$this->result = $this->get('uploadresult');
			break;
			
			case 'autocomplete':
				// pass the request to the model to show a list of autocomplete options
				$this->result = $this->get('autocompleteresult');
			break;
			
			case 'navigate':
				// pass the request to the model to show the content of the selected folder
				$this->result = $this->get('ImagesFtp');
			break;
			
			case 'addimages':
				// pass the request to the model to show the content of the selected folder
				$this->result = $this->get('addImages');
			break;
			case 'checkmemory':
				// pass the request to the model to show the content of the selected folder
				$this->result = $this->get('checkMemory');
			break;
		}
		
		parent::display($tpl);
	}
	
	protected function _getResult($items, $total)
	{
		$result = new stdClass();
		
		$result->items = $items;
		$result->total = $total;
		
		return $result;
	}
}