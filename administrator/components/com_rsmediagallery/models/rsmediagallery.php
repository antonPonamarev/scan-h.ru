<?php
/**
* @package RSMediaGallery!
* @copyright (C) 2011-2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/licenses/gpl-3.0.html
*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class RSMediaGalleryModelRSMediaGallery extends JModelLegacy
{
	protected $_result = array();
	
	public function __construct()
	{
		parent::__construct();
		
		$mainframe 		= JFactory::getApplication();
		
		$this->_db 		= JFactory::getDBO();
		$this->_option	= 'com_rsmediagallery';
		$this->_query 	= $this->_buildQuery();
		
		// Get pagination request variables
		// no need to remember pages now
		//$limit 		= $mainframe->getUserStateFromRequest($this->_option.'.items.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		//$limitstart 	= $mainframe->getUserStateFromRequest($this->_option.'.items.limitstart', 'limitstart', 0, 'int');
		
		$limit 		= $this->getLimit();
		$limitstart = JRequest::getInt('limitstart', 0);

		// In case limit has been changed, adjust it
		// no need to adjust it now
		//$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
		
		$this->setState($this->_option.'.items.limit', $limit);
		$this->setState($this->_option.'.items.limitstart', $limitstart);
	}
	
	public function _buildQuery($split=false)
	{
		list($columns, $operators, $values) = $this->getFilters();
		list($order, $direction)			= $this->getOrderings();
		$join		= '';
		$where 		= array();
		$group		= '';
		$having		= '';
		$where_tags  = array();
		$having_tags = array();
		
		if ($order == 't.tag')
			$join = "LEFT JOIN #__rsmediagallery_tags t ON (i.id=t.item_id)";
		
		for ($i=0; $i<count($columns); $i++)
		{
			$column 	= $columns[$i];			
			$operator 	= $operators[$i];
			$value 		= $values[$i];
			$is_tag		= false;
			
			switch ($column)
			{
				case 'tags':
					$join 	= "LEFT JOIN #__rsmediagallery_tags t ON (i.id=t.item_id)";
					$column = 't.tag';
					$is_tag = true;
				break;
				
				case 'published':
					$column = 'i.'.$column;
					$value 	= '1';
				break;
				
				default:
					$column = 'i.'.$column;
				break;
			}
			
			switch ($operator)
			{
				case 'contains':
					if ($is_tag)
						$having_tags[] = "tags LIKE '%".$this->_db->escape($value, true)."%'";
						
					$operator = 'LIKE';
					$value	  = '%'.$this->_db->escape($value, true).'%';
				break;
				
				case 'contains_not':
					if ($is_tag)
						$having_tags[] = "tags NOT LIKE '%".$this->_db->escape($value, true)."%'";
						
					$operator = 'NOT LIKE';
					$value	  = '%'.$this->_db->escape($value, true).'%';
				break;
				
				case 'is':
					if ($is_tag)
						$having_tags[] = "tags LIKE '%,".$this->_db->escape($value, true).",%'";
						
					$operator = '=';
					$value = $this->_db->escape($value);
				break;
				
				case 'is_not':
					if ($is_tag)
						$having_tags[] = "tags NOT LIKE '%,".$this->_db->escape($value, true).",%'";
				
					$operator = '<>';
					$value = $this->_db->escape($value);
				break;
			}
			
			if ($is_tag)
				$where_tags[]  = "(".$this->_db->escape($column)." ".$operator." '".$value."')";
			else
				$where[] = "AND (".$this->_db->escape($column)." ".$operator." '".$value."')";
		}
		
		$where = implode(' ', $where);
		if ($where_tags)
		{
			// should be enough
			$this->_db->setQuery("SET @@group_concat_max_len = 8092");
			$this->_db->execute();
			
			$this->_db->setQuery("SELECT DISTINCT(item_id), CONCAT(',', GROUP_CONCAT(tag), ',') tags FROM #__rsmediagallery_tags t GROUP BY item_id HAVING ".implode(" AND ", $having_tags));
			$item_ids = $this->_db->loadColumn();
			
			$where .= " AND (".implode(" OR ", $where_tags).")";
			if ($item_ids)
				$where .= " AND i.id IN (".implode(',', $item_ids).")";
			else
				$where .= " AND i.id = 0";
			$group  = " GROUP BY t.item_id ";
		}
		
		$query = "SELECT i.* FROM #__rsmediagallery_items i ".$join." WHERE 1 ".$where." ".$group." ".$having." ORDER BY ".$this->_db->escape($order)." ".$this->_db->escape($direction);
		
		if ($split)
		{
			return array(
				'select' 	=> 'i.*',
				'join'	 	=> $join,
				'where'  	=> $where,
				'group'		=> $group,
				'having'	=> $having,
				'order'	 	=> $order,
				'direction'	=> $direction
			);
		}
		
		return $query;
	}
	
	public function getItems()
	{
		if (empty($this->_data))
			$this->_data = $this->_getList($this->_query, $this->getState($this->_option.'.items.limitstart'), $this->getState($this->_option.'.items.limit'));

		$tmp = array();
		foreach ( $this->_data as $item ) 
		{
			$item->tags = $this->getItemTags($item->id);
			$tmp[] 		= $item;
		}

		$this->_data = $tmp;
		
		return $this->_data;
	}
	
	public function getHasNoItems()
	{
		$this->_db->setQuery("SELECT id FROM #__rsmediagallery_items LIMIT 1");
		return !$this->_db->loadResult() ? true : false;
	}
	
	public function getTotal()
	{
		if (empty($this->_total))
			$this->_total = $this->_getOptimizedListCount($this->_query, 'DISTINCT(i.id)');
		
		return $this->_total;
	}
	
	public function _getOptimizedListCount($query, $count='COUNT(*)')
	{
		if (strpos($query,'FROM') !== false)
		{
			$tmp   = explode('FROM', $query, 2);
			$query = "SELECT $count FROM ".$tmp[1];
			$this->_db->setQuery($query);
			
			if (strpos($count,'COUNT(') !== false)
				return $this->_db->loadResult();
			else
				return count($this->_db->loadColumn());
		}
		
		return $this->_getListCount($query);
	}
	
	public function delItems()
	{
		$select_all = JRequest::getInt('selectall');
		
		// import classes
		jimport('joomla.filesystem.folder');
		jimport('joomla.filesystem.file');
		
		// path where files are stored
		$path = JPATH_SITE.'/components/com_rsmediagallery/assets/gallery';
		
		// get all other thumbs
		$folders = JFolder::folders($path, 'x', false, true, array('original'));
		
		$cid = JRequest::getVar('cid');
		JArrayHelper::toInteger($cid);
		
		if ($select_all) // delete all items
		{
			extract($this->_buildQuery(true));
			
			// get ids
			$this->_db->setQuery("SELECT i.id, i.filename FROM #__rsmediagallery_items i ".$join." WHERE 1 ".$where.$group.$having);
			$tmp = $this->_db->loadObjectList();
			$cid   = array();
			$files = array();
			foreach ($tmp as $result)
			{
				$cid[]   = $result->id;
				$files[] = $result->filename;
			}
		}
		
		if ($cid)
		{
			if (!isset($files))
			{
				// get filenames
				$this->_db->setQuery("SELECT `filename` FROM #__rsmediagallery_items WHERE `id` IN (".implode(',', $cid).")");
				$files = $this->_db->loadColumn();
			}
			
			// delete entries
			$this->_db->setQuery("DELETE FROM #__rsmediagallery_items WHERE `id` IN (".implode(',', $cid).")");
			$this->_db->execute();
			$this->_db->setQuery("DELETE FROM #__rsmediagallery_tags WHERE `item_id` IN (".implode(',', $cid).")");
			$this->_db->execute();
			
			// physically delete files
			if (!empty($files))
				foreach ($files as $file)
				{
					if (JFile::exists($path.'/'.$file))
						JFile::delete($path.'/'.$file);
					if (JFile::exists($path.'/original/'.$file))
						JFile::delete($path.'/original/'.$file);
					if (JFile::exists($path.'/filter/'.$file))
						JFile::delete($path.'/filter/'.$file);
					if (JFile::exists($path.'/original/filter/'.$file))
						JFile::delete($path.'/original/filter/'.$file);
					
					// delete all other thumbs
					if ($folders)
						foreach ($folders as $folder) {
							if (JFile::exists($folder.'/'.$file))
								JFile::delete($folder.'/'.$file);
							if (JFile::exists($folder.'/filter/'.$file))
								JFile::delete($folder.'/filter/'.$file);
						}
				}
		}
	}
	
	public function saveOrder()
	{
		$cid = JRequest::getVar('cid');
		JArrayHelper::toInteger($cid);
		
		$ordering = JRequest::getVar('ordering');
		JArrayHelper::toInteger($ordering);
		
		// save ordering
		if (count($cid))
		{
			$query = "UPDATE #__rsmediagallery_items SET `ordering` = CASE `id` ";
			for ($i=0; $i<count($cid); $i++)
				$query .= " WHEN ".$cid[$i]." THEN ".$ordering[$i];
			$query .= " END WHERE `id` IN (".implode(",", $cid).")";
			
			$this->_db->setQuery($query);
			$this->_db->execute();
		}
	}
	
	public function changeStatus($published=1)
	{
		$published  = (int) $published;
		$select_all = JRequest::getInt('selectall');
		$join  		= '';
		$where 		= '';
		
		if ($select_all) // change status for all
		{
			extract($this->_buildQuery(true), EXTR_OVERWRITE);
			$this->_db->setQuery("SELECT i.* FROM #__rsmediagallery_items i ".$join." WHERE 1 ".$where." ".$group." ".$having);
			$cid = $this->_db->loadColumn();
		}
		else // change status for selected
		{
			$cid = JRequest::getVar('cid');
			JArrayHelper::toInteger($cid);
		}
		
		if ($cid)
		{
			$where = " AND i.id IN (".implode(',', $cid).")";
			$this->_db->setQuery("UPDATE #__rsmediagallery_items i SET i.`published`='".$published."' WHERE 1 ".$where);
			$this->_db->execute();
		}
	}
	
	public function _prepareTags($tags)
	{
		foreach ($tags as $i => $tag)
		{
			// trim whitespace
			$tag = trim($tag);
			
			// don't add empty tags
			if (!$tag)
			{
				unset($tags[$i]);
				continue;
			}
			
			$tag = "'".$this->_db->escape($tag)."'";
			$tags[$i] = $tag;
		}
		return $tags;
	}
	
	public function changeTag($tagged=1)
	{
		$tagged 	= (int) $tagged;
		$select_all	= JRequest::getInt('selectall');
		$tags 		= explode(',', JRequest::getVar('tags'));
		$tags		= $this->_prepareTags($tags);
		$join 		= '';
		$where 		= '';
		
		if ($select_all)
		{
			extract($this->_buildQuery(true));
			
			if ($tagged)
			{
				foreach ($tags as $tag)
				{
					$query = "SELECT i.id, $tag FROM #__rsmediagallery_items i ".$join." WHERE 1 ".$where.$group.$having;
					$this->_db->setQuery("INSERT IGNORE INTO #__rsmediagallery_tags (`item_id`, `tag`) ".$query);
					$this->_db->execute();
				}
			}
			else
			{
				$this->_db->setQuery("SELECT i.id FROM #__rsmediagallery_items i ".$join." WHERE 1 ".$where.$group.$having);
				$cids = $this->_db->loadColumn();
				if ($cids)
				{
					$this->_db->setQuery("DELETE FROM #__rsmediagallery_tags WHERE `item_id` IN (".implode(',', $cids).") AND `tag` IN (".implode(',', $tags).")");
					$this->_db->execute();
				}
			}
		}
		else
		{
			$cids = JRequest::getVar('cid');
			JArrayHelper::toInteger($cids);
			if ($cids)
			{
				if ($tagged)
				{
					$values = array();
					foreach ($tags as $tag)
						foreach ($cids as $cid)
							$values[] = "('".$cid."', $tag)";
					if ($values)
					{
						$this->_db->setQuery("INSERT IGNORE INTO #__rsmediagallery_tags (`item_id`, `tag`) VALUES ".implode(',', $values));
						$this->_db->execute();
					}
				}
				else
				{
					$conditions = array();
					foreach ($tags as $tag)
						foreach ($cids as $cid)
							$conditions[] = "(`item_id`='".$cid."' AND `tag`=$tag)";
					
					if ($conditions)
					{
						$this->_db->setQuery("DELETE FROM #__rsmediagallery_tags WHERE ".implode(' OR ', $conditions));
						$this->_db->execute();
					}
				}
			}
		}
	}
	
	public function getItem()
	{
		$cid = JRequest::getInt('cid');
	
		// get item
		$this->_db->setQuery("SELECT * FROM #__rsmediagallery_items WHERE `id`='".$cid."'");
		$result = $this->_db->loadObject();
		
		if ($result)
		{
			// convert params
			$result->params = $result->params ? @unserialize($result->params) : array();
			
			// correct date
			if ($result->modified != $this->_db->getNullDate() && $result->modified != $result->created)
				$result->modified = JHTML::_('date',  $result->modified, JText::_('DATE_FORMAT_LC2'));
			else
				$result->modified = JText::_('COM_RSMEDIAGALLERY_NEVER');
				
			if ($result->created != $this->_db->getNullDate())
				$result->created = JHTML::_('date',  $result->created, JText::_('DATE_FORMAT_LC2'));
			else
				$result->created = JText::_('COM_RSMEDIAGALLERY_NEVER');
			
			// append tags
			$this->_db->setQuery("SELECT `tag` FROM #__rsmediagallery_tags WHERE `item_id`='".$result->id."'");
			$result->tags = implode(', ', $this->_db->loadColumn());
			
			// image details
			$path 	= JPATH_SITE.'/components/com_rsmediagallery/assets/gallery/original/';
			$filename = $result->filename;	
			$size	= filesize($path.$filename);
			$size 	= ($size / 1024) / 1024;
			$result->imgsize = round($size,2).'MB';
		}
		
		// return
		return $result;
	}
	
	public function saveItem()
	{
		$cid = JRequest::getInt('cid');
		
		if ($item = $this->getItem())
		{
			// get date object
			$date = JFactory::getDate();
			
			// save to db
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_rsmediagallery/tables');
			$image = JTable::getInstance('Items', 'RSMediaGalleryTable');
			
			// requested filters
			$filters = JRequest::getString('filters');
			if ($filters != '') {
				$filters = str_replace('filters=','',$filters);
			}
			
			// don't create the thumb if nothing has changed
			$create_thumb = $item->params['selection']['x1'] != JRequest::getInt('x1') || $item->params['selection']['y1'] != JRequest::getInt('y1') || $item->params['selection']['x2'] != JRequest::getInt('x2') || $item->params['selection']['y2'] != JRequest::getInt('y2') || (isset($item->params['filters']) && $item->params['filters'] != $filters);
			//var_dump($create_thumb);
			
			// new selection
			$item->params['selection']['x1'] = JRequest::getInt('x1');
			$item->params['selection']['y1'] = JRequest::getInt('y1');
			$item->params['selection']['x2'] = JRequest::getInt('x2');
			$item->params['selection']['y2'] = JRequest::getInt('y2');
			
			// new admin thumb params
			$item->params['admin_thumb']['w'] = JRequest::getInt('admin_w');
			$item->params['admin_thumb']['h'] = JRequest::getInt('admin_h');
			
			//new filters
			$item->params['filters'] = $filters;
			
			if ($item->params['selection']['x2'] - $item->params['selection']['x1'] < 1 || $item->params['selection']['y2'] - $item->params['selection']['y1'] < 1)
				return $this->_setResult(array('error' => '1', 'message' => JText::_('COM_RSMEDIAGALLERY_PLEASE_SELECT_THUMBNAIL_ERROR')));
			
			$image->id 			= $item->id;
			$image->title 		= JRequest::getVar('title');
			$image->description = JRequest::getVar('description', '', 'default', 'none', JREQUEST_ALLOWHTML);
			$image->url 		= JRequest::getVar('url');
			$image->params 		= serialize($item->params);
			$image->free_aspect	= JRequest::getInt('free_aspect');
			$image->hits 		= JRequest::getInt('hits');
			$image->published	= JRequest::getInt('published');
			
			// update modified time
			$image->modified 	= $date->toSql();
			
			// empty tags
			$this->_db->setQuery("DELETE FROM #__rsmediagallery_tags WHERE `item_id`='".$image->id."'");
			$this->_db->execute();
			
			// empty filters
			$this->_db->setQuery("DELETE FROM #__rsmediagallery_effects WHERE `item_id`='".$image->id."'");
			$this->_db->execute();
			
			// add the new tags
			JRequest::setVar('cid', array($image->id));
			$this->changeTag(1);
			
			if ($create_thumb)
			{
				$upload_location = JPATH_SITE.'/components/com_rsmediagallery/assets/gallery';
				
				// delete all other thumbs
				jimport('joomla.filesystem.folder');
				jimport('joomla.filesystem.file');
				if ($folders = JFolder::folders($upload_location, 'x', false, true, array('original')))
				{
					foreach ($folders as $folder)
						if (file_exists($folder.'/'.$item->filename))
							JFile::delete($folder.'/'.$item->filename);
				}
		
				// no error so far - check file extension
				$ext = strtolower(JFile::getExt($item->filename));
				
				require_once JPATH_ADMINISTRATOR.'/components/com_rsmediagallery/helpers/phpthumb/phpthumb.class.php';
				
				$phpThumb = new phpThumb();
				$phpThumb->src = $upload_location.'/original/'.$item->filename;
				$phpThumb->w   = JRequest::getInt('admin_w');
				$phpThumb->h   = JRequest::getInt('admin_h');
				$phpThumb->iar = 1;
				$phpThumb->zc  = 0;
				
				$ratio = $item->params['info'][0] / 380;
				
				$phpThumb->sx = round($item->params['selection']['x1']*$ratio);
				$phpThumb->sy = round($item->params['selection']['y1']*$ratio);
				$phpThumb->sw = round(JRequest::getInt('w')*$ratio);
				$phpThumb->sh = round(JRequest::getInt('h')*$ratio);
				
				if ($ext=='png' || $ext=='gif') {
					$phpThumb->fltr = 'stc|FFFFFF|5|10';
					$phpThumb->f   = $ext;
				}
						
				// bug fix
				if ($phpThumb->sx == 1 && $phpThumb->sw == $item->params['info'][0] - 1)
				{
					$phpThumb->sx = 0;
					$phpThumb->sw = $item->params['info'][0];
				}
				if ($phpThumb->sy == 1 && $phpThumb->sh == $item->params['info'][1] - 1)
				{
					$phpThumb->sy = 0;
					$phpThumb->sh = $item->params['info'][1];
				}
				
				if ($phpThumb->GenerateThumbnail())
				{
					if (!$phpThumb->RenderToFile($upload_location.'/'.$item->filename))
					{
						return $this->_setResult(array('error' => '1', 'message' => JText::sprintf('COM_RSMEDIAGALLERY_COULD_NOT_WRITE_TO_FILE', $upload_location.'/'.$item->filename)));
					}
				}
				else
					return $this->_setResult(array('error' => '1', 'message' => JText::sprintf('COM_RSMEDIAGALLERY_THUMBNAIL_NOT_CREATED', $item->original_filename, $phpThumb->fatalerror)));
				
				// set file permissions
				$componentParams = JComponentHelper::getParams('com_rsmediagallery');
				$perms 			 = octdec('0'.$componentParams->get('file_perms', '644'));
				@chmod($upload_location.'/'.$item->filename, $perms);
				
				if($item->params['filters'] != '') {
					require_once JPATH_ADMINISTRATOR.'/components/com_rsmediagallery/helpers/helper.php';
					// for the thumb
					RSMediaGalleryHelper::applyFilters($item->params['filters'], $item->filename, $upload_location.'/', true);
					// for the original image
					RSMediaGalleryHelper::applyFilters($item->params['filters'], $item->filename, $upload_location.'/original/', true);
						
				}
			}
			
			if ($image->store())
			{
				$image->load();
				$image->params = unserialize($image->params);
				return $this->_setResult(array('error' => '0', 'success' => '1', 'item' => $image->getProperties()));
			}
		}
	}
	
	public function uploadItem()
	{
		$file = JRequest::getVar('upload', array(), 'files');
		
		// form was not submitted ?
		if (empty($file))
			return $this->_setResult(array('error' => '1', 'message' => JText::_('COM_RSMEDIAGALLERY_NO_UPLOAD_REQUESTED')));
		
		// error, display message
		if ($file['error'] > 0)
		{
			switch ($file['error'])
			{
				case 1;
					return $this->_setResult(array('error' => '1', 'message' => JText::_('COM_RSMEDIAGALLERY_UPLOAD_EXCEEDS_SERVER_SIZE')));
				break;
				
				case 2;
					return $this->_setResult(array('error' => '1', 'message' => JText::_('COM_RSMEDIAGALLERY_UPLOAD_EXCEEDS_FORM_SIZE')));
				break;
				
				case 3;
					return $this->_setResult(array('error' => '1', 'message' => JText::_('COM_RSMEDIAGALLERY_UPLOAD_PARTIALLY')));
				break;
				
				default:
				case 4;
					return $this->_setResult(array('error' => '1', 'message' => JText::_('COM_RSMEDIAGALLERY_NO_FILE_UPLOADED')));
				break;
				
				case 6;
					return $this->_setResult(array('error' => '1', 'message' => JText::_('COM_RSMEDIAGALLERY_NO_TMP_FOLDER')));
				break;
				
				case 7;
					return $this->_setResult(array('error' => '1', 'message' => JText::_('COM_RSMEDIAGALLERY_FAILED_TO_WRITE')));
				break;
				
				case 8;
					return $this->_setResult(array('error' => '1', 'message' => JText::_('COM_RSMEDIAGALLERY_PHP_STOPPED_UPLOAD')));
				break;
			}
		}
		
		jimport('joomla.filesystem.file');
		
		// no error so far - check file extension
		$ext = strtolower(JFile::getExt($file['name']));
		
		// allowed extensions
		$allowed = array('jpg', 'jpeg', 'png', 'gif');
		
		// not allowed
		if (!in_array($ext, $allowed))
			return $this->_setResult(array('error' => '1', 'message' => JText::sprintf('COM_RSMEDIAGALLERY_EXTENSION_NOT_ALLOWED', $ext)));
		
		// check if it is actually an image
		$info = getimagesize($file['tmp_name']);
		// no info can be retrieved - correct extension but not an image
		if (!$info)
			return $this->_setResult(array('error' => '1', 'message' => JText::sprintf('COM_RSMEDIAGALLERY_CORRECT_EXTENSION_NOT_IMAGE', $file['name'])));
		
		$hash 			 = md5(uniqid($file['name']));
		$upload_location = JPATH_SITE.'/components/com_rsmediagallery/assets/gallery';
		
		// upload file to original directory
		// we need to keep the original file in order to create high quality crops and thumbs
		if (!JFile::upload($file['tmp_name'], $upload_location.'/original/'.$hash.'.'.$ext))
			return $this->_setResult(array('error' => '1', 'message' => JText::sprintf('COM_RSMEDIAGALLERY_IMAGE_NOT_UPLOADED', $upload_location.'/original')));
		
		// try to rotate
		if (in_array($ext, array('jpg', 'jpeg'))) {
			if (function_exists('exif_read_data') && function_exists('imagerotate') && ($exif_data = exif_read_data($upload_location.'/original/'.$hash.'.'.$ext))) {
				if (isset($exif_data['Orientation']) && $exif_data['Orientation'] != 1) {
					$image = imagecreatefromjpeg($upload_location.'/original/'.$hash.'.'.$ext);
					
					// flip it based on exif data
					switch ($exif_data['Orientation']) {
						case 2:
							$image = $this->imageFlip($image, 2);
						break;
						
						case 3:
							$image = $this->imageFlip($image, 3);
						break;
						
						case 4:
							$image = $this->imageFlip($image, 3);
							$image = $this->imageFlip($image, 2);
						break;
						
						case 5:
							$image = imagerotate($image, 270, 0);
							$image = $this->imageFlip($image, 2);
						break;
						
						case 6:
							$image = imagerotate($image, 270, 0);
						break;
						
						case 7:
							$image = GDImageFlip($image, 2);
							$image = imagerotate($image, 270, 0);
						break;
						
						case 8:
							$image = imagerotate($image, 90, 0);
						break;
					}
					
					// save new image
					imagejpeg($image, $upload_location.'/original/'.$hash.'.'.$ext, 100);
				}
			}
		}
		
		// set file permissions
		$componentParams = JComponentHelper::getParams('com_rsmediagallery');
		$perms 			 = octdec('0'.$componentParams->get('file_perms', '644'));
		@chmod($upload_location.'/original/'.$hash.'.'.$ext, $perms);
		
		// create the initial thumb
		require_once JPATH_ADMINISTRATOR.'/components/com_rsmediagallery/helpers/phpthumb/phpthumb.class.php';
		$phpThumb = new phpThumb();
		$phpThumb->src = $upload_location.'/original/'.$hash.'.'.$ext;
		$phpThumb->w  = 280;
		$phpThumb->h  = 210;
		$phpThumb->zc = $info[1] > $info[0] ? 'TC' : 1; // need to disable zoom-crop when portrait photos are used
		
		if ($ext=='png' || $ext=='gif') {
			$phpThumb->fltr = 'stc|FFFFFF|5|10';
			$phpThumb->f   = $ext;
		}
		
		if ($phpThumb->GenerateThumbnail())
		{
			if (!$phpThumb->RenderToFile($upload_location.'/'.$hash.'.'.$ext))
			{
				return $this->_setResult(array('error' => '1', 'message' => JText::sprintf('COM_RSMEDIAGALLERY_COULD_NOT_WRITE_TO_FILE', $upload_location.'/'.$hash.'.'.$ext)));
			}
		}
		else
			return $this->_setResult(array('error' => '1', 'message' => JText::sprintf('COM_RSMEDIAGALLERY_THUMBNAIL_NOT_CREATED', $file['name'], $phpThumb->fatalerror)));
		
		// set file permissions
		@chmod($upload_location.'/'.$hash.'.'.$ext, $perms);
		
		// get date object
		$date = JFactory::getDate();
		
		// save to db
		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_rsmediagallery/tables');
		$image = JTable::getInstance('Items', 'RSMediaGalleryTable');
		$image->original_filename 	= $file['name'];
		$image->filename 			= $hash.'.'.$ext;
		$image->title 				= JFile::stripExt($file['name']);
		$image->description 		= '';
		$image->type 				= 'image';
		// set correct ordering
		$image->ordering 			= $image->getNextOrder();
		
		$ratio  = 380 / $phpThumb->source_width;
		$height = $phpThumb->source_height * $ratio;
		
		$image->params = serialize(array(
			'info' 		=> $info,
			'selection' => array(
				'x1' => round($phpThumb->thumbnailCropX*$ratio),
				'y1' => round($phpThumb->thumbnailCropY*$ratio),
				'x2' => round($phpThumb->thumbnailCropW*$ratio + $phpThumb->thumbnailCropX*$ratio),
				'y2' => round($height - $phpThumb->thumbnailCropY*$ratio)
			),
			'admin_thumb' => array(
				'w' => $phpThumb->thumbnail_image_width,
				'h' => $phpThumb->thumbnail_image_height,
				'ratio' => $ratio
			),
			'filters' => ''
		));
		
		$image->created  = $date->toSql();
		$image->modified = $date->toSql();
		
		// set it to unpublished
		$image->published			= JRequest::getInt('published');
		if ($image->store())
		{
			// add tags
			JRequest::setVar('cid', array($image->id));
			$this->changeTag(1);
			
			return $this->_setResult(array('error' => '0', 'success' => '1', 'message' => JText::sprintf('COM_RSMEDIAGALLERY_IMAGE_SUCCESSFULLY_UPLOADED', $file['name']), 'id' => $image->id));
		}
	}
	
	public function getSuggestions()
	{
		$column 	= JRequest::getVar('filter_columns', '');
		$operator 	= JRequest::getVar('filter_operators', '');
		$value 		= JRequest::getVar('filter_values', '');
		$filter		= $value;
		
		if (!$filter)
			return $this->_setResult(array());
		
		$value = '%'.$this->_db->escape($value, true).'%';
		
		switch ($column)
		{
			case 'title':
				$this->_db->setQuery("SELECT DISTINCT(`title`) FROM #__rsmediagallery_items WHERE `title` LIKE '".$value."' AND `title` <> '' ORDER BY `title` ASC", 0, 10);
				$results = $this->_db->loadColumn();				
				if ($results)
					foreach ($results as $i => $result)
						$this->_trim($results[$i], $filter, $operator);
						
				$this->_setResult($results);
			break;
			
			case 'description':
				$this->_db->setQuery("SELECT `description` FROM #__rsmediagallery_items WHERE `description` LIKE '".$value."' AND `description` <> '' ORDER BY `description` ASC", 0, 10);
				$results = $this->_db->loadColumn();
				if ($results)
					foreach ($results as $i => $result)
						$this->_trim($results[$i], $filter, $operator);
						
				$this->_setResult($results);
			break;
			
			case 'tags':
				$this->_db->setQuery("SELECT DISTINCT(`tag`) FROM #__rsmediagallery_tags WHERE `tag` LIKE '".$value."' ORDER BY `tag` ASC", 0, 10);
				$this->_setResult($this->_db->loadColumn());
			break;
		}
	}
	
	public function _trim(&$value, $filter, $operator)
	{
		$max = 60;
		
		if ($operator == 'contains_not' || $operator == 'is_not')
		{
			$string_tmp = '';
			$words = explode(' ',$value);
			
			for ($i=0; $i<count($words); $i++)
			{
				if (strlen($string_tmp) + strlen($words[$i]) < $max)
					$string_tmp .= $words[$i].' ';
				else
					break;
			}
			$value = substr($string_tmp,0,-1);
		}
		else
		{
			$lcvalue 	= strtolower($value);
			$lcfilter 	= strtolower($filter);
			$pos 		= strpos($lcvalue, $lcfilter);
			
			$max 		= $max - strlen($filter);
			$stopwords 	= array(' ', ',', '.', ';', '!', '?');
			
			if ($pos !== false)
			{
				$left  = substr($value, 0, $pos);
				$right = substr($value, $pos+strlen($filter));
				
				$left_max 	= min(ceil($max/2), strlen($left));
				$right_max 	= min(ceil($max/2), strlen($right));
				
				$left_result = '';
				for ($i=strlen($left)-1; $i>=0; $i--)
				{
					if (in_array($left[$i], $stopwords) && strlen($left_result) >= $left_max)
						break;
					
					$left_result .= $left[$i];
				}
				$left_result = strrev($left_result);
				
				$right_result = '';
				for ($i=0; $i<strlen($right); $i++)
				{
					if (in_array($right[$i], $stopwords) && strlen($right_result) >= $right_max)
						break;
					
					$right_result .= $right[$i];
				}
				
				$value = $left_result.$filter.$right_result;
			}
		}
	}
	
	public function getFilters()
	{
		$mainframe 	= JFactory::getApplication();
		
		if (JRequest::getInt('dont_remember'))
		{
			$columns 	= JRequest::getVar('filter_columns', 	array());
			$operators 	= JRequest::getVar('filter_operators', 	array());
			$values		= JRequest::getVar('filter_values', 	array());
		}
		else
		{
			$columns 	= $mainframe->getUserStateFromRequest($this->_option.'.items.filter_columns', 	'filter_columns', 	array(), 'array');
			$operators 	= $mainframe->getUserStateFromRequest($this->_option.'.items.filter_operators', 'filter_operators', array(), 'array');
			$values 	= $mainframe->getUserStateFromRequest($this->_option.'.items.filter_values', 	'filter_values', 	array(), 'array');
		}
		
		if ($columns && $columns[0] == '')
			$columns = $operators = $values = array();
		
		if (!is_array($columns))
			$columns = array($columns);
		if (!is_array($operators))
			$operators = array($operators);
		if (!is_array($values))
			$values = array($values);
		
		return array($columns, $operators, $values);
	}
	
	public function getOrderings()
	{
		$mainframe 	= JFactory::getApplication();
		
		if (JRequest::getInt('dont_remember'))
		{
			$order 		= JRequest::getVar('order',		'ordering');
			$direction 	= JRequest::getVar('direction', 'asc');
		}
		else
		{
			$order 		= $mainframe->getUserStateFromRequest($this->_option.'.items.order', 	 'order', 		'ordering');
			$direction 	= $mainframe->getUserStateFromRequest($this->_option.'.items.direction', 'direction', 	'asc');
		}
		
		$order 		= $order == 'tags' ? 't.tag' : 'i.'.$order;
		
		return array($order, $direction);
	}
	
	public function getLimit()
	{
		$mainframe = JFactory::getApplication();
		
		if (JRequest::getInt('limitall'))
			return JRequest::getInt('limit', $mainframe->getCfg('list_limit'));
			
		return $mainframe->getUserStateFromRequest($this->_option.'.items.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
	}
	
	public function _setResult($param)
	{		
		if (is_array($param))
			foreach ($param as $key => $val)
				$this->_result[$key] = $val;
	}
	
	public function getUploadResult()
	{
		$this->uploadItem();
		
		return $this->_result;
	}
	
	public function getSaveResult()
	{
		$this->saveItem();
		
		return $this->_result;
	}
	
	public function getAutocompleteResult()
	{
		$this->getSuggestions();
		
		return $this->_result;
	}
	
	protected function imageFlip($imgsrc, $mode) {
		$width  = imagesx($imgsrc);
		$height = imagesy($imgsrc);

		$src_x = 0;
		$src_y = 0;
		$src_width  = $width;
		$src_height = $height;

		switch ($mode) {
			case 1:
				$src_y = $height - 1;
				$src_height = -$height;
			break;

			case 2:
				$src_x = $width - 1;
				$src_width = -$width;
			break;

			case 3:
				$src_x = $width - 1;
				$src_y = $height - 1;
				$src_width 	= -$width;
				$src_height = -$height;
			break;

			default:
				return $imgsrc;
			break;
		}

		$imgdest = imagecreatetruecolor($width, $height);
		if (imagecopyresampled($imgdest, $imgsrc, 0, 0, $src_x, $src_y , $width, $height, $src_width, $src_height)) {
			return $imgdest;
		}

		return $imgsrc;
	}

	protected function getItemTags($item_id=null)
	{
		$tags = '';
		if($item_id)
		{
			$this->_db->setQuery("SELECT CONCAT( GROUP_CONCAT(tag), ', ') tags FROM #__rsmediagallery_tags t WHERE `item_id` = '".$item_id."'");
			$tags = $this->_db->loadResult();
		}

		return rtrim($tags,', ');
	}
	
	public function getUpdateCode() {
		$componentParams = JComponentHelper::getParams('com_rsmediagallery');
		return $componentParams->get('code');
	}
	
	// Import functions (using local FTP)
	
	public function getImagesFtp() {
		jimport('joomla.filesystem.folder');
		jimport('joomla.filesystem.file');
		require_once JPATH_ADMINISTRATOR.'/components/com_media/helpers/media.php';
		
		$startPath = JRequest::getVar('path');
		
		static $list;

		// Only process the list once per request
		if (is_array($list))
		{
			return $list;
		}
		
		$images		= array ();
		$folders	= array ();
		$docs		= array ();
		
		
		$fileList = false;
		$folderList = false;
		
		$mediaBase = str_replace(DIRECTORY_SEPARATOR, '/', JPATH_SITE.'/');

			$basePath = JPATH_SITE.'/'.$startPath;
		
		
		$fileList	= JFolder::files($basePath);
		$folderList = JFolder::folders($basePath);
		
		// Iterate over the files if they exist
		if ($fileList !== false)
		{
			foreach ($fileList as $file)
			{
				if (is_file($basePath.'/'.$file) && substr($file, 0, 1) != '.' && strtolower($file) !== 'index.html')
				{
					$tmp = new JObject;
					$tmp->name = $file;
					$tmp->title = $file;
					$tmp->path = str_replace(DIRECTORY_SEPARATOR, '/', JPath::clean($basePath . '/' . $file));
					$tmp->path_relative = str_replace($mediaBase, '', $tmp->path);
					$tmp->size = filesize($tmp->path);

					$ext = strtolower(JFile::getExt($file));
					switch ($ext)
					{
						// Image
						case 'jpg':
						case 'png':
						case 'gif':
						case 'jpeg':
						
							$info = @getimagesize($tmp->path);
							$tmp->width		= @$info[0];
							$tmp->height	= @$info[1];
							$tmp->type		= @$info[2];
							$tmp->mime		= @$info['mime'];

							if (($info[0] > 60) || ($info[1] > 60))
							{
								$dimensions = MediaHelper::imageResize($info[0], $info[1], 60);
								$tmp->width_60 = $dimensions[0];
								$tmp->height_60 = $dimensions[1];
							}
							else {
								$tmp->width_60 = $tmp->width;
								$tmp->height_60 = $tmp->height;
							}

							if (($info[0] > 30) || ($info[1] > 30))
							{
								$dimensions = MediaHelper::imageResize($info[0], $info[1], 30);
								$tmp->width_30 = $dimensions[0];
								$tmp->height_30 = $dimensions[1];
							}
							else {
								$tmp->width_20 = $tmp->width;
								$tmp->height_20 = $tmp->height;
							}

							$images[] = $tmp;
							break;
					}
				}
			}
		}

		// Iterate over the folders if they exist
		if ($folderList !== false)
		{
			foreach ($folderList as $folder)
			{
				$tmp = new JObject;
				$tmp->name = basename($folder);
				$tmp->path = str_replace(DIRECTORY_SEPARATOR, '/', JPath::clean($basePath . '/' . $folder));
				$tmp->path_relative = str_replace($mediaBase, '', $tmp->path);
				$count = MediaHelper::countFiles($tmp->path);
				$tmp->files = $count[0];
				$tmp->folders = $count[1];

				$folders[] = $tmp;
			}
		}
		
		$expBackPath = explode('/',$startPath);
		$nrDirectories = count($expBackPath);
		$backPath = array();
		
		if ($expBackPath[($nrDirectories - 1)] != 'images') {
			for($i=0; $i <= ($nrDirectories - 2); $i++) {
				$backPath[] = $expBackPath[$i];
			}
		}

		$list = array('folders' => $folders, 'images' => $images, 'backpath' => $backPath);

		return $list;
	}
	
	
	
	public function getaddImages() {
		$response = array();
		jimport('joomla.filesystem.folder');
		jimport('joomla.filesystem.file');
		
		$basePath = JRequest::getVar('basepath');
		$singleImgPath = $basePath;
		$items = JRequest::getVar('items');
		
		$imgTypes = array('jpg', 'jpeg', 'png', 'gif');
		
		foreach ($items as $item) {
			$ext = explode('.',$item);
			$ext = array_pop($ext);
			if(in_array($ext,$imgTypes)) {
				// add simple images
				$itemPath = JPath::clean(JPATH_SITE.'/'.$singleImgPath.'/'.$item);
				$response[] =$this->addFTPImg($itemPath, $item, $ext, $singleImgPath);
			}
			else {
				// add images in the folders
				$basePath = JPATH_SITE.'/'.$singleImgPath.'/'.$item;
				
				$fileList	= JFolder::files($basePath);
				if ($fileList !== false)
				{
					foreach ($fileList as $file)
					{
						if (is_file($basePath.'/'.$file) && substr($file, 0, 1) != '.' && strtolower($file) !== 'index.html')
						{
							
							$ext = strtolower(JFile::getExt($file));
							switch ($ext)
							{
								// Image
								case 'jpg':
								case 'png':
								case 'gif':
								case 'jpeg':
									$itemPath = JPath::clean($basePath . '/' . $file);
									// adding the image to the gallery
									$response[] = $this->addFTPImg($itemPath, $file, $ext, $singleImgPath.'/'.$item);
								break;
							}
						}
					}
				}
			}
		}
		return $response;
	}
	
	public function addFTPImg($image, $name, $ext, $singleImgPath) {
		$error_pic_name = $singleImgPath.'/'.$name;
		$status = array();
		if (file_exists($image)){
			$info = getimagesize($image);
			// no info can be retrieved - correct extension but not an image
			if (!$info)
				$status[] = array('error' => '1', 'message' => JText::sprintf('COM_RSMEDIAGALLERY_IMPORT_CORRECT_EXTENSION_NOT_IMAGE',$error_pic_name));
			
			$image_size = filesize($image);
			
			
			$hash 			 = md5($name.$image_size);
			$upload_location = JPATH_SITE.'/components/com_rsmediagallery/assets/gallery';
			
			// build first destination to original
			$to = $upload_location.'/original/'.$hash.'.'.$ext;
			
			// check if the image already exists
			$exists = false;
			if(file_exists($to)) $exists = true;
			
			if($exists) {
				$status[] = array ('error' => '1', 'message' => JText::sprintf('COM_RSMEDIAGALLERY_IMPORT_IMAGE_ALREADY_LOADED',$error_pic_name));
				return $status;
			}
			// copy file to original directory
			// we need to keep the original file in order to create high quality crops and thumbs
			if (!JFile::copy($image, $to)) {
				$status[] = array('error' => '1', 'message' => JText::sprintf('COM_RSMEDIAGALLERY_IMPORT_COULD_NOT_WRITE_TO_FILE',$error_pic_name));
			}
			
			// try to rotate
			if (in_array($ext, array('jpg', 'jpeg'))) {
				if (function_exists('exif_read_data') && function_exists('imagerotate') && ($exif_data = exif_read_data($upload_location.'/original/'.$hash.'.'.$ext))) {
					if (isset($exif_data['Orientation']) && $exif_data['Orientation'] != 1) {
						$image = imagecreatefromjpeg($upload_location.'/original/'.$hash.'.'.$ext);
						
						// flip it based on exif data
						switch ($exif_data['Orientation']) {
							case 2:
								$image = $this->imageFlip($image, 2);
							break;
							
							case 3:
								$image = $this->imageFlip($image, 3);
							break;
							
							case 4:
								$image = $this->imageFlip($image, 3);
								$image = $this->imageFlip($image, 2);
							break;
							
							case 5:
								$image = imagerotate($image, 270, 0);
								$image = $this->imageFlip($image, 2);
							break;
							
							case 6:
								$image = imagerotate($image, 270, 0);
							break;
							
							case 7:
								$image = GDImageFlip($image, 2);
								$image = imagerotate($image, 270, 0);
							break;
							
							case 8:
								$image = imagerotate($image, 90, 0);
							break;
						}
						
						// save new image
						imagejpeg($image, $upload_location.'/original/'.$hash.'.'.$ext, 100);
					}
				}
			}
			
			// set file permissions
			$componentParams = JComponentHelper::getParams('com_rsmediagallery');
			$perms 			 = octdec('0'.$componentParams->get('file_perms', '644'));
			@chmod($upload_location.'/original/'.$hash.'.'.$ext, $perms);
			
			// create the initial thumb
			require_once JPATH_ADMINISTRATOR.'/components/com_rsmediagallery/helpers/phpthumb/phpthumb.class.php';
			$phpThumb = new phpThumb();
			$phpThumb->src = $upload_location.'/original/'.$hash.'.'.$ext;
			$phpThumb->w  = 280;
			$phpThumb->h  = 210;
			$phpThumb->zc = $info[1] > $info[0] ? 'TC' : 1; // need to disable zoom-crop when portrait photos are used
			
			if ($ext=='png' || $ext=='gif') {
				$phpThumb->fltr = 'stc|FFFFFF|5|10';
				$phpThumb->f   = $ext;
			}
			
			if ($phpThumb->GenerateThumbnail())
			{
				if (!$phpThumb->RenderToFile($upload_location.'/'.$hash.'.'.$ext))
				{
					$status[] = array('error' => '1', 'message' => JText::sprintf('COM_RSMEDIAGALLERY_COULD_NOT_WRITE_TO_FILE', $upload_location.'/'.$hash.'.'.$ext));
				}
			}
			else
				$status[] = array('error' => '1', 'message' => JText::sprintf('COM_RSMEDIAGALLERY_IMPORT_THUMBNAIL_NOT_CREATED',$error_pic_name, $phpThumb->fatalerror));
			
			// set file permissions
			@chmod($upload_location.'/'.$hash.'.'.$ext, $perms);
			
			// get date object
			$date = JFactory::getDate();
			
			// save to db
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_rsmediagallery/tables');
			$image = JTable::getInstance('Items', 'RSMediaGalleryTable');
			$image->original_filename 	= $name;
			$image->filename 			= $hash.'.'.$ext;
			$image->title 				= JFile::stripExt($name);
			$image->description 		= '';
			$image->type 				= 'image';
			// set correct ordering
			$image->ordering 			= $image->getNextOrder();
			
			$ratio  = 380 / $phpThumb->source_width;
			$height = $phpThumb->source_height * $ratio;
			
			$image->params = serialize(array(
				'info' 		=> $info,
				'selection' => array(
					'x1' => round($phpThumb->thumbnailCropX*$ratio),
					'y1' => round($phpThumb->thumbnailCropY*$ratio),
					'x2' => round($phpThumb->thumbnailCropW*$ratio + $phpThumb->thumbnailCropX*$ratio),
					'y2' => round($height - $phpThumb->thumbnailCropY*$ratio)
				),
				'admin_thumb' => array(
					'w' => $phpThumb->thumbnail_image_width,
					'h' => $phpThumb->thumbnail_image_height,
					'ratio' => $ratio
				),
				'filters' => ''
			));
			
			$image->created  = $date->toSql();
			$image->modified = $date->toSql();
			
			// set it to unpublished
			$tags 	 = explode(',',JRequest::getVar('tags'));
			$tags 	 = $this->_prepareTags($tags);
			$nr_tags = count($tags);
			
			
			$image->published = JRequest::getInt('published');
			if ($image->store())
			{
				// add tags
				if ($nr_tags>0) {
					JRequest::setVar('cid', array($image->id));
					JRequest::setVar('selectall', 0);
					$this->changeTag(1);
				}
				
				$status[] = array('error' => '0', 'success' => '1', 'message' => JText::sprintf('COM_RSMEDIAGALLERY_IMPORT_IMAGE_SUCCESSFULLY_ADDED',$error_pic_name)); 
			}
			return $status;
		}
	}
	
	public function buildFilterThumb(){
		require_once JPATH_ADMINISTRATOR.'/components/com_rsmediagallery/helpers/helper.php';
		
		// parameters
		$res	 = JRequest::getVar('resolution');
		$file 	 = JRequest::getVar('file');
		$filters = JRequest::getVar('filters');
		$path 	 = JPATH_SITE.'/components/com_rsmediagallery/assets/gallery/original/';
		
		echo RSMediaGalleryHelper::applyFilters($filters, $file, $path, false, true, $res);
		jexit();
	}
	
	public function getcheckMemory()
	{
		jimport('joomla.filesystem.file');
		
		$file 	= JRequest::getVar('file', '');
		$ext = JFile::getExt($file);
		$path 	= JPATH_SITE.'/components/com_rsmediagallery/assets/gallery/original/';
		$image 	= $path.$file;
		require_once JPATH_ADMINISTRATOR.'/components/com_rsmediagallery/helpers/helper.php';
		
		$memoryNeeded = RSMediaGalleryHelper::checkMemoryUsage($image,$ext);
		$execute = RSMediaGalleryHelper::changeMemoryUsage($memoryNeeded);
		
		$this->_setResult(array('status'=>$execute));
		
		return $this->_result;
		
	}
	// End Import functions (using local FTP)
}