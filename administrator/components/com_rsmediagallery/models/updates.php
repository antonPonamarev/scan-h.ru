<?php
/**
* @package RSMediaGallery!
* @copyright (C) 2011-2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/licenses/gpl-3.0.html
*/

defined('_JEXEC') or die('Restricted access');

class RSMediaGalleryModelUpdates extends JModelLegacy
{
	public function getHash() {
		$version = new RSMediaGalleryVersion();
		return md5($this->getUpdateCode().$version->key);
	}
	
	public function getUpdateCode() {
		$componentParams = JComponentHelper::getParams('com_rsmediagallery');
		return $componentParams->get('code');
	}
	
	public function getJoomlaVersion() {
		$jversion = new JVersion();
		return $jversion->getShortVersion();
	}
	
	public function getVersion() {
		$version = new RSMediaGalleryVersion();
		return (string) $version;
	}
}