<?php
/**
* @package RSMediaGallery!
* @copyright (C) 2011-2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/licenses/gpl-2.0.html
*/

defined('_JEXEC') or die('Restricted access');

class com_rsmediagalleryInstallerScript
{
	public function preflight($type, $parent) {
		$app = JFactory::getApplication();
		
		$jversion = new JVersion();
		if (!$jversion->isCompatible('2.5.7')) {
			$app->enqueueMessage('Please upgrade to at least Joomla! 2.5.7 before continuing!', 'error');
			return false;
		}
		
		return true;
	}
	
	public function postflight($type, $parent) 
	{
		$db			= JFactory::getDBO();
		$messages 	= array();

		if ($type == 'update') 
		{
			$source = $parent->getParent()->getPath('source');
			$this->runSQL($source, 'install.sql');
			
			$db->setQuery("SHOW COLUMNS FROM #__rsmediagallery_items WHERE `Field`='free_aspect'");
			if (!$db->loadResult()) 
			{
				$db->setQuery("ALTER TABLE `#__rsmediagallery_items` ADD `free_aspect` TINYINT(1) NOT NULL AFTER `params`");
				$db->query();
			}
		}

		// show message
		$this->showInstallMessage($messages);
	}
	
	protected function runSQL($source, $file) {
		$db = JFactory::getDbo();
		$driver = strtolower($db->name);
		if ($driver == 'mysqli') {
			$driver = 'mysql';
		} elseif ($driver == 'sqlsrv') {
			$driver = 'sqlazure';
		}
		
		$sqlfile = $source.'/admin/sql/'.$driver.'/'.$file;
		
		if (file_exists($sqlfile)) {
			$buffer = file_get_contents($sqlfile);
			if ($buffer !== false) {
				$queries = JInstallerHelper::splitSql($buffer);
				foreach ($queries as $query) {
					$query = trim($query);
					if ($query != '' && $query{0} != '#') {
						$db->setQuery($query);
						if (!$db->execute()) {
							JError::raiseWarning(1, JText::sprintf('JLIB_INSTALLER_ERROR_SQL_ERROR', $db->stderr(true)));
						}
					}
				}
			}
		}
	}
	
	protected function explode($string) {
		$string = str_replace(array("\r\n", "\r"), "\n", $string);
		return explode("\n", $string);
	}
	
	protected function quoteImplode($array) {
		$db = JFactory::getDbo();
		foreach ($array as $k => $v) {
			$array[$k] = $db->quote($v);
		}
		
		return implode(',', $array);
	}
	
	protected function showInstallMessage($messages=array()) {
?>
<style type="text/css">
.version-history {
	margin: 0 0 2em 0;
	padding: 0;
	list-style-type: none;
}
.version-history > li {
	margin: 0 0 0.5em 0;
	padding: 0 0 0 4em;
}
.version,
.version-new,
.version-fixed,
.version-upgraded {
	float: left;
	font-size: 0.8em;
	margin-left: -4.9em;
	width: 4.5em;
	color: white;
	text-align: center;
	font-weight: bold;
	text-transform: uppercase;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
}

.version {
	background: #000;
}

.version-new {
	background: #7dc35b;
}
.version-fixed {
	background: #e9a130;
}
.version-upgraded {
	background: #61b3de;
}

.install-ok {
	background: #7dc35b;
	color: #fff;
	padding: 3px;
}

.install-not-ok {
	background: #E9452F;
	color: #fff;
	padding: 3px;
}

#installer-left {
	float: left;
	width: 230px;
	padding: 5px;
}

#installer-right {
	float: left;
}

.com-rsmediagallery-button {
	display: inline-block;
	background: #459300 url(components/com_rsmediagallery/assets/images/bg-button-green.gif) top left repeat-x !important;
	border: 1px solid #459300 !important;
	padding: 2px;
	color: #fff !important;
	cursor: pointer;
	margin: 0;
	-webkit-border-radius: 5px;
     -moz-border-radius: 5px;
          border-radius: 5px;
}
</style>
	<div id="installer-left">
		<img src="components/com_rsmediagallery/assets/images/rsmediagallery-box.png" alt="RSMediaGallery! Box" />
	</div>
	<div id="installer-right">
		
		<ul class="version-history">
			<li><span class="version">Ver</span> 1.7.6</li>
			<li><span class="version-fixed">Fix</span> Images inside albums weren't being found if tags contained spaces.</li>
		</ul>
		<a class="com-rsmediagallery-button" href="index.php?option=com_rsmediagallery">Start using RSMediaGallery!</a>
		<a class="com-rsmediagallery-button" href="http://www.rsjoomla.com/support/documentation/view-knowledgebase/162-rsmediagallery.html" target="_blank">Read the RSMediaGallery! User Guide</a>
		<a class="com-rsmediagallery-button" href="http://www.rsjoomla.com/customer-support/tickets.html" target="_blank">Get Support!</a>
	</div>
	<div style="clear: both;"></div>
		<?php
	}
}