<?php
/**
* @package RSMediaGallery!
* @copyright (C) 2011-2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/licenses/gpl-3.0.html
*/

defined('_JEXEC') or die('Restricted access');

class JFormFieldWidthresolution extends JFormField
{
	protected $type = 'Widthresolution';
	
	public function __construct($parent = null) {
		static $added;
		parent::__construct($parent);
		
	}
	
	protected function getInput()
	{
		$html 		= array();
		
		$name  		= $this->name;
		$fieldname 	= $this->fieldname;
		$value 		= $this->value;
		$node  		= $this->element;
		$class 		= $this->element['class'];

		$size 		= isset($this->element['size']) ? 'size="'.$this->element['size'].'"' : '';
		$value		= is_array($value) ? $value : explode(',', $value);		
		if (!isset($value[1])) $value[1] = '0';
		
		$options = array(
			JHtml::_('select.option', 'w', JText::_('COM_RSMEDIAGALLERY_PARAM_WIDTH'))
		);
		
		$select = JHtml::_('select.genericlist', $options, $name.'[]', 'onchange="rsmg_change_other(\''.addslashes($fieldname).'\', this.value)" class="'.$class.'"', 'value', 'text', $value[0], $fieldname.'_w');
		$input	= ' <input style="text-align: center;" type="text" name="'.$name.'[]" id="'.$fieldname.'_res" value="'.(int) $value[1].'" class="'.$class.'" '.$size.' /> ';
		$other	= JText::_('COM_RSMEDIAGALLERY_PARAM_HEIGHT');
		
		// because Joomla! 2.5 doesn't behave quite right with params and we don't want to keep this whole HTML code in the language files, we need to make this ugly workaround
		$words = explode(' ', JText::_('COM_RSMEDIAGALLERY_PARAM_SIZE_ADJUST'));
		
		$html[] = '<table cellpadding="1" cellspacing="2" border="0" style="float:left; font-size:12px">';
		$html[] = '<tr>';
		$html[] = '<td>';
		foreach ($words as $word)
		{
			// found replacement, wrap it in a <td>
			if ($word[0] == '%')
			{
				$html[] = '</td>';
				// 3rd replacement should be $other - add an id to the <td> so that we can dynamically change the text
				$html[] = '<td'.(strstr($word, '%3$s') ? ' id="rsmg_other_'.$fieldname.'"' : '').'>'.$word.'</td>';
				$html[] = '<td>';
			}
			else
				$html[] = $word;
		}
		$html[] = '</td>';
		$html[] = '</tr>';
		$html[] = '</table>';
		
		return sprintf(implode("\r\n", $html), $select, $input, $other);
	}
}