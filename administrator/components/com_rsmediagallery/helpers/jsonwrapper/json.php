<?php
/**
* @package RSMediaGallery!
* @copyright (C) 2011-2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/licenses/gpl-3.0.html
*/

defined( '_JEXEC' ) or die( 'Restricted access' );

# In PHP 5.2 or higher we don't need to bring this in
if (!function_exists('json_encode'))
	require_once dirname(__FILE__).'/jsonwrapper_inner.php';