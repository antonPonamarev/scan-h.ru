<?php
/**
* @package RSMediaGallery!
* @copyright (C) 2011-2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/licenses/gpl-3.0.html
*/

defined('_JEXEC') or die('Restricted access');

function _iniRSMediaGalleryBootstrap()
{
	$jqueryHelper 	= RSMediaGalleryBootstrap::getInstance();
	// we're running onAfterRender() so set this to 1
	$jqueryHelper->afterRender = true;
	// force our own mode since the only one available now is the smart load
	$jqueryHelper->mode = 'smart';
	// try to add jQuery
	$jqueryHelper->addBootstrap();
}

class RSMediaGalleryBootstrap
{
	public $mode = 'auto';
	public $afterRender = false;
	
	protected $BootstrapPattern = '#src="([\\\/a-zA-Z0-9_:\.\-\\\!]*)\/bootstrap([0-9\.-]|core|min|pack)*?.js|(v\?\=)"#';
	
	public static function getInstance()
	{
		static $inst;
		
		if (empty($inst))
		{
			jimport('joomla.application.component.helper');
	
			$params = JComponentHelper::getParams('com_rsmediagallery');
			$inst 	= new RSMediaGalleryBootstrap($params->get('bootstrap', 'auto'));
		}
		
		return $inst;
	}
	
	public function __construct($mode)
	{
		$this->mode = $mode;
		// if debug is on, we need to add .src as well since our scripts include it
		if (JDEBUG)
			$this->BootstrapPattern = str_replace('|min|pack)', '|min|src|pack)', $this->BootstrapPattern);
	}
	
	public function addBootstrap()
	{		
		// this if() clause will be run only once since 'auto' will be changed to something else
		// attempt to detect best approach
		if ($this->mode == 'auto')
		{
			// did we find multiple instances of jQuery ?
			// we haven't added anything yet so it means something else loaded jQuery this is why it's > 0
			if ($this->foundMultipleBootstrap() > 0)
				// ok, try to load just one
				$this->mode = 'smart';
			// otherwise, add our own
			else
				$this->mode = 'own';
			
			// attach the function on both cases since we're on auto and we need to make sure that a single jquery instance runs
			$mainframe = JFactory::getApplication();
			$mainframe->registerEvent('onAfterRender', '_iniRSMediaGalleryBootstrap');
		}
		
		switch ($this->mode)
		{
			case 'own':
				// just add our script
				$this->addScript();
			break;
			
			case 'smart':
				// we can't run now since the "smart load" mode requires "onAfterRender()"
				if (!$this->afterRender)
				{
					// just add our script...
					$this->addScript();
					
					// just attach the event so it can run
					$mainframe = JFactory::getApplication();
					$mainframe->registerEvent('onAfterRender', '_iniRSMediaGalleryBootstrap');
					return true;
				}
					
				// if found multiple instances AND we are running onAfterRender() we can proceed
				// we've already added our own jQuery so this means that we need to check if we have more than one
				if ($this->foundMultipleBootstrap() > 1)
					$this->replaceBootstrap();
			break;

			case 'no':
				if ( RSMediaGalleryhelper::isJ3() ) {
					JHtml::_('bootstrap.framework');
				}
				// do nothing - do not load
				return true;
			break;
		}
	}
	
	protected function addScript()
	{
		$document = JFactory::getDocument();
		$src 	  =  JDEBUG ? '.src' : '';
		
		$document->addScript(JURI::root(true).'/components/com_rsmediagallery/assets/js/bootstrap'.$src.'.js');
		$document->addStyleSheet(JURI::root(true).'/components/com_rsmediagallery/assets/css/bootstrap.css');
		$document->addStyleSheet(JURI::root(true).'/components/com_rsmediagallery/assets/css/bootstrap-responsive.css');
		
	}

	public function foundMultipleBootstrap($where=null)
	{
		$body = $where ? $where : JResponse::getBody();
		// find jQuery versions
		$found = preg_match_all($this->BootstrapPattern, $body, $matches);
		
		return $found;
	}
	
	public function replaceBootstrap($where=null)
	{
		$src  = JDEBUG ? '.src' : '';
		$body = $where ? $where : JResponse::getBody();
		// remove all other references to jQuery library
		$body = preg_replace($this->BootstrapPattern, 'GARBAGE', $body);
		// remove newly empty scripts
		$body = preg_replace('#<script[^>]*GARBAGE[^>]*></script>#', '', $body);
		// jQuery
		$bootstrap 	= '<script type="text/javascript" src="'.JURI::root(true).'/components/com_rsmediagallery/assets/js/bootstrap'.$src.'.js'.'"></script>';
		
		$body = str_replace('<head>', '<head>'."\r\n".$bootstrap."\r\n", $body);
		
		if ($where)
			return $body;
			
		JResponse::setBody($body);
	}
}