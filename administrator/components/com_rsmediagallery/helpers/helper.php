<?php
/**
* @package RSMediaGallery!
* @copyright (C) 2011-2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/licenses/gpl-3.0.html
*/

defined('_JEXEC') or die('Restricted access');

class RSMediaGalleryHelper
{
	// Check for Joomla! 3.0
	public static function isJ3() {
		return version_compare(JVERSION, '3.0', '>=');
	}

	public static function escape($text)
	{
		return htmlentities($text, ENT_COMPAT, 'utf-8');
	}
	
	public static function getItemsQuery($tags, $order='i.ordering', $direction='ASC')
	{
		$db = JFactory::getDBO();
		if ($order == 'random' || $order == 'i.random')
			$order = 'RAND()';
		elseif ($order[1] != '.')
			$order = $order == 'tags' ? 't.tag' : 'i.'.$order;
		
		$where = array();
		
		if (!is_array($tags))
			$tags = explode(',', $tags);
		
		foreach ($tags as $tag)
		{
			if (trim($tag) == '')
				continue;
			$where[] = "t.tag='".$db->escape(trim($tag))."'";
		}
		
		$query = "SELECT DISTINCT(i.id), i.filename, i.title, i.url, i.description, i.params, i.hits, i.created, i.modified FROM #__rsmediagallery_items i LEFT JOIN #__rsmediagallery_tags t ON (i.id=t.item_id) WHERE i.published='1' AND (".implode(' OR ', $where).") ORDER BY ".$db->escape($order)." ".$db->escape($direction);
		return $query;
	}
	
	public static function getItems($tags, $order='i.ordering', $direction='ASC', $limitstart=0, $limit=0)
	{
		$db = JFactory::getDBO();
		$db->setQuery(RSMediaGalleryHelper::getItemsQuery($tags, $order, $direction), $limitstart, $limit);
		return $db->loadObjectList();
	}
	
	public static function getImageSize($width, $height, $original_width, $original_height)
	{
		if ($width && !$height)
		{
			$ratio 	= $width / $original_width;
			$height = round($original_height * $ratio);
		}
		elseif (!$width && $height)
		{
			$ratio 	= $height / $original_height;
			$width	= round($original_width * $ratio);
		}
		return array($width, $height);
	}
	
	public static function getImage($mixed, &$width=280, &$height=210, $xhtml=true)
	{
		// cache
		static $cache;
		
		// object ?
		if (is_object($mixed) && isset($mixed->filename))
		{
			
			$image    = $mixed;
			$image_id = $image->id;
			$id    	  = $image->filename;
			$params   = $image->params;
		}
		// array ?
		elseif (is_array($mixed) && isset($mixed['filename']))
		{
			$image    = $mixed;
			$image_id = $image['id'];
			$id    	  = $image['filename'];
			$params   = $image['params'];
		}
		// numeric id ? load from database
		elseif ((int) $mixed > 0)
		{
			JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_rsmediagallery/tables');
			$image = JTable::getInstance('Items', 'RSMediaGalleryTable');
			if ($image->load($mixed))
			{
				$image_id = $image->id;
				$id 	  = $image->filename;
				$params   = $image->params;
			}
			else
				return false; // not found
		}
		// cannot continue
		else
		{
			return false;
		}
		
		if ($params)
		{
			if (!is_array($params))
				$params = @unserialize($params);
				
			if (is_array($params))
			{
				$original_width  		= $params['selection']['x2'] - $params['selection']['x1'];
				$original_height 		= $params['selection']['y2'] - $params['selection']['y1'];
				
				list($width, $height) 	= RSMediaGalleryHelper::getImageSize($width, $height, $original_width, $original_height);
			}
		}
		
		if (!$width && !$height) {
			return false;
		}
		
		$res = $width.'x'.$height;		
		// check cache
		if (!isset($cache[$id][$res]))
		{
			$create_thumb = true;
			$filter = ((isset($params['filters']) &&  $params['filters'] != '') ? 'filter/' : '');
			$extraCacheFilter = ($filter!='' ? '?v='.uniqid('') : '');
			
			if(isset($params['filters'])) {
				$hashFilter 	  = md5($res.$params['filters']);
				$db 	 = JFactory::getDBO();
				$db->setQuery("SELECT hash FROM #__rsmediagallery_effects WHERE ".$db->qn('item_id').'='.$db->q($image_id)." AND ".$db->qn('hash').'='.$db->q($hashFilter));
				$isNewFilter     = !$db->loadResult();
			}
			else $isNewFilter = false;
			
			// admin thumb ?
			if ($width == 280 && $height == 210)
			{
				if (file_exists(JPATH_SITE.'/components/com_rsmediagallery/assets/gallery/'.$filter.$id) && !$isNewFilter)
				{
					
					$cache[$id][$res] = JURI::root().'components/com_rsmediagallery/assets/gallery/'.$filter.$id.$extraCacheFilter;
					$create_thumb 	  = false;
				}
			}
			else
			{
				if (file_exists(JPATH_SITE.'/components/com_rsmediagallery/assets/gallery/'.$res.'/'.$filter.$id) && !$isNewFilter)
				{
					$cache[$id][$res] = JURI::root().'components/com_rsmediagallery/assets/gallery/'.$res.'/'.$filter.$id.$extraCacheFilter;
					$create_thumb 	  = false;
				}
			}
			
			if ($create_thumb)
			{
				$hash = md5($res.JPATH_SITE);
				
				$db = JFactory::getDbo();
				$db->setQuery("INSERT INTO #__rsmediagallery_thumbs SET ".$db->qn('hash')."=".$db->q($hash));
				$db->execute();
				
				if(isset($params['filters'])) {
					$db->setQuery("INSERT INTO #__rsmediagallery_effects SET ".$db->qn('item_id')."=".$db->q($image_id).", ".$db->qn('hash')."=".$db->q($hashFilter));
					$db->execute();
				}
				
				jimport('joomla.filesystem.file');
				$filepart = JFile::stripExt($id);
				$fileext  = JFile::getExt($id);
				
				// don't add to cache now - chances are if the user is showing multiple instances of the same image(s) on a page, only the first ones will work
				$cache[$id][$res] = JRoute::_('index.php?option=com_rsmediagallery&task=createthumb&resolution='.$width.'x'.$height.'&id='.$filepart.'&ext='.$fileext, $xhtml);
			}
		}
		
		return $cache[$id][$width.'x'.$height];
	}
	
	public static function parseParams($params)
	{
		// not an object, can't continue
		if (!is_object($params))
			return false;
		
		// already parsed
		if ($params->get('thumb_width') > 0 || $params->get('thumb_height') > 0)
			return $params;
		
		$params->set('full_width', 		0);
		$params->set('full_height', 	0);
		$params->set('thumb_width', 	0);
		$params->set('thumb_height', 	0);
		
		if ($res = $params->get('thumb_resolution'))
		{
			if (!is_array($res))
				$res = explode(',', $res);
				
			// graceful deprecation
			if ((int) $res[0] > 0 && (int) $res[1] > 0)
			{
				$res[1] = $res[0];
				$res[0] = 'w';
			}
			
			$thumb_size = $res[0];
			if ($thumb_size == 'h')
			{
				$params->set('thumb_width',  0);
				$params->set('thumb_height', $res[1]);
			}
			else
			{
				$params->set('thumb_width', $res[1]);
				$params->set('thumb_height', 0);
			}
		}
		if ($res = $params->get('full_resolution'))
		{
			if (!is_array($res))
				$res = explode(',', $res);
				
			// graceful deprecation
			if ((int) $res[0] > 0 && (int) $res[1] > 0)
			{
				$res[1] = $res[0];
				$res[0] = 'w';
			}
			
			$full_size = $res[0];
			if ($full_size == 'h')
			{
				$params->set('full_width',  0);
				$params->set('full_height', $res[1]);
			}
			else
			{
				$params->set('full_width', $res[1]);
				$params->set('full_height', 0);
			}
		}
		
		return $params;
	}

	public static function parseItem($item, $params, $xhtml=true)
	{
		jimport('joomla.filesystem.file');
		
		$db	= JFactory::getDBO();
		
		// ensure this is parsed
		$params					 = RSMediaGalleryHelper::parseParams($params);
		// thumb default sizes
		$thumb_width 			 = $params->get('thumb_width',  280);
		$thumb_height 			 = $params->get('thumb_height', 210);
		// full resolution default sizes
		$full_width				 = $params->get('full_width',  800);
		$full_height			 = $params->get('full_height', 600);
		// params - list view
		$show_title_list 	  	 = (int) $params->get('show_title_list', 1);
		$show_description_list 	 = (int) $params->get('show_description_list', 1);
		$open_in 				 = $params->get('open_in', 'slideshow');
		// params - detail view
		$show_title_detail		 = (int) $params->get('show_title_detail', 1);
		$show_description_detail = (int) $params->get('show_description_detail', 1);
		$use_original			 = (int) $params->get('use_original', 0);
		$download_original		 = (int) $params->get('download_original', 0);
		$show_hits				 = (int) $params->get('show_hits', 1);
		$show_tags				 = (int) $params->get('show_tags', 1);
		$show_created			 = (int) $params->get('show_created', 1);
		$show_modified			 = (int) $params->get('show_modified', 1);
		$open_in_new_page		 = (int) $params->get('open_in_new_page', 0);
		$view 					 = ($params->exists('direction_album') ? 'album' : 'rsmediagallery' );
		$albumTag				 = '&tag='.urlencode(JRequest::getString('tag', 0));
		$itemParams 			 = unserialize($item->params);
		$filter					 = ((isset($itemParams['filters']) &&  $itemParams['filters'] != '') ? 'filter/' : '');
		$extraCacheFilter 		 = ($filter!='' ? '?v='.uniqid('') : '');
		
		if ($albumTag == '&tag=0') $albumTag = '';
		
		$item->filepart 	= JFile::stripExt($item->filename);
		$item->fileext  	= JFile::getExt($item->filename);
		$item->href 		= $open_in == 'url' ? $item->url : RSMediaGalleryRoute::_('index.php?option=com_rsmediagallery&view='.$view.'&layout=image'.$albumTag.'&id='.$item->filepart.'&ext='.$item->fileext, $xhtml);
		$item->thumb 		= RSMediaGalleryHelper::getImage($item, $thumb_width, $thumb_height, $xhtml);
		$item->thumb_width 	= (int) $thumb_width;
		$item->thumb_height = (int) $thumb_height;
		$item->full			= $use_original ? JURI::root(true).'/components/com_rsmediagallery/assets/gallery/original/'.$filter.$item->filename.$extraCacheFilter : RSMediaGalleryHelper::getImage($item, $full_width, $full_height, $xhtml);
		$item->download	= RSMediaGalleryRoute::_('index.php?option=com_rsmediagallery&task=downloaditem&id='.$item->filepart.'&ext='.$item->fileext, $xhtml);
		// correct date - modified time
		if ($item->modified != $db->getNullDate() && $item->modified != $item->created)
			$item->modified = JHTML::_('date',  $item->modified, JText::_('DATE_FORMAT_LC2'));
		else
			$item->modified = JText::_('COM_RSMEDIAGALLERY_NEVER');
		// correct date - created time
		if ($item->created != $db->getNullDate())
			$item->created = JHTML::_('date',  $item->created, JText::_('DATE_FORMAT_LC2'));
		else
			$item->created = JText::_('COM_RSMEDIAGALLERY_NEVER');
		
		$item->show_title_list 		 	= $show_title_list;
		$item->show_description_list 	= $show_description_list;
		$item->show_title_detail 		= $show_title_detail;
		$item->show_description_detail 	= $show_description_detail;
		$item->show_hits 				= $show_hits;
		$item->show_tags 				= $show_tags;
		$item->show_created 			= $show_created;
		$item->show_modified 			= $show_modified;
		$item->download_original 		= $download_original;
		$item->open_in_new_page 		= $open_in_new_page;
		
		// parse description
		@list($item->description, $item->full_description) = explode('{readmore}', $item->description, 2);
		$item->full_description = $item->description.$item->full_description;
		
		return $item;
	}
	
	public static function parseAlbumItem($item, $params, $xhtml=true)
	{
		jimport('joomla.filesystem.file');
		
		$db	= JFactory::getDBO();
		
		// ensure this is parsed
		$params					 = RSMediaGalleryHelper::parseParams($params);
		// thumb default sizes
		$thumb_width 			 = $params->get('thumb_width',  280);
		$thumb_height 			 = $params->get('thumb_height', 210);
		// full resolution default sizes
		$full_width				 = $params->get('full_width',  800);
		$full_height			 = $params->get('full_height', 600);
		

		$item->thumb 		= RSMediaGalleryHelper::getImage($item, $thumb_width, $thumb_height, $xhtml);
		$item->full			= $use_original ? JURI::root(true).'/components/com_rsmediagallery/assets/gallery/original/'.$item->filename : RSMediaGalleryHelper::getImage($item, $full_width, $full_height, $xhtml);
		
		return $item;
	}
	
	public static function addTags(&$items)
	{
		$db  = JFactory::getDBO();
		$ids = array();
		
		foreach ($items as $item)
			$ids[] = $item->id;
		
		if ($ids)
		{
			$db->setQuery("SELECT * FROM #__rsmediagallery_tags WHERE `item_id` IN (".implode(',', $ids).") ORDER BY `item_id`, `tag` ASC");
			$tags = $db->loadObjectList();
			
			foreach ($items as $i => $item)
			{
				$item->tags = array();
				foreach ($tags as $tag)
					if ($tag->item_id == $item->id)
						$item->tags[] = $tag->tag;
						
				$item->tags = implode(', ', $item->tags);
				$item->niceTags = self::niceTag($item->tags);
				$items[$i] = $item;
			}
		}
	}
	
	public static function niceTag($tag) {
		$tags = explode(',',$tag);
		
		foreach($tags as &$tag) {
			$tag = strtolower($tag);
			$tag = trim($tag);
			$tag = self::escape($tag);
			$tag = str_replace(' ','_',$tag);
			$tag = preg_replace('/[^\da-z\_]/i', '', $tag);
		}
		$tag = implode(' ',$tags);
		return $tag;
	}
	
	public static function setPredefinedEffect($filter, $filters, $arguments) {
		$output = new stdClass;
		$output->filters = array();
		$output->arguments = array();
		
		$actualFilters = array(
				'greyscale' => IMG_FILTER_GRAYSCALE,
				'colorize' => IMG_FILTER_COLORIZE,
				'brightness' => IMG_FILTER_BRIGHTNESS,
				'contrast' => IMG_FILTER_CONTRAST,
				'sketchy'=>IMG_FILTER_MEAN_REMOVAL
		);
		$tmpFiltersArray = array();
		foreach ($filters as $filt) {
			$tmpFiltersArray[$filt] = $actualFilters[$filt];
		}
		$tmpArgumentsArray = array();
		
		foreach ($arguments as $type => $argument) {
			foreach($argument as $val) {
				$tmpArgumentsArray[$type][] = $val;
			}
		}
		$output->filters[$filter] = $tmpFiltersArray;
		$output->arguments[$filter] = $tmpArgumentsArray;
		
		return $output;
	}
	
	public static function applyFilters($filters, $file, $path, $create = true, $resize = false, $res = null) {
		//$phpVersion = (int)str_replace('.','',phpversion());
		
		jimport('joomla.filesystem.file');
		
		$execute = true;
		$allowed_ext = array('jpeg','jpg','gif','png');
		$hashTemp = md5($file.$filters.$res);
		
		// verifications
		
			//check if file actually exist
			if ($execute) $execute = JFile::exists($path.$file);
			
			//check for file extension
			$ext = JFile::getExt($file);
			if ($execute) {
				$execute = in_array($ext, $allowed_ext);
			}
			
			// check if the file is actually an image
			if ($execute) {
				$info = getimagesize($path.$file);
				// no info can be retrieved - correct extension but not an image
				if (!$info) {
					$execute = false;
				}
			}
		
		/// check if the memory_usage of the php allows the use of the filters for this image
		$memoryNeeded = RSMediaGalleryHelper::checkMemoryUsage($path.$file,$ext);
		$execute = RSMediaGalleryHelper::changeMemoryUsage($memoryNeeded);
		
		// conversion
		if ($execute) {
			require_once JPATH_ADMINISTRATOR.'/components/com_rsmediagallery/helpers/phpthumb/phpthumb.functions.php';
			
			
			$output = false;
			$new_filters = array();
			$arguments = array();
			$filters = explode('-x-', $filters);
			
			$allowColorize = version_compare(PHP_VERSION, '5.2.5') >= 0;
			
			foreach($filters as $filter) {
				$arg = explode('-a-',$filter);
				$filter = $arg[0];
				switch($filter) {
					// simple filters
					case 'greyscale':
						$new_filters[$filter] = IMG_FILTER_GRAYSCALE;
					break;
					case 'negate':
						$new_filters[$filter] = IMG_FILTER_NEGATE;
					break;
					case 'sketchy':
						$new_filters[$filter] = IMG_FILTER_MEAN_REMOVAL;
					break;
					
					// arguments filters
					case 'brightness':
						$new_filters[$filter] = IMG_FILTER_BRIGHTNESS;
						foreach($arg as $i=>$val) {
							if ($i > 0) {
								$val = (int) $val;
								if($val < -255) $val = -255; // the lowest value
								if($val > 255) $val = 255; // the top value
								$arguments[$filter][] = $val;
							}
						}
						$nr_arguments = count($arguments[$filter]);
						if($nr_arguments < 1) $arguments[$filter][] = 0;
						else if($nr_arguments > 1) {
							$arguments[$filter] = array_slice($arguments[$filter], 0, 1);
						}
					break;
					case 'contrast':
						$new_filters[$filter] = IMG_FILTER_CONTRAST;
						foreach($arg as $i=>$val) {
							if ($i > 0) {
								$val = (int) $val;
								$val = -1 * $val;
								if($val < -100) $val = -100; // the lowest value
								if($val > 100) $val = 100; // the top value
								$arguments[$filter][] = $val;
							}
						}
						$nr_arguments = count($arguments[$filter]);
						if($nr_arguments < 1) $arguments[$filter][] = 0;
						else if($nr_arguments > 1) {
							$arguments[$filter] = array_slice($arguments[$filter], 0, 1);
						}
					break;
					
					// predefined effects
					case 'sepia':
						if($allowColorize){
							$settings = RSMediaGalleryHelper::setPredefinedEffect($filter,array('greyscale','colorize'), array('colorize'=>array(20,-5,-40,0)));
							$new_filters = $settings->filters;
							$arguments = $settings->arguments;
						}
					break;
					case 'amaro':
						if($allowColorize){
							$settings = RSMediaGalleryHelper::setPredefinedEffect($filter,array('contrast','colorize'), array('colorize'=>array(13,18,-7,0),'contrast'=>array(-11)));
							$new_filters = $settings->filters;
							$arguments = $settings->arguments;
						}
					break;
					case 'nashville':
						if($allowColorize){	
							$settings = RSMediaGalleryHelper::setPredefinedEffect($filter,array('brightness','contrast','colorize'), array('colorize'=>array(-7,9,21,0),'contrast'=>array(-14),'brightness'=>array(39)));
							$new_filters = $settings->filters;
							$arguments = $settings->arguments;
						}
					break;
					case 'seventyseven':
						if($allowColorize){
							$settings = RSMediaGalleryHelper::setPredefinedEffect($filter,array('brightness','contrast','colorize'), array('colorize'=>array(60,15,19,0),'contrast'=>array(17),'brightness'=>array(15)));
							$new_filters = $settings->filters;
							$arguments = $settings->arguments;
						}
					break;
					
					case 'overblown':
						if($allowColorize){
							$settings = RSMediaGalleryHelper::setPredefinedEffect($filter,array('sketchy','colorize'), array('colorize'=>array(130,-40,-50,24)));
							$new_filters = $settings->filters;
							$arguments = $settings->arguments;
						}
					break;
					case 'mist':
						if($allowColorize){
							$settings = RSMediaGalleryHelper::setPredefinedEffect($filter,array('greyscale','colorize','brightness','contrast'), array('colorize'=>array(-155,-115,-65,24), 'brightness'=>array(40), 'contrast'=>array(-5)));
							$new_filters = $settings->filters;
							$arguments = $settings->arguments;
						}
					break;
					case 'wornvintage':
						if($allowColorize){
							$settings = RSMediaGalleryHelper::setPredefinedEffect($filter,array('greyscale','colorize','contrast','brightness'), array('colorize'=>array(135,-10,-125,24), 'brightness'=>array(55), 'contrast'=>array(190)));
							$new_filters = $settings->filters;
							$arguments = $settings->arguments;
						}
					break;
					case 'deepfocus':
						if($allowColorize){
							$settings = RSMediaGalleryHelper::setPredefinedEffect($filter,array('greyscale','colorize','contrast','brightness'), array('colorize'=>array(-115,-10,-20,79), 'brightness'=>array(55), 'contrast'=>array(-10)));
							$new_filters = $settings->filters;
							$arguments = $settings->arguments;
						}
					break;
					case 'oldphoto':
						if($allowColorize){
							$settings = RSMediaGalleryHelper::setPredefinedEffect($filter,array('greyscale','colorize','brightness','contrast'), array('colorize'=>array(100,-15,-115,85), 'brightness'=>array(55), 'contrast'=>array(-10)));
							$new_filters = $settings->filters;
							$arguments = $settings->arguments;
							
							
						}
					break;
					case 'lavaland':
						if($allowColorize){
							$settings = RSMediaGalleryHelper::setPredefinedEffect($filter,array('greyscale','colorize','brightness','contrast'), array('colorize'=>array(130,25,-195,48), 'brightness'=>array(25), 'contrast'=>array(-10)));
							$new_filters = $settings->filters;
							$arguments = $settings->arguments;
							
							
						}
					break;
					case 'morning':
						if($allowColorize){
							$settings = RSMediaGalleryHelper::setPredefinedEffect($filter,array('greyscale','colorize','brightness','contrast'), array('colorize'=>array(-160,-220,-150,48), 'brightness'=>array(25), 'contrast'=>array(-10)));
							$new_filters = $settings->filters;
							$arguments = $settings->arguments;
						
							
						}
					break;
					case 'lifeonmars':
						if($allowColorize){
							$settings = RSMediaGalleryHelper::setPredefinedEffect($filter,array('greyscale','colorize'), array('colorize'=>array(190,25,-210,48)));
							$new_filters = $settings->filters;
							$arguments = $settings->arguments;
							
							
						}
					break;
					case 'diva':
						if($allowColorize){
							$settings = RSMediaGalleryHelper::setPredefinedEffect($filter,array('greyscale','colorize'), array('colorize'=>array(175,10,90,48)));
							$new_filters = $settings->filters;
							$arguments = $settings->arguments;
							
						}
					break;
					case 'coldbreeze':
						if($allowColorize){
							$settings = RSMediaGalleryHelper::setPredefinedEffect($filter,array('greyscale','colorize'), array('colorize'=>array(-130,35,90,48)));
							$new_filters = $settings->filters;
							$arguments = $settings->arguments;
							
							
						}
					break;
					case 'grounded':
						if($allowColorize){
							$settings = RSMediaGalleryHelper::setPredefinedEffect($filter,array('greyscale','colorize'), array('colorize'=>array(135,35,-30,48)));
							$new_filters = $settings->filters;
							$arguments = $settings->arguments;
							
							
						}
					break;
					case 'greenhornet':
						if($allowColorize){
							$settings = RSMediaGalleryHelper::setPredefinedEffect($filter,array('brightness','contrast','colorize'), array('colorize'=>array(-90,80,-10,48), 'brightness'=>array(-10), 'contrast'=>array(200)));
							$new_filters = $settings->filters;
							$arguments = $settings->arguments;
							
							
						}
					break;
					case 'stretch':
						if($allowColorize){
							$settings = RSMediaGalleryHelper::setPredefinedEffect($filter,array('brightness','contrast','colorize'), array('colorize'=>array(50,-5,-185,30), 'brightness'=>array(-10), 'contrast'=>array(200)));
							$new_filters = $settings->filters;
							$arguments = $settings->arguments;
							
							
						}
					break;
					case 'blueseasoning':
						if($allowColorize){
							$settings = RSMediaGalleryHelper::setPredefinedEffect($filter,array('brightness','contrast','colorize'), array('colorize'=>array(-25,100,140,30), 'brightness'=>array(-10), 'contrast'=>array(200)));
							$new_filters = $settings->filters;
							$arguments = $settings->arguments;
							
							
						}
					break;
					case 'hot':
						if($allowColorize){
							$settings = RSMediaGalleryHelper::setPredefinedEffect($filter,array('colorize','brightness','contrast'), array('colorize'=>array(120,55,40,20), 'brightness'=>array(-10), 'contrast'=>array(-10)));
							$new_filters = $settings->filters;
							$arguments = $settings->arguments;
							
							
						}
					break;
					case 'vintage':
						if($allowColorize){
							$settings = RSMediaGalleryHelper::setPredefinedEffect($filter,array('colorize','contrast','brightness'), array('colorize'=>array(60,60,60,-60), 'brightness'=>array(-10), 'contrast'=>array(-10)));
							$new_filters = $settings->filters;
							$arguments = $settings->arguments;
							
							
						}
					break;
					case 'light':
						if($allowColorize){
							$settings = RSMediaGalleryHelper::setPredefinedEffect($filter,array('brightness'), array('brightness'=>array(15)));
							$new_filters = $settings->filters;
							$arguments = $settings->arguments;
							
							
						}
					break;
					case 'highcontrast':
						if($allowColorize){
							$settings = RSMediaGalleryHelper::setPredefinedEffect($filter,array('contrast','brightness'), array('brightness'=>array(10), 'contrast'=>array(-20)));
							$new_filters = $settings->filters;
							$arguments = $settings->arguments;
							
							
						}
					break;
					
					
					case 'pixelate':
						if(version_compare(PHP_VERSION, '5.3.0') >= 0){
							$new_filters[$filter] = IMG_FILTER_PIXELATE;
							foreach($arg as $i=>$val) {
								if ($i > 0) {
									$val = (int)$val;
									if($val < 1) $val = 1; // the lowest value
									if($val > 10) $val = 10; // the top value
									$arguments[$filter][] = $val;
								}
							}
							$nr_arguments = count($arguments[$filter]);
							if($nr_arguments < 1) $arguments[$filter][] = 1;
							else if($nr_arguments > 1) {
								$arguments[$filter] = array_slice($arguments[$filter], 0, 1);
							}
						}
					break;
				}
				
			}
			
			if (!$create) {
				header('Content-Type: '.phpthumb_functions::ImageTypeToMIMEtype($ext));
				header('Content-Disposition: inline; filename="'.$file.'"');
				if ($resize) {
					$res = explode('x',$res);
					$w = $res[0];
					$h = $res[1];
				}
			}
			$type = $ext;
			if ($type == 'jpg') $type = 'jpeg';
			
			$im = call_user_func('imagecreatefrom'.$type,$path.$file);
			
			if($im)
			{
				$status_filter = true;
				foreach ($new_filters as $name=>$filter) {
					if($status_filter) {
						if(is_array($filter)) {
							foreach($filter as $nameFilter=>$actualFilter) {
								if (isset($arguments[$name][$nameFilter])) { 
									$build_arg = array($im,$actualFilter);
									$build_arg = array_merge($build_arg,$arguments[$name][$nameFilter]);
									$status_filter = call_user_func_array('imagefilter',$build_arg);
								}
								else {	
									$status_filter = imagefilter($im, $actualFilter);
								}
							}
						}
						else {
							if (isset($arguments[$name])) { 
								$build_arg = array($im,$filter);
								$build_arg = array_merge($build_arg,$arguments[$name]);
								$status_filter = call_user_func_array('imagefilter',$build_arg);
								
							}
							else {	
								
								$status_filter = imagefilter($im, $filter);
							}
						}
					}
				}
				
				
				if ($status_filter) {
				
					if ($create) {
							jimport('joomla.filesystem.folder');
							if(!file_exists($path.'filter/')) {
								JFolder::create($path.'filter/');
							}
							if(file_exists($path.'filter/'.$file)) {
								JFile::delete($path.'filter/'.$file);
							}
					}
					
					$old_w = imagesx($im);
					$old_h = imagesy($im);
					if(!$resize) {
						$w = $old_w;
						$h = $old_h;
					}
					$im2 = ImageCreateTrueColor($w, $h);
					
					if ($type == 'png') {
						imagesavealpha($im2, true);
						
						imagealphablending($im2, false);
						imagesavealpha($im2, true);
						$transparent = imagecolorallocatealpha($im2, 255, 255, 255, 127);
						imagefilledrectangle($im2, 0, 0, $w, $h, $transparent);
					}
					else if($type=='gif'){
						 $background = imagecolorallocate($im2, 0, 0, 0);
						  // removing the black from the placeholder
						  imagecolortransparent($im2, $background);
					}
					imagecopyResampled ($im2, $im, 0, 0, 0, 0, $w, $h, $old_w, $old_h);
					
					if (!$create) {
						$output = call_user_func('image'.$type,$im2);
					}
					else {
						call_user_func('image'.$type,$im2,$path.'filter/'.$file);	
						$output = true;
						@chmod($path.'filter/'.$file, 0644);
					}
					imagedestroy($im2);
				}
				else return false;
				
			}
			else
			{
				return false;
			}
			imagedestroy($im);
			return $output;
		}
		else return false;
	}
	
	public static function checkMemoryUsage($image,$ext) {
		$info = getimagesize($image);
		if($ext == 'png') {
			$memoryNeeded = Round(($info[0] * $info[1] * $info['bits'] / 8 + Pow(2, 16)) * 1.65);
		}
		else {
			$memoryNeeded = Round(($info[0] * $info[1] * $info['bits'] * $info['channels'] / 8 + Pow(2, 16)) * 1.65);
		}
		$memoryNeeded = ($memoryNeeded / 1024) / 1024;
		$memoryNeeded = round($memoryNeeded,2);
		return $memoryNeeded;
	}
	
	public static function changeMemoryUsage($memoryNeeded) {
		$memoryLimit  = (int)get_cfg_var('memory_limit');
		$changed = false;
		if(($memoryNeeded * 3) > $memoryLimit) {
			$memoryNeeded = (int)($memoryNeeded * 2);
			$setMemory = $memoryLimit + $memoryNeeded;
			if (@ini_set("memory_limit",$setMemory."M")) $changed = true;
		}
		else $changed = true;
		
		return $changed;
	}
}

class RSMediaGalleryRoute
{
	public static function _($url, $xhtml = true, $ssl = null)
	{
		if (strpos($url, 'Itemid=') === false)
		{			
			if ($Itemid = JRequest::getInt('Itemid'))
			{
				$Itemid = 'Itemid='.$Itemid;
				$url .= (strpos($url, '?') === false) ? '?'.$Itemid : '&'.$Itemid;
			}
			else
			{
				$app 	= JFactory::getApplication();
				$menu 	= $app->getMenu();
				if ($active = $menu->getActive())
				{
					$Itemid = 'Itemid='.$active->id;
					$url .= (strpos($url, '?') === false) ? '?'.$Itemid : '&'.$Itemid;
				}
			}
		}
		return JRoute::_($url, $xhtml, $ssl);
	}
}