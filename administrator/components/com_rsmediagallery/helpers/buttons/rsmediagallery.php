<?php
/**
* @package RSMediaGallery!
* @copyright (C) 2011-2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/licenses/gpl-3.0.html
*/

defined('_JEXEC') or die('Restricted access');

// Joomla! version check
$jversion = new JVersion();
$is30 	  = $jversion->isCompatible('3.0');

if ($is30) { // 3.0
	class RSToolbarButton extends JToolbarButton {
		public function fetchButton($type='', $name='', $suffix='', $task=null) {
			return parent::fetchButton($type, $name, $suffix, $task);
		}
	}
} else { // 2.5
	class RSToolbarButton extends JButton {
		public function fetchButton($type='', $name='', $suffix='', $task=null) {
			return parent::fetchButton($type, $name, $suffix, $task);
		}
	}
}

class RSButtonRSMediaGallery extends RSToolbarButton
{
	public function fetchButton($type='', $name='', $suffix='', $task=null)
	{
		$this->_name = $name;
		
		$html = '';
		$text  = JText::_('COM_RSMEDIAGALLERY_'.strtoupper(str_replace('-', '_', $name)));
		$class = $this->fetchIconClass($name);
		
		if (!$task)
			$task = $name;
		
		$html  = '<a class="rsmg_'.$name.$suffix.'" href="javascript: rsmg_submit(jQuery, \''.$task.'\');">'."\n";
		$html .= '<span>'."\n";
		$html .= "$text\n";
		$html .= '</span>'."\n";
		$html .= '</a>'."\n";

		return $html;
	}

	public function fetchId($type, $name)
	{
		return 'toolbar-'.$name;
	}
	
	public function render(&$definition)
	{
		/*
		 * Initialize some variables
		 */
		 
		$html = '';
		$type = $definition[1];
		switch ($type)
		{
			case 'toolbar-start':
				$html .= '<td>'."\n";
				$html .= '<div class="rsmg_toolbar" id="rsmg_toolbar'.$definition[2].'">'."\n";
				$html .= '<ul>'."\n";
			break;
			
			case 'toolbar-end':
				$html .= '</ul>'."\n";
				$html .= '</div>'."\n";
				$html .= '</td>'."\n";
			break;
			
			default:
				$action	= call_user_func_array(array(&$this, 'fetchButton'), $definition);

				// Build the HTML Button
				$html	.= "<li>\n";
				$html	.= $action;
				$html	.= "</li>\n";
			break;
		}

		return $html;
	}
}

if ($is30) { // 3.0
	class JToolbarButtonRSMediaGallery extends RSButtonRSMediaGallery {}
} else { // 2.5
	class JButtonRSMediaGallery extends RSButtonRSMediaGallery {}
}