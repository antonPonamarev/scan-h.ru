<?php
/**
* @package RSMediaGallery!
* @copyright (C) 2011-2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/licenses/gpl-3.0.html
*/

defined('_JEXEC') or die('Restricted access');

class RSMediaGalleryVersion
{
	public $version  = '1.7.6';
	public $key		 = 'MG7UP211H1';
	// Unused
	public $revision = null;
	
	// Get version
	public function __toString() {
		return $this->version;
	}
	
	// Legacy, keep revision
	public function __construct() {
		list($j, $revision, $bugfix) = explode('.', $this->version);
		$this->revision = $revision;
	}
}

$version = new RSMediaGalleryVersion();
define('_RSMEDIAGALLERY_VERSION', $version->revision);