<?php
/**
* @package RSMediaGallery!
* @copyright (C) 2011-2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/licenses/gpl-3.0.html
*/

defined('_JEXEC') or die('Restricted access');

if (!class_exists('RSMediaGalleryHelper'))
	require_once dirname(__FILE__).'/helper.php';
if (!class_exists('RSMediaGalleryjQuery'))
	require_once dirname(__FILE__).'/jquery.php';

class RSMediaGalleryIntegration
{
	protected $index 		= 0;
	protected $initialized	= false;
	protected $namespace 	= 'default';

	public static function getInstance() {
		static $inst;
		
		if (empty($inst))
			$inst = new RSMediaGalleryIntegration();
		
		return $inst;
	}

	protected function addScripts() {
		$jqueryHelper 	 = RSMediaGalleryjQuery::getInstance();
		$document 		 = JFactory::getDocument();
		
		$src = JDEBUG ? '.src' : '';
		
		// JS
		$jqueryHelper->addjQuery();
		// no longer needed
		//$document->addScript(JURI::root(true).'/components/com_rsmediagallery/assets/js/jquery.ui'.$src.'.js');
		$document->addScriptDeclaration("jQuery.noConflict();");
		$document->addScript(JURI::root(true).'/components/com_rsmediagallery/assets/integration/js/jquery.pirobox'.$src.'.js');
		$document->addScript(JURI::root(true).'/components/com_rsmediagallery/assets/integration/js/jquery.script'.$src.'.js');
	}

	protected function addStyleSheets() {
		jimport('joomla.application.component.helper');
		
		$document 		 = JFactory::getDocument();
		$componentParams = JComponentHelper::getParams('com_rsmediagallery');

		$document->addStyleSheet(JURI::root(true).'/components/com_rsmediagallery/assets/integration/css/lightbox.css');
		$document->addStyleSheet(JURI::root(true).'/components/com_rsmediagallery/assets/integration/css/'.$componentParams->get('contrast', 'light').'.css');
	}

	protected function addInitScript() {
		$document = JFactory::getDocument();
		$document->addScriptDeclaration("jQuery(document).ready(function($) { rsmg_init_gallery($, '".$this->namespace."'); });");
	}

	protected function addOwnStyleSheet() {
		// this makes sense only when the namespace is 'default'
		// namespacing allows you to define your own layout when showing the gallery with your own stylesheet
		if ($this->namespace == 'default') {
			$document = JFactory::getDocument();
			$document->addStyleSheet(JURI::root(true).'/components/com_rsmediagallery/assets/integration/css/listing.css');
		}
	}

	/* @param mixed $tags 		can be either array eg. array('tag1', 'tag2') or a string with comma separated values eg. 'tag1, tag2' *
	 * @param mixed $options 	array or object of options */
	public function display($tags, $options=array(), $namespace='default') {
		$html = '';
		$this->namespace = preg_replace('/(\s|[^A-Za-z0-9\-])+/', '_', $namespace);

		$params = new JObject();
		if (is_array($options)) {
			foreach ($options as $option => $value) {
				$params->set($option, $value);
			}
		} elseif (is_object($options)) {
			// already a JParameter or JObject or JRegistry?
			if (!is_callable(array($options, 'get'))) {
				foreach (get_object_vars($options) as $option => $value) {
					$params->set($option, $value);
				}
			} else {
				$params = $options;
			}
		}
		
		// convert thumb_width & thumb_height
		if (!$params->get('thumb_resolution')) {
			if ($params->get('thumb_width')) {
				$params->set('thumb_resolution', 'w,'.$params->get('thumb_width'));
			} elseif ($params->get('thumb_height')) {
				$params->set('thumb_resolution', 'h,'.$params->get('thumb_height'));
			} else {
				$params->set('thumb_resolution', 'w,200');
			}
		}
		// same for full_width & full_height
		if (!$params->get('full_resolution')) {
			if ($params->get('full_width')) {
				$params->set('full_resolution', 'w,'.$params->get('full_width'));
			} elseif ($params->get('full_height')) {
				$params->set('full_resolution', 'h,'.$params->get('full_height'));
			} else {
				$params->set('full_resolution', 'w,600');
			}
		}
		
		$params->set('thumb_width', 0);
		$params->set('thumb_height', 0);
		
		$params = RSMediaGalleryHelper::parseParams($params);
		
		$order 				= $params->get('ordering', 'ordering');
		$direction 			= $params->get('direction', 'ASC');
		$limitstart 		= (int) $params->get('limitstart', 0);
		$limit				= (int) $params->get('limit', 0);
		$show_title			= (int) $params->get('show_title', 1);
		$show_description 	= (int) $params->get('show_description', 1);
		$use_original 		= (int) $params->get('use_original', 0);
		$image				= (int) $params->get('image', 0);

		if (!empty($tags) && $items = RSMediaGalleryHelper::getItems($tags, $order, $direction, 0, $limit)) {
			if (!$this->initialized) {
				$this->addScripts();
				$this->addStyleSheets();
				$this->initialized = true;
			}
			
			$this->addOwnStyleSheet();
			$this->addInitScript();
			
			$html .= '<ul class="rsmg_'.$this->namespace.'_gallery">';
			foreach ($items as $i => $item) {
				$item 			= RSMediaGalleryHelper::parseItem($item, $params);
				
				$small_image 	= $item->thumb;
				$big_image   	= $item->full;
				$thumb_width 	= $item->thumb_width;
				$thumb_height 	= $item->thumb_height;
				
				$title			= '';
				if ($show_title || $show_description) {
					if ($show_title) {
						$title .= '<b>'.$item->title.'</b>';
					}
					if ($show_description) {
						$title .= ($show_title ? '<br />' : '').$item->full_description;
					}
					$title = ' title="'.$this->escape($title).'"';
				}
				
				$html .= (!$image || $image && $i+1 == $image) ? '<li>' : '<li style="display: none;">';
				$html .= '<div class="rsmg_'.$this->namespace.'_container">';
				$html .= '<a href="'.$big_image.'" rel="gallery" class="pirobox_gall_'.$this->namespace.''.$this->index.'"'.$title.'><img src="'.$small_image.'" style="width: '.$thumb_width.'px; height: '.$thumb_height.'px;" width="'.$thumb_width.'" height="'.$thumb_height.'" alt="'.$this->escape($item->title).'" /></a>';
				$html .= '</div>';
				$html .= '</li>';
			}
			$html .= '</ul>';
			$html .= '<span class="rsmg_'.$this->namespace.'_clear"></span>';
			
			$this->index++;
		}
		
		return $html;
	}

	protected function escape($string) {
		return RSMediaGalleryHelper::escape($string);
	}

	public function getTagsList() {
		$db 	= JFactory::getDBO();
		$query 	= $db->getQuery(true);
		
		$query->select('DISTINCT('.$db->qn('tag').')')
			  ->from($db->qn('#__rsmediagallery_tags'))
			  ->order($db->qn('tag').' '.$db->escape('asc'));
		
		$db->setQuery($query);
		return $db->loadColumn();
	}
}