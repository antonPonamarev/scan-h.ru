<?php
defined('_JEXEC') or die;

/**
 * Template for Joomla! CMS, created with Artisteer.
 * See readme.txt for more details on how to use the template.
 */

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'functions.php';

// Create alias for $this object reference:
$document = $this;

// Shortcut for template base url:
$templateUrl = $document->baseurl . '/templates/' . $document->template;

Artx::load("Artx_Page");

// Initialize $view:
$view = $this->artx = new ArtxPage($this);

// Decorate component with Artisteer style:
$view->componentWrapper();

JHtml::_('behavior.framework', true);

?>
<!DOCTYPE html>
<html dir="ltr" lang="<?php echo $document->language; ?>">
<head>
    <jdoc:include type="head" />
    <link rel="stylesheet" href="<?php echo $document->baseurl; ?>/templates/system/css/system.css" />
    <link rel="stylesheet" href="<?php echo $document->baseurl; ?>/templates/system/css/general.css" />

    <!-- Created by Artisteer v4.2.0.60623 -->
    
    

    <!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <link rel="stylesheet" href="<?php echo $templateUrl; ?>/css/template.css" media="screen" type="text/css" />
    <!--[if lte IE 7]><link rel="stylesheet" href="<?php echo $templateUrl; ?>/css/template.ie7.css" media="screen" /><![endif]-->

<link rel="shortcut icon" href="<?php echo $templateUrl; ?>/favicon.ico" type="image/x-icon" />
    <script>if ('undefined' != typeof jQuery) document._artxJQueryBackup = jQuery;</script>
    <script src="<?php echo $templateUrl; ?>/jquery.js"></script>
    <script>jQuery.noConflict();</script>

    <script src="<?php echo $templateUrl; ?>/script.js"></script>
    <script src="<?php echo $templateUrl; ?>/modules.js"></script>
    <?php $view->includeInlineScripts() ?>
    <script>if (document._artxJQueryBackup) jQuery = document._artxJQueryBackup;</script>
</head>
<body>

<div id="gidra-main">
<header class="gidra-header"><?php echo $view->position('position-30', 'gidra-nostyle'); ?>

    <div class="gidra-shapes">
        
            </div>


<div class="gidra-positioncontrol gidra-positioncontrol-1851382471" id="CONTROL-ID">
<?php echo $view->position('position-31', 'gidra-nostyle'); ?></div>
<div class="gidra-positioncontrol gidra-positioncontrol-1992536674" id="CONTROL-ID-1">
<?php echo $view->position('position-32', 'gidra-nostyle'); ?></div>
<div class="gidra-positioncontrol gidra-positioncontrol-1243119808" id="CONTROL-ID-3">
<?php echo $view->position('position-33', 'gidra-nostyle'); ?></div>


<a href="http://www.scan-h.ru/" class="gidra-logo gidra-logo-1818670524">
    <img src="<?php echo $templateUrl; ?>/images/logo-1818670524.png" alt="" />
</a>
<div class="gidra-textblock gidra-object166416295">
    <form class="gidra-search" name="Search" action="<?php echo $document->baseurl; ?>/index.php" method="post">
    <input type="text" value="" name="searchword" />
        <input type="hidden" name="task" value="search" />
<input type="hidden" name="option" value="com_search" />
<input type="submit" value="" name="search" class="gidra-search-button" />
        </form>
</div>
                        
                    
</header>
<?php if ($view->containsModules('position-1', 'position-28', 'position-29')) : ?>
<nav class="gidra-nav">
    <div class="gidra-nav-inner">
    
<?php if ($view->containsModules('position-28')) : ?>
<div class="gidra-hmenu-extra1"><?php echo $view->position('position-28'); ?></div>
<?php endif; ?>
<?php if ($view->containsModules('position-29')) : ?>
<div class="gidra-hmenu-extra2"><?php echo $view->position('position-29'); ?></div>
<?php endif; ?>
<?php echo $view->position('position-1'); ?>
 
        </div>
    </nav>
<?php endif; ?>
<div class="gidra-sheet clearfix">
            <?php echo $view->position('position-15', 'gidra-nostyle'); ?>
<?php echo $view->positions(array('position-16' => 33, 'position-17' => 33, 'position-18' => 34), 'gidra-block'); ?>
<div class="gidra-layout-wrapper">
                <div class="gidra-content-layout">
                    <div class="gidra-content-layout-row">
                        <?php if ($view->containsModules('position-7', 'position-4', 'position-5')) : ?>
<div class="gidra-layout-cell gidra-sidebar1">
<?php echo $view->position('position-7', 'gidra-block'); ?>
<?php echo $view->position('position-4', 'gidra-block'); ?>
<?php echo $view->position('position-5', 'gidra-block'); ?>




                        </div>
<?php endif; ?>

                        <div class="gidra-layout-cell gidra-content">
<?php
  echo $view->position('position-19', 'gidra-nostyle');
  if ($view->containsModules('position-2'))
    echo artxPost($view->position('position-2'));
  echo $view->positions(array('position-20' => 50, 'position-21' => 50), 'gidra-article');
  echo $view->position('position-12', 'gidra-nostyle');
  echo artxPost(array('content' => '<jdoc:include type="message" />', 'classes' => ' gidra-messages'));
  echo '<jdoc:include type="component" />';
  echo $view->position('position-22', 'gidra-nostyle');
  echo $view->positions(array('position-23' => 50, 'position-24' => 50), 'gidra-article');
  echo $view->position('position-25', 'gidra-nostyle');
?>



                        </div>
                    </div>
                </div>
            </div>
<?php echo $view->positions(array('position-9' => 33, 'position-10' => 33, 'position-11' => 34), 'gidra-block'); ?>
<?php echo $view->position('position-26', 'gidra-nostyle'); ?>


    </div>
<footer class="gidra-footer">
  <div class="gidra-footer-inner">
<div class="gidra-content-layout">
    <div class="gidra-content-layout-row">
    <div class="gidra-layout-cell" style="width: 33%">
<?php if ($view->containsModules('position-34')) : ?>
    <?php echo $view->position('position-34', 'gidra-nostyle'); ?>
<?php else: ?>
        <p><br /></p>
        <p><br /></p>
    <?php endif; ?>
</div><div class="gidra-layout-cell" style="width: 34%">
<?php if ($view->containsModules('position-35')) : ?>
    <?php echo $view->position('position-35', 'gidra-nostyle'); ?>
<?php else: ?>
        <p><br /></p>
    <?php endif; ?>
</div><div class="gidra-layout-cell" style="width: 33%">
<?php if ($view->containsModules('position-36')) : ?>
    <?php echo $view->position('position-36', 'gidra-nostyle'); ?>
<?php else: ?>
        <p><br /></p>
    <?php endif; ?>
</div>
    </div>
</div>

  </div>
</footer>

</div>


<?php echo $view->position('debug'); ?>
</body>
</html>