$(document).ready(function(){
   $(".region-spoiler-title").click(function(){
       $(".region-spoiler-content").slideToggle();
       $(this).toggleClass("region-open-spoiler");
    
    })
    
       $(".region-link").click(function(){


           $("body").addClass("show-region-modal");
           $(".region-wrap-include").removeClass("region-wrap-hide");
        });
        
       $(document).on("click",".region-wrap-include .close-btn, .region-wrap-include .shadow",function(){
           $("body").removeClass("show-region-modal");
           $(".region-wrap-include").addClass("region-wrap-hide");
        })

          // Forms
 $("#callback").iziModal({
    title: 'Заказать обратный звонок',
    subtitle: 'Перезвоним как можно скорее',
    headerColor: '#2b2b2b',
    icon: null,
    background: null,
    iconText: null,
    /*iconColor: '',*/
    rtl: false,
    width: 600,
    top: null,
    bottom: null,
    borderBottom: true,
    padding: 18,
    radius: 0,
    /*fullscreen: true,*/
    closeOnEscape: true,
    closeButton: true,
    zindex: 999,
  });
 $("#modal-success").iziModal({
    title: "Ваше сообщение отправлено",
    subtitle: "Мы обязательно с Вами свяжемся",
    icon: null,
    headerColor: '#00af66',
    width: 600,
    timeout: 4000,
    timeoutProgressbar: true,
    transitionIn: 'fadeInUp',
    transitionOut: 'fadeOutDown',
    bottom: 0,
    loop: true,
    pauseOnHover: true
  });
 $("#modal-error").iziModal({
    title: "Произошла ошибка",
    subtitle: "Попробуйте отправить ваще обращение снова",
    icon: null,
    headerColor: '#BE0F18',
    width: 600,
    timeout: 4000,
    timeoutProgressbar: true,
    transitionIn: 'fadeInUp',
    transitionOut: 'fadeOutDown',
    bottom: 0,
    loop: true,
    pauseOnHover: true
  });
})