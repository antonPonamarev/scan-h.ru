<?php
defined('_JEXEC') or die;

/**
 * Template for Joomla! CMS, created with Artisteer.
 * See readme.txt for more details on how to use the template.
 */
 
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'functions.php';

// Create alias for $this object reference:
$document = $this;

// Shortcut for template base url:
$templateUrl = $document->baseurl . '/templates/' . $document->template;

Artx::load("Artx_Page");

// Initialize $view:
$view = $this->artx = new ArtxPage($this);

// Decorate component with Artisteer style:
$view->componentWrapper();

JHtml::_('behavior.framework', true);
global $_REGION;
?>
<!DOCTYPE html>
<html dir="ltr" lang="<?php echo $document->language; ?>">
<head>
    <jdoc:include type="head" />
    <link rel="stylesheet" href="<?php echo $document->baseurl; ?>/templates/system/css/system.css" />
    <link rel="stylesheet" href="<?php echo $document->baseurl; ?>/templates/system/css/general.css" />
    <link rel="stylesheet" href="<?php echo $document->baseurl; ?>/templates/gidravlika2019/css/lb.css" />
    <link rel="stylesheet" href="<?php echo $document->baseurl; ?>/templates/gidravlika2019/css/izimodal.css" />
    <!-- Created by Artisteer v4.2.0.60623 -->
    
    

    <!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <link rel="stylesheet" href="<?php echo $templateUrl; ?>/css/template.css" media="screen" type="text/css" />
    <!--[if lte IE 7]><link rel="stylesheet" href="<?php echo $templateUrl; ?>/css/template.ie7.css" media="screen" /><![endif]-->

<link rel="shortcut icon" href="<?php echo $templateUrl; ?>/favicon.ico" type="image/x-icon" />
    <script>if ('undefined' != typeof jQuery) document._artxJQueryBackup = jQuery;</script>
    <!-- <script src="<?php echo $templateUrl; ?>/jquery.js"></script> -->
 
    <script>jQuery.noConflict();</script>

    <script src="<?php echo $templateUrl; ?>/script.js"></script>
    <script src="<?php echo $templateUrl; ?>/modules.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="<?php echo $templateUrl; ?>/izimodal.js"></script>
    <script src="<?php echo $templateUrl; ?>/valid.js"></script>

    <?php $view->includeInlineScripts() ?>
    <script>if (document._artxJQueryBackup) jQuery = document._artxJQueryBackup;</script>
    <meta name="yandex-verification" content="36361bff58f2e0b8" />
<meta name="google-site-verification" content="VMjebd7tLcwEZ76dKV_CuvMkbqJZ4MV9JgGFwYViKeI" />


    <!-- Yandex.Metrika counter --> <script type="text/javascript" > (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(56240416, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); </script> <noscript><div><img src="https://mc.yandex.ru/watch/56240416" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
</head>
<body>

<div id="gidra-main">
<header class="gidra-header"><?php echo $view->position('position-30', 'gidra-nostyle'); ?>

    <div class="gidra-shapes">
        
            </div>

<?if($_REGION["CODE"] == "sankt-peterburg"):?>
<div class="gidra-positioncontrol gidra-positioncontrol-1851382471" id="CONTROL-ID">
<?php echo $view->position('position-31', 'gidra-nostyle'); ?></div>
<?endif;?>
<div class="gidra-positioncontrol gidra-positioncontrol-1992536674" id="CONTROL-ID-1">
<?php echo $view->position('position-32', 'gidra-nostyle'); ?></div>
<?if($_REGION["CODE"] == "sankt-peterburg"):?>
<div class="gidra-positioncontrol gidra-positioncontrol-1243119808" id="CONTROL-ID-3">
<?php echo $view->position('position-33', 'gidra-nostyle'); ?></div>
<div class="gidra-positioncontrol gidra-positioncontrol-645538716" id="CONTROL-ID-2">
<?php echo $view->position('position-34', 'gidra-nostyle'); ?></div>
<?endif;?>
<div class="gidra-positioncontrol gidra-positioncontrol-944710001" id="CONTROL-ID-4">
<?php echo $view->position('position-35', 'gidra-nostyle'); ?>
</div>


<span class="gidra-logo gidra-logo-1818670524">
    <a href="http://www.scan-h.ru/" >
        <img src="<?php echo $templateUrl; ?>/images/logo-1818670524.png" alt="" />
       
    </a>
    <a href="#" class="region-link">г. [%TOWN%]</a>
</span>

                        
                    
</header>
<?php if ($view->containsModules('position-1', 'position-28', 'position-29')) : ?>
<nav class="gidra-nav">
    <div class="gidra-nav-inner">
    
<?php if ($view->containsModules('position-28')) : ?>
<div class="gidra-hmenu-extra1"><?php echo $view->position('position-28'); ?></div>
<?php endif; ?>
<?php if ($view->containsModules('position-29')) : ?>
<div class="gidra-hmenu-extra2"><?php echo $view->position('position-29'); ?></div>
<?php endif; ?>
<?php echo $view->position('position-1'); ?>
 
        </div>
    </nav>
<?php endif; ?>
<div class="gidra-sheet clearfix">
            <?php echo $view->position('position-15', 'gidra-nostyle'); ?>
<?php echo $view->positions(array('position-16' => 33, 'position-17' => 33, 'position-18' => 34), 'gidra-block'); ?>
<div class="gidra-layout-wrapper">
                <div class="gidra-content-layout">
                    <div class="gidra-content-layout-row">
                        <?php if ($view->containsModules('position-7', 'position-4', 'position-5')) : ?>
<div class="gidra-layout-cell gidra-sidebar1">
<?php echo $view->position('position-7', 'gidra-block'); ?>
<?php echo $view->position('position-4', 'gidra-block'); ?>
<?php echo $view->position('position-5', 'gidra-block'); ?>




                        </div>
<?php endif; ?>

                        <div class="gidra-layout-cell gidra-content">
<?php
  echo $view->position('position-19', 'gidra-nostyle');
  if ($view->containsModules('position-2'))
    echo artxPost($view->position('position-2'));
  echo $view->positions(array('position-20' => 50, 'position-21' => 50), 'gidra-article');
  echo $view->position('position-12', 'gidra-nostyle');
  echo artxPost(array('content' => '<jdoc:include type="message" />', 'classes' => ' gidra-messages'));
  echo '<jdoc:include type="component" />';

  if($_REGION["CODE"] != "sankt-peterburg" && $_SERVER["REQUEST_URI"] == "/kontakty"){
?>
 <div id="YMapsID-renostart-rostov" style="width:100%; height:420px; box-shadow: 1px 1px 3px #9f9f9f; "> </div>
    <script type="text/javascript" src="https://api-maps.yandex.ru/2.0/?coordorder=longlat&load=package.full&lang=ru-RU&onload=ymap_renostart_rostov_fid"></script> <script type="text/javascript">
     function ymap_renostart_rostov_fid(ymaps) {
        var contactCoords = [[[%MAP_COORDS%]]];
        var map = new ymaps.Map("YMapsID-renostart-rostov", {center: contactCoords[0], zoom: 9, type: "yandex#map"});map.controls.add("zoomControl").add("mapTools").add(new ymaps.control.TypeSelector(["yandex#map", "yandex#satellite", "yandex#hybrid", "yandex#publicMap"]));
        var objB1 = new ymaps.Placemark(contactCoords[0],  {balloonContent: "<b>СЕРВИС ШЛАНГОВ</b><br />"}, { preset: "twirl#darkgreenDotIcon", iconImageSize: [42, 51],                     iconImageOffset: [-10, -58],iconLayout: "default#image", balloonShadow: true });  map.geoObjects.add(objB1); //objB1.balloon.open();
};</script>

<?
  }
  else{
  echo $view->position('position-22', 'gidra-nostyle');
  echo $view->positions(array('position-23' => 50, 'position-24' => 50), 'gidra-article');
  echo $view->position('position-25', 'gidra-nostyle');
  }
 
?>



                        </div>
                    </div>
                </div>
            </div>
<?php echo $view->positions(array('position-9' => 33, 'position-10' => 33, 'position-11' => 34), 'gidra-block'); ?>
<?php echo $view->position('position-26', 'gidra-nostyle'); ?>


    </div>
<footer class="gidra-footer">
  <div class="gidra-footer-inner">
  <div class="gidra-content-layout">
    <div class="gidra-content-layout-row">
    <div class="gidra-layout-cell" style="width: 100%">
    <div class="region-spoiler-wrap" >
			<div class="region-spoiler-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							Регионы <img src="/images/down-arrow.png" alt="Регионы">
						</div>
					</div>
				</div>
			</div>
			<div class="region-spoiler-content" >
				<br>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
                            <?global $arRegions;?>
							<ul>
								
                            <?foreach($arRegions as $region):?>
        <li>
        <?if($region["CODE"] == $_REGION["CODE"]):?>
        <b><?=$region["NAME"]?></b>
    <?else:?>
          <a href="https://<?=$region["CODE"]?>.scan-h.ru/"><?=$region["NAME"]?></a>
    <?endif;?>
        </li>
    <?endforeach;?>
		
		
													</ul>
							<br>
						</div>
					</div>
				</div>
			</div>
		
		</div>


</div>
    </div>
</div>

<div class="gidra-content-layout">
    <div class="gidra-content-layout-row">
    <div class="gidra-layout-cell" style="width: 33%">
<?php if ($view->containsModules('position-36')) : ?>
    <?php echo $view->position('position-36', 'gidra-nostyle'); ?>
<?php else: ?>
        <p><br /></p>
        <p><br /></p>
    <?php endif; ?>
</div><div class="gidra-layout-cell" style="width: 34%">
<?php if ($view->containsModules('position-37')) : ?>
    <?php echo $view->position('position-37', 'gidra-nostyle'); ?>
<?php else: ?>
        <p><br /></p>
    <?php endif; ?>
</div><div class="gidra-layout-cell" style="width: 33%">
<?php if ($view->containsModules('position-38')) : ?>
    <?php echo $view->position('position-38', 'gidra-nostyle'); ?>
<?php else: ?>
        <p><br /></p>
    <?php endif; ?>
</div>
    </div>
</div>

  </div>
</footer>

</div>
<script src="<?php echo $templateUrl; ?>/lb.js"></script>

<?php echo $view->position('debug'); ?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(52847773, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/52847773" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->







            
<div class="region-wrap-include region-wrap region-wrap-hide">
    <div class="form-region-wrap">
        <div class="form-region">
           
                <div class="title">
                    <p>Все регионы</p>
                </div>
                <div class="close-btn">
                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 47.971 47.971" style="enable-background:new 0 0 47.971 47.971;" xml:space="preserve">
<g>
	<path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88
		c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242
		C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879
		s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"/>
</g>
</svg>
                
                </div>
                <div class="search-input">
                    <hr>
                </div>
                <div class="region-list">
				
                    <ul>

                    <?foreach($arRegions as $region):?>
        <li>
        <?if($region["CODE"] == $_REGION["CODE"]):?>
        <b><?=$region["NAME"]?></b>
    <?else:?>
          <a href="http://<?=$region["CODE"]?>.scan-h.ru/"><?=$region["NAME"]?></a>
    <?endif;?>
        </li>
    <?endforeach;?>
                    </ul>
                </div>
            
        </div>
    </div>

</div>

<div class="region-shadow"></div>


<div id="callback" class="black">
        <form class="ajax-form-callback" action="ajax/send.php" method="POST" data-goal="sendTopForm">
            <input type="text" required="" placeholder="Имя" name="name">
            <div class="form__error"></div>
<!--            <input type="text" required="" placeholder="Телефон" name="phn">-->
              <input required="" placeholder="Телефон" name = "phn" type="text" class="validate phone">
            <div class="form__error" ></div>
            <input type="hidden" name="source" value="">
            <input class="hide" type="text" name="phone">
          
            <p class="text-center small">Нажимая на кнопку, вы соглашаетесь с <a href="/agreement/" class="underline">политикой конфиденциальности</a></p><br>
            
            <input type="submit" class="btn button waves-effect waves-light text-uppercase" value="Отправить">
        </form>
    </div>
             
</body>
</html>