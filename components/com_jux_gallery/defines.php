<?php
/**
 * @version		$Id$
 * @author		JoomlaUX
 * @package		Joomla.Site
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/********************************************************************
 * DEFINE CONSTANTS.
 *******************************************************************/

define('JUX_GALLERY_COM_PATH',	JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_jux_gallery');

/********************************************************************
 * LOAD LIBRARIES.
 *******************************************************************/

require_once(JUX_GALLERY_COM_PATH.DIRECTORY_SEPARATOR.'libraries'.DIRECTORY_SEPARATOR.'factory.php');
//require_once(JUX_GALLERY_COM_PATH.DIRECTORY_SEPARATOR.'libraries'.DIRECTORY_SEPARATOR.'route.php');
require_once(JUX_GALLERY_COM_PATH.DIRECTORY_SEPARATOR.'libraries'.DIRECTORY_SEPARATOR.'template.php');
require_once(JUX_GALLERY_COM_PATH.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'jux_gallery.php');

jimport('joomla.utilities.utility');
jimport('joomla.application.component.model');
JModelLegacy::addIncludePath(JUX_GALLERY_COM_PATH.DIRECTORY_SEPARATOR.'models');