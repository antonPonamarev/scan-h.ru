<?php

/**
 * @version	$Id$
 * @author	JoomlaUX
 * @package	Joomla.Site
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license	http://www.gnu.org/licenses/gpl.html
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

// Include dependancies
jimport('joomla.application.component.controller');

require_once(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'defines.php');
//load jQuery
$app = JFactory::getApplication();
$document = JFactory::getDocument();
$params = $app->getParams();

$jQueryHandling = $params->get('jQueryHandling', '1.8remote');
$loadjQuery = $params->get('load_jquery',1);
$loadjQueryNoconflict = $params->get('load_jquery_noconflict',1);

// add assets
//jux_galleryTemplate::addStyleSheet('magnific-popup');
jux_galleryTemplate::addStyleSheet('jquery.justifiedgallery');
jux_galleryTemplate::addStyleSheet('buttons');

//Check loaded Jquery from Joomla 3.0
JLoader::import('joomla.version');
$version = new JVersion();
if (version_compare($version->RELEASE, '2.5', '<='))
{
    if (JFactory::getApplication()->get('jquery') !== true)
    {
	// load jQuery here
	JFactory::getApplication()->set('jquery', true);
    }
} else
{
    JHtml::_('jquery.framework');
}

//Check loaded Script from JUX_Portfolio
if (!defined("JUX_Gallery_Script"))
{
    if ($loadjQuery == 1)
    {
	if ($jQueryHandling && strpos($jQueryHandling, 'remote') == true)
	{
	    $document->addScript('http://ajax.googleapis.com/ajax/libs/jquery/' . str_replace('remote', '', $jQueryHandling) . '/jquery.min.js');
	} elseif ($jQueryHandling && strpos($jQueryHandling, 'remote') == false)
	{
	    jux_galleryTemplate::addScript('jquery-' . $jQueryHandling . '.min');
	}
    }
//    if($loadjQueryNoconflict == 1){
//	jux_galleryTemplate::addScript('jquery.noconflict');
//    }
    
    jux_galleryTemplate::addScript('jquery.magnific-popup');
    jux_galleryTemplate::addScript('modernizr.custom');
    jux_galleryTemplate::addScript('jquery.justifiedgallery');
//    jux_galleryTemplate::addScript('jquery.justifiedgallery_2');

    define("JUX_Gallery_Script", 1);
}

// set the table directory
JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'tables');
JHtml::addIncludePath(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'helpers');

// require the base controller
require_once(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'controller.php');

// Get an instance of the controller prefixed by jux_gallery
$controller = JControllerLegacy::getInstance('jux_gallery');

// Perform the Request task
$controller->execute(JRequest::getCmd('task'));

// Redirect if set by the controller
$controller->redirect();
