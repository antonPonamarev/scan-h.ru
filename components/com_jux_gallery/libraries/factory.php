<?php

/**
 * @version	$Id$
 * @author	JoomlaUX
 * @package	Joomla.Site
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license	http://www.gnu.org/licenses/gpl.html
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

/**
 * jux_gallery Component - Factory Library
 * 
 * @package	Joomla.Site
 * @subpackage	com_jux_gallery
 */
class JUX_GalleryFactory
{

    /**
     * Get model of component
     */
    public function &getModel($type, $prefix = 'JUX_GalleryModel', $config = array())
    {
	static $instance;

	if (!isset($instance[$type]) || !is_object($instance[$type]))
	{
	    $instance[$type] = JModelLegacy::getInstance($type, $prefix, $config);
	}

	return $instance[$type];
    }

}
