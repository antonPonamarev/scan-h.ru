<?php

/**
 * @version		$Id$
 * @author		JoomlaUX
 * @package		Joomla.Site
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * jux_portfolio Component - Template Library
 * 
 * @package		Joomla.Site
 * @subpackage	com_jux_gallery
 */
class JUX_GalleryTemplate extends JObject
{

    /** @var array Variables of template */
    var $_vars = null;

    /** @var string Template file */
    var $_file = null;

    /** @var string Template layout */
    var $_layout = null;

    /**
     * Callback for escaping.
     *
     * @var string
     * @access private
     */
    var $_escape = 'htmlspecialchars';

    /**
     * Charset to use in escaping mechanisms; defaults to urf8 (UTF-8)
     *
     * @var string
     * @access private
     */
    var $_charset = 'UTF-8';

    /**
     * Constructor
     */
    public function __construct($file = null, $layout = null)
    {
	$this->_file = $file;
	$this->_layout = $layout;
	$this->baseurl = JURI::base(true);
    }

    /**
     * Method to get template full path name.
     */
    function _getTemplateFullPath($file, $layout = null)
    {
	$mainframe = JFactory::getApplication();
//		$config		= &jux_portfolioFactory::getConfigs();
	$config = JComponentHelper::getParams('com_jux_gallery');

	jimport('joomla.filesystem.file');
	jimport('joomla.filesystem.folder');

	$file_name = $file;

	if (isset($layout) && $layout != 'default')
	{
	    $arr = explode('.', $file_name);
	    $arr[0] .= '_' . $layout;
	    $file_name = implode('.', $arr);
	}
	// test if template override exists in joomla's template folder
	$override_path = JPATH_ROOT . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . $mainframe->getTemplate() . DIRECTORY_SEPARATOR . 'html';
	$override_exists = JFolder::exists($override_path . DIRECTORY_SEPARATOR . 'com_jux_gallery');
	$template = JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . $config->get('template') . DIRECTORY_SEPARATOR . $file_name . '.php';

	// test override path first
	if (JFile::exists($override_path . DIRECTORY_SEPARATOR . 'com_jux_gallery' . DIRECTORY_SEPARATOR . $file_name . '.php'))
	{
	    // load the override template
	    $file = $override_path . DIRECTORY_SEPARATOR . 'com_jux_gallery' . DIRECTORY_SEPARATOR . $file_name . '.php';
	} else if (JFile::exists($template) && !$override_exists)
	{
	    // if override fails, try the template set in config
	    $file = $template;
	} else
	{
	    // we assume to use the default template
	    $file = JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'default' . DIRECTORY_SEPARATOR . $file_name . '.php';
	}

	return $file;
    }

    /**
     * Method to set template variable.
     */
    public function set($name, $value = null)
    {
	$this->_vars[$name] = $value;
    }

    /**
     * Method to set template variable by reference.
     */
    function setRef($name, &$value)
    {
	$this->_vars[$name] = &$value;
    }

    /**
     * Method to add template style sheet.
     */
    public static function addStyleSheet($file = '', $folder = '')
    {
	$mainframe = JFactory::getApplication();
//		$config		= &jux_portfolioFactory::getConfigs();
	$config = JComponentHelper::getParams('com_jux_gallery');

	if (!JString::strpos($file, '.css'))
	{
	    jimport('joomla.filesystem.file');
	    jimport('joomla.filesystem.folder');

	    $file_name = $file;
	    if ($folder)
	    {
		$folder_path = $folder . DIRECTORY_SEPARATOR;
		$folder_link = $folder . '/';
	    } else
	    {
		$folder_path = '';
		$folder_link = '';
	    }
	    // test if template override exists in joomla's template folder
	    $override_path = JPATH_ROOT . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . $mainframe->getTemplate() . DIRECTORY_SEPARATOR . 'html';
	    $override_exists = JFolder::exists($override_path . DIRECTORY_SEPARATOR . 'com_jux_gallery');
	    $template = JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . $config->get('template') . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR . $folder_path . $file_name . '.css';

	    // test override path first
	    if (JFile::exists($override_path . DIRECTORY_SEPARATOR . 'com_jux_gallery' . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR . $folder_path . $file_name . '.css'))
	    {
		// load the override template
		$file = '/templates/' . $mainframe->getTemplate() . '/html/com_jux_gallery/assets/css/' . $folder_link . $file_name . '.css';
	    } else if (JFile::exists($template) && !$override_exists)
	    {
		// if override fails, try the template set in config
		$file = '/components/com_jux_gallery/templates/' . $config->get('template') . '/assets/css/' . $folder_link . $file_name . '.css';
	    } else
	    {
		// we assume to use the default template
		$file = '/components/com_jux_gallery/templates/default/assets/css/' . $folder_link . $file_name . '.css';
	    }
	}

	JUX_GalleryTemplate::attachAssets($file, 'css', rtrim(JURI::root(), '/'));
    }

    /**
     * Method to add template style sheet.
     */
    public static function addScript($file = '', $folder = '')
    {
	$mainframe = JFactory::getApplication();
//		$config		= &jux_portfolioFactory::getConfigs();
	$config = JComponentHelper::getParams('com_jux_gallery');

	if (!JString::strpos($file, '.js'))
	{
	    jimport('joomla.filesystem.file');
	    jimport('joomla.filesystem.folder');

	    $file_name = $file;
	    if ($folder)
	    {
		$folder_path = $folder . DS;
		$folder_link = $folder . '/';
	    } else
	    {
		$folder_path = '';
		$folder_link = '';
	    }
	    // test if template override exists in joomla's template folder
	    $override_path = JPATH_ROOT . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . $mainframe->getTemplate() . DIRECTORY_SEPARATOR . 'html';
	    $override_exists = JFolder::exists($override_path . DIRECTORY_SEPARATOR . 'com_jux_gallery');
	    $template = JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . $config->get('template') . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . $folder_path . $file_name . '.js';

	    // test override path first
	    if (JFile::exists($override_path . DIRECTORY_SEPARATOR . 'com_jux_gallery' . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR . $folder_path . $file_name . '.js'))
	    {
		// load the override template
		$file = '/templates/' . $mainframe->getTemplate() . '/html/com_jux_gallery/assets/js/' . $folder_link . $file_name . '.js';
	    } else if (JFile::exists($template) && !$override_exists)
	    {
		// if override fails, try the template set in config
		$file = '/components/com_jux_gallery/templates/' . $config->get('template') . '/assets/js/' . $folder_link . $file_name . '.js';
	    } else
	    {
		// we assume to use the default template
		$file = '/components/com_jux_gallery/templates/default/assets/js/' . $folder_link . $file_name . '.js';
	    }
	}

	JUX_GalleryTemplate::attachAssets($file, 'js', rtrim(JURI::root(), '/'));
    }

    /**
     * Method to attach component assets
     */
    public static function attachAssets($path, $type, $asset_path = '')
    {
	$document = JFactory::getDocument();

	if ($document->getType() != 'html')
	{
	    return;
	}

	if (!empty($asset_path))
	{
	    $path = $asset_path . $path;
	} else
	{
	    $path = JURI::root() . 'components/com_jux_gallery' . $path;
	}

	if (!defined('JOOMLAUX_COMPONENT_ASSET_' . md5($path)))
	{
	    define('JOOMLAUX_COMPONENT_ASSET_' . md5($path), 1);

	    switch ($type)
	    {
		case 'js':
		    $document->addScript($path);
		    break;
		case 'css':
		    $document->addStyleSheet($path);
		    break;
	    }
	}
    }

    /**
     * Method to allow a temmplate include other template and inherit all the variables.
     */
    function load($file, $layout = null)
    {
	if ($this->_vars)
	{
	    extract($this->_vars, EXTR_REFS);
	}

	$file = $this->_getTemplateFullPath($file, $layout);
	include($file);

	return $this;
    }

    /**
     * Method to open, parse, and return the template file.
     */
    function fetch($file = null, $layout = null)
    {
	$file_layout = $this->_getTemplateFullPath($file, $layout);

	if (!$file_layout || !JFile::exists($file_layout))
	{
	    $file = $this->_getTemplateFullPath($file);
	} else
	{
	    $file = $file_layout;
	}
	if (!$file)
	{
	    $file = $this->_file;
	}

	if (!isset($this->_vars['config']) || empty($this->_vars['config']))
	{
//			$this->_vars['config'] = &jux_portfolioFactory::getConfig();
	    $this->_vars['config'] = JComponentHelper::getParams('com_jux_gallery');
	}

	if (!isset($this->_vars['user']) || empty($this->_vars['user']))
	{
	    $this->_vars['user'] = &JFactory::getUser();
	}
	if (!isset($this->_vars['live_site']) || empty($this->_vars['live_site']))
	{
	    $this->_vars['live_site'] = JURI::root();
	}
	if ($this->_vars)
	{
	    // extract the variables to local namespace
	    extract($this->_vars, EXTR_REFS);
	}
	ob_start();
	require($file);
	$contents = ob_get_contents();
	ob_end_clean();
	return $contents;
    }

    /**
     * Sets the _escape() callback.
     *
     * @param mixed $spec The callback for _escape() to use.
     */
    function setEscape($spec)
    {
	$this->_escape = $spec;
    }

    /**
     * Escapes a value for output in a view script.
     *
     * If escaping mechanism is one of htmlspecialchars or htmlentities, uses
     * {@link $_encoding} setting.
     *
     * @param  mixed $var The output to escape.
     * @return mixed The escaped value.
     */
    function escape($var)
    {
	if (in_array($this->_escape, array('htmlspecialchars', 'htmlentities')))
	{
	    return call_user_func($this->_escape, $var, ENT_COMPAT, $this->_charset);
	}

	return call_user_func($this->_escape, $var);
    }

}