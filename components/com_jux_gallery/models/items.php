<?php

/**
 * @version		$Id$
 * @author		JoomlaUX
 * @package		Joomla.Site
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modellist');

/**
 * jux_gallery Component - Items Model
 * 
 * @package		Joomla.Site
 * @subpackage	com_jux_gallery
 * @since 1.0
 */
class JUX_GalleryModelItems extends JModelList
{

    /**
     * Constructor.
     *
     * @param	array	An optional associative array of configuration settings.
     * @see		JController
     * @since	1.0
     */
    public function __construct($config = array())
    {
	if (empty($config['filter_fields']))
	{
	    $config['filter_fields'] = array(
		'id', 'a.id',
		'title', 'a.title',
		'alias', 'a.alias',
		'published', 'a.published',
		'created', 'a.created',
		'created_by', 'a.created_by',
		'modified', 'a.modified',
		'modified_by', 'a.modified_by',
		'cat_id', 'a.cat_id', 'category_title',
		'description', 'a.description',
		'checked_out', 'a.checked_out',
		'checked_out_time', 'a.checked_out_time',
		'access', 'a.access',
		'language', 'a.language',
		'ordering', 'a.ordering'
	    );
	}

	parent::__construct($config);
    }

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @since	1.0
     */
    protected function populateState($ordering = null, $direction = null)
    {
	// Initialise variables.
	$app = JFactory::getApplication();
	$user = JFactory::getUser();

	// Load the parameters. Merge Global and Menu Item params into new object
	$params = $app->getParams();
	$menuParams = new JRegistry;

	if ($menu = $app->getMenu()->getActive())
	{
	    $menuParams->loadString($menu->params);
	}
	$mergedParams = clone $menuParams;
	$mergedParams->merge($params);

	$this->setState('params', $mergedParams);
	$this->setState('user', $user);

	// Filter by state
	$this->setState('filter.published', 1);

	// filter by access
	$this->setState('filter.access', true);
	$orderCol = $app->getUserStateFromRequest('com_jux_gallery.items.list.filter_order', 'filter_order', '', 'string');
	if (!in_array($orderCol, $this->filter_fields))
	{
	    $orderCol = $this->_buildQueryOrderBy($params);
	    $listOrder = '';
	} else
	{
	    $listOrder = $app->getUserStateFromRequest('com_jux_gallery.items.list.filter_order_Dir', 'filter_order_Dir', '', 'cmd');
	    if (!in_array(strtoupper($listOrder), array('ASC', 'DESC', '')))
	    {
		$listOrder = 'ASC';
	    }
	}
	// Filter by access
	$this->setState('filter.access', true);

	// Filter by language
	$this->setState('filter.language', $app->getLanguageFilter());

	// List state information.
	$this->setState('list.ordering', $this->_buildQueryOrderBy($params));
	$this->setState('list.direction', $listOrder);
	
	// Optional filter text
	$this->setState('list.filter', JRequest::getString('filter-search'));

	$this->setState('layout', JRequest::getCmd('layout'));
    }

    /**
     * Method to get a store id based on model configuration state.
     *
     * This is necessary because the model is used by the component and
     * different modules that might need different sets of data or different
     * ordering requirements.
     *
     * @param	string		$id	A prefix for the store id.
     *
     * @return	string		A store id.
     * @since	1.0
     */
    protected function getStoreId($id = '')
    {
	// Compile the store id.
	$id .= ':' . $this->getState('filter.published');
	$id .= ':' . $this->getState('filter.cat_id');
	$id .= ':' . $this->getState('filter.access');
	$id .= ':' . $this->getState('filter.language');

	return parent::getStoreId($id);
    }

    /**
     * Method to build an SQL query to load the list data.
     *
     * @return	string	An SQL query
     * @since	1.0
     */
    protected function getListQuery()
    {
	$user = JFactory::getUser();
	$params = $this->getState('params');
	// Create a new query object.
	$db = $this->getDbo();
	$query = $db->getQuery(true);

	$query->select($this->getState('list.select', 'a.*'));
	$query->from($db->quoteName('#__jux_gallery_items') . ' AS a');

	// Join over the categories
	$query->select('c.title AS category_title, c.alias AS category_alias, c.ordering');
	$query->leftJoin('#__jux_gallery_categories AS c ON c.id = a.cat_id');

	// Join over the language
	$query->select('l.title AS language_title');
	$query->leftJoin($db->quoteName('#__languages') . ' AS l ON l.lang_code = a.language');
	
	// Filter by state
	$state = $this->getState('filter.published');
	if (is_numeric($state))
	{
	    $query->where('a.published = ' . (int) $state);
	} elseif (is_array($state))
	{
	    JArrayHelper::toInteger($state);
	    $state = implode(',', $state);
	    $query->where('a.published IN (' . $state . ')');
	}
	//Filter by multi categories
	$categories = $params->get('listAlbum');
	$allCategories = FALSE;
	foreach ($categories as $category){
	    if($category == '')
		$allCategories = TRUE;
	}
	if(!$allCategories){
	    $categories = implode(',', $categories);
	    $query->where('a.cat_id IN ('.$categories.')');
	}
	
	//Filter by access categories
	$query->where('c.published = 1');

	// Filter by access
	if ($this->getState('filter.access'))
	{
	    $groups = implode(',', $user->getAuthorisedViewLevels());
	    $query->where('a.access IN (' . $groups . ')');
	    $query->where('c.access IN (' . $groups . ')');
	}

	$query->where('a.language in (' . $db->quote(JFactory::getLanguage()->getTag()) . ',' . $db->quote('*') . ')');

	// Ordering
	//Ordering by Categories
	$query->order('c.ordering ASC');
	$query->order($db->escape($this->getState('list.ordering', 'a.ordering')) . ' ' . $db->escape($this->getState('list.direction', 'ASC')));
//	  echo nl2br(str_replace('#__','j30_',$query));die;  


	return $query;
    }

    /**
     * Build the orderby for the query
     *
     * @return	string	$orderby portion of query
     */
    protected function _buildQueryOrderBy()
    {
	$params = $this->state->params;
	$order_by = '';
	$order = JRequest::getCmd('orderBy', $params->get('orderBy', 'order'));
	switch ($order)
	{
	    case 'date':
		$order_by .= 'a.created';
		break;
	    case 'rdate':
		$order_by .= 'a.created DESC';
		break;
	    case 'alpha':
		$order_by .= 'a.title';
		break;
	    case 'ralpha':
		$order_by .= 'a.title DESC';
		break;
	    case 'order':
		$order_by .= 'a.ordering';
		break;
	    default:
		$order_by .= 'a.ordering';
	}
	return $order_by;
    }

    /**
     * Method to build an SQL query to get the category of each user.
     *
     * @return	category
     * @since	1.0
     */
    public function getCategories()
    {
	// let load the content if it doesn't already exist
	$db = JFactory::getDbo();
	$query = $db->getQuery(true);
	$query->select('a.*');
	$query->from('#__jux_gallery_categories AS a');
	$query->where('a.published = 1');
	
	//Filter By Category
	$params = $this->getState('params');
	$categories = $params->get('listAlbum');
	$allCategories = FALSE;
	foreach ($categories as $category){
	    if($category == '')
		$allCategories = TRUE;
	}
	if(!$allCategories){
	    $categories = implode(',', $categories);
	    $query->where('a.id IN ('.$categories.')');
	}
	
	$user = JFactory::getUser();
	$groups = implode(',', $user->getAuthorisedViewLevels());
	$query->where('a.access IN (' . $groups . ')');
	$query->where('a.language in (' . $db->quote(JFactory::getLanguage()->getTag()) . ',' . $db->quote('*') . ')');
	$query->order('a.ordering');
	$db->setQuery($query);
	$categories = $db->loadObjectList();

	return $categories;
    }

}
