<?php
/**
 * @version	$Id$
 * @author	JoomlaUX
 * @package	Joomla.Site
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license	http://www.gnu.org/licenses/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Utility class for creating HTML lists for jux_gallery component
 * 
 * @package	Joomla.Site
 * @subpackage	com_jux_gallery
 * @since	1.0
 */
class JHtmlJUX_Gallery {

}
