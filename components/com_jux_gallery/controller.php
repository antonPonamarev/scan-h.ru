<?php

/**
 * @version		$Id$
 * @author		JoomlaUX
 * @package		Joomla.Site
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

/**
 * jux_gallery Component Controller
 *
 * @package		Joomla.Site
 * @subpackage	com_jux_gallery
 * @since		1.0
 */
class JUX_GalleryController extends JControllerLegacy {

	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @since   1.0
	 */
	public function __construct($config = array()) {
		parent::__construct($config);
	}

	/**
	 * Method to display a view.
	 *
	 * @param	boolean			If true, the view output will be cached
	 * @param	array			An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return	JController		This object to support chaining.
	 * @since	1.0
	 */
	public function display($cachable = false, $urlparams = false) {
		
		$this->input = JFactory::getApplication()->input;
		$view = $this->input->getCmd('view');
		$user = JFactory::getUser();
		// set a default view if none exists
		if (!$view) {
			JRequest::setVar('view', 'items');
		}
		if (JRequest::getCmd('view') == 'items')
		{
			if($this->input->getCmd('view')){
			if(($this->input->getCmd('cat')== '') && !empty($user)){
				$this->input->set('cat', $user->get('username'));
				}
			}
		}
		parent::display($cachable, $urlparams);

		return $this;
	}

}
