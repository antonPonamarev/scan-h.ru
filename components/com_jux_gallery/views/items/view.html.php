<?php

/**
 * @version	$Id$
 * @author	JoomlaUX
 * @package	Joomla.Site
 * @subpackage	com_jux_gallery
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * HTML Items list View class for the jux_gallery component
 *
 * @package		Joomla.Site
 * @subpackage	com_jux_gallery
 * @since 1.0
 */
class JUX_GalleryViewItems extends JViewLegacy
{

    /**
     * Override display method for display a template script.
     *
     * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
     *
     * @return  mixed  A string if successful, otherwise a JError object.
     * @since   1.0
     */
    function display($tpl = null)
    {
	$app = JFactory::getApplication();
	$user = JFactory::getUser();

	//get params

	$params = $app->getParams();
	$menuParams = new JRegistry;

	if ($menu = $app->getMenu()->getActive())
	{
	    $menuParams->loadString($menu->params);
	}
	$mergedParams = clone $menuParams;
	$mergedParams->merge($params);
	$params = $mergedParams;

	// Get some data from the models
	$state = $this->get('State');
	$params = $state->params;
	$items = $this->get('Items');
	$category = $this->get('Category');
	$categories = $this->get('Categories');
	$pagination = $this->get('Pagination');
	$model = $this->getModel();

	// Check for errors.
	if (count($errors = $this->get('Errors')))
	{
	    JError::raiseError(500, implode("\n", $errors));
	    return false;
	}

	// Plugins
	$view = JRequest::getVar('view');
	$task = JRequest::getVar('task');
//		foreach ($items as $item) {
//			$item = $model->execPlugins($item, $view, $task);
//		}
	// Pathway & Title
	$menus = $app->getMenu();
	$pathway = $app->getPathway();
	$title = null;

	// Because the application sets a default page title,
	// we need to get it from the menu item itself
	$menu = $menus->getActive();

	if ($menu)
	{
	    $params->def('page_heading', $params->get('page_title', $menu->title));
	} else
	{
	    $params->def('page_heading', JText::_('COM_JUX_GALLERY_ITEMS_PAGE_TITLE'));
	}

	// add pathway
	if (!is_object($menu) || !isset($menu->query['option']) || $menu->query['option'] != 'com_jux_gallery' || !isset($menu->query['view']) || $menu->query['view'] != 'items')
	{
	    $pathway->addItem(@$menu->title ? JText::_($menu->title) : JText::_('COM_JUX_GALLERY_ITEMS'));
	}

	$title = $params->get('page_title', '');
	if (empty($title))
	{
	    $title = JText::_('COM_JUX_GALLERY_ITEMS_PAGE_TITLE');
	}

	$this->document->setTitle($title);

	$tmpl = new jux_galleryTemplate();
	$layout = $this->getLayout();

	$tmpl->set('state', $state);
	$tmpl->set('params', $params);
	$tmpl->set('items', $items);
	$tmpl->set('pagination', $pagination);
	$tmpl->set('user', $user);
	$tmpl->set('category', $category);
	$tmpl->set('categories', $categories);

	echo $tmpl->fetch($this->getName(), $layout);
    }

}
