<?php
/**
 * @version	$Id$
 * @author	JoomlaUX
 * @package	Joomla.Site
 * @subpackage	com_jux_portfolio
 * @copyright	Copyright (C) 2012 by JoomlaUX. All rights reserved.
 * @license	http://www.gnu.org/licenses/gpl.html
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

//JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
//JHtml::_('behavior.tooltip');
//JHtmlBehavior::framework();
/* JHtml::_('behavior.disable','behavior'); */
?>

<?php if ($params->get('styleAlbum', 1) == 1): ?>
    <div id="justified-filter" class="modern">
        <ul>
    	<!--<li data-category="catall" class="selected-filter-item">All the Items</li>-->
	    <?php
	    $i = 0;
	    foreach ($categories as $category):
		?>
		<li data-category="<?php echo $category->alias ?>" class="<?php echo ($i == 0) ? 'selected-filter-item' : ''; ?>"><?php echo $category->title; ?></li>
		<?php
		$i++;
	    endforeach;
	    ?>
        </ul>
    </div>
<?php endif; ?>
<?php if ($params->get('styleAlbum', 1) == 0): ?>
    <div class="topbar">
        <div class="jux-button-group-row">
	    <div class="jux-button-group">
		<a id="close" class="jux-button is-danger is-embossed has-icon"><i class="jux-icon-back"></i></a>
	    </div>
	    <div class="jux-dropdown"> <span class="jux-dropdown-toggle" tabindex="0"></span>
		<div class="jux-dropdown-text" id="currentAlbum" tabindex="0"></div>
		<ul class="jux-dropdown-content" id="listAlbum">
			<?php foreach ($categories as $category): ?>
			    <li data-title="<?php echo $category->title; ?>"><a href="javascript:void(0)" data-album="<?php echo $category->alias;?>"><?php echo $category->title ?></a></li>
			<?php endforeach; ?>
		</ul>
            </div>
        </div>

    </div>
<?php endif; ?>
<div id="justified-gallery">
    <?php foreach ($items as $item): ?>
	<?php if ($params->get('styleAlbum', 1) == 0): ?>
	    <li data-pile="<?php echo $item->category_title; ?>">
	    <?php endif; ?>
    	<a class="<?php echo $item->category_alias; ?>"
    	   data-pile="<?php echo $item->category_title; ?>"
	   data-url="<?php echo (trim($item->url)!= '') ? ($item->url) : "#"; ?>"
    	   title="<?php echo $item->title; ?>"
    	   data-largesrc="<?php echo $item->large_image; ?>"
    	   data-title="<?php echo $item->title; ?>"
    	   data-description="<?php echo $item->description; ?>"
    	   >
    	    <img alt="<?php echo $item->title; ?>" src="<?php echo $item->large_image; ?>" />
    	</a>
	    <?php if ($params->get('styleAlbum', 1) == 0): ?>
	    </li>
	<?php endif; ?>
    <?php endforeach; ?>
    <?php if ($params->get('styleAlbum', 1) == 0): ?>
        <div id='justified'></div>
    <?php endif; ?>
</div>
<script type="text/javascript">
<?php if ($params->get('styleAlbum', 1) == 0): ?>
        var grid = jQuery("#justified-gallery"),
    	    jux_loader = jQuery('<div class="loader"><i></i><i></i><i></i><i></i><i></i><i></i><span>Loading...</span></div>').insertBefore(grid),
    	    jux_close = jQuery('#close'),
    	    current_album = jQuery('#currentAlbum'),
	    list_album = jQuery('#listAlbum'),
    	    stapel = grid.stapel({
    	onLoad: function() {
    	    jux_loader.remove();
    	},
    	onBeforeOpen: function(pileName) {
    	    jux_close.parent().parent().css('display', 'table');
    	    jux_close.parent().css('display', 'block');
    	    jux_close.css('display', 'inline-block');
    	    current_album.parent().css('display', 'inline-block');
	    current_album.css('display','');

    	    current_album.html(pileName);
	    
    	},
    	'usedSuffix': 'lt500',
    	'justifyLastRow':<?php echo $params->get('lastRow',true); ?>,
    	'rowHeight':<?php echo $params->get('rowHeight',200); ?>,
    	'fixedHeight':<?php echo $params->get('fixedHeight',true); ?>,
    	'lightbox': false,
    	'captions':<?php echo $params->get('captions',true); ?>,
    	'margins':<?php echo $params->get('margins',5); ?>,
    	'category': false,
    	'pile': true
        });

        jux_close.on('click', function() {
	    jux_close.hide();
	    current_album.empty();
	    current_album.hide();
	    current_album.parent().hide();	    
	    stapel.closePile();
        });
<?php else: ?>
        jQuery("#justified-gallery").justifiedGallery({
    	'usedSuffix': 'lt500',
    	'justifyLastRow':<?php echo $params->get('lastRow',true); ?>,
    	'rowHeight':<?php echo $params->get('rowHeight',200); ?>,
    	'fixedHeight':<?php echo $params->get('fixedHeight',true); ?>,
    	'lightbox': false,
    	'captions':<?php echo $params->get('captions',true); ?>,
    	'margins':<?php echo $params->get('margins',5); ?>,
    	'category': true,
    	'pile': false
        });
<?php endif; ?>
</script>