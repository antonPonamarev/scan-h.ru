<?php
/**
* @package RSMediaGallery! Showcase
* @copyright (C) 2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/

// no direct access
defined('_JEXEC') or die('Restricted access'); 
?>
<div class="rsp_container<?php echo ' '.modRSMediaGalleryShowcase::escape($moduleclass_sfx); ?>" id="rsp_container<?php echo $module->id;?>">
	<?php foreach ($pictures as $picture) {
		$title 		 = $showcase->escape($picture->title);
		$description = $showcase->escape($picture->description);
		$style 		 = $showcase->getStyle($picture);
	?>
		<div class="rsp_picture_container pull-left" data-target="<?php echo $picture->full;?>" data-title="<?php echo $title; ?>" data-description="<?php echo $description; ?>">
			<div class="rsp_picture pull-left" style="width:<?php echo $thumb_width; ?>px; height:<?php echo $thumb_width; ?>px;">
			<?php if ($openUrl) { ?>
				<a href="<?php echo $picture->url; ?>" <?php echo $target; ?> title="<?php echo $title; ?>">
			<?php } ?>
					<img src="<?php echo $picture->thumb; ?>" <?php echo $style; ?> title="<?php echo $title; ?>" />
			<?php if ($openUrl) { ?>
				</a>
			<?php } ?>
			</div>
		</div>
	<?php } ?>
</div>