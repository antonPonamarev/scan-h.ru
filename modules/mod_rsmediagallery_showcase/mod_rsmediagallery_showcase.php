<?php
/**
* @package RSMediaGallery! Showcase
* @copyright (C) 2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// Load our helper class
require_once dirname(__FILE__).'/helpers/helper.php';

// Initialize it
$showcase = new modRSMediaGalleryShowcase($params);

try {
	// Get available pictures
	$pictures = $showcase->getPictures();
	
	// Load jQuery
	$showcase->loadjQuery();
	
	// Init scripts
	$showcase->initJs($module->id);
	
	// Load stylesheets
	$showcase->loadCss('font-awesome');
	$showcase->loadCss('style');
} catch (Exception $e) {
	JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
	return;
}

$thumb_width 		= $params->get('th_width', 100);
$open_in	 		= $params->get('open_in', 'slideshow');
$openUrl	 		= $open_in == 'url';
$target		 		= $params->get('open_in_page', 'same') != 'same' ? 'target="_blank"' : '';
$moduleclass_sfx	= $params->get('moduleclass_sfx','');

require JModuleHelper::getLayoutPath('mod_rsmediagallery_showcase', 'default');