<?php
/**
* @package RSMediaGallery! Showcase
* @copyright (C) 2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

class pinterestElements {
	public $username;
	public $boardname;
	public $limit;
	public $thumb_width;
	
	public function __construct($params) {
		$this->username 	 = $params->get('pinterest_username');
		$this->boardname 	 = $params->get('pinterest_boardname');
		$this->limit 		 = $params->get('limit', 10);
		$this->thumb_width   = $params->get('th_width', 100);
		
		if (empty($this->username)) {
			throw new Exception(JText::_('MOD_RSMEDIAGALLERY_SHOWCASE_PINTEREST_NO_USERNAME'));
		}
	}
	
	public function getItems() {
		$items = array();
		
		// Initialize caching
		$cache = JFactory::getCache('mod_rsmediagallery_showcase');
		$cache->setCaching(true);
		
		$data = $cache->call(array('pinterestElements', 'getElements'), $this->username, $this->boardname, $this->limit);
		
		if ($data) {
			foreach ($data as $photo) {
				$photo = (object) $photo;
				
				$thumb_data = $this->getBestThumb($photo);
				$photo->thumb 		= str_replace('{res}', $thumb_data->res, $photo->image);
				$photo->full  		= str_replace('{res}', 'originals', $photo->image);
				$photo->url   		= $photo->link;
				$photo->orientation = $thumb_data->orientation;
				
				$items[] = $photo;
			}
		}
		
		if (!$items) {
			throw new Exception(JText::_('MOD_RSMEDIAGALLERY_SHOWCASE_PINTEREST_NO_PICTURES_FOUND'));
		}
		
		return $items;
	}
	
	protected function getBestThumb($item) {
		$all_widths = array(
			'70x'  	=> 70,
			'192x' 	=> 192,
			'236x'	=> 236,
			'736x'	=> 736
		);
		
		$diff_w_found = 0;
		$width_found  = '';
	
		foreach ($all_widths as $key => $width) {
			$diff_w	= $width - (int) $this->thumb_width;
			if ($diff_w >= 0 && ($diff_w_found > $diff_w  || $diff_w_found == 0)) {
				$diff_w_found = $diff_w;
				$width_found = $key;
			}
		}

		$new_image = str_replace('{res}', $width_found, $item->image);
		list($width_new, $height_new) = getimagesize($new_image);
		$orientation  = $width_new >= $height_new ? 'landscape' : 'portrait';
		
		$return =  new stdClass();
		
		if ($height_new < $this->thumb_width) {
			$return->res = $this->getNextElement($all_widths, $width_found);
		} else {
			$return->res =  $width_found;
		}
		$return->orientation =  $orientation;
		
		return $return;
	}
	
	protected function getNextElement($array, $key) {
		$continue = 0;
		$found = '';
		foreach($array as $res=>$val){
			if ($key == $res) {
				$continue = 1;
			}
			if ($continue == 1) {
				$found = $res;
				$continue = 2;
			}
		}
		
		return $found;
	}
	
	public static function getElements($username, $boardname, $limit) {
		if (empty($boardname)){
			$pinsfeed = 'http://pinterest.com/'.$username.'/feed.rss';
		} else {
			$pinsfeed = 'http://pinterest.com/'.$username.'/'.$boardname.'/rss';
		}
		
		libxml_use_internal_errors(true);
		$rss = @simplexml_load_file($pinsfeed);
		if ($errors = libxml_get_errors()) {
			$error = reset($errors);
			throw new Exception($error->message, $error->code);
		}
		libxml_clear_errors();
		libxml_use_internal_errors(false);
		
		$items 	= array();

		$i = 0;
		foreach ($rss->channel->item as $node) {
			$image = pinterestElements::getItemImage($node->description);
			if (!is_null($image) && $limit > $i) {
				$item = array (
					'title' => (string) $node->title,
					'description' => strip_tags((string) $node->description),
					'link' => (string) $node->link,
					'image' => pinterestElements::getItemImage((string) $node->description)
				);
				$items[] = $item;
				$i++;
			}
		}

		if (!$items) {
			throw new Exception(JText::_('MOD_RSMEDIAGALLERY_SHOWCASE_PINTEREST_NO_PICTURES_FOUND'));
		}
		
		return $items;
	}
	
	public static function getItemImage($description) {
		$data = explode('img src="', $description);
		if (count($data) > 1) {
			$img = explode('">',$data[1]);
			$img = $img[0];
			
			return str_replace('192x', '{res}',$img);
		}
		else return;
	}
}