<?php
/**
* @package RSMediaGallery! Showcase
* @copyright (C) 2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

class flickrElements {
	public $tags;
	public $api_key;
	public $user_id;
	public $limit;
	public $thumb_width;
	
	public function __construct($params) {
		$this->tags 		 = $params->get('flickr_tags');
		$this->api_key 		 = $params->get('flickr_api_key');
		$this->user_id 		 = $params->get('flickr_user_id', '');
		$this->limit 		 = $params->get('limit', 10);
		$this->thumb_width   = $params->get('th_width', 100);
		
		// Clean tags
		$this->tags = explode(',', $this->tags);
		foreach ($this->tags as $i => $tag) {
			$this->tags[$i] = trim($tag);
		}
		
		if (!$this->api_key) {
			throw new Exception(JText::_('MOD_RSMEDIAGALLERY_SHOWCASE_FLICKR_NO_API_KEY'));
		}
	}

	public function getItems() {
		$items = array();
		
		// Initialize caching
		$cache = JFactory::getCache('mod_rsmediagallery_showcase');
		$cache->setCaching(true);
		
		if ($data = $cache->call(array('flickrElements', 'getElements'), $this->api_key, $this->user_id, $this->limit, $this->tags)) {
			foreach ($data as $dataset) {
				foreach ($dataset as $photo) {
					$url = false;
					if (isset($photo->url_o)) {
						$url = 'url_o';
					}
					if (isset($photo->url_l)) {
						$url = 'url_l';
					}
					if ($url) {
						$thumb_data = $this->getBestThumb($photo);
						
						if ($thumb_data) {
							$photo->description = $photo->description->_content;
							$photo->thumb  	    = $photo->{$thumb_data->res};
							$photo->full  	    = $photo->{$url};
							$photo->url  	    = $photo->{$url};
							$photo->orientation = $thumb_data->orientation;
							
							$items[] = $photo;
						}
					}
				}
			}
		}
		
		if (!$items) {
			throw new Exception(JText::_('MOD_RSMEDIAGALLERY_SHOWCASE_FLICKR_NO_PICTURES_FOUND'));
		}
		
		return $items;
	}
	
	protected function getBestThumb($item) {
		$resolutions = array('url_sq', 'url_t', 'url_s','url_q', 'url_m', 'url_n', 'url_z', 'url_c', 'url_l', 'url_o');
		
		$all_widths = array();
		foreach ($resolutions as $rez) {
			if (isset($item->{$rez})) {
				$ref = explode('_', $rez);
				$ref = array_pop($ref);
				$all_widths[$rez] = (int) $item->{'width_'.$ref};
			}
		}
		
		$diff_w_found = 0;
		$width_found  = '';
		
		foreach ($all_widths as $key => $width) {
			$diff_w	= $width - (int) $this->thumb_width;
			if ($diff_w >= 0 && ($diff_w_found > $diff_w  || $diff_w_found == 0)) {
				$diff_w_found = $diff_w;
				$width_found = $key;
			}
		}
		if ($width_found) {
			$element = str_replace('url_', 'height_', $width_found);
			$height_new = (int) $item->{$element};
			$width_new  = (int) $item->{$width_found};
			
			$orientation  = $width_new >= $height_new ? 'landscape' : 'portrait';
			
			$return =  new stdClass();
			
			if ($height_new < $this->thumb_width) {
				$return->res = $this->getNextElement($all_widths, $width_found);
			} else {
				$return->res = $width_found;
			}
			$return->orientation =  $orientation;
			
			return $return;
		}
		else return false;
	}
	
	protected function getNextElement($array, $key) {
		$continue = 0;
		$found = '';
		foreach($array as $res=>$val){
			if ($key == $res) {
				$continue = 1;
			}
			if ($continue == 1) {
				$found = $res;
				$continue = 2;
			}
		}
		
		return $found;
	}
	
	public static function getElements($api_key, $user_id, $limit, $tags = null) {
		$items  = array();
		$error 	= '';
		
		if ($limit < 500) {
			$per_page = $limit;
			$pages 	  = 1;
		} else {
			$per_page = 500;
			$pages = ceil($limit / 500); 
		}

		$http = JHttpFactory::getHttp();
		
		$params = array(
			'method' 			=> 'flickr.photos.search',
			'api_key' 			=> $api_key,
			'per_page'			=> $per_page,
			'extras'			=> 'description,tags,url_sq,url_t,url_s,url_q,url_m,url_n,url_z,url_c,url_l,url_o',
			'format'			=> 'json',
			'nojsoncallback' 	=> 1
		);
		
		// Add user_id if requested
		if ($user_id) {
			$params['user_id'] = $user_id;
		}
		
		// Add tags if requested
		if (is_array($tags)) {
			$params['tags'] = implode(',', $tags);
		}
		
		for ($i = 1; $i <= $pages; $i++) {
			// Set the page
			$params['page'] = $i;
			
			// Create the URL
			$url = 'https://api.flickr.com/services/rest/?'.http_build_query($params);
			
			$response = $http->get($url, array(), 3);
			$response = json_decode($response->body);
			if ($response->stat == 'ok') {
				$items[] 		 = $response->photos->photo;
				$nr_photos_found = count($response->photos->photo);
				// Check if there are more photos so that we can get the next page
				if ($nr_photos_found == 0) {
					break;
				}
			} else {
				$error = $response->stat;
				break;
			}
		}
		
		if (!$items) {
			throw new Exception(JText::_('MOD_RSMEDIAGALLERY_SHOWCASE_FLICKR_NO_PICTURES_FOUND').($error != '' ? '<br/>'.JText::_('MOD_RSMEDIAGALLERY_SHOWCASE_ERROR').': '.$error : ''));
		}
		
		return $items;
	}
}