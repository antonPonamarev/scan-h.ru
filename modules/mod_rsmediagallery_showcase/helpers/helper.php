<?php
/**
* @package RSMediaGallery! Showcase
* @copyright (C) 2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

class modRSMediaGalleryShowcase {
	public $mode;
	public $tags;
	public $order;
	public $direction;
	public $limit;
	public $thumb_width;
	public $params;
	
	public function __construct($params) {
		$this->params 		 = $params;
		
		$this->mode 		 = $params->get('mode','rsmediagallery');
		$this->tags 		 = $params->get('tags');
		$this->order 		 = $params->get('ordering', 'ordering');
		$this->direction	 = $params->get('direction', 'ASC');
		$this->limit 		 = $params->get('limit', 10);
		$this->thumb_width   = $params->get('th_width', 100);
	}

	public function getPictures() {
		$items = array();
		
		if ($this->limit == 0) {
			throw new Exception(JText::_('MOD_RSMEDIAGALLERY_SHOWCASE_ZERO_LIMIT'));
		}
		
		switch ($this->mode) {
			case 'rsmediagallery':
				if (!file_exists(JPATH_ADMINISTRATOR.'/components/com_rsmediagallery/helpers/helper.php')) {
					throw new Exception(JText::_('MOD_RSMEDIAGALLERY_SHOWCASE_RSMEDIAGALLERY_NOT_INSTALLED'));
				}
				
				require_once JPATH_ADMINISTRATOR.'/components/com_rsmediagallery/helpers/helper.php';
				
				// additional params for creating thumbs and full images
				$this->params->set('thumb_resolution', 'w,'.($this->thumb_width * 2));
				$this->params->set('use_original', 1);
				
				$gallery_params	= RSMediaGalleryHelper::parseParams($this->params);

				if (!$this->tags) {
					throw new Exception(JText::sprintf('MOD_RSMEDIAGALLERY_SHOWCASE_PARAM_TAGS_NOT_SET'));
				}
				
				$items = RSMediaGalleryHelper::getItems($this->tags, $this->order, $this->direction, 0, $this->limit);
				if (empty($items)) { 
					throw new Exception(JText::sprintf('MOD_RSMEDIAGALLERY_SHOWCASE_PARAM_TAGS_NO_RESULTS', $this->tags));
				}

				// Calculating slider width or height
				foreach ($items as $i => $item) {
					$items[$i] 		= RSMediaGalleryHelper::parseItem($item, $gallery_params);
					$items[$i]->url = $items[$i]->href;
				}
			break;
			
			case 'flickr':
				require_once dirname(__FILE__).'/flickr.php';
				
				$flickr = new flickrElements($this->params);
				$items  = $flickr->getItems();
			break;
			
			case 'pinterest':				
				require_once dirname(__FILE__).'/pinterest.php';
				
				$pinterest 	= new pinterestElements($this->params);
				$items 		= $pinterest->getItems();
			break;
		}
		
		return $items;
	}
	
	public function escape($string) {
		return htmlentities($string, ENT_COMPAT, 'utf-8');
	}
	
	public function getStyle($picture) {
		$style = '';
		
		if ($this->mode != 'rsmediagallery') {
			if ((isset($picture->orientation) && $picture->orientation == 'landscape')) {
				$measure = 'height: ';
			} else {
				$measure = 'width: ';
			}
			
			$style = sprintf('style="%s: %dpx;"', $measure, $this->thumb_width);
		}
		
		return $style;
	}
	
	public function loadjQuery() {
		if ($this->params->get('load_jquery', 1)) {
			$version = new JVersion();
			if ($version->isCompatible('3.0')) {
				JHtml::_('jquery.framework');
			} else {
				// Load on 2.5
				JHtml::script('mod_rsmediagallery_showcase/jquery.min.js', false, true);
				JHtml::script('mod_rsmediagallery_showcase/jquery-noconflict.js', false, true);
			}
		}
	}
	
	public function loadCss($file) {
		JHtml::stylesheet('mod_rsmediagallery_showcase/'.$file.'.css', array(), true);
	}
	
	public function loadJs($file) {
		JHtml::script('mod_rsmediagallery_showcase/'.$file.'.js', false, true);
	}
	
	public function initJs($moduleId) {
		$open_in = $this->params->get('open_in', 'slideshow');
		
		$this->loadJs('script');
		if ($open_in == 'slideshow') {
			$this->loadJs('lightbox');
		}
		JFactory::getDocument()->addScriptDeclaration("
			RSShowcaseScript.Init.Start({'id':'#rsp_container{$moduleId}', 'title':".(int)$this->params->get('show_title', 1).", 'description':".(int)$this->params->get('show_description', 1).", 'numbering':".(int)$this->params->get('show_pictures_numbering', 1).", 'openIn': '".$this->escapeJs($this->params->get('open_in', 'slideshow'))."'});
		");
	}
	
	public function escapeJs($string) {
		return  str_replace("'", "\'", $string);
	}
}