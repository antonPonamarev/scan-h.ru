<?php 
/*
# ------------------------------------------------------------------------
# Extensions for Joomla 2.5.x - Joomla 3.x
# ------------------------------------------------------------------------
# Copyright (C) 2011-2015 Ext-Joom.com. All Rights Reserved.
# @license - PHP files are GNU/GPL V2.
# Author: Ext-Joom.com
# Websites:  http://www.ext-joom.com 
# Date modified: 01/08/2014 - 13:00
# ------------------------------------------------------------------------
*/

// no direct access
defined('_JEXEC') or die;
$document 					= JFactory::getDocument();
$document->addStyleSheet(JURI::base() . 'modules/mod_ext_sly_images/assets/css/style.css');


$moduleclass_sfx			= $params->get('moduleclass_sfx');
$ext_id 					= "mod_".$module->id;
$ext_jquery_ver				= $params->get('ext_jquery_ver', '1.10.2');
$ext_load_jquery			= (int)$params->get('ext_load_jquery', 1);
$ext_load_base				= (int)$params->get('ext_load_base', 1);
$ext_load_easing			= (int)$params->get('ext_load_easing', 1);
$ext_height					= (int)$params->get('ext_height', 200);
$ext_item_width				= (int)$params->get('ext_item_width', 200);
$scrollbar_poss				= $params->get('scrollbar_poss', 'top');
$ext_controls_btn			= (int)$params->get('ext_controls_btn', 0);
$sly_css3_effects			= (int)$params->get('sly_css3_effects', 1);




// Options Sly https://github.com/darsain/sly/wiki/Options
//---------------------------------------------------------------------
$ext_horizontal				= (int)$params->get('ext_horizontal', 1);
$ext_itemnav				= $params->get('ext_itemnav', 'basic');
$ext_itemselector 			= $params->get('ext_itemselector', 'null');
$ext_smart					= (int)$params->get('ext_smart', 1);
$ext_activateon				= $params->get('ext_activateon', 'click');
$ext_activatemiddle			= (int)$params->get('ext_activatemiddle', 1);
$ext_scrollsource			= $params->get('ext_scrollsource', 'null');
$ext_scrollby				= (int)$params->get('ext_scrollby', 1);
$ext_dragsource				= $params->get('ext_dragsource', 'null');
$ext_mousedragging			= (int)$params->get('ext_mousedragging', 1);
$ext_touchdragging			= (int)$params->get('ext_touchdragging', 1);
$ext_releaseswing			= (int)$params->get('ext_releaseswing', 1);
$ext_swingspeed				= $params->get('ext_swingspeed', '0.2');
$ext_elasticbounds			= (int)$params->get('ext_elasticbounds', 0);
$ext_interactive			= $params->get('ext_interactive', 'null');
$ext_draghandle				= (int)$params->get('ext_draghandle', 1);
$ext_dynamichandle			= (int)$params->get('ext_dynamichandle', 1);
$ext_minhandlesize			= (int)$params->get('ext_minhandlesize', 50);
$ext_clickbar				= (int)$params->get('ext_clickbar', 1);
$ext_syncspeed				= $params->get('ext_syncspeed', '0.5');
$ext_cycleby				= $params->get('ext_cycleby', 'null');
$ext_cycleinterval			= (int)$params->get('ext_cycleinterval', 1000);
$ext_pauseonhover			= (int)$params->get('ext_pauseonhover', 0);
$ext_startpaused			= (int)$params->get('ext_startpaused', 0);
$ext_moveby					= (int)$params->get('ext_moveby', 300);
$ext_speed					= (int)$params->get('ext_speed', 300);
$ext_easing					= $params->get('ext_easing', 'swing');
$ext_startat				= (int)$params->get('ext_startat', 0);
$ext_keyboardnavby			= $params->get('ext_keyboardnavby', 'null');
$ext_draggedclass			= $params->get('ext_draggedclass', 'dragged');
$ext_activeclass			= $params->get('ext_activeclass', 'active');
$ext_disabledclass			= $params->get('ext_disabledclass', 'disabled');





	
// Load jQuery
//---------------------------------------------------------------------

$ext_script = <<<SCRIPT


var jSkyImg = false;
function initJQ() {
	if (typeof(jQuery) == 'undefined') {
		if (!jSkyImg) {
			jSkyImg = true;
			document.write('<scr' + 'ipt type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/$ext_jquery_ver/jquery.min.js"></scr' + 'ipt>');
		}
		setTimeout('initJQ()', 500);
	}
}
initJQ(); 
 
 if (jQuery) jQuery.noConflict();    
  
  
 

SCRIPT;

if ($ext_load_jquery  > 0) {
	$document->addScriptDeclaration($ext_script);		
}
if ($ext_load_easing  > 0) { 
	$document->addCustomTag('<script type = "text/javascript" src = "'.JURI::root().'modules/mod_ext_sly_images/assets/js/jquery.easing.1.3.js"></script>'); 
}
if ($ext_load_base  > 0) { 
	$document->addCustomTag('<script type = "text/javascript" src = "'.JURI::root().'modules/mod_ext_sly_images/assets/js/sly.min.js"></script>'); 	
}

// Load img params
//---------------------------------------------------------------------

$names = array('img', 'alt', 'url', 'target', 'html');
$max = 20;
foreach($names as $name) {
    ${$name} = array();
    for($i = 1; $i <= $max; ++$i)
        ${$name}[] = $params->get($name . $i);
}	
require JModuleHelper::getLayoutPath('mod_ext_sly_images', $params->get('layout', 'default'));
?>