<?php
/*
# ------------------------------------------------------------------------
# Extensions for Joomla 2.5.x - Joomla 3.x
# ------------------------------------------------------------------------
# Copyright (C) 2011-2015 Ext-Joom.com. All Rights Reserved.
# @license - PHP files are GNU/GPL V2.
# Author: Ext-Joom.com
# Websites:  http://www.ext-joom.com 
# Date modified: 01/08/2014 - 13:00
# ------------------------------------------------------------------------
*/

// no direct access
defined('_JEXEC') or die;
$sly_css3_effects_class = '';
if ($sly_css3_effects > 0) $sly_css3_effects_class = 'sly_css3_effects'; 


$document->addCustomTag('
<style type="text/css">
ul.sly_items_'.$ext_id.' li {
	width: '.$ext_item_width.'px;
}
</style>');

?>


<script type="text/javascript">
jQuery(document).ready(function(){
		jQuery('#ext_sly_<?php echo $ext_id; ?>').sly({
			horizontal: <?php echo $ext_horizontal; ?>,			
			itemNav: '<?php echo $ext_itemnav; ?>',			
			itemSelector: <?php echo $ext_itemselector; ?>,
			smart: <?php echo $ext_smart; ?>,
			activateOn:   '<?php echo $ext_activateon; ?>',
			activateMiddle: <?php echo $ext_activatemiddle; ?>,
			scrollSource: <?php echo $ext_scrollsource; ?>,
			scrollBy: <?php echo $ext_scrollby; ?>,
			dragSource:  <?php echo $ext_dragsource; ?>,
			mouseDragging: <?php echo $ext_mousedragging; ?>,
			touchDragging: <?php echo $ext_touchdragging; ?>, 
			releaseSwing: <?php echo $ext_releaseswing; ?>, 
			swingSpeed: <?php echo $ext_swingspeed; ?>,
			elasticBounds: <?php echo $ext_elasticbounds; ?>,
			interactive: <?php echo $ext_interactive; ?>,
			scrollBar: '#scrollbar_<?php echo $ext_id; ?>',
			dragHandle: <?php echo $ext_draghandle; ?>,
			dynamicHandle: <?php echo $ext_dynamichandle; ?>,
			minHandleSize: <?php echo $ext_minhandlesize; ?>,
			clickBar: <?php echo $ext_clickbar; ?>,
			syncSpeed: <?php echo $ext_syncspeed; ?>,			
			cycleBy: '<?php echo $ext_cycleby; ?>',
			cycleInterval: <?php echo $ext_cycleinterval; ?>,
			pauseOnHover: <?php echo $ext_pauseonhover; ?>,
			startPaused: <?php echo $ext_startpaused; ?>, 
			moveBy: <?php echo $ext_moveby; ?>,  
			speed: <?php echo $ext_speed; ?>,   
			easing: '<?php echo $ext_easing; ?>',
			startAt: <?php echo $ext_startat; ?>,
			keyboardNavBy: '<?php echo $ext_keyboardnavby; ?>',
			draggedClass:  '<?php echo $ext_draggedclass; ?>',
			activeClass:   '<?php echo $ext_activeclass; ?>',					
			disabledClass: '<?php echo $ext_disabledclass; ?>',
			
			prev: '#sly_btn_prev_<?php echo $ext_id; ?>',
			next: '#sly_btn_next_<?php echo $ext_id; ?>'
			
			
		});	
  
});
</script>

<div class="mod_ext_sly_images <?php echo $moduleclass_sfx ?>">	
	
	
	<?php if ($scrollbar_poss == 'top') : ?>
	<div id="scrollbar_<?php echo $ext_id; ?>" class="sly_scrollbar">
		<div class="handle">
			<div class="mousearea"></div>
		</div>
	</div>	
	<?php endif; ?>
	
	<div id="ext_sly_<?php echo $ext_id; ?>" class="sly_frame <?php echo $sly_css3_effects_class; ?>">
		<ul class="sly_items_<?php echo $ext_id; ?>" >	
			<?php	
			for($n=0;$n < count($img);$n++) {			
				if( $img[$n] != '') {		
					if ($url[$n] != '') {
						echo '<li><div class="ext-sly-img-item-wrap">';
						echo '<a href="'.$url[$n].'" target="'.$target[$n].'"><img class="sly_img" src="'.$img[$n].'" alt="'.$alt[$n].'" /></a>';
						if ($html[$n] != '') {
							echo '<div class="ext-item-html">'.$html[$n].'</div>';
						}
						echo '</div></li>';
						
					
					} else {
							echo '<li><div class="ext-sly-img-item-wrap">';
							echo '<img class="sly_img" src="'.$img[$n].'" alt="'.$alt[$n].'" />';
							if ($html[$n] != '') {
								echo '<div class="ext-item-html">'.$html[$n].'</div>';
							}
							echo '</div></li>';
						}

				}
			}	
			?>
		</ul>	
	</div>	
		
	<?php if ($scrollbar_poss == 'bottom') : ?>
	<div id="scrollbar_<?php echo $ext_id; ?>" class="sly_scrollbar">
		<div class="handle">
			<div class="mousearea"></div>
		</div>
	</div>	
	<?php endif; ?>
	
	<?php if ($ext_controls_btn > 0) : ?>
	<div class="sly_controls">
		<button id="sly_btn_prev_<?php echo $ext_id; ?>" class="btn prev"><i class="icon-chevron-left"></i> prev</button>		
		<button id="sly_btn_next_<?php echo $ext_id; ?>" class="btn next"><i class="icon-chevron-right"> next</i></button>
	</div>
	<?php endif; ?>
	
	
	
	
	<div style="clear:both;"></div>
</div>

