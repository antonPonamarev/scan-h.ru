<?php
/**
 * ------------------------------------------------------------------------
 * JA Image Hotspot Module for Joomla 2.5 & 3.4
 * ------------------------------------------------------------------------
 * Copyright (C) 2004-2016 J.O.O.M Solutions Co., Ltd. All Rights Reserved.
 * @license - GNU/GPL, http://www.gnu.org/licenses/gpl.html
 * Author: J.O.O.M Solutions Co., Ltd
 * Websites: http://www.joomlart.com - http://www.joomlancers.com
 * ------------------------------------------------------------------------
 */
defined('_JEXEC') or die('Restricted access');
?>

<div class="jai-map-wrap<?php echo $params->get( 'moduleclass_sfx' );?>" id="ja-imagesmap<?php echo $module->id;?>">
	<?php if($modules_des):?>
	<div class="jai-map-description">
	<?php echo $modules_des;?>
	</div>
	<?php endif;?>
	
	<?php if(in_array($dropdownPosition, array('top-left', 'top-right', 'middle-left', 'middle-right'))): ?>
		<?php require $layoutSelect; ?>
	<?php endif; ?>
	<div class="jai-map-container <?php echo $displaytooltips ? 'always-popup' : 'hover-popup'; ?>">
		<?php
		foreach($description as $i => $des):
			$bgimg='';
			$bgcolor = ($des->bgcolor === 'dark') ? 'dark':'light';
			$classpoint = isset($des->classpoint) ? $des->classpoint:'';
			if (!empty($des->ptype) && $des->ptype == 'image') {
				$classpoint .= ' point-img ';
				$bgimg = 'background-image:url(\''.$des->ptype_image.'\');';
			}
			if (!empty($des->ptype) && $des->ptype == 'jaset') {
				$classpoint .= ' point-ico ';
				$bgimg = 'background-image:url(\''.$des->jasetimage.'\');';
			}
			if ($des->ptype != 'icon' && $des->ptype != 'image' && $des->ptype != 'jaset') {
				$classpoint .= ' fa fa-map-marker';
			} elseif (!empty($des->ptype) && $des->ptype == 'icon') {
				if (!empty($des->icon))
					$classpoint .= ' fa fa-'.$des->icon;
				else 
					$classpoint .= ' fa fa-map-marker';
			}
			if($des->offsety > 100) $des->offsety = 95;
			if($des->offsetx > 100) $des->offsetx = 95;
		?>
			<a style="<?php echo $bgimg; ?>text-align:center;background-size:cover;color:<?php echo (!empty($des->iconcolor) ? $des->iconcolor : '#000'); ?>;top:<?php echo $des->offsety; ?>%;left:<?php echo $des->offsetx; ?>%"
			   class="point <?php echo $classpoint.' point'.$i; ?>"
			   href="javascript:void(0)"
			   id="<?php echo 'ja-marker-'.$des->imgid; ?>"
			   data-bgcolor="<?php echo $bgcolor; ?>"
			   data-content_url="<?php echo $des->content_url; ?>"
			   data-link="<?php echo $des->link; ?>"
			   data-vwidth="<?php echo $des->vwidth; ?>"
			   data-vheight="<?php echo $des->vheight; ?>"
			   title="">
				<span class="hidden">Point</span>
			</a>
        <?php endforeach; ?>
        <img src="<?php echo $imgpath;?>" alt=""/>
    </div>

	<?php if(in_array($dropdownPosition, array('bottom-left', 'bottom-right'))): ?>
		<?php require $layoutSelect; ?>
    <?php endif; ?>
	
</div>