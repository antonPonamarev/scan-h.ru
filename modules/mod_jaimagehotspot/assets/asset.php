<?php
/**
 * ------------------------------------------------------------------------
 * JA Image Hotspot Module for Joomla 2.5 & 3.4
 * ------------------------------------------------------------------------
 * Copyright (C) 2004-2016 J.O.O.M Solutions Co., Ltd. All Rights Reserved.
 * @license - GNU/GPL, http://www.gnu.org/licenses/gpl.html
 * Author: J.O.O.M Solutions Co., Ltd
 * Websites: http://www.joomlart.com - http://www.joomlancers.com
 * ------------------------------------------------------------------------
 */

defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$lang = jFactory::getLanguage();
$basepath = JURI::root(true).'/modules/' . $module->module . '/assets/';
//$description =$params->get('description');
jimport('joomla.filesystem.file');


//Load library jquery
require_once(dirname(__FILE__).'/behavior.php');
JHTML::_('JABehavior.jquery');

if(!defined('T3')){
    //Load popover and dropdown library
    $doc->addScript($basepath.'js/bootstrap-tooltip.js?v=1');
    $doc->addScript($basepath.'js/bootstrap-popover.js?v=1');
}
if (!defined('_MODE_JAIMAGESHOSTSPOT_')) {
    define('_MODE_JAIMAGESHOSTSPOT_', 1);
    $doc->addScript($basepath.'js/modernizr.custom.63321.js');

    if($lang->isRTL() == 1){
        $doc->addScript($basepath.'js/jquery.dropdown.rtl.js');
    }else{
        $doc->addScript($basepath.'js/jquery.dropdown.js');
    }
    $doc->addStyleSheet($basepath.'css/style.css?v=1');
    if(version_compare(JVERSION, '3.0', 'lt')) {
        $doc->addStyleSheet($basepath.'css/style_nonbs.css');
    }
    if($lang->isRTL() == 1){
        $doc->addStyleSheet($basepath.'css/style.rtl.css');
    }
    //load override css
    $templatepath = 'templates/'.$app->getTemplate().'/css/'.$module->module.'.css';
    if(file_exists(JPATH_SITE . '/' . $templatepath)) {
        $doc->addStyleSheet(JURI::root(true).'/'.$templatepath);
    }

    $doc->addStyleSheet($basepath.'elements/popover/jquery.webui-popover.css');
    $doc->addScript($basepath.'elements/popover/jquery.webui-popover.js');
}

$displaytooltips = $params->get('displaytooltips',1); // still keep number to compatiable with old version.
$multiple = $params->get('displaymultiple', 0);
$trigger = 'hover';
if ($displaytooltips == 1)
	$trigger = 'sticky';
if ($displaytooltips == 2)
	$trigger = 'click';
$hidedelay = (int) $params->get('hidedelay', 2000);

$animation = $params->get('animation', 'pop');
$data = array();
foreach ($description AS $k => $v) {
	$data[$v->imgid] = $v;
}

$menuID = $app->getMenu()->getActive()->id;
$data = json_encode($data);
//escape special characters
$data = preg_replace("/(\\\\(n|r|t)|')/", '\\\\$1', $data);
$data = str_replace('\"', '\\\"', $data);
$script = "
var rootURL = \"".JUri::root(true)."/\";
var menuID = ".$menuID.";
var jadir = jQuery('html').attr('dir');
    ;(function($){
    	// seperate pointer config.
    	var desc = $.parseJSON('".$data."');
    	// all pointer config.
    	var jaihp_settings = {
			hideDelay: ".$hidedelay.",
			trigger: '".$trigger."',
			multiple: ".$multiple.",
			anim: '".$animation."'
		};

		$(window).load(function(){
			// Remove Chosen Select.
			if ($('#ja-imagesmap".$module->id." #cd-dropdown').hasClass('chzn-done'))
				$('#ja-imagesmap".$module->id." #cd-dropdown').chosen('destroy');
			$('#ja-imagesmap".$module->id." #cd-dropdown').jadropdown({
                gutter : 0,
                stack : false
            });

			$('#ja-imagesmap".$module->id." .cd-dropdown ul li').click(function() {
				target = $(this).attr('data-value');
// 				data = desc[target.replace('ja-marker-','')];
				var _e = $('#ja-imagesmap".$module->id." #'+target);
                setTimeout(function() {
                    WebuiPopovers.show('#ja-imagesmap".$module->id." #'+target);
                }, 100);
			});

// 			setTimeout(function() {
				$('#ja-imagesmap".$module->id." a.point').each(function() {
					var data = desc[$(this).attr('id').replace('ja-marker-','')];
					var _e = $(this);
					OverAllData(_e, data, jaihp_settings);
					if (jaihp_settings.trigger == 'hover' && !is_mobile_device()) {
						// add click to the url if not click event.
						_e.click(function(event) {
							if ($(this).data('link') && data.content_type == 'default') {
								window.open($(this).data('link'), '_blank');
								return;
							}
							if ($(this).data('content_url'))
								window.open($(this).data('content_url'), '_blank');
						});
					}
					if (jaihp_settings.trigger == 'sticky') {
						_e.off().unbind().click(function(event) {
							_e.webuiPopover('destroy');
							_e.off().unbind();
							jaihp_settings2=jaihp_settings;
							jaihp_settings2.trigger='hover';
							OverAllData(_e, data, jaihp_settings2);
						});
					}
				});
// 			}, 1000);
		});
	 })(jQuery);";


$doc->addScriptDeclaration($script);
$doc->addScript($basepath.'js/script.js');