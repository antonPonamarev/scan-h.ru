/**
 * ------------------------------------------------------------------------
 * JA Image Hotspot Module for Joomla 2.5 & 3.4
 * ------------------------------------------------------------------------
 * Copyright (C) 2004-2016 J.O.O.M Solutions Co., Ltd. All Rights Reserved.
 * @license - GNU/GPL, http://www.gnu.org/licenses/gpl.html
 * Author: J.O.O.M Solutions Co., Ltd
 * Websites: http://www.joomlart.com - http://www.joomlancers.com
 * ------------------------------------------------------------------------
 */

// only call this function on touch device.
function mobileCenter($e) {
	jQuery('.japopwarper').fadeIn();
	$e.css('position', 'fixed');
	// always center left and right.
	centerObjResize($e);
}

// Center obj with screen
function centerObj(obj) {
	if (obj.length) {
		_left = (jQuery(window).width() - obj.width())/2;
		obj.css('left', _left+'px');
		_top = jQuery(window).scrollTop() + (jQuery(window).height()-obj.height())/2;
		obj.css('top', (_top)+'px');
	}
}

// Center obj fixed.
function centerObjResize(obj) {
	if (obj.length) {
		_left = (jQuery(window).width() - obj.width())/2;
		obj.css('left', _left+'px');
		_top = (jQuery(window).height() - obj.height())/2;
		obj.css('top', _top+'px');
	}
}

function is_iDevice(){
	return navigator.platform.match(/iPhone|iPod|iPad/i);
}

function mobileCenterAfterOrient() {
	// center obj after orientation change
	centerObjResize(jQuery('.webui-popover.in'));
	centerObjResize(jQuery('.japopover.japopmedia'));
}

function detectVideo(_e, $e) {
	if ($e.find('.jashowvideo').length) {
		$e.find('.jashowvideo').off().unbind().bind('click', function() {
			jQuery('.japopwarper').fadeIn();
			vwidth=_e.data('vwidth');
			vheight=_e.data('vheight');
			_e.after('<div class="japopover japopmedia '+_e.data('bgcolor')+' touchdv popover fade in" style="position:fixed;z-index: 99999;width:'+vwidth+'px;max-width:'+(vwidth)+'px;height:'+(vheight)+'px;display: block;">'+
			'<span class="popover-close"> <i class="fa fa-remove"></i> </span>'+
			'<div class="popover-content">'+jQuery(this).data('ifr')+'</div>'+
			'</div>');
			_e.next().find('.popover-close').off().unbind().bind('click', function(){
				jQuery(this).parent('.japopmedia').remove();
				jQuery('.japopwarper').fadeOut();
			});
			_left = (jQuery(window).width() - vwidth)/2;
			jQuery('.japopover.japopmedia').css('left', _left+'px');
			_top = (jQuery(window).height() - vheight)/2;
			jQuery('.japopover.japopmedia').css('top', _top+'px');
			WebuiPopovers.hideAll(); // Hide all popovers
		});
	}
}

function OverAllData(_e, data, jaihp_settings) {
	var item_settings = {
		content:data.details,
		width:(data.popwidth == undefined ? 'auto' : data.popwidth), // default width
		height:(data.popheight == undefined ? 'auto' : data.popheight), // default height
// 		container: jQuery('body'), // container will always be window.
		placement:data.placement ? data.placement : 'auto',
// 		cache:true,
		style: /*'japopover popover '+*/_e.data('bgcolor'),
		direction:jadir,
		trigger:is_mobile_device() ? 'click' : jaihp_settings.trigger, // always click on mobile device.
		arrow:!is_mobile_device(), // do not show arrow on mobile device.
		multi: (jaihp_settings.trigger == 'click' && jaihp_settings.multiple == 1) ? true : false,
		animation:jaihp_settings.anim,
		onShow : function($e) {
			if (is_mobile_device())
				mobileCenter($e);
		},
		onHide: function($e) {
            jQuery('a[data-target="'+$e.attr('id')+'"]').show();
        },
		delay: {hide: jaihp_settings.hideDelay}
	};

	item_settings.type='async';
	item_settings.url=rootURL+'index.php?option=com_ajax&module=jaimagehotspot&format=raw&method=getcontent&Itemid='+menuID;
	item_settings.async= {
		type:'POST',
		data: data,
		before: function(that, xhr, settings) {},//executed before ajax request
		success: function(that, data) {
			if (data.search('twitter') !== -1) {
				that.setContent(that.content);
				var $targetContent = that.getContentElement();
				$targetContent.removeAttr('style');
				that.displayContent();
			} else {
				jQuery('.japopwarper_content').html(data);
				if (jQuery('.japopwarper_content').find('img').length) {
					if (jQuery('.japopwarper_content').find('img').length > 1)
						$img = jQuery('.japopwarper_content').find('img').first();
					else 
						$img = jQuery('.japopwarper_content').find('img');
					$img.load(function(){
						var $targetContent = that.getContentElement();
						$targetContent.html(jQuery('.japopwarper_content').html());
						// check for video
						detectVideo(that.$element, $targetContent);
						$targetContent.removeAttr('style');
						that.displayContent();
						if (is_mobile_device()) 
							mobileCenter(that.$target);
					});
				} else {
					var $targetContent = that.getContentElement();
					$targetContent.html(data);
					$targetContent.removeAttr('style');
					// check for video
					detectVideo(that.$element, $targetContent);
					that.displayContent();
				}

			}
			if (is_mobile_device()) 
				mobileCenter(that.$target);
		},//executed after successful ajax request
		error: function(that, xhr, data) {} //executed after error ajax request
	};
	if (data.content_type == 'social') {
		if (data.content_url.match('facebook')) {
			item_settings.width=(data.popwidth == undefined ? 370 : data.popwidth);
			item_settings.height=(data.popheight == undefined ? 105 : data.popheight);
			if (is_mobile_device()) {
				item_settings.width=310;
				item_settings.height=105;
			}
		}
		if (data.content_url.match('twitter')) {
			item_settings.width=(data.popwidth == undefined ? 400 : data.popwidth);
			item_settings.height=(data.popheight == undefined ? 230 : data.popheight);
			item_settings.cache=false;
			item_settings.animation=null;
			if (is_mobile_device()) {
				item_settings.width=310;
				item_settings.height=203;
			}
		}
		if (data.content_url.match('pinterest') || data.content_url.match('instagram')) {
			item_settings.width=(data.popwidth == undefined ? 280 : data.popwidth);
			item_settings.height=(data.popheight == undefined ? 'auto' : data.popheight);
			if (is_mobile_device()) {
				item_settings.width=280;
				item_settings.height='auto';
			}
		}
	}
	if (data.content_type == 'video') { // video only.
		item_settings.width=(data.popwidth == undefined ? 280 : data.popwidth);
		item_settings.height=(data.popheight == undefined ? 315 : data.popheight);
		if (is_mobile_device()) {
			item_settings.width=280;
			item_settings.height=315;
		}
	}
	if (data.content_type == 'website') {
		item_settings.width=(data.popwidth == undefined ? 300 : data.popwidth);
		item_settings.height=(data.popheight == undefined ? 'auto' : data.popheight);
		if (is_mobile_device()) {
			item_settings.width=300;
			item_settings.height='auto';
		}
	}
	if (data.content_type == 'default') {
		item_settings.title=data.title;
		item_settings.width=(data.popwidth == undefined ? 300 : data.popwidth);
		item_settings.height=(data.popheight == undefined ? 'auto' : data.popheight);
		if (is_mobile_device()) {
			item_settings.width=300;
			item_settings.height='auto';
		}
	}
	
	// not use ajax if do not have image and default type.
	if (data.content_type == 'default' && data.content_img == '') {
		item_settings.type='html';
		item_settings.url=null;
		item_settings.async=null;
	}
	
	_e.webuiPopover(item_settings);
}

// check if mobile.
function is_mobile_device() {
	try {
		document.createEvent("TouchEvent");
		if (jQuery(window).width() >= 768)
			return false;
		return true;
	} catch (e) {
		return false;
	}
}
function is_landscape() {
	return (jQuery(window).width() > jQuery(window).height());
}

jQuery(document).ready(function() {
	jQuery('body').prepend('<div class="japopwarper_content" style="display:none;"></div>');
	if (!jQuery('.japopwarper').length) {
		jQuery('.jai-map-container').append('<div class="japopwarper"></div>');
		if (!is_mobile_device()) _ev = 'click';
			_ev = 'touchend';
		jQuery('.japopwarper').off().unbind().on(_ev, function(){
			jQuery('.japopwarper').fadeOut();
			jQuery('.japopmedia').remove();
			WebuiPopovers.hideAll(); // Hide all popovers
		});
	}
	jQuery(window).on('resize', function(event) {
		if (is_mobile_device()) setTimeout(mobileCenterAfterOrient(), 1000);
    });
});