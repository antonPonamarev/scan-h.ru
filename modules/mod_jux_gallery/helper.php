<?php

/**
 * @version	$Id$
 * @author	JoomlaUX
 * @package	Joomla.Site
 * @subpackage	mod_jux_gallery
 * @copyright	Copyright (C) 2013 JoomlaUX. All rights reserved.
 * @license	License GNU General Public License version 2 or later; see LICENSE.txt, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

class modJUX_GalleryHelper
{

    public static function getItems($params)
    {
	$db = JFactory::getDbo();
	$query = $db->getQuery(true);

	$query->select('a.*');
	$query->from('#__jux_gallery_items  AS a');

	// Join over the categories
	$query->select('c.title AS category_title, c.alias AS category_alias');
	$query->leftJoin('#__jux_gallery_categories AS c ON c.id = a.cat_id');

	// Filter by state
	$query->where('a.published = 1');

	$query->where('c.published = 1');
	//Filter by cat_id
	$catid = $params->get('listAlbum');
	if ($catid[0] == '')
	{
	    $query = $query;
	} else
	{
	    JArrayHelper::toInteger($catid);
	    $catid = implode(',', $catid);
	    $query->where('a.cat_id IN (' . $catid . ')');
	}

	//Filter by Access
	$user = JFactory::getUser();
	$groups = implode(',', $user->getAuthorisedViewLevels());
	
	$query->where('a.access IN (' . $groups . ')');
	$query->where('c.access IN (' . $groups . ')');
	$query->order('c.ordering ASC');
	//Filter by language
	$query->where('a.language in (' . $db->quote(JFactory::getLanguage()->getTag()) . ',' . $db->quote('*') . ')');

	// Filter by Order
	$order_by = '';
	$order = $params->get('orderBy');
	switch ($order)
	{
	    case 'date':
		$order_by .= 'a.created';
		break;
	    case 'rdate':
		$order_by .= 'a.created DESC';
		break;
	    case 'alpha':
		$order_by .= 'a.title';
		break;
	    case 'ralpha':
		$order_by .= 'a.title DESC';
		break;
	    case 'order':
		$order_by .= 'a.ordering';
		break;
	    default:
		$order_by .= 'a.ordering';
	}

	$query->order($order_by);
	$db->setQuery($query);
	
	$items = $db->loadObjectList();

	return $items;
    }

    public static function getCategories($params)
    {
	$db = JFactory::getDbo();

	$query = $db->getQuery(true);
	$query->select('a.*');
	$query->from('#__jux_gallery_categories as a');
	//Filter By Category
	$listAlbum = $params->get('listAlbum');
	$allCategories = FALSE;
	foreach ($listAlbum as $album){
	    if($album == '')
		$allCategories = TRUE;
	}
	if(!$allCategories){
	    $listAlbum = implode(',', $listAlbum);
	    $query->where('a.id IN ('.$listAlbum.')');
	}
	$query->select('COUNT(i.id) as count');
	$query->leftJoin('#__jux_gallery_items as i ON a.id = i.cat_id');
	$query->group('a.id');
	
	$query->where('a.published = 1');
	$query->order('a.ordering ASC');

	$db->setQuery($query);
	$categories = $db->loadObjectList();
	return $categories;
    }

}
