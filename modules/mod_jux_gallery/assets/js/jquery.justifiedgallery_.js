/* 
Justified Gallery
Version: 1.0.3
Author: Miro Mannino
Author URI: http://miromannino.it

Copyright 2012 Miro Mannino (miro.mannino@gmail.com)

This file is part of Justified Gallery.

This work is licensed under the Creative Commons Attribution 3.0 Unported License. 

To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/ 
or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
*/

__justifiedGallery_galleryID = 0;

/*
* debouncedresize: special jQuery event that happens once after a window resize
*
* latest version and complete README available on Github:
* https://github.com/louisremi/jquery-smartresize/blob/master/jquery.debouncedresize.js
*
* Copyright 2011 @louis_remi
* Licensed under the MIT license.
*/
var $event = $.event,
$special,
resizeTimeout;

$special = $event.special.debouncedresize = {
	setup: function() {
		$( this ).on( "resize", $special.handler );
	},
	teardown: function() {
		$( this ).off( "resize", $special.handler );
	},
	handler: function( event, execAsap ) {
		// Save the context
		var context = this,
			args = arguments,
			dispatch = function() {
				// set correct event type
				event.type = "debouncedresize";
				$event.dispatch.apply( context, args );
			};

		if ( resizeTimeout ) {
			clearTimeout( resizeTimeout );
		}

		execAsap ?
			dispatch() :
			resizeTimeout = setTimeout( dispatch, $special.threshold );
	},
	threshold: 250
};

// ======================= imagesLoaded Plugin ===============================
// https://github.com/desandro/imagesloaded

// $('#my-container').imagesLoaded(myFunction)
// execute a callback when all images have loaded.
// needed because .load() doesn't work on cached images

// callback function gets image collection as argument
//  this is the container

// original: MIT license. Paul Irish. 2010.
// contributors: Oren Solomianik, David DeSandro, Yiannis Chatzikonstantinou

// blank image data-uri bypasses webkit log warning (thx doug jones)
var BLANK = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==';

$.fn.imagesLoaded = function( callback ) {
	var $this = this,
		deferred = $.isFunction($.Deferred) ? $.Deferred() : 0,
		hasNotify = $.isFunction(deferred.notify),
		$images = $this.find('img').add( $this.filter('img') ),
		loaded = [],
		proper = [],
		broken = [];

	// Register deferred callbacks
	if ($.isPlainObject(callback)) {
		$.each(callback, function (key, value) {
			if (key === 'callback') {
				callback = value;
			} else if (deferred) {
				deferred[key](value);
			}
		});
	}

	function doneLoading() {
		var $proper = $(proper),
			$broken = $(broken);

		if ( deferred ) {
			if ( broken.length ) {
				deferred.reject( $images, $proper, $broken );
			} else {
				deferred.resolve( $images );
			}
		}

		if ( $.isFunction( callback ) ) {
			callback.call( $this, $images, $proper, $broken );
		}
	}

	function imgLoaded( img, isBroken ) {
		// don't proceed if BLANK image, or image is already loaded
		if ( img.src === BLANK || $.inArray( img, loaded ) !== -1 ) {
			return;
		}

		// store element in loaded images array
		loaded.push( img );

		// keep track of broken and properly loaded images
		if ( isBroken ) {
			broken.push( img );
		} else {
			proper.push( img );
		}

		// cache image and its state for future calls
		$.data( img, 'imagesLoaded', { isBroken: isBroken, src: img.src } );

		// trigger deferred progress method if present
		if ( hasNotify ) {
			deferred.notifyWith( $(img), [ isBroken, $images, $(proper), $(broken) ] );
		}

		// call doneLoading and clean listeners if all images are loaded
		if ( $images.length === loaded.length ){
			setTimeout( doneLoading );
			$images.unbind( '.imagesLoaded' );
		}
	}

	// if no images, trigger immediately
	if ( !$images.length ) {
		doneLoading();
	} else {
		$images.bind( 'load.imagesLoaded error.imagesLoaded', function( event ){
			// trigger imgLoaded
			imgLoaded( event.target, event.type === 'error' );
		}).each( function( i, el ) {
			var src = el.src;

			// find out if this image has been already checked for status
			// if it was, and src has not changed, call imgLoaded on it
			var cached = $.data( el, 'imagesLoaded' );
			if ( cached && cached.src === src ) {
				imgLoaded( el, cached.isBroken );
				return;
			}

			// if complete is true and browser supports natural sizes, try
			// to check for image status manually
			if ( el.complete && el.naturalWidth !== undefined ) {
				imgLoaded( el, el.naturalWidth === 0 || el.naturalHeight === 0 );
				return;
			}

			// cached images don't fire load sometimes, so we reset src, but only when
			// dealing with IE, or image is complete (loaded) and failed manual check
			// webkit hack from http://groups.google.com/group/jquery-dev/browse_thread/thread/eee6ab7b2da50e1f
			if ( el.readyState || el.complete ) {
				el.src = BLANK;
				el.src = src;
			}
		});
	}

	return deferred ? deferred.promise( $this ) : $this;
};

(function($){
 
		// global
		var $window = $( window ),
			Modernizr = window.Modernizr;

		$.Stapel = function( options, element ) {
			
			this.el = $( element );
			this._init( options );
			
		};

		// the options
		$.Stapel.defaults = {
			// space between the items
			gutter : 40,
			// the rotations degree for the 2nd and 3rd item 
			// (to give a more realistic pile effect)
			pileAngles : 2,
			// animation settings for the clicked pile's items
			pileAnimation : { 
				openSpeed : 400,
				openEasing : 'cubic-bezier(.47,1.34,.9,1.03)',
				closeSpeed : 400,
				closeEasing : 'ease-in-out'
			},
			// animation settings for the other piles
			otherPileAnimation : { 
				openSpeed : 400,
				openEasing : 'ease-in-out',
				closeSpeed : 350,
				closeEasing : 'ease-in-out'
			},
			// delay for each item of the pile
			delay : 0,
			// random rotation for the items once opened
			randomAngle : false,
			onLoad : function() { return false; },
			onBeforeOpen : function( pileName ) { return false; },
			onAfterOpen : function( pileName, totalItems ) { return false; },
			onBeforeClose : function( pileName ) { return false; },
			onAfterClose : function( pileName, totalItems ) { return false; }
		};

		$.Stapel.prototype = {

			_init : function( options ) {
				
				// options
				this.options = $.extend( true, {}, $.Stapel.defaults, options );

				// cache some elements
				this._config();
				
				// preload images
				var self = this;
				this.el.imagesLoaded( function() {
					self.options.onLoad();
					self._layout();
					self._initEvents();
				} );

			},
			_config : function() {

				// css transitions support
				this.support = Modernizr.csstransitions;

				var transEndEventNames = {
						'WebkitTransition' : 'webkitTransitionEnd',
						'MozTransition' : 'transitionend',
						'OTransition' : 'oTransitionEnd',
						'msTransition' : 'MSTransitionEnd',
						'transition' : 'transitionend'
					},
					transformNames = {
						'WebkitTransform' : '-webkit-transform',
						'MozTransform' : '-moz-transform',
						'OTransform' : '-o-transform',
						'msTransform' : '-ms-transform',
						'transform' : 'transform'
					};

				if( this.support ) {

					this.transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ] + '.cbpFWSlider';
					this.transformName = transformNames[ Modernizr.prefixed( 'transform' ) ];

				}

				// true if one pile is opened
				this.spread = false;

				// the li's
				this.items = this.el.children( 'li' ).hide();
				
				// close pile
				this.close = $( '#tp-close' );

			},
			_getSize : function() {

				this.elWidth = this.el.outerWidth( true );

			},
			_initEvents : function() {

				var self = this;
				$window.on( 'debouncedresize.stapel', function() { self._resize(); } );
				this.items.on( 'click.stapel', function() {
					
					var $item = $( this );

					if( !self.spread && $item.data( 'isPile' ) ) {

						self.spread = true;
						self.pileName = $item.data( 'pileName' );
						self.options.onBeforeOpen( self.pileName );
						self._openPile();						
						return false;

					}

				} );

			},
			_layout : function() {

				/*
				piles() : save the items info in a object with the following structure:

				this.piles = {
					
					pileName : {
						
						// elements of this pile (note that an element can be also in a different pile)
						// for each element, the finalPosition is the position of the element when the pile is opened
						elements : [
							{ el : HTMLELEMENT, finalPosition : { left : LEFT, top : TOP } },
							{},
							{},
							...
						],
						// this is the position of the pile (all elements of the pile) when the pile is closed
						position : { left : LEFT, top : TOP },
						index : INDEX
					},

					// more piles
					...

				}
				*/
				this._piles();

				// items width & height
				// assuming here that all items have the same width and height
				this.itemSize = { width : this.items.outerWidth( true ) , height : this.items.outerHeight( true ) };

				// remove original elements
				this.items.remove();

				// applies some initial style for the items
				this._setInitialStyle();

				this.el.css( 'min-width', this.itemSize.width + this.options.gutter );

				// gets the current ul size (needed for the calculation of each item's position)
				this._getSize();

				// calculate and set each Pile's elements position based on the current ul width
				// this function will also be called on window resize
				this._setItemsPosition();
				
				// new items
				this.items = this.el.children( 'li' ).show();
				// total items
				this.itemsCount	= this.items.length;

			},
			_piles : function() {

				this.piles = {};
				var pile, self = this, idx = 0;
				this.items.each( function() {
						
					var $item = $( this ),
						itemPile = $item.attr( 'data-pile' ) || 'nopile-' + $item.index(),
						attr = itemPile.split( ',' );

					for( var i = 0, total = attr.length; i < total; ++i ) {
						
						var pileName = $.trim( attr[i] );
						pile = self.piles[ pileName ];

						if( !pile ) {

							pile = self.piles[ pileName ] = {
								elements : [],
								position : { left : 0, top : 0 },
								index : idx
							};

							++idx;
					
						}
						
						var clone = $item.clone().get(0);
						pile.elements.push( { el : clone, finalPosition : { left : 0, top : 0 } } );
						$( clone ).appendTo( self.el );
					
					}
				
				} );

			},
			_setInitialStyle : function() {

				for( var pile in this.piles ) {

					var p = this.piles[pile];

					for( var i = 0, len = p.elements.length; i < len; ++i ) {

						var $el = $( p.elements[i].el ),
							styleCSS = { transform : 'rotate(0deg)' };

						this._applyInitialTransition( $el );
							
						if( i === len - 2 ) {
							styleCSS = { transform : 'rotate(' + this.options.pileAngles + 'deg)' };
						}
						else if( i === len - 3 ) {
							styleCSS = { transform : 'rotate(-' + this.options.pileAngles + 'deg)' };
						}
						else if( i !== len - 1 ) {
							var extraStyle = { visibility : 'hidden' };
							$el.css( extraStyle ).data( 'extraStyle', extraStyle );
						}
						else if( pile.substr( 0, 6 ) !== 'nopile' ) {
							$el.data( 'front', true ).append( '<div class="tp-title"><span>' + pile + '</span><span>' + len + '</span></div>' );
						}

						$el.css( styleCSS ).data( {
							initialStyle : styleCSS,
							pileName : pile,
							pileCount : len,
							shadow : $el.css( 'box-shadow' ),
							isPile : pile.substr( 0, 6 ) === 'nopile' ? false : true
						} );

					}

				}

			},
			_applyInitialTransition : function( $el ) {

				if( this.support ) {
					$el.css( 'transition', 'left 400ms ease-in-out, top 400ms ease-in-out' );
				}	

			},
			_setItemsPosition : function() {

				var accumL = 0, accumT = 0, 
					l, t, ml = 0,
					lastItemTop = 0;

				for( var pile in this.piles ) {

					var p = this.piles[pile],
						stepW = this.itemSize.width + this.options.gutter,

						accumIL = 0, accumIT = 0, il, it;

					if( accumL + stepW <= this.elWidth ) {

						l = accumL;
						t = accumT;
						accumL += stepW;

					}
					else {

						if( ml === 0 ) {
							ml = Math.ceil( ( this.elWidth - accumL + this.options.gutter ) / 2 );
						}

						accumT += this.itemSize.height + this.options.gutter;
						l = 0;
						t = accumT;
						accumL = stepW;

					}

					p.position.left = l;
					p.position.top = t;

					for( var i = 0, len = p.elements.length; i < len; ++i ) {

						var elem = p.elements[i],
							fp = elem.finalPosition;

						if( accumIL + stepW <= this.elWidth ) {

							il = accumIL;
							it = accumIT;
							accumIL += stepW;

						}
						else {

							accumIT += this.itemSize.height + this.options.gutter;
							il = 0;
							it = accumIT;
							accumIL = stepW;

						}

						fp.left = il;
						fp.top = it;

						var $el = $( elem.el );

						if( pile !== this.pileName ) {
							
							$el.css( { left : p.position.left, top : p.position.top } );

						}
						else {

							lastItemTop = elem.finalPosition.top;
							$el.css( { left : elem.finalPosition.left, top : lastItemTop } );

						}

					}

				}

				// the position of the items will influence the final margin left value and height for the ul
				// center the ul
				lastItemTop = this.spread ? lastItemTop : accumT;
				this.el.css( {
					marginLeft : ml,
					height : lastItemTop + this.itemSize.height
				} );

			},
			_openPile : function() {

				if( !this.spread ) {
					return false;
				}

				// final style
				var fs;
				var $justified = $('#justified');
				for( var pile in this.piles ) {

					var p = this.piles[ pile ], cnt = 0;
					
					for( var i = 0, len = p.elements.length; i < len; ++i ) {
						var elem = p.elements[i],
							$item = $( elem.el ),
							$img = $item.find( 'img' ),
							styleCSS =  pile === this.pileName ? {
								zIndex : 9999,
								visibility : 'hidden',
								 transition : this.support ? 'left ' + this.options.pileAnimation.openSpeed + 'ms ' + ( ( len - i - 1 ) * this.options.delay ) + 'ms ' + this.options.pileAnimation.openEasing + ', top ' + this.options.pileAnimation.openSpeed + 'ms ' + ( ( len - i - 1 ) * this.options.delay ) + 'ms ' + this.options.pileAnimation.openEasing + ', ' + this.transformName + ' ' + this.options.pileAnimation.openSpeed + 'ms ' + ( ( len - i - 1 ) * this.options.delay ) + 'ms ' + this.options.pileAnimation.openEasing : 'none' 
							} :  {
								zIndex : 1,
								transition : this.support ? 'opacity ' + this.options.otherPileAnimation.closeSpeed + 'ms ' + this.options.otherPileAnimation.closeEasing : 'none'
							};

						if( pile === this.pileName ) {

							if( $item.data( 'front' ) ) {
								$item.find( 'div.tp-title' ).hide();
							}

							if( i < len - 1  ) {
								$img.css( 'visibility', 'hidden' );
							}
							
							fs = elem.finalPosition;
							fs.transform = this.options.randomAngle && i !== p.index ? 'rotate(' + Math.floor( Math.random() * ( 5 + 5 + 1 ) - 5 ) + 'deg)' : 'none';

							if( !this.support ) {
								$item.css( 'transform', 'none' );
							}

							// hack: remove box-shadow while animating to prevent the shadow stack effect
							if( i < len - 3 ) {
								$item.css( 'box-shadow', 'none' );
							}
								$item.find('a').clone().appendTo($justified);
							 /* $justified.append($item.find('a'));  */
						}
						else if( i < len - 1  ) {
							$img.css( 'visibility', 'hidden' );
						}

						$item.css( styleCSS );

						var self = this;

						pile === this.pileName ?
							this._applyTransition( $item, fs, this.options.pileAnimation.openSpeed, function( evt ) {

								var target = this.target || this.nodeName;
								if( target !== 'LI' ) {
									return;
								}

								var $el = $( this );

								// hack: remove box-shadow while animating to prevent the shadow stack effect
								$el.css( 'box-shadow', $el.data( 'shadow' ) );

								if( self.support ) {
									$el.off( self.transEndEventName );
								}

								++cnt;
								
								if( cnt === $el.data( 'pileCount' ) ) {

									$( document ).one( 'mousemove.stapel', function() {
										self.el.addClass( 'tp-open' );
									} );
									self.options.onAfterOpen( self.pileName, cnt );

								}

							} ) :
							this._applyTransition( $item, { opacity : 0 }, this.options.otherPileAnimation.closeSpeed );

					}
					
				}
				 /* $('#justified-gallery').empty(); */
//				 $('#justified-gallery').append($justified); 
				this.el.css( 'height', fs.top + this.itemSize.height );	
				$justified.justifiedGallery({
					'usedSuffix':'lt500', 
					'justifyLastRow':this.options.justifyLastRow, 
					'rowHeight':this.options.rowHeight, 
					'fixedHeight':this.options.fixedHeight, 
					'lightbox':this.options.lightbox, 
					'captions':this.options.captions, 
					'margins':this.options.margins,
					'category':this.options.category,
					'pile':this.options.pile
				}); 

			},
			
			_closePile : function() {
				$('#justified').empty();
				var self = this;

				// close..
				if( this.spread ) {

					this.spread = false;

					this.options.onBeforeClose( this.pileName );

					this.el.removeClass( 'tp-open' );

					// final style
					var fs;
					for( var pile in this.piles ) {

						var p = this.piles[ pile ], cnt = 0;
						
						for( var i = 0, len = p.elements.length; i < len; ++i ) {

							var $item = $( p.elements[i].el ),
								styleCSS = pile === this.pileName ? {
									transition : this.support ? 'left ' + this.options.pileAnimation.closeSpeed + 'ms ' + this.options.pileAnimation.closeEasing + ', top ' + this.options.pileAnimation.closeSpeed + 'ms ' + this.options.pileAnimation.closeEasing + ', ' + this.transformName + ' ' + this.options.pileAnimation.closeSpeed + 'ms ' + this.options.pileAnimation.closeEasing : 'none'
								} : {
									transition : this.support ? 'opacity ' + this.options.otherPileAnimation.openSpeed + 'ms ' + this.options.otherPileAnimation.openEasing : 'none'
								};

							$item.css( styleCSS );
							
							fs = p.position;

							if( pile === this.pileName ) {

								$.extend( fs, $item.data( 'initialStyle' ) );

								// hack: remove box-shadow while animating to prevent the shadow stack effect
								if( i < len - 3 ) {
									$item.css( 'box-shadow', 'none' );
								}

							}

							pile === this.pileName ? this._applyTransition( $item, fs, this.options.pileAnimation.closeSpeed, function( evt ) {

								var target = this.target || this.nodeName;
								if( target !== 'LI' ) {
									return;
								}

								var $el = $( this ), extraStyle = $el.data( 'extraStyle' );

								// hack: remove box-shadow while animating to prevent the shadow stack effect
								$el.css( 'box-shadow', $el.data( 'shadow' ) );

								if( self.support ) {
									$el.off( self.transEndEventName );
									self._applyInitialTransition( $el );
								}
								else {
									$el.css( $el.data( 'initialStyle' ) );
								}

								if( extraStyle ) {
									$el.css( extraStyle );
								}

								++cnt;

								if( $el.data( 'front' ) ) {
									$el.find( 'div.tp-title' ).show();
								}
								
								$el.css( 'visibility', 'visible' );

								if( cnt === $el.data( 'pileCount' ) ) {
									self.options.onAfterClose( $el.data( 'pileName' ), cnt );
								}

							} ) : this._applyTransition( $item, { opacity : 1 }, this.options.otherPileAnimation.openSpeed, function( evt ) {

								var target = this.target || this.nodeName;
								if( target !== 'LI' ) {
									return;
								}

								var $el = $( this );

								if( $el.index() < len - 1  ) {
									$el.find( 'img' ).css( 'visibility', 'visible' );
								}

								if( self.support ) {
									$el.off( self.transEndEventName );
									self._applyInitialTransition( $el );
								}

							} );

						}

					}

					// reset pile name
					this.pileName = '';

					// update ul height
					this.el.css( 'height', fs.top + this.itemSize.height );

				}
				
				return false;

			},
			_resize : function() {

				// get ul size again
				this._getSize();
				// reset items positions
				this._setItemsPosition();

			},
			_applyTransition : function( el, styleCSS, speed, fncomplete ) {

				$.fn.applyStyle = this.support ? $.fn.css : $.fn.animate;

				if( fncomplete && this.support ) {

					el.on( this.transEndEventName, fncomplete );

				}

				fncomplete = fncomplete || function() { return false; };

				el.stop().applyStyle( styleCSS, $.extend( true, [], { duration : speed + 'ms', complete : fncomplete } ) );

			},
			closePile : function() {

				this._closePile();

			}

		};
		
		var logError = function( message ) {

			if ( window.console ) {

				window.console.error( message );
			
			}

		};
		
		$.fn.stapel = function( options ) {

			var instance = $.data( this, 'stapel' );
			
			if ( typeof options === 'string' ) {
				
				var args = Array.prototype.slice.call( arguments, 1 );
				
				this.each(function() {
				
					if ( !instance ) {

						logError( "cannot call methods on stapel prior to initialization; " +
						"attempted to call method '" + options + "'" );
						return;
					
					}
					
					if ( !$.isFunction( instance[options] ) || options.charAt(0) === "_" ) {

						logError( "no such method '" + options + "' for stapel instance" );
						return;
					
					}
					
					instance[ options ].apply( instance, args );
				
				});
			
			} 
			else {
			
				this.each(function() {
					
					if ( instance ) {

						instance._init();
					
					}
					else {

						instance = $.data( this, 'stapel', new $.Stapel( options, this ) );
					
					}

				});
			
			}
			
			return instance;
			
		};
 
   $.fn.justifiedGallery = function(options){

		var settings = $.extend( {
			'sizeSuffixes' : {'lt100':'', 'lt240':'', 'lt320':'', 'lt500':'', 'lt640':'', 'lt1024':''},
			'usedSuffix' : 'lt240',
			'justifyLastRow' : true,
			'rowHeight' : 120,
			'fixedHeight' : false,
			'lightbox' : false,
			'captions' : true,
			'margins' : 1,
			'extension' : '.jpg',
			'refreshTime' : 500,
			'minHeight' : 500,
			'speed' : 350,
			'easing' : 'ease',
			'pile':true
		}, options);
			
		function getErrorHtml(message, classOfError){
			return "<div class=\"" + classOfError + "\"style=\"font-size: 12px; border: 1px solid red; background-color: #faa; margin: 10px 0px 10px 0px; padding: 5px 0px 5px 5px;\">" + message + "</div>";
		}

		return this.each(function(index, cont){
			$(cont).addClass("justifiedGallery");

			var loaded = 0;
			var images = new Array($(cont).find("img").length);

			__justifiedGallery_galleryID++;

			if(images.length == 0) return;
			
			$(cont).append("<div class=\"jg-loading\"><div class=\"jg-loading-img\"></div></div>");

			$(cont).find("a").each(function(index, entry){
				var img = $(entry).find("img");

				images[index] = new Array(9);
				images[index]["src"] = $(img).attr("src");
				images[index]["alt"] = $(img).attr("alt");
				images[index]["href"] = $(entry).attr("href");
				images[index]["title"] = $(entry).data("title");
				images[index]["rel"] = "lightbox[gallery-" + __justifiedGallery_galleryID + "]";
				//data for grid				
				images[index]["largesrc"] = $(entry).data("largesrc");
				images[index]["title"] = $(entry).data("title");
				images[index]["description"] = $(entry).data("description");
				//data for category
				images[index]["category"] = $(entry).attr("class");
				$(entry).remove(); //remove the image, we have its data
				
				var img = new Image();
  
				$(img).load(function() {
					if(images[index]["height"] != settings.rowHeight)
						images[index]["width"] = Math.ceil(this.width / (this.height / settings.rowHeight));
					else
						images[index]["width"] = this.width;
					images[index]["height"] = settings.rowHeight;
					images[index]["src"] = images[index]["src"].slice(0, images[index]["src"].length 
										 - (settings.sizeSuffixes[settings.usedSuffix] + settings.extension).length);
		    		if(++loaded == images.length) startProcess(cont, images, settings);
				});
				
				$(img).error(function() {
					$(cont).prepend(getErrorHtml("The image can't be loaded: \"" + images[index]["src"] +"\"", "jg-usedPrefixImageNotFound"));
					images[index] = null;
					if(++loaded == images.length) startProcess(cont, images, settings);
				});
				
				$(img).attr('src', images[index]["src"]);

			});
		});
		
		function startProcess(cont, images, settings){
			//FadeOut the loading image and FadeIn the images after their loading
			$(cont).find(".jg-loading").fadeOut(500, function(){
				$(this).remove(); //remove the loading image
				if(settings.category === true && settings.pile === false)
				{
				    $(cont).parent().find('#justified-filter').css('display','inherit');
//				    var category = $(cont).parent().find('.selected-filter-item').data('category');
//				    images = filterImages($, cont, images, category, settings);
				    //processesImages($, cont, filterImages($, cont, images, category, settings), 0, settings);
				}
				processesImages($, cont, images, 0, settings); 	
			});

		
		}

		function buildImage(image, suffix, nw, nh, l, minRowHeight, settings){
			var ris;
			ris =  "<div class=\"jg-image\" style=\"left:" + l + "px\">";
			ris += " <a href=\"" + image["href"] + "\" ";
			ris += "class=\""+ image["category"] +"\"";
			ris += "data-largesrc=\""+ image["largesrc"] +"\"";
			ris += "data-title=\""+ image["title"] +"\"";
			ris += "data-description=\""+ image["description"] +"\"";
			if(settings.lightbox == true)
				ris += "rel=\"" + image["rel"] + "\"";
			else
				// ris +=     "target=\"_blank\"";
			ris += "\">";
			// ris +=     "title=\"" + image["title"] + "\">";
			ris += "  <img alt=\"" + image["alt"] + "\" src=\"" + image["src"] + suffix + settings.extension + "\"";
			ris +=        "style=\"width: " + nw + "px; height: " + nh + "px;\">";
			
			if(settings.captions)
				ris += "  <div style=\"bottom:" + (nh - minRowHeight + 20) + "px;\" class=\"jg-image-label\">" + image["alt"] + "</div>";

			ris += " </a></div>";
			return ris;
		}

		function buildContRow(row, images, extraW, settings){
			var j, l = 0;
			var minRowHeight;
			for(var j = 0; j < row.length; j++){
				row[j]["nh"] = Math.ceil(images[row[j]["indx"]]["height"] * 
					            ((images[row[j]["indx"]]["width"] + extraW) / 
							 	images[row[j]["indx"]]["width"]));
				
				row[j]["nw"] = images[row[j]["indx"]]["width"] + extraW;

				row[j]["suffix"] = getSuffix(row[j]["nw"], row[j]["nh"], settings);

				row[j]["l"] = l;

				if(!settings.fixedHeight){
					if(j == 0) 
						minRowHeight = row[j]["nh"];
					else
						if(minRowHeight > row[j]["nh"]) minRowHeight = row[j]["nh"];
				}
				 
				l += row[j]["nw"] + settings.margins;
			}

			if(settings.fixedHeight) minRowHeight = settings.rowHeight;
			
			var rowCont = "";
			for(var j = 0; j < row.length; j++){
				rowCont += buildImage(images[row[j]["indx"]], row[j]["suffix"], 
					                  row[j]["nw"], row[j]["nh"], row[j]["l"], minRowHeight, settings);
			}
			
			return "<li class=\"jg-rows\"><div class=\"jg-row\" style=\"height: " + minRowHeight + "px; margin-bottom:" + settings.margins + "px;\">" + rowCont + "</div></li>";
		}

		function getSuffix(nw, nh, settings){
			var n;
			if(nw > nh) n = nw; else n = nh;
			if(n <= 100){
				return settings.sizeSuffixes.lt100; //thumbnail (longest side:100)
			}else if(n <= 240){
				return settings.sizeSuffixes.lt240; //small (longest side:240)
			}else if(n <= 320){
				return settings.sizeSuffixes.lt320; //small (longest side:320)
			}else if(n <= 500){
				return settings.sizeSuffixes.lt500; //small (longest side:320)
			}else if(n <= 640){
				return settings.sizeSuffixes.lt640; //medium (longest side:640)
			}else{
				return settings.sizeSuffixes.lt1024; //large (longest side:1024)
			}
		}

		function processesImages($, cont, images, lastRowWidth, settings){	
			var row = new Array();
			var row_i, i;
			var partialRowWidth = 0;
			var extraW;
			var rowWidth = $(cont).width();

			for(i = 0, row_i = 0; i < images.length; i++){
				if(images[i] == null) continue;
				if(partialRowWidth + images[i]["width"] + settings.margins <= rowWidth){
					//we can add the image
					partialRowWidth += images[i]["width"] + settings.margins;
					row[row_i] = new Array(5);
					row[row_i]["indx"] = i;
					row_i++;
				}else{
					//the row is full
					extraW = Math.ceil((rowWidth - partialRowWidth + 1) / row.length); 
					$(cont).append(buildContRow(row, images, extraW, settings));

					row = new Array();
					row[0] = new Array(5);
					row[0]["indx"] = i;
					row_i = 1;
					partialRowWidth = images[i]["width"] + settings.margins;
				}
			}

			//last row----------------------
			//now we have all the images index loaded in the row arra
			if(settings.justifyLastRow){
				extraW = Math.ceil((rowWidth - partialRowWidth + 1) / row.length);	
			}else{
				extraW = 0;
			}
			$(cont).append(buildContRow(row, images, extraW, settings));
			//---------------------------

			//lightbox-------------------
			if(settings.lightbox){
				try{
					/* $(cont).find('.jg-image').children('a').magnificPopup({
					  delegate: 'a',
					  type: 'image',
					  tLoading: 'Loading image #%curr%...',
					   mainClass: 'mfp-img-mobile',
					  gallery: {
						enabled: true,
						navigateByImgClick: true,
						preload: [0,1] // Will preload 0 - before current, and 1 after the current image
					  }
					   image: {
						tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
						titleSrc: function(item) {
						  return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
						}
					  } 
					});   */ 
				}catch(e){
					$(cont).html(getErrorHtml("No Colorbox founded!", "jg-noColorbox"));
				} 
			}

			//Captions---------------------
			if(settings.captions){
				$(cont).find(".jg-image").mouseenter(function(sender){
					$(sender.currentTarget).find(".jg-image-label").stop();
					$(sender.currentTarget).find(".jg-image-label").fadeTo(500, 0.7);
				});
				$(cont).find(".jg-image").mouseleave(function(sender){
					$(sender.currentTarget).find(".jg-image-label").stop();
					$(sender.currentTarget).find(".jg-image-label").fadeTo(500, 0); 
				});
			}
			
			$(cont).find(".jg-resizedImageNotFound").remove();
			
			//fade in the images that we have changed and need to be reloaded
			$(cont).find(".jg-image img").load(function(){
					$(this).fadeTo(500, 1);
			}).error(function(){
				$(cont).prepend(getErrorHtml("The image can't be loaded: \"" +  $(this).attr("src") +"\"", "jg-resizedImageNotFound"));
			}).each(function(){
					if(this.complete){ 
						$(this).load();		
					}
			});
			
			//Justified filter
			$(cont).parent().find('#justified-filter li').on( 'click', function(e) {
			if($(this).attr('class') != 'selected-filter-item'){
				$(this).parent().find('.selected-filter-item').removeClass('selected-filter-item');
				$(this).addClass( "selected-filter-item");
				var category = $(this).parent().find('.selected-filter-item').data('category');
				$(cont).children().remove();
				processesImages($, cont, filterImages($, cont, images, category, settings), 0, settings);
			}				
			});
			
			checkWidth($, cont, images, rowWidth, settings);
			//No Lightbox---------------------
			if(settings.lightbox !== true){
			var Grid = (function() {

						// list of items
					var $grid = $('#justified-gallery'),
						// the items
						$items = $grid.find('.jg-image'),
						// current expanded item's index
						current = -1,
						// position (top) of the expanded item
						// used to know if the preview will expand in a different row
						previewPos = -1,
						// extra amount of pixels to scroll the window
						scrollExtra = 0,
						// extra margin when expanded (between preview overlay and the next items)
						marginExpanded = 10,
						$window = $( window ), winsize,
						$body = $( 'html, body' ),
						// transitionend events
						transEndEventNames = {
							'WebkitTransition' : 'webkitTransitionEnd',
							'MozTransition' : 'transitionend',
							'OTransition' : 'oTransitionEnd',
							'msTransition' : 'MSTransitionEnd',
							'transition' : 'transitionend'
						},
						transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
						// support for csstransitions
						support = Modernizr.csstransitions,
						// default settings
						settings = {
							minHeight : 500,
							speed : 350,
							easing : 'ease'
						};

					function init( config ) {
						
						// the settings..
						settings = $.extend( true, {}, settings, config );

						// preload all images
						$grid.imagesLoaded( function() {

							// save item´s size and offset
							saveItemInfo( true );
							// get window´s size
							getWinSize();
							// initialize some events
							initEvents();

						} );

					}

					// add more items to the grid.
					// the new items need to appended to the grid.
					// after that call Grid.addItems(theItems);
					function addItems( $newitems ) {

						$items = $items.add( $newitems );

						$newitems.each( function() {
							var $item = $( this );
							$item.data( {
								offsetTop : $item.offset().top,
								height : $item.height()
							} );
						} );

						initItemsEvents( $newitems );

					}

					// saves the item´s offset top and height (if saveheight is true)
					function saveItemInfo( saveheight ) {
						$items.each( function() {
							var $item = $( this );
							$item.data( 'offsetTop', $item.offset().top );
							if( saveheight ) {
								$item.data( 'height', $item.height() );
							}
						} );
					}

					function initEvents() {
						
						// when clicking an item, show the preview with the item´s info and large image.
						// close the item if already expanded.
						// also close if clicking on the item´s cross
						initItemsEvents( $items );
						
						// on window resize get the window´s size again
						// reset some values..
						$window.on( 'debouncedresize', function() {
							
							scrollExtra = 0;
							previewPos = -1;
							// save item´s offset
							saveItemInfo();
							getWinSize();
							var preview = $.data( this, 'preview' );
							if( typeof preview != 'undefined' ) {
								hidePreview();
							}

						} );

					}

					function initItemsEvents( $items ) {
						//Show Preview Item when 'a' clicked	
						$items.children( 'a' ).on( 'click', function(e) {
							var $item = $( this ).parent();
							// check if item already opened
							current === $item.index('.jg-image') ? hidePreview() : showPreview( $item );
							return false;
						} );
						//Close Preview Item
						$items.parent().parent('.jg-rows').on( 'click', 'span.og-close', function() {
							hidePreview();
							return false;
						} );
						// Next item when button right clicked
						$items.parent().parent('.jg-rows').on( 'click', '.og-arrow-right', function() {
							var $this = $(this).parent().parent().parent().parent().find('.og-expanded');
							if($this.next().length  !== 0){
								var $item = $this.next();
							}else if($this.parent().parent().next().length !== 0){
								var $item = $this.parent().parent().next().find('.jg-image')[0];
								$item = $($item);
							}else{
								var $item = $($items[0]);
							}							
							current === $item.index() ? showPreview($item.next()) : showPreview( $item );
							return false;
						} );
						// Prev item when button left clicked
						$items.parent().parent('.jg-rows').on( 'click', '.og-arrow-left', function() {
							var $this = $(this).parent().parent().parent().parent().find('.og-expanded');
							if($this.prev().length  !== 0){
								var $item = $this.prev();
							}else if($this.parent().parent().prev().length !== 0){
								var itemLength = $this.parent().parent().prev().find('.jg-image').length;
								var $item = $this.parent().parent().prev().find('.jg-image')[itemLength - 1];
								$item = $($item);
							}else{
								var length = $items.length - 1;
								var $item = $($items[length]);
							}
							
							current === $item.index() ? showPreview($item.prev()) : showPreview( $item );
							return false;
						} );
						
						// Lightbox popup
						
						
					}

					function getWinSize() {
						winsize = { width : $window.width(), height : $window.height() };
					}

					function showPreview( $item ) {

						var preview = $.data( this, 'preview' ),
							// item´s offset top
							position = $item.data( 'offsetTop' );

						scrollExtra = 0;

						// if a preview exists and previewPos is different (different row) from item´s top then close it
						if( typeof preview != 'undefined' ) {

							// not in the same row
							if( previewPos !== position ) {
								// if position > previewPos then we need to take te current preview´s height in consideration when scrolling the window
								if( position > previewPos ) {
									scrollExtra = preview.height;
								}
								hidePreview();
							}
							// same row
							else {
								preview.update( $item );
								return false;
							}
							
						}

						// update previewPos
						previewPos = position;
						// initialize new preview for the clicked item
						preview = $.data( this, 'preview', new Preview( $item ) );
						// expand preview overlay
						preview.open();

					}

					function hidePreview() {
						current = -1;
						var preview = $.data( this, 'preview' );
						preview.close();
						$.removeData( this, 'preview' );
					}

					// the preview obj / overlay
					function Preview( $item ) {
						this.$item = $item;
						this.expandedIdx = this.$item.index('.jg-image');
						this.create();
						this.update();
					}

					Preview.prototype = {
						create : function() {
							// create Preview structure:
							this.$title = $( '<h3></h3>' );
							this.$description = $( '<p></p>' );
							this.$href = $( '<a href="#">Visit website</a>' );
							this.$details = $( '<div class="og-details"></div>' ).append( this.$title, this.$description, this.$href );
							this.$loading = $( '<div class="og-loading"></div>' );
							this.$fullimage = $( '<div class="og-fullimg"></div>' ).append( this.$loading );
							this.$closePreview = $( '<span class="og-close"></span>' );
							this.$right = $('<button class="og-arrow og-arrow-right"></button>');
							this.$left = $('<button class="og-arrow og-arrow-left"></button>');
							this.$previewInner = $( '<div class="og-expander-inner"></div>' ).append( this.$closePreview, this.$fullimage, this.$details, this.$left, this.$right );							
							this.$previewEl = $( '<div class="og-expander"></div>' ).append( this.$previewInner );
							// append preview element to the item
							this.$item.parent().parent().append( this.getEl() );
							// set the transitions for the preview and the item
							if( support ) {
								this.setTransition();
							}
						},
						update : function( $item ) {

							if( $item ) {
								this.$item = $item;
							}
							
							// if already expanded remove class "og-expanded" from current item and add it to new item
							if( current !== -1 ) {
								var $currentItem = $items.eq( current );
								$currentItem.removeClass( 'og-expanded' );
								this.$item.addClass( 'og-expanded' );
								// position the preview correctly
								this.positionPreview();
							}

							// update current value
							current = this.$item.index('.jg-image');

							// update preview´s content
							var $itemEl = this.$item.children( 'a' ),
								eldata = {
									href : $itemEl.attr( 'href' ),
									largesrc : $itemEl.data( 'largesrc' ),
									title : $itemEl.data( 'title' ),
									description : $itemEl.data( 'description' )
								};

							this.$title.html( eldata.title );
							this.$description.html( eldata.description );
							this.$href.attr( 'href', eldata.href );

							var self = this;
							
							// remove the current image in the preview
							if( typeof self.$largeImg != 'undefined' ) {
								self.$largeImg.remove();
							}

							// preload large image and add it to the preview
							// for smaller screens we don´t display the large image (the media query will hide the fullimage wrapper)
							if( self.$fullimage.is( ':visible' ) ) {
								this.$loading.show();
								$( '<img/>' ).load( function() {
									var $img = $( this );
									if( $img.attr( 'src' ) === self.$item.children('a').data( 'largesrc' ) ) {
										self.$loading.hide();
										self.$fullimage.find( 'img' ).remove();
										self.$largeImg = $img.fadeIn( 350 );
										self.$fullimage.append( self.$largeImg );
									}
								} ).attr( 'src', eldata.largesrc );	
							}

						},
						open : function() {

							setTimeout( $.proxy( function() {	
								// set the height for the preview and the item
								var rowHeight = this.$item.parent().height();
								this.setHeights();
								// scroll to position the preview in the right place
								this.positionPreview();
								/* var ogHeight = this.$item.parent().parent().find('.og-expander').height(); */
								var liHeight = 500 + rowHeight;
								this.$item.parent().parent('.jg-rows').css("height",liHeight);
								this.$item.parent().parent('.jg-rows').css("margin-bottom",30);
							}, this ), 25 );

						},
						close : function() {

							var self = this,
								onEndFn = function() {
									if( support ) {
										$( this ).off( transEndEventName );
									}
									self.$item.removeClass('og-expanded' );
									self.$item.parent().parent('.jg-rows').css("height","");
									self.$item.parent().parent('.jg-rows').css("margin-bottom","");
									self.$previewEl.remove();
								};

							setTimeout( $.proxy( function() {

								if( typeof this.$largeImg !== 'undefined' ) {
									this.$largeImg.fadeOut( 'fast' );
								}
								this.$previewEl.css( 'height', 0 );
								// the current expanded item (might be different from this.$item)
								var $expandedItem = $items.eq( this.expandedIdx );
								$expandedItem.css( 'height', $expandedItem.data( 'height' ) ).on( transEndEventName, onEndFn );

								if( !support ) {
									onEndFn.call();
								}

							}, this ), 25 );
							
							return false;

						},
						calcHeight : function() {

							var heightPreview = winsize.height - this.$item.data( 'height' ) - marginExpanded,
								itemHeight = winsize.height;

							if( heightPreview < settings.minHeight ) {
								heightPreview = settings.minHeight;
								itemHeight = settings.minHeight + this.$item.data( 'height' ) + marginExpanded;
							}

							this.height = heightPreview;
							this.itemHeight = itemHeight;

						},
						setHeights : function() {

							var self = this,
								onEndFn = function() {
									if( support ) {
										self.$item.off( transEndEventName );
									}
									self.$item.addClass( 'og-expanded' );
								};

							this.calcHeight();
							this.$previewEl.css( 'height', this.height );
							this.$item.css( 'height', this.itemHeight ).on( transEndEventName, onEndFn );

							if( !support ) {
								onEndFn.call();
							}

						},
						positionPreview : function() {

							// scroll page
							// case 1 : preview height + item height fits in window´s height
							// case 2 : preview height + item height does not fit in window´s height and preview height is smaller than window´s height
							// case 3 : preview height + item height does not fit in window´s height and preview height is bigger than window´s height
							var position = this.$item.data( 'offsetTop' ),
								previewOffsetT = this.$previewEl.offset().top - scrollExtra,
								scrollVal = this.height + this.$item.data( 'height' ) + marginExpanded <= winsize.height ? position : this.height < winsize.height ? previewOffsetT - ( winsize.height - this.height ) : previewOffsetT;
							
							$body.animate( { scrollTop : scrollVal }, settings.speed );

						},
						setTransition  : function() {
							this.$previewEl.css( 'transition', 'height ' + settings.speed + 'ms ' + settings.easing );
							this.$item.css( 'transition', 'height ' + settings.speed + 'ms ' + settings.easing );
						},
						getEl : function() {
							return this.$previewEl;
						}
					}

					return { 
						init : init,
						addItems : addItems
					};

				})();
				Grid.init();
				}			
		
			//No Category Button--------------
			
			
		}
		
		function filterImages($, cont, images, category, settings){
			var filterItems = new Array();
			var i,j=0;
/* 			$(cont).find('.jg-image').css('display','none');
			$(cont).find('.jg-image').each(function(){
				var itemCat = $(this).find('a').attr('class').split(' ');
				if($.inArray(category,itemCat) != -1){
					$(this).css('display','');
				}
			}); */
			for(i = 0; i < images.length; i++){
				if(images[i] == null) continue;
				if(images[i]['category'].indexOf(category)!= -1){
					//we can add the image
					filterItems[j] = new Array();
					filterItems[j]['alt']= images[i]['alt'];
					filterItems[j]['category']= images[i]['category'];
					filterItems[j]['description']= images[i]['description'];
					filterItems[j]['height']= images[i]['height'];
					filterItems[j]['href']= images[i]['href'];
					filterItems[j]['largesrc']= images[i]['largesrc'];
					filterItems[j]['length']= images[i]['length'];
					filterItems[j]['rel']= images[i]['rel'];
					filterItems[j]['src']= images[i]['src'];
					filterItems[j]['title']= images[i]['title'];
					filterItems[j]['width']= images[i]['width'];
					j++;
				}
			}
			return filterItems;
		}
		
		function checkWidth($, cont, images, lastRowWidth, settings){
			var id = setInterval(function(){
				if(lastRowWidth != $(cont).width()){
					$(cont).find(".jg-row").remove();
					clearInterval(id);
					processesImages($, cont, images, lastRowWidth, settings);
					return;
				}
			}, settings.refreshTime);
		}
}
 
})(jQuery);