<?php

/**
 * @version	$Id$
 * @author	JoomlaUX
 * @package	Joomla.Site
 * @subpackage	mod_jux_gallery
 * @copyright	Copyright (C) 2013 NooTheme. All rights reserved.
 * @license	License GNU General Public License version 2 or later; see LICENSE.txt, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die('Restricted access');
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'helper.php');


if (!file_exists(JPATH_BASE . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_jux_gallery' . DIRECTORY_SEPARATOR . 'defines.php'))
{
    print'<pre>';
    print_r("You must be installed JUX_Gallery Component! ");
    print'</pre>';
} else
{
    require_once (JPATH_ROOT . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_jux_gallery' . DIRECTORY_SEPARATOR . 'defines.php');
    
$document = JFactory::getDocument();
$jQueryHandling = $params->get('jQueryHandling');
$jQueryLoading = $params->get('load_jquery');

$document->addStyleSheet(JURI::base() . 'modules/mod_jux_gallery/assets/css/jquery.justifiedgallery.css');
$document->addStyleSheet(JURI::base() . 'modules/mod_jux_gallery/assets/css/buttons.css');
$document->addStyleSheet(JURI::base() . 'modules/mod_jux_gallery/assets/css/magnific-popup.css');

//Check load Java Script
if (!defined("JUX_Gallery_Script"))
{
    if ($jQueryLoading == 1)
    {
	if ($jQueryHandling && strpos($jQueryHandling, 'remote') == true)
	{
	    $document->addScript('http://ajax.googleapis.com/ajax/libs/jquery/' . str_replace('remote', '', $jQueryHandling) . '/jquery.min.js');
	} elseif ($jQueryHandling && strpos($jQueryHandling, 'remote') == false)
	{
	    $document->addScript(JURI::base() . 'modules/mod_jux_gallery/assets/js/jquery-' . $jQueryHandling . '.min.js');
	}
    }
//    $document->addScript(JURI::base() . 'modules/mod_jux_gallery/assets/js/modernizr.custom.js');
//    $document->addScript(JURI::base() . 'modules/mod_jux_gallery/assets/js/jquery.justifiedgallery.js');
//    $document->addScript(JURI::base() . 'modules/mod_jux_gallery/assets/js/jquery.magnific-popup.js');


    define("JUX_Gallery_Script", 1);
}

if ($params->get('load_jquery_noconflict', 1) == 1)
{
    $document->addScript(JURI::base() . 'modules/mod_jux_gallery/assets/js/jquery.noconflict.js');
}
JHTML::_('behavior.tooltip');
$items = modJUX_GalleryHelper::getItems($params);
$categories = modJUX_GalleryHelper::getCategories($params);

require JModuleHelper::getLayoutPath('mod_jux_gallery');
}