<?php
/* ------------------------------------------------------------------------
  # mod_klixo_particles  - Version 1.6.0 - 20170918
  # ------------------------------------------------------------------------
  # Copyright (C) 2012-2017 Klixo. All Rights Reserved.
  # @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
  # Author: JF Thier Klixo
  # Website: http://www.Klixo.se
  ------------------------------------------------------------------------- */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.html.html');
jimport('joomla.form.formfield');

JHtml::stylesheet(JURI::root() . "modules/mod_klixo_particles/fields/slider.min.css");

class JFormFieldslider extends JFormField {

    protected $type = 'slider';

    protected function getTitle() {
        return $this->getLabel();
    }

    protected function getInput() {

        $document = JFactory::getDocument();

        if (version_compare(JVERSION, '3.0', 'ge')) {
            // Load jQuery slider helper script for Joomla 3.x
            JHTML::script(JURI::root() . 'modules/mod_klixo_particles/js/jquery-ui_draggable.min.js');
            JHTML::script(JURI::root() . 'modules/mod_klixo_particles/js/slider.min.js');

            $document->addScriptDeclaration('
                if (typeof (jQuery) === "undefined") {
                    jQuery = jQuery.noConflict();
                }
                (function ($) {
                    var kSlider;
                    $(window).load(function () {
                        kSlider = new $.kSlider({
                            knob : "#knob_' . $this->id . '",
                            track : "#slider_' . $this->id . '",
                            input : "#' . $this->id . '",
                            inc:  ' . $this->element["step"] . ',
                            max:  ' . $this->element["max"] . ' , 
                            min:  ' . $this->element["min"] . ' , 
                        });
                        kSlider.sliderInit();
                    });
                })(jQuery);
                ');
        } 

        return '<div id="cursor_' . $this->id . '" class="sliderContainer" >  <div id="slider_' . $this->id . '" class="slider"><div id="knob_' . $this->id . '" class="knob"></div></div><input id="' . $this->id . '" name= "' . $this->name . '" type="text" size="5"  class="sliderVal" value="' . $this->value . '" /></div>';
    }

}

?>