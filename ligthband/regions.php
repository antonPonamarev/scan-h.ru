<?
global $arRegions;
define("PREG_SUBDOMAIN", '/([^.]+)\.scan-h\.ru/is');
$arRegions = array(
    'novosibirsk' => array(
        'CODE' => 'novosibirsk',
        'REGION' => 'Новосибирская область',
        'V_OBL' => 'Новосибирской области',
        'PO_OBL' => 'Новосибирской области',
        'NAME' => 'Новосибирск',
        'REGION_V' => 'в Новосибирске',
        'REGION_PO' => 'по Новосибирску',
        'MAP_COORDS' => '55.0083526,82.9357327',
        'MAP_ZOOM' => '6.0000',
    ),
    'ekaterinburg' => array(
        'CODE' => 'ekaterinburg',
        'REGION' => 'Свердловская область',
        'V_OBL' => 'Свердловской области',
        'PO_OBL' => 'Свердловской области',
        'NAME' => 'Екатеринбург',
        'REGION_V' => 'в Екатеринбурге',
        'REGION_PO' => 'по Екатеринбургу',
        'MAP_COORDS' => '56.8389261,60.6057025',
        'MAP_ZOOM' => '6.0000',
    ),
    'nizhniy-novgorod' => array(
        'CODE' => 'nizhniy-novgorod',
        'REGION' => 'Нижегородская область',
        'V_OBL' => 'Нижегородской области',
        'PO_OBL' => 'Нижегородской области',
        'NAME' => 'Нижний Новгород',
        'REGION_V' => 'в Нижнем Новгороде',
        'REGION_PO' => 'по Нижнему Новгороду',
        'MAP_COORDS' => '56.2965039,43.936059',
        'MAP_ZOOM' => '7.0000',
    ),
    'kazan' => array(
        'CODE' => 'kazan',
        'REGION' => 'Республика Татарстан',
        'V_OBL' => 'Татарстане',
        'PO_OBL' => 'Татарстану',
        'NAME' => 'Казань',
        'REGION_V' => 'в Казани',
        'REGION_PO' => 'по Казани',
        'MAP_COORDS' => '55.8304307,49.0660806',
        'MAP_ZOOM' => '6.0000',
    ),
    'chelyabinsk' => array(
        'CODE' => 'chelyabinsk',
        'REGION' => 'Челябинская область',
        'V_OBL' => 'Челябинской области',
        'PO_OBL' => 'Челябинской области',
        'NAME' => 'Челябинск',
        'REGION_V' => 'в Челябинске',
        'REGION_PO' => 'по Челябинску',
        'MAP_COORDS' => '55.1644419,61.4368432',
        'MAP_ZOOM' => '7.0000',
    ),
    
        'sankt-peterburg' => array(
            'CODE' => 'sankt-peterburg',
            'REGION' => 'Ленинградская область',
            'V_OBL' => 'Ленинградской области',
            'PO_OBL' => 'Ленинградской области',
            'NAME' => 'Санкт-Петербург',
            'REGION_V' => 'в Санкт-Петербурге',
            'REGION_PO' => 'по Санкт-Петербургу',
            'MAP_COORDS' => '59.939095,30.315868',
            'MAP_ZOOM' => '6.0000',
        ),
    
    'omsk' => array(
        'CODE' => 'omsk',
        'REGION' => 'Омская область',
        'V_OBL' => 'Омской области',
        'PO_OBL' => 'Омской области',
        'NAME' => 'Омск',
        'REGION_V' => 'в Омске',
        'REGION_PO' => 'по Омску',
        'MAP_COORDS' => '54.9884804,73.3242361',
        'MAP_ZOOM' => '5.0000',
    ),
    'samara' => array(
        'CODE' => 'samara',
        'REGION' => 'Самарская область',
        'V_OBL' => 'Самарской области',
        'PO_OBL' => 'Самарской области',
        'NAME' => 'Самара',
        'REGION_V' => 'в Самаре',
        'REGION_PO' => 'по Самаре',
        'MAP_COORDS' => '53.2415041,50.2212463',
        'MAP_ZOOM' => '7.0000',
    ),
    'ufa' => array(
        'CODE' => 'ufa',
        'REGION' => 'Республика Башкортостан',
        'V_OBL' => 'Башкортостане',
        'PO_OBL' => 'Башкортостану',
        'NAME' => 'Уфа',
        'REGION_V' => 'в Уфе',
        'REGION_PO' => 'по Уфе',
        'MAP_COORDS' => '54.7387621,55.9720554',
        'MAP_ZOOM' => '7.0000',
    ),
    'krasnoyarsk' => array(
        'CODE' => 'krasnoyarsk',
        'REGION' => 'Красноярский край',
        'V_OBL' => 'Красноярском крае',
        'PO_OBL' => 'Красноярскому краю',
        'NAME' => 'Красноярск',
        'REGION_V' => 'в Красноярске',
        'REGION_PO' => 'по Красноярску',
        'MAP_COORDS' => '56.0152834,92.8932476',
        'MAP_ZOOM' => '6.0000',
    ),
    'perm' => array(
        'CODE' => 'perm',
        'REGION' => 'Пермский край',
        'V_OBL' => 'Пермском крае',
        'PO_OBL' => 'Пермскому краю',
        'NAME' => 'Пермь',
        'REGION_V' => 'в Перми',
        'REGION_PO' => 'по Перми',
        'MAP_COORDS' => '58.0296813,56.2667916',
        'MAP_ZOOM' => '6.0000',
    ),
    'voronezh' => array(
        'CODE' => 'voronezh',
        'REGION' => 'Воронежская область',
        'V_OBL' => 'Воронежской области',
        'PO_OBL' => 'Воронежской области',
        'NAME' => 'Воронеж',
        'REGION_V' => 'в Воронеже',
        'REGION_PO' => 'по Воронежу',
        'MAP_COORDS' => '51.6754966,39.2088823',
        'MAP_ZOOM' => '7.0000',
    ),
    'volgograd' => array(
        'CODE' => 'volgograd',
        'REGION' => 'Волгоградская область',
        'V_OBL' => 'Волгоградской области',
        'PO_OBL' => 'Волгоградской области',
        'NAME' => 'Волгоград',
        'REGION_V' => 'в Волгограде',
        'REGION_PO' => 'по Волгограду',
        'MAP_COORDS' => '48.708048,44.5133035',
        'MAP_ZOOM' => '6.0000',
    ),
    'moskva' => array(
        'CODE' => 'moskva',
        'REGION' => 'Московская область',
        'V_OBL' => 'Московской области',
        'PO_OBL' => 'Московской области',
        'NAME' => 'Москва',
        'REGION_V' => 'в Москве',
        'REGION_PO' => 'по Москве',
        'MAP_COORDS' => '55.753233,37.622621',
        'MAP_ZOOM' => '9.0000',
    ),
    'krasnodar' => array(
        'CODE' => 'krasnodar',
        'REGION' => 'Краснодарский край',
        'V_OBL' => 'Краснодарском крае',
        'PO_OBL' => 'Краснодарскому краю',
        'NAME' => 'Краснодар',
        'REGION_V' => 'в Краснодаре',
        'REGION_PO' => 'по Краснодару',
        'MAP_COORDS' => '45.0392674,38.9872209',
        'MAP_ZOOM' => '7.0000',
    ),
    'saratov' => array(
        'CODE' => 'saratov',
        'REGION' => 'Саратовская область',
        'V_OBL' => 'Саратовской области',
        'PO_OBL' => 'Саратовской области',
        'NAME' => 'Саратов',
        'REGION_V' => 'в Саратове',
        'REGION_PO' => 'по Саратову',
        'MAP_COORDS' => '51.5923654,45.9608031',
        'MAP_ZOOM' => '6.0000',
    ),
    'tyumen' => array(
        'CODE' => 'tyumen',
        'REGION' => 'Тюменская область',
        'V_OBL' => 'Тюменской области',
        'PO_OBL' => 'Тюменской области',
        'NAME' => 'Тюмень',
        'REGION_V' => 'в Тюмени',
        'REGION_PO' => 'по Тюмени',
        'MAP_COORDS' => '57.1612975,65.5250172',
        'MAP_ZOOM' => '6.0000',
    ),
    'tolyatti' => array(
        'CODE' => 'tolyatti',
        'REGION' => 'Самарская область',
        'V_OBL' => 'Самарской области',
        'PO_OBL' => 'Самарской области',
        'NAME' => 'Тольятти',
        'REGION_V' => 'в Тольятти',
        'REGION_PO' => 'по Тольятти',
        'MAP_COORDS' => '53.5086002,49.4198344',
        'MAP_ZOOM' => '7.0000',
    ),
    'izhevsk' => array(
        'CODE' => 'izhevsk',
        'REGION' => 'Удмуртская Республика',
        'V_OBL' => 'Удмуртии',
        'PO_OBL' => 'Удмуртии',
        'NAME' => 'Ижевск',
        'REGION_V' => 'в Ижевске',
        'REGION_PO' => 'по Ижевску',
        'MAP_COORDS' => '56.8618601,53.2324284',
        'MAP_ZOOM' => '6.0000',
    ),
    'barnaul' => array(
        'CODE' => 'barnaul',
        'REGION' => 'Алтайский край',
        'V_OBL' => 'Алтайском крае',
        'PO_OBL' => 'Алтайскому краю',
        'NAME' => 'Барнаул',
        'REGION_V' => 'в Барнауле',
        'REGION_PO' => 'по Барнаулу',
        'MAP_COORDS' => '53.3547792,83.7697833',
        'MAP_ZOOM' => '6.0000',
    ),
    'ulyanovsk' => array(
        'CODE' => 'ulyanovsk',
        'REGION' => 'Ульяновская область',
        'V_OBL' => 'Ульяновской области',
        'PO_OBL' => 'Ульяновской области',
        'NAME' => 'Ульяновск',
        'REGION_V' => 'в Ульяновске',
        'REGION_PO' => 'по Ульяновску',
        'MAP_COORDS' => '54.3181598,48.3837915',
        'MAP_ZOOM' => '7.0000',
    ),
    'rostov-na-donu' => array(
        'CODE' => 'rostov-na-donu',
        'REGION' => 'Ростовская область',
        'V_OBL' => 'Ростовской области',
        'PO_OBL' => 'Ростовской области',
        'NAME' => 'Ростов-на-Дону',
        'REGION_V' => 'в Ростове-на-Дону',
        'REGION_PO' => 'по Ростову-на-Дону',
        'MAP_COORDS' => '39.774079,47.278015',
        'MAP_ZOOM' => '7',
    ),
);

defined(MAIN_DOMAIN,"scan-h.ru");
defined(SUBDOMAIN,".scan-h.ru");
function initRegion($url, $arRegions,$SDEK){
    $SUB_DOMAIN =  getRegionCode($url);
    global $_REGION;
    
    //Выбираем город по умолчанию
    $_REGION = $arRegions['sankt-peterburg'];

    if(!empty($SUB_DOMAIN)) {

        if( $SUB_DOMAIN == '' )
            $_REGION = $arRegions['sankt-peterburg'];
        else
            $_REGION = $arRegions[$SUB_DOMAIN];
        
        if($_REGION === false || $SUB_DOMAIN == "sankt-peterburg" ) {
            
            header("HTTP/1.1 301 Moved Permanently"); 
            header("Location: " . MAIN_DOMAIN . $_SERVER['REQUEST_URI']);
        }

    }


}

function changeContent($content){
    global $_REGION;

    $content = str_replace("[%TOWN%]", 			$_REGION['NAME'], $content);
    $content = str_replace("[%TOWN_V%]", 		 $_REGION['REGION_V'], $content);
    $content = str_replace("[%TOWN_PO%]", 		$_REGION['REGION_PO'], $content);
    $content = str_replace("[%REGION%]", 		$_REGION['REGION'], $content);
    $content = str_replace("[%REGION_V%]", 		$_REGION['V_OBL'], $content);
    $content = str_replace("[%REGION_PO%]", 	$_REGION['REGION_PO'], $content);
    $content = str_replace("sankt-peterburg.scan-h.ru","scan-h.ru", $content);
    $content = str_replace("в Москве", 		 $_REGION['REGION_V'], $content);
    $content = str_replace("[%MAP_COORDS%]", 	implode(',',array_reverse(explode(',',$_REGION["MAP_COORDS"]))), $content);
    if($_REGION["CODE"] != "sankt-peterburg"){
        $content = preg_replace('/<section(.*)region-hide(.*)>([\s\S]*)<\/section>/imU', '', $content);
    }
    return $content;
}

function getRegionCode($url){
    $url = str_replace(array("http://", "https://", "www"), "", $url);
    $result = preg_match(PREG_SUBDOMAIN, $url, $matches);
    return $matches[1];
}



?>